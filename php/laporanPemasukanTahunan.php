<div id="divTitle">
		<label class="lblTitle">LAPORAN PEMASUKAN TAHUNAN</label>
</div>

<div style="padding-left:30px;">
	<br>
	<form id="formLaporanPemasukanTahunan" method="post">
	<table>
		<tr>
			<td>Tahun</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="thnLapPemasukanTahunan" maxlength=4 class='thnLapPemasukanTahunan' style="width:50px;" type="number" min=2000 max=9999 placeholder='tahun' required></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>
				<input type="submit" value="Tampilkan" style="margin-right:10px; width:100px; height:25px;" id="btnTampilkanLapPemasukanTahunan" name="btnTampilkanLapPemasukanTahunan">
			</td>
		</tr>
	</table>
	</form>
	<br>
	<div id="divLapPemasukanTahunan">
		<table id="laporan_pemasukan_tahunan">
		<style type="text/css">
			@media print {

			    table, td {
			    	border: 1px solid black;

				}

				table{
					margin-left: 200px;
				}
				table#kopPrint{
					border: 0px solid black;
					margin-left: 80px;
				}
			}
		</style>
		</table>
	</div>

		<br>
		<br>
		<button onclick="printData('laporan_bulanan','judulLaporanBln','perTanggalLaporanBln','perTanggalTampilLaporanBln')" id='cetakLaporanBln'>Cetak</button>
		<label id="lblExportlaporan_bulanan">Export : </label>
		<select id='selectExportlaporan_pemasukan_tahunan' class="selectExportlaporan_pemasukan_tahunan" onchange="exportLaporan('laporan_pemasukan_tahunan','selectExportlaporan_pemasukan_tahunan','jmlPemasukanPerBulan')">
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
		<br>
	<br>
</div>