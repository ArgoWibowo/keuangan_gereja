
<div id="divTitle">
		<label class="lblTitle">MUTASI REKENING</label>
</div>

<div style="padding-left:30px;">
	<br>
	<form id="formTampilMutasiPerRek" method="post">
	<table>
		<tr>
			<td>
				Tahun Pembukuan
			</td>
			<td class="kolomTitikDua"> : </td>
			<td colspan="3">
				<input id="thnPembukuanMutasiPerRek" maxlength=4 style="width:172px;" type="number" min=2000 max=9999 placeholder='Tahun Pembukuan' required>
			</td>
		</tr>
		<tr>
			<td>Mulai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglMulaiMutasi" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required></td>
			<td ><input id="blnMulaiMutasi" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required></td>
			<td ><input id="thnMulaiMutasi" maxlength=4 class='thnMulaiLaporan' style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required></td>
			<td>&nbsp;&nbsp;</td>
			<td>Sampai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglSampaiMutasi" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required></td>
			<td ><input id="blnSampaiMutasi" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required></td>
			<td ><input id="thnSampaiMutasi" maxlength=4 class='thnSampaiLaporan' style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required></td>
		</tr>
		<tr>
			<td>
				Kelompok Akun
			</td>
			<td class="kolomTitikDua"> : </td>
			<td colspan="3">
				<select id='selectMutasiKelompokAkun' style="width:179px;" onchange="loadselectMutasiKelompokAkun()">
					<option value="semua">Semua</option>
					<option value="K">K</option>
					<option value="D">D</option>
				</select>
			</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>
				Kode Golongan
			</td>
			<td class="kolomTitikDua"> : </td>
			<td colspan="3">
				<select id='selectMutasiKodeGolongan' style="width:179px;"onchange="loadselectMutasiKodeGolongan()">
					<option value="semua">Semua</option>
				</select>
			</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			
		</tr>

		<tr>
			<td>
				Kode Sub Golongan
			</td>
			<td class="kolomTitikDua"> : </td>
			
			<td colspan="3">
				<select id='selectMutasiKodeSubGolongan' style="width:179px;"onchange="loadselectMutasiKodeSubGolongan()">
					<option value="semua">Semua</option>
				</select>
			</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>
				Nomer Rekening
			</td>
			<td class="kolomTitikDua"> : </td>
			<td colspan="3">
				<select id='selectMutasiJenisAkun' style="width:179px;">
					<option value="semua">Semua</option>
				</select>
			</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>
				<input type="submit" value="Tampilkan"  style="margin-right:10px; width:100px; height:25px;" id="btnTampilMutasiPerRek" name="btnTampilMutasiPerRek">
			</td>
		</tr>
		
	</table>
	</form>
	</br>
	</br>
	<div>
		<label id="judulMutasiPerRek" >MUTASI REKENING</label>
	</div>
	<br>
	<div>
		<label id="perTanggalMutasiPerRek" >Periode </label>
		<label id="perTanggalTampilMutasiPerRek" >01/01/2000 - 01/01/2000</label>
	</div>
	</br>
	<table id="mutasi_per_rek">
	<style type="text/css">
		@media print {

		    table, td {
		    	border: 1px solid black;

			}

			table{
				margin-left: 60px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 80px;
			}
		}
	</style>
		
		
	</table>
		<br>
		<br>
		<button onclick="printData('mutasi_per_rek','judulMutasiPerRek','perTanggalMutasiPerRek','perTanggalTampilMutasiPerRek')" id='cetakLaporan'>Cetak</button>
		<label id="lblExportmutasi_per_rek">Export : </label>
		<select id='selectExportmutasi_per_rek' class="selectExportmutasi_per_rek" onchange="exportLaporan('mutasi_per_rek','selectExportmutasi_per_rek','judulMutasiPerRek','perTanggalMutasiPerRek','perTanggalTampilMutasiPerRek')">
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
		<br>
		<br>
		</div>