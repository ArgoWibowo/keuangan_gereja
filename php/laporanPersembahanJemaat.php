
<div id="divTitle">
		<label class="lblTitle">LAPORAN PERSEMBAHAN JEMAAT</label>
</div>

<div style="padding-left:30px;">
<br>
	<form id="formTampilPerJem" method="post">	
	<table>
	<tr>
			<td>Mulai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglMulaiPerJem" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder="tgl" required></td>
			<td ><input id="blnMulaiPerJem" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder="bln" required></td>
			<td ><input id="thnMulaiPerJem" maxlength=4 class='thnMulaiLaporan'style="width:50px;" type="number" min=2000 placeholder="thn" required></td>
			<td>&nbsp;</td>
			<td>Sampai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglPerJem" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder="tgl" required></td>
			<td ><input id="blnPerJem" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder="bln"required></td>
			<td ><input id="thnPerJem" maxlength=4 class='thnSampaiLaporan' style="width:50px;" type="number" min=2000 placeholder="thn" required></td>
			<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			Nama Jemaat
		</td>
		<td class="kolomTitikDua"> : </td>
		<td colspan="3">
			<select id='selectLaporanPersembahanTiapJemaat' style="width:179px;" >
				<option value="semua">Semua</option>
			</select>
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		
		<td>
			Jenis Akun
		</td>
		<td class="kolomTitikDua"> : </td>
		<td colspan="3">
			<select id='selectLaporanPersembahanJenisAkun' style="width:179px;">
				<option value="semua">Semua</option>
			</select>
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><input type="submit" value="Tampilkan"  style="margin-right:10px; width:100px; height:25px;" id="btnTampilPerJem" name="btnTampilPerJem"></td>
	</tr>
	</table>
	</form>
	</br>
	</br>
	<div>
		<label id="judulPerJemaat" >LAPORAN PERSEMBAHAN JEMAAT</label>
	</div>
	<br>
	<div>
		<label id="perTanggalPerJemaat" >Periode </label>
		<label id="perTanggalTampilPerJemaat" >01/01/2000 - 01/01/2000</label>
	</div>
	</br>
	<div id= "tolaporanPerJemaat">
	<table id= "laporanPerJemaat">
	<style type="text/css">
		@media print {

		    table, td {
		    	border: 1px solid black;

			}

			table{
				margin-left: 60px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 80px;
			}
		}
	</style>
	</table>
	</div>
	</br>
	</br>
	<button onclick="printData('laporanPerJemaat','judulPerJemaat','perTanggalPerJemaat','perTanggalTampilPerJemaat')" id='cetakLaporan'>Cetak</button>
	<label id="lblExportlaporanPenInduvidu">Export : </label>
	<select id='selectExportlaporanPerJemaat' class="selectExportlaporanlaporanPerJemaat" onchange="exportLaporan('laporanPerJemaat','selectExportlaporanPerJemaat','judulPerJemaat','perTanggalPerJemaat','perTanggalTampilPerJemaat')">
		<option value=''>Pilih</option>
	 	<option value='XLS'>XLS</option>
	 	<option value='PDF'>PDF</option>
	</select>
	</br>
	</br>
</div>