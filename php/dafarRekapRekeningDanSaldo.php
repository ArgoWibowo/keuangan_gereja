<div id="divTitle">
		<label class="lblTitle">REKAP SALDO REKENING</label>
</div>
<br>
	<div style="padding-left:30px;">
	<form id="formTampilRekapSaldoRekening" method="post">
	<table>
	<tr>
		<td>
			Tahun Pembukuan
		</td>
		<td class="kolomTitikDua"> : </td>
		<td colspan="3">
			<input id="thnPemRekapRekSaldo" maxlength=4 style="width:174px;" type="number" min=2000 max=9999 placeholder='Tahun Pembukuan' required>
		</td>
	</tr>
	<tr>
			<td>Per Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglRekapSalRek" maxlength=2 style="width:50px;" type="number" min=1 max=31  placeholder="tgl" required></td>
			<td ><input id="blnRekapSalRek" maxlength=2 style="width:50px;" type="number" min=1 max=12  placeholder="bln" required></td>
			<td ><input id="thnRekapSalRek" maxlength=4 style="width:50px;" type="number" min=2000 max=9999  placeholder="thn" required></td>
			<td>&nbsp;</td>
			<td><input type="submit" value="Tampilkan"  style="margin-right:10px; width:100px; height:25px;" id="btnTampilRekapSaldoRekening" name="btnTampilRekapSaldoRekening"></td>
	</tr>
	</table>
	</form>
	</br>
	</br>
	<div>
		<label id="judulRekSaldo" >REKAP SALDO REKENING</label>
	</div>
	<br>
	<div>
		<label id="perTanggalRekSaldo" >Per Tanggal</label>
		<label id="perTanggalTampilRekSaldo" >01/01/2000</label>
	</div>
	</br>
	<table id= "rekapRekeningSaldo">
	<style type="text/css">
		@media print {

		    table, td {
		    	border: 1px solid black;

			}

			label{
				margin-left: 40px;
			}

			table{
				margin-left: 40px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 40px;
			}
		}
	</style>
		<thead>	
		<tr>
			<td id="rekDaftarSaldo">
				Nomer dan Nama Rekening
			</td>
			<td id="saldoDaftarSaldo">
				Saldo Rp
			</td>
		</tr>
		</thead>
		<tbody>
		</tbody>

		</table>
		</br>
		</br>
		<button onclick="printData('rekapRekeningSaldo','judulRekSaldo','perTanggalRekSaldo','perTanggalTampilRekSaldo')" id='cetakLaporan'>Cetak</button>
		<label id="lblExportRekSaldo">Export : </label>
		<select id='selectExportRekapRekSaldo' class="selectExportRekapRekSaldo" onchange="exportLaporan('rekapRekeningSaldo','selectExportRekapRekSaldo','judulRekSaldo','perTanggalRekSaldo','perTanggalTampilRekSaldo')">
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
		</br>
		</br>
</div>