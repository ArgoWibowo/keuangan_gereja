<div id="divTitle">
		<label class="lblTitle">BUKU BESAR</label>
</div>

<div style="padding-left:30px;">
<br>
	<form id="formTampilBukuBesar" method="post">
	<table>
	<tr>
		<td>
			Tahun Pembukuan
		</td>
		<td class="kolomTitikDua"> : </td>
		<td colspan="3">
			<input id="thnPembukuanBukBes" maxlength=4 style="width:174px;" type="number" min=2000 max=9999 placeholder='Tahun Pembukuan' required>
		</td>
	</tr>
	<tr>
			<td>Per Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglBukBes" maxlength=2 style="width:50px;" type="number" min=1 max=31  placeholder="Tgl" required></td>
			<td ><input id="blnBukBes" maxlength=2 style="width:50px;" type="number" min=1 max=12  placeholder="Bln" required></td>
			<td ><input id="thnBukBes" maxlength=4 style="width:50px;" type="number" min=2000 max=9999  placeholder="Thn" required></td>
			<td>&nbsp;</td>
			<td><input type="submit" value="Tampilkan"  style="margin-right:10px; width:100px; height:25px;" id="btnTampilBukuBesar" name="btnTampilBukuBesar"></td>
	</tr>
	</table>
	</form>

	</br>
	</br>
	<div>
		<label id="judulBukuBesar" >BUKU BESAR</label>
	</div>
	<br>
	<div>
		<label id="perTanggalBukuBesar" >Per Tanggal</label>
		<label id="perTanggalTampilBukuBesar" >01/01/2000</label>
	</div>
	</br>
	<div id='printBukuBesar'>
	<table id="buku_besar_aktiva">
	<style type="text/css">
		@media print {

		    table, td {
		    	border: 1px solid black;

			}

			label{
				margin-left: 50px;
			}

			table{
				margin-left: 50px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 40px;
			}
		}
	</style>
		<thead >	
		<tr>
			<td id="kodeBukuBesar">
				Kode
			</td>
			<td id="namaBukuBesar">
				Nama Buku Besar
			</td>
			<td id="saldoBukuBesar">
				Saldo Rp
			</td>
		</tr>
		</thead>
		<tbody>
		</tbody>

	</table>
	<br>
	<br>
	<table id="buku_besar_pasiva">
	<style type="text/css">
		@media print {

		    table, td {
		    	border: 1px solid black;

			}

			table{
				margin-left: 50px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 40px;
			}
		}
	</style>
		<thead >	
		<tr>
			<td id="kodeBukuBesar">
				Kode
			</td>
			<td id="namaBukuBesar">
				Nama Buku Besar
			</td>
			<td id="saldoBukuBesar">
				Saldo Rp
			</td>
		</tr>
		</thead>
		<tbody>
		</tbody>

	</table>
	</div>

	</br>
	</br>
	<button onclick="printData('printBukuBesar','judulBukuBesar','perTanggalBukuBesar','perTanggalTampilBukuBesar')" id='cetakLaporan'>Cetak</button>
	<label id="lblExportBukuBesar">Export : </label>
	<select id='selectExportDataBukuBesar' class="selectExportDataBukuBesar" onchange="exportLaporanBukBek('buku_besar_aktiva', 'buku_besar_pasiva','selectExportDataBukuBesar','judulBukuBesar','perTanggalBukuBesar','perTanggalTampilBukuBesar')">
		<option value=''>Pilih</option>
	 	<option value='XLS'>XLS</option>
	 	<option value='PDF'>PDF</option>
	</select>
	</br>
	</br>

</div>