
<div id="divTitle">
		<label class="lblTitle">LAPORAN PENERIMAAN INDIVIDU</label>
</div>

<div style="padding-left:30px;">
<br>
	<form id="formTampilPenInd" method="post">	
	
	<table>
	<tr>
			<td>Mulai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglMulaiPenInd" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder="tgl" required></td>
			<td ><input id="blnMulaiPenInd" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder="bln" required></td>
			<td ><input id="thnMulaiPenInd" maxlength=4 class='thnMulaiLaporan'style="width:50px;" type="number" min=2000  placeholder="thn" required></td>
			<td>&nbsp;</td>
			<td>Sampai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglPenInd" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder="tgl" required></td>
			<td ><input id="blnPenInd" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder="bln" required></td>
			<td ><input id="thnPenInd" maxlength=4 class='thnSampaiLaporan' style="width:50px;" type="number" min=2000 placeholder="thn" required></td>
			<td>&nbsp;</td>
			<td><input type="submit" value="Tampilkan"  style="margin-right:10px; width:100px; height:25px;" id="btnTampilPenInd" name="btnTampilPenInd"></td>
	</tr>
	</table>
	</form>
	</br>
	</br>
	<div>
		<label id="judulPenInd" >LAPORAN PENERIMAAN INDIVIDU</label>
	</div>
	<br>
	<div>
		<label id="perTanggalPenInd" >Periode </label>
		<label id="perTanggalTampilPenInd" >01/01/2000 - 01/01/2000</label>
	</div>
	</br>
	<div id= "tolaporanPenInduvidu">
	<table id="laporanPenInduvidu">
	<style type="text/css">
		@media print {

		    table, td {
		    	border: 1px solid black;

			}

			#laporanPenInduvidu{
				border: 1px solid black;
			}

			tbody{
				border: 1px solid black;
			}

			thrad{
				border: 1px solid black;
			}

			table{
				margin-left: 60px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 80px;
			}
		}
	</style>
	
	</table>
	</div>
	</br>
	</br>
	<button onclick="printData('laporanPenInduvidu','judulPenInd','perTanggalPenInd','perTanggalTampilPenInd')" id='cetakLaporan'>Cetak</button>
	<label id="lblExportlaporanPenInduvidu">Export : </label>
	<select id='selectExportlaporanPenInduvidu' class="selectExportlaporanPenInduvidu" onchange="exportLaporan('laporanPenInduvidu','selectExportlaporanPenInduvidu','judulPenInd','perTanggalPenInd','perTanggalTampilPenInd')">
		<option value=''>Pilih</option>
	 	<option value='XLS'>XLS</option>
	 	<option value='PDF'>PDF</option>
	</select>
	</br>
	</br>
</div>