<div id="divTitle">
		<label class="lblTitle">MUTASI PER AKUN</label>
</div>

<div style="padding-left:30px;">
	<br>
	<form id="formMutasiPerAkun" method="post">
	<table>
		<tr>
			<td>
				Nomer Rekening
			</td>
			<td class="kolomTitikDua"> : </td>
			<td colspan="3">
				<select id='selectMutasiPerAkun' style="width:179px;">
				<option value='' disabled selected style='display: none;'>Pilih Rekening</option>
				</select>
			</td>
			<td>&nbsp;&nbsp;</td>
			<!--<td>
				Tahun Pembukuan
			</td>
			<td class="kolomTitikDua"> : </td>
			<td colspan="3">
				<input id="thnPembukuanMutasiPerAkun" maxlength=4 style="width:172px;" type="number" min=2000 max=9999 placeholder='Tahun Pembukuan' required>
			</td>-->
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<!--<tr>
			
		</tr>-->
		<tr>
			<td>Mulai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td >
				<input id="tglMulaiPerAkun" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required>
			</td>
			<td >
				<input id="blnMulaiPerAkun" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required>
			</td>
			<td >
				<input id="thnMulaiPerAkun" maxlength=4 class='thnMulaiLaporan' style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required>
			</td>
			<td>&nbsp;&nbsp;</td>
			<td>Sampai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td >
				<input id="tglSampaiPerAkun" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required>
			</td>
			<td >
				<input id="blnSampaiPerAkun" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required>
			</td>
			<td >
				<input id="thnSampaiPerAkun" maxlength=4 class='thnSampaiLaporan' style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required>
			</td>
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="Tampilkan" style="margin-right:10px; width:100px; height:25px;" id="btnTampilMutasiPerAkun" name="btnTampilMutasiPerAkun">
			</td>
		</tr>
		
	</table>
	</form>

	</br>
	</br>
	<div>
		<label id="judulMutasiPerAkun">MUTASI PER AKUN</label>
	</div>
	<br>
	<div>
		<label id="namaMutasiPerAkun" >NAMA AKUN : </label>
		<label id="perNamaMutasiPerAkun" ></label>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<label id="perMutasiPerAkun" >TAHUN : </label>
		<label id="perTahunMutasiPerAkun" ></label>
	</div>
	</br>
	<table id="mutasi_per_akun">
	<style type="text/css">
		@media print {

			table, td {
		    	border: 1px solid black;

			}

			label{
				margin-left: 50px;
			}

			table{
				margin-left: 50px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 40px;
			}
		}
	</style>
	<thead>	
		<tr>
			<td td id='noBuktiMutasiPerAkun' rowspan='2'>
				No.Bukti
			</td>
			<td id='tglMutasiPerAkun' rowspan='2'>
				Tanggal
			</td>
			<td id='uraianPerAkun' rowspan='2'>
				Uraian
			</td>
			<td id='tipeMutasi' colspan='2' rowspan='1' >
				Mutasi
			</td>
			<td id='saldo' rowspan='2' >
				Saldo
		</tr>
		<tr>
			<td id='debetMutasi'>
				Debet
			</td>
			<td id='kreditMutasi'>
				Kredit
			</td>
		</tr>
	</thead>
		<tbody>

		</tbody>

		</table>
		<br>
		<br>
		<button onclick="printData('mutasi_per_akun','judulMutasiPerAkun','perTanggalMutasiPerAkun','perTanggalTampilMutasiPerAkun')" id='cetakLaporan'>Cetak</button>
		<label id="lblExportmutasi_kas">Export : </label>
		<select id='selectExportmutasi_kas' class="selectExportmutasi_kas" onchange="exportLaporan('mutasi_per_akun','selectExportmutasi_kas','judulMutasiPerAkun','pperTanggalMutasiPerAkun','perTanggalTampilMutasiPerAkun')">
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
		<br>
	<br>
	
</div>