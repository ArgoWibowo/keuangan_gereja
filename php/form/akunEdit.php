<div id="formPengaturanAkunEdit" >
	<label align="center">UBAH INFORMASI ADMIN</label>
	<a href="#" class="close"><img src="image/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
	
	<form id="formEditAkunProfil" name="formEditAkunProfil" method="POST" enctype="multipart/form-data">
	<table border="0">

		<tr>
			<td style="width: 140px;">Nama Admin</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td><input type="text" id="akunnamaAdmin" required></td>
			<td id="messageTambahAdmin"></td>
		</tr>

		<tr>
			<td>Pertanyaan</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td>
					<select id="akunselect_pertanyaan_pemulihan"  style="width: 172.5px;" required>
					<option value="" disabled selected style="display: none;">Pilih Pertanyaan</option>
					<option value="Siapa nama ayah anda?">Siapa nama ayah anda?</option>
					<option value="Siapa nama ibu anda?">Siapa nama ibu anda?</option>
					<option value="Siapa guru Sekolah Minggu yang paling anda sukai?">Siapa guru Sekolah Minggu yang paling anda sukai?</option>
					<option value="Siapa nama paman yang paling anda sukai?">Siapa nama paman yang paling anda sukai?</option>
					<option value="Dimana anda lahir?">Dimana anda lahir?</option>
					<option value="Berapa 3 angka terakhir nomer telepon pertama anda?">Berapa 3 angka terakhir nomer telepon pertama anda?</option>
					<option value="Dimana ayah anda lahir?">Dimana ayah anda lahir?</option>
					<option value="Ayat paling anda sukai?">Ayat paling anda sukai?</option>
					<option value="Tokoh Alkitab yang paling menginspirasi?">Tokoh Alkitab yang paling menginspirasi?</option>
					</select></td>
		</tr>
		<tr>
			<td style="width: 140px;">Jawaban Pertanyaan</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td><input type="text" id="akunjawabanPertanyaan" required></td>
			<td id="messageAlamatAdmin"></td>
		</tr>
		<tr style="vertical-align: top;">
			<td style="width: 140px;">Alamat</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td><textarea placeholder="Alamat Admin" style="width: 140px; height: 40px;" name ="akunalamatAdmin" id ="akunalamatAdmin" required></textarea></td>
			<td id="messageAlamatAdmin"></td>
		</tr>
		<tr>
			<td style="width: 140px;">No Telepon</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td><input type="text" id="akunteleponAdmin" required></td>
			<td id="messageTeleponAdmin"></td>
		</tr>

		<tr>
			<td style="width: 140px;">Email</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td><input type="email" id="akunemailBaru" required></td>
		</tr>
	
	</table>
	<br>
	
	<div id="buttonEditProfilAkunAll">
		<button type="button" id"buttBatalAkun" onclick="kembaliEditAkunAdmin('#PengaturanAkun')">Batal</button>
		<input id"buttEditAkun" type="submit" value="Simpan"  id="btnPengaturanAdmin" name="btnPengaturanAdmin">	
	</div>
	</form>	
</div>
