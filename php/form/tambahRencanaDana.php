
<div id="divTitle">
		<label class="lblTitle">Tambah Rencana Anggaran</label>
</div>
<div id="tambahRencanaAnggaran" style="padding-left:5pxpx;">
	<form id="formTambahRencanaAnggaran" name="formTambahRencanaAnggaran" method="POST" enctype="multipart/form-data">
	<table >
		<tr>
			<td class="kolomLabelRencanaRekening" style="width: 200px;">Kode Jenis Akun</td>
			<td class="kolomTitikDua"> : </td>
			<td><select id='pilihanRencanaKodeAkunRekening' name="pilihanRencanaKodeAkunRekening" style="width: 257.5px;" required>
			 	<option value="" disabled selected style="display: none;">Pilih Kode</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="kolomLabelRencanaRekening" style="width: 200px;">Rencana Anggaran</td>
			<td class="kolomTitikDua"> : </td>
			<td>
				<input type="text" class="titikKoma" id="rencanaDanaJenisAkunRekening" maxlength="12" name="rencanaDanaJenisAkunRekening" style="width: 250px;" placeholder="Masukan rencana dana" required/>
			</td>
		</tr>
		<tr>
			<td class="kolomLabelRencanaRekening" style="width: 200px;">Tahun</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="number" id="tahunJenisAkunRekening" name="tahunJenisAkunRekening" min="2000" max="9999"style="width: 250px;" placeholder="Masukan tahun" required/></td>
		</tr>
	</table>
	<div id="errTambahRencanaAnggaran">
			<label>&nbsp;</label>
	</div>
	<br>

	<input type="submit" value="Tambah"  id="btnTambahRencanaJenisAkunRekening" name="btnTambahRencanaJenisAkunRekening" class="button" >
	<button type="button" id='btnBatalRencanaJenisAkunRekening' onclick="batalTambahRencanaAnggaran()">Batal</button>
	</form>
</div>
