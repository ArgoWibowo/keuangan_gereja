<div id="pengaturanAdmin" method="post">
	
	<a href="#" class="close"><img src="image/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
	<form id="formEditGereja" name="formEditGereja" method="POST" enctype="multipart/form-data">
	<table border="0">
		<tr>
			<td colspan = 4 style="width: 140px;" align="center"><label>UBAH PROFIL GEREJA</label></td>
		</tr>
		<tr>
			<td colspan = 4 style="width: 140px;" align="center">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 140px;">Nama Gereja</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td><input type="text" id="namaGereja" placeholder="Nama Gereja" maxlength=80 required></td>
			<td id="messageTambahAdmin"></td>
		</tr>
		<tr style="vertical-align: top;">
			<td style="width: 140px;">Alamat</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td><textarea placeholder="Alamat Gereja" style="width: 140px; height: 40px;" name ="alamatGereja" id ="alamatGereja" maxlength=80 required></textarea></td>
			<td id="messageDivisi"></td>
		</tr>
		<tr>
			<td style="width: 140px;">No Telepon</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td><input  placeholder="Nomer Telepon Gereja" type='text' id="teleponGereja" maxlength=40 required></td>
			<td id="messageTeleponAdmin"></td>
		</tr>

		<tr>
			<td style="width: 140px;">Email</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td><input  placeholder="Email Gereja" type="email" id="emailGereja" maxlength=30 required></td>
			<td id="messageTeleponAdmin"></td>
		</tr>
		<tr>
			<td style="width: 140px;">Tahun Pembukuan</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td><input  placeholder="Tahun Pembukuan" type="number" id="tahunPembukuanProfilGereja" min="2000" max="9999" required></td>
			<td id="messageTeleponAdmin"></td>
		</tr>
	</table>
	<div id ='divLogoGereja' align="center">
		<div class="fileUpload btn btn-primary">
		    <span>Ganti Logo</span>
		    <input type="file" accept="image/*" class="uploadLogo" name="foto" id="inputLogoGereja" onchange="readURL(this)"><br>
		</div>
		<img src="image/logoukdw.png" id='logoGereja' name="tamp" alt='LogoGKJ' style="width:200px; height: 200px;">
	</div>

	<br>
	<div id="buttonEditProfilAkunAll">
		<button type="button" onclick="kembaliEditProfil('#PengaturanAkun')">Batal</button>
		<input type="submit" value="Simpan"  id="btnEditProfilGereja" name="btnEditProfilGereja">
	</div>
	</form>
	
</div>