<form id="forminputpersembahan" method="post">
	<table>
		<tr>
			<td>Jenis Persembahan</td>
			<td> : <select class="selectjenispersembahan" style="width: 250px;" required>
					<option value="1">Persembahan Rutin</option>
					<option value="2">Persembahan Perpuluhan</option>
					<option value="3">Persembahan Perjamuan Kudus</option>
					<option value="4">Persembahan Khusus</option>
					<option value="5">Persembahan Lain-lain</option>
				   </select><input class="trick" id="selectjenispersembahan" tabindex="-1">
			</td> 
		</tr>
		<tr>
			<td>Wujud</td>
			<td> : <select class="selectwujud" style="width: 250px;" required>
				   </select><input class="trick" id="selectwujud" tabindex="-1">
			</td>
		</tr>
		<tr>
			<td>Jumlah</td>
			<td> : <input id="jumlahpersembahan" type="number" min="0" style="width: 246px;" required></td>
		</tr>
		<tr>
			<td>Keterikatan</td>
			<td> : <select style="width: 250px;">
					<option value="1">Tidak Terikat</option>
					<option value="2">Terikat Temporer</option>
					<option value="3">Terikat Permanen</option>
				   </select>
			</td>
		</tr>
		<tr style="vertical-align: top;">
			<td>Deskripsi</td>
			<td style="padding-bottom: 0px;"> : <textarea style="width: 244px; height: 70px;"></textarea></td>
		</tr>
		<tr>
			<td>Tanggal Persembahan</td>
			<td> : <input type="date" style="width: 245px;"></td>
		</tr>
		<tr>
			<td>ID Pemberi</td>
			<td> : <input style="width: 246px;"></td>
		</tr>
		<tr>
			<td>ID Penghitung</td>
			<td> : <input style="width: 246px;"></td>
		</tr>
		<tr>
			<td>ID Penyetor</td>
			<td> : <input style="width: 246px;"></td>
		</tr>
		<tr>
			<td>Nomor Rekening</td>
			<td> : <input style="width: 246px;"></td>
		</tr>
		<tr><td colspan="2"><input type="submit" value="input" style="width: 200px;"></td></tr>
	</table>
	
</form>
<!-- Jangan lupa:			<br>					
id_persembahan 			<br>
id_admin 				<br>
tgl_input 				<hr> -->