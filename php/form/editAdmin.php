<div id="divTitle">
		<label class="lblTitle">UBAH ADMIN</label>
</div>
<div style="padding-left:15px;" method="post">
	
	<form id="formEditAdmin" name="formEditAdmin" method="POST" enctype="multipart/form-data">
	<table id="editadmin" border="0">
		<tr>
			<td style="width: 140px;">Kode Admin / Username</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="editkodeTambahAdmin" style="this.style.color='#f00'" maxlength=8 placeholder="Max 8 karakter" required readonly></td>
		</tr>
		<tr>
			<td style="width: 140px;">Nama Admin</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="editnamaAdmin" placeholder="Nama Admin" required></td>
			<td id="messageTambahAdmin"></td>
		</tr>
		<tr>
			<td>Kode</td>
			<td class="kolomTitikDua"> : </td>
			<td>
					<select onchange="editGantiKode('')" id="editselect_kode"  style="width: 172.5px;">
					<option value="" disabled selected style="display: none;">Pilih Kode</option>
					<option value="KOMISI">Komisi</option>
					<option value="BIDANG">Bidang</option>
					<option value="TIDAKADA">Tidak Ada</option>
					</select>
			</td>
		</tr>

		<tr id="editkode_bidang_komisi">
			<td id="editkodeTerpakai"style="width: 140px;">Kode Komisi</td>
			<td class="kolomTitikDua"> : </td>
			<td>
			<select id="editkomisiBidangAdmin" style="width: 172.5px;"></select>
			<option value="" disabled selected style="display: none;">Pilih Kode</option>
			</td>
			<td id="pesanKomisiBidangAdmin"></td>
		</tr>
		<tr>
			<td><label><input type="checkbox" id="checkGantiPass" value="Ganti Password" name="checkGantiPass" onclick="checkBoxPass();">Ganti Password?</label></td>
		</tr>
		<tr id="editpass1">
			<td>Password</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="password" id="editpasswordBaru" placeholder="Password min 8 karakter"></td>
			<td class="warningPassEditAdmin"><img id="warningPassEditAdmin1" src="image/warning.png" alt="warning"></td>
			<td id="lblwarningPassEditAdmin1" style="width: 200px;"></td>
		</tr>
		<tr id="editpass2">
			<td>Konfirmasi Password</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="password" id="editpasswordBaru2" placeholder="Cek Password min 8 karakter"></td>
			<td class="warningPassEditAdmin"><img id="warningPassEditAdmin2" src="image/warning.png" alt="warning"></td>
			<td id="lblwarningPassEditAdmin2" style="width: 200px;"></td>
		</tr>
		<tr>
			<td>Pertanyaan</td>
			<td class="kolomTitikDua"> : </td>
			<td>
					<select id="editselect_pertanyaan_pemulihan"  style="width: 172.5px;">
					<option value="" disabled selected style="display: none;">Pilih Pertanyaan</option>
					<option value="Siapa nama ayah anda?">Siapa nama ayah anda?</option>
					<option value="Siapa nama ibu anda?">Siapa nama ibu anda?</option>
					<option value="Siapa guru Sekolah Minggu yang paling anda sukai?">Siapa guru Sekolah Minggu yang paling anda sukai?</option>
					<option value="Siapa nama paman yang paling anda sukai?">Siapa nama paman yang paling anda sukai?</option>
					<option value="Dimana anda lahir?">Dimana anda lahir?</option>
					<option value="Berapa 3 angka terakhir nomer telepon pertama anda?">Berapa 3 angka terakhir nomer telepon pertama anda?</option>
					<option value="Dimana ayah anda lahir?">Dimana ayah anda lahir?</option>
					<option value="Ayat paling anda sukai?">Ayat paling anda sukai?</option>
					<option value="Tokoh Alkitab yang paling menginspirasi?">Tokoh Alkitab yang paling menginspirasi?</option>
					</select></td>
		</tr>
		<tr>
			<td style="width: 140px;">Jawaban Pertanyaan</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="editjawabanPertanyaan" placeholder="Jawaban" required></td>
			<td id="messageAlamatAdmin"></td>
		</tr>
		<tr style="vertical-align: top;">
			<td style="width: 140px;">Alamat</td>
			<td class="kolomTitikDua"> : </td>
			<td style="padding-bottom: 0px;"><textarea placeholder="Alamat Admin" style="width: 244px; height: 70px;" name ="editalamatAdmin" id ="editalamatAdmin" required></textarea></td>
			<td id="messageAlamatAdmin"></td>
		</tr>
		<tr>
			<td style="width: 140px;">No Telepon</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="editteleponAdmin" placeholder="Nomer Telepon Admin" required></td>
			<td id="messageTeleponAdmin"></td>
		</tr>

		<tr>
			<td style="width: 140px;">Email</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="email" id="editemailBaru" placeholder="Emain Admin" required></td>
		</tr>
		<tr>
			<td>Otoritas Admin</td>
			<td class="kolomTitikDua"> : </td>
			<td>
					<select id="editselect_otoritas_admin"  style="width: 172.5px;" required>
					<option value="" disabled selected style="display: none;">Pilih Otoritas Admin</option>
					<option value="BACA">Baca</option>
					<option value="TULIS">Tulis</option>
					<option value="BACATULIS">BacaTulis</option>
					<option value="TIDAKADA">TidakAda</option>
					</select></td>
		</tr>
		<tr>
			<td>Otoritas Pemasukan</td>
			<td class="kolomTitikDua"> : </td>
			<td>
					<select id="editselect_otoritas_pemasukan"  style="width: 172.5px;" required>
					<option value="" disabled selected style="display: none;">Pilih Otoritas Pemasukan</option>
					<option value="BACA">Baca</option>
					<option value="TULIS">Tulis</option>
					<option value="BACATULIS">BacaTulis</option>
					<option value="TIDAKADA">TidakAda</option>
					</select></td>
		</tr>
		<tr>
			<td>Otoritas Pengeluaran</td>
			<td class="kolomTitikDua"> : </td>
			<td>
					<select id="editselect_otoritas_pengeluaran"  style="width: 172.5px;" required>
					<option value="" disabled selected style="display: none;">Pilih Otoritas Pengeluaran</option>
					<option value="BACA">Baca</option>
					<option value="TULIS">Tulis</option>
					<option value="BACATULIS">BacaTulis</option>
					<option value="TIDAKADA">TidakAda</option>
					</select></td>
		</tr>
		<tr>
			<td>Status</td>
			<td class="kolomTitikDua"> : </td>
			<td>
					<select id="editselect_status"  style="width: 172.5px;" required>
					<option value="" disabled selected style="display: none;">Pilih Status</option>
					<option value="0">Aktif</option>
					<option value="1">Tidak Aktif</option>
					</select></td>
		</tr>
	</table>
	<br>
	<div id="tombolAdminEditTambah">
	<input type="submit" value="Simpan"  id="btnEditAdmin" name="btnEditAdmin">
	<button type="button" onclick="batalEditAdmin()" id="btnBatalEditAdmin">Batal</button>
	</div>
	</form>
	</br>
	</br>
	</br>
</div>