<div id="detailAdmin" >
	<label id="labelDetailAdmin">DETAIL ADMIN</label>
	<a href="#" class="close"><img src="image/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
	
	<form id="formDetailAdmin">
	<table border="0">

		<tr>
			<td style="width: 140px;">Nama Admin</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="namaDetailAdmin" disabled></td>
		</tr>
		<tr>
			<td style="width: 140px;">Kode</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="kodeDetailAdmin" disabled></td>
		</tr>
		<tr>
			<td id="bidangKomisiDetail" style="width: 140px;">Bidang / Komisi</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="kodeTerpilihDetailAdmin" disabled></td>
		</tr>
		<tr style="vertical-align: top;">
			<td style="width: 140px;">Alamat</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td style="padding-bottom: 0px;"><textarea style="width: 244px; height: 70px;" name ="alamatDetailAdmin" id ="alamatDetailAdmin" disabled></textarea></td>
		</tr>
		<tr>
			<td style="width: 140px;">No Telepon</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="teleponDetailAdmin" disabled></td
		</tr>

		<tr>
			<td style="width: 140px;">Email</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="emailDetailAdmin" disabled></td>
		</tr>
		<tr>
			<td style="width: 140px;">Status</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input type="text" id="statusDetailAdmin" disabled></td>
		</tr>
	
	</table>
	</form>
	<br>
	
	<div id="buttonEditProfilAkunAll">
		<button type="button" id"buttBatalAkun" onclick="closeDetailAdmin();">Tutup</button>
	</div>
</div>
