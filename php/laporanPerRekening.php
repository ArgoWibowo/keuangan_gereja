
<div id="divTitle">
		<label class="lblTitle">LAPORAN PER REKENING</label>
</div>


<div style="padding-left:30px;">
<br>
	<form id="formTampilLapPerRek" method="post">
	<table>
		<tr>
			<td>
				Tahun Pembukuan
			</td>
			<td class="kolomTitikDua"> : </td>
			<td colspan="3">
				<input id="thnPembukuanLapPerRek" maxlength=4 style="width:175px;" type="number" min=2000 max=9999 placeholder='Tahun Pembukuan' required>
			</td>
		</tr>
		<tr>
			<td>
				<label>Kode Rekening  </label>
			</td>
			<td class="kolomTitikDua"> : </td>
			<td>
				<select id='selectTampilLapKelAkun' style="width:182px;" required>
				<option value='' disabled selected style='display: none;'>Pilih Rekening</option>
					
				</select>
			</td>
			<td>&nbsp;</td>
			<td>Per Tanggal </td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglKelAK" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required></td>
			<td ><input id="blnKelAK" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required></td>
			<td ><input id="thnKelAK" maxlength=4 style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required></td>
			<td>&nbsp;</td>
			<td><input type="submit" value="Tampilkan"  style="margin-right:10px; width:100px; height:25px;" id="btnTampilLapPerRek" name="btnTampilLapPerRek"></td>
		</tr>
	</table>
	</form>
	</br>
	</br>
	<div>
		<label id="judulLaporanPerRekening" >LAPORAN PER REKENING</label>
	</div>
	<br>
	<div>
		<label id="perTanggalLaporanPerRekening" >Per Tanggal </label>
		<label id="perTanggalTampilLaporanPerRekening" >//</label>
	</div>
	</br>
	<table id= "LaporanPerRekening">

	<style type="text/css">
		@media print {

			table, td {
		    	border: 1px solid black;

			}

			label{
				margin-left: 50px;
			}

			table{
				margin-left: 50px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 40px;
			}
		}
	</style>
	
		<thead>	
		<tr>
			<td id="noRekLapKeg" rowspan="2">
				No
			</td>
			<td id="kodeRekLapKeg" rowspan="2">
				Kode
			</td>
			<td id="namaRekLapKeg" rowspan="2">
				Nama Rekening
			</td>
			<td id="anggaranTahunanRekLapKeg" rowspan="2">
				Anggaran Tahunan
			</td>
			<td id="realisasiRekLapKeg" colspan="2">
				Realisasi
			</td>
			<td id="sisaRekLapKeg" colspan="2">
				Sisa Anggaran
			</td>
		</tr>
		<tr>
			<td id="jumlahrealisasiRekLapKeg">
				Jumlah
			</td>
			<td id="perrealisasiRekLapKeg">
				%
			</td>
			<td id="jumlahsisaRekLapKeg">
				Jumlah
			</td>
			<td id="persisaRekLapKeg">
				%
			</td>
		</tr>
		</thead>
		<tbody>
		
		</tbody>

		</table>
		</br>
		</br>
		<button onclick="printData('LaporanPerRekening','judulLaporanPerRekening','perTanggalLaporanPerRekening','perTanggalTampilLaporanPerRekening')" id='cetakLaporan'>Cetak</button>
		<label id="lblExportLaporanPerRekening">Export : </label>
		<select id='selectExportLaporanPerRekening' class="selectExportLaporanPerRekening" onchange="exportLaporan('LaporanPerRekening','selectExportLaporanPerRekening','judulLaporanPerRekening','perTanggalLaporanPerRekening','perTanggalTampilLaporanPerRekening')">
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
		</br>
		</br>
</div>