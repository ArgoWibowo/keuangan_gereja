<div id="divTitle">
		<label class="lblTitle">MUTASI KAS</label>
</div>

<div style="padding-left:30px;">
	<br>
	<form id="formMutasiKas" method="post">
	<table>
		<tr>
			<td>
				Tahun Pembukuan
			</td>
			<td class="kolomTitikDua"> : </td>
			<td colspan="3">
				<input id="thnPembukuanMutasiKas" maxlength=4 style="width:172px;" type="number" min=2000 max=9999 placeholder='Tahun Pembukuan' required>
			</td>
		</tr>
		<tr>
			<td>Mulai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td >
				<input id="tglMulaiKas" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required>
			</td>
			<td >
				<input id="blnMulaiKas" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required>
			</td>
			<td >
				<input id="thnMulaiKas" maxlength=4 class='thnMulaiLaporan' style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required>
			</td>
			<td>&nbsp;&nbsp;</td>
			<td>Sampai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td >
				<input id="tglSampaiKas" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required>
			</td>
			<td >
				<input id="blnSampaiKas" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required>
			</td>
			<td >
				<input id="thnSampaiKas" maxlength=4 class='thnSampaiLaporan' style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required>
			</td>
			<td>&nbsp;&nbsp;</td>
			<td>
				<input type="submit" value="Tampilkan" style="margin-right:10px; width:100px; height:25px;" id="btnTampilMutasiKas" name="btnTampilMutasiKas">
			</td>
		</tr>
	</table>
	</form>

	</br>
	</br>
	<div>
		<label id="judulMutasiKas">MUTASI KAS</label>
	</div>
	<br>
	<div>
		<label id="perMutasiKas" >Periode </label>
		<label id="perTanggalMutasiKas" >01/01/2000 - 01/01/2000</label>
	</div>
	</br>
	<table id="mutasi_kas">
	<style type="text/css">
		@media print {

		    table, td {
		    	border: 1px solid black;

			}

			table{
				margin-left: 60px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 80px;
			}
		}
	</style>
	</table>
	<br>
		<br>
		<button onclick="printData('mutasi_kas','judulMutasiKas','perTanggalMutasiKas','perTanggalTampilMutasiKas')" id='cetakLaporan'>Cetak</button>
		<label id="lblExportmutasi_kas">Export : </label>
		<select id='selectExportmutasi_kas' class="selectExportmutasi_kas" onchange="exportLaporan('mutasi_kas','selectExportmutasi_kas','judulMutasiKas','perTanggalMutasiKas','perTanggalTampilMutasiKas')">
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
		<br>
	<br>
	
</div>