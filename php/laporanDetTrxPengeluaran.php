<div id="divTitle">
		<label class="lblTitle">Laporan Detil Transaksi Pengeluaran</label>
</div>

<div style="padding-left:30px;">
<br>
	<form id="formDetTrxPengeluaran" method="post">
	<table>
	<tr>
		<td>
			Tahun Pembukuan
		</td>
		<td class="kolomTitikDua"> : </td>
		<td colspan="3">
			<input id="thnPembukuanDetPeng" maxlength=4 style="width:174px;" type="number" min=2000 max=9999 placeholder='Tahun Pembukuan' required>
		</td>
	</tr>
	<tr>
			<td>Per Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglDetPeng" maxlength=2 style="width:50px;" type="number" min=1 max=31  placeholder="Tgl" required></td>
			<td ><input id="blnDetPeng" maxlength=2 style="width:50px;" type="number" min=1 max=12  placeholder="Bln" required></td>
			<td ><input id="thnDetPeng" maxlength=4 style="width:50px;" type="number" min=2000 max=9999  placeholder="Thn" required></td>
			<td>&nbsp;</td>
			<td><input type="submit" value="Tampilkan"  style="margin-right:10px; width:100px; height:25px;" id="btnTampilBukuBesar" name="btnTampilDetPeng"></td>
	</tr>
	</table>
	</form>

	</br>
	</br>
	<div>
		<label id="judulDetPeng">Laporan Detil Transaksi Pengeluaran</label>
	</div>
	<br>
	<div>
		<label id="perTanggalDetPeng" >Per Tanggal</label>
		<label id="perTanggalTampilDetPeng" >01/01/2000</label>
	</div>
	</br>
	<div id='printDetPeng'>
		<table id="detil_pengeluaran">
		<style type="text/css">
			@media print {

			    table, td {
			    	border: 1px solid black;

				}

				label{
					margin-left: 50px;
				}

				table{
					margin-left: 50px;
				}
				table#kopPrint{
					border: 0px solid black;
					margin-left: 40px;
				}
			}
		</style>
			<thead >	
			<tr>
				<td id="kodeNota">
					Kode Nota
				</td>
				<td id="tglPengeluaran">
					Tanggal Pengeluaran
				</td>
				<td id="namaToko">
					Nama Toko
				</td>
				<td id="kodeTrx">
					Kode Transaksi
				</td>
				<td id="jmlh">
					Jumlah
				</td>
				<td id="keterangan">
					Keterangan
				</td>
			</tr>
			</thead>
			<tbody>
			</tbody>

		</table>
		<br>
		<br>
	</div>

	</br>
	</br>
	<button onclick="printData('printDetPeng','judulDetPeng','perTanggalDetPeng','perTanggalTampilDetPeng')" id='cetakLaporan'>Cetak</button>
	<label id="lblExportBukuBesar">Export : </label>
	<select id='selectExportDataDetPeng' class="selectExportDataDetPeng" onchange="exportLaporanDetPeng('detil_pengeluaran','selectExportDataDetPeng','judulDetPeng','perTanggalDetPeng','perTanggalTampilDetPeng')">
		<option value=''>Pilih</option>
	 	<option value='XLS'>XLS</option>
	 	<option value='PDF'>PDF</option>
	</select>
	</br>
	</br>

</div>