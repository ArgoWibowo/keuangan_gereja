<div id="divTitle">
		<label class="lblTitle">PENGELUARAN PER TAHUN</label>
</div>

<div style="padding-left:30px;">
	<br>
	<form id="formPengeluaranPerTahun" method="post">
	<table>
		<tr>
			<td>
				Tahun Pembukuan
			</td>
			<td class="kolomTitikDua"> : </td>
			<td colspan="3">
				<input id="thnPembukuanPengeluaranPerThn" maxlength=4 style="width:175px;" type="number" min=2000 max=9999 placeholder='Tahun Pembukuan' required>
			</td>
			<td>&nbsp;&nbsp;</td>
			<td>
				<input type="submit" value="Tampilkan" style="margin-right:10px; width:100px; height:25px;" id="btnTampilPengeluaranPerThn" name="btnTampilPengeluaranPerThn">
			</td>
		</tr>
	</table>
	</form>

	</br>
	
	<img id="loading" src="image/loading.gif" hidden="true">
	
	</br>
	<div>
		<label id="judulPengeluaranPerThn">LAPORAN PENGELUARAN PER TAHUN</label>
	</div>
	<br>
	<div>
		<label id="thnPengeluaranThn" >TAHUN : </label>
		<label id="thnPengeluaranPerThn" ></label>
	</div>
	</br>
	<div id= "tolaporanPengeluaranPerTahun">
	<table id="pengeluaran_per_tahun">
	<style type="text/css">
		@media print {

			table, td {
		    	border: 1px solid black;

			}

			label{
				margin-left: 50px;
			}

			table{
				margin-left: 50px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 40px;
			}
		}
	</style>
		</table>
		</div>
		<br>
		<br>
		<button onclick="printData('pengeluaran_per_tahun','judulPengeluaranPerThn','thnPengeluaranPerThn','')" id='cetakLaporan'>Cetak</button>
		<label id="lblExportmutasi_kas">Export : </label>
		<select id='selectExportPengeleruan_per_thn' class="selectExportPengeleruan_per_thn" onchange="exportLaporan('pengeluaran_per_tahun','judulPengeluaranPerThn','thnPengeluaranThn','thnPengeluaranPerThn')">
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
		<br>
	<br>
	
</div>