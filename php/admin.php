<div id="divTitle">
		<label class="lblTitle">DAFTAR ADMIN</label>
</div>

<div style="padding-left:20px;" >
	<div id="navSearchAdmin">
         	<ul class="nav">
				<li class='searchAdmin'>
					<div id='divSearchAdmin'>
						<input onchange="loadTableAdmin();" onkeyup="loadTableAdmin();" id='searchAdmin' type='text' placeholder="Cari Admin">
					</div>
				</li>
				<li class='searchAdmin2'>
					<div id='divLabelSearchAdmin'>
						<label id="labelSearchAdmin">Search</label>
					</div>
				</li>
			</ul>
    </div>
    <div id="navSearchAdmin">
          	<ul class="nav">
				<li class='searchAdmin'>
					<div id='divSearchAdmin'>
						<select onchange="orderBy()" id="selectOrderBy">
						  <option value="kode_admin">kode admin</option>
						  <option value="nama_admin">nama admin</option>
						  <option value="status">status</option>
						</select>
					</div>
				</li>
				<li class='searchAdmin2'>
					<div id='divLabelSearchAdmin'>
						<label id="labelSearchAdmin">Urutkan berdasarkan</label>
					</div>
				</li>
			</ul>
    </div>
	<div>
	<table id="tabeladmin" style="width:98%;">
			<thead id="header_tabel" style="width:98.5%;">
				<tr>
				<td align="left" id="no_urut_admin" style="width:5%;" rowspan = 2>No</td>
				<td align="left" id="id_admin_table" style="width:14.7%;" rowspan = 2>Kode Admin / Username</td>
				<td align="left" id="nama_admin_table" style="width:24.4%;" rowspan = 2>Nama Admin</td>
				<td id="otoritas_admin_table"  style="width: 45.09%;" colspan = 3>Otoritas</td>
				<td id="status_admin_table" style="width:10.81%;" rowspan = 2>Status</td>
				
				</tr>
				<tr>
					<td id="otoritas_admin_table">Administrasi</td>
					<td id="otoritas_pemasukan_table">Penerimaan</td>
					<td id="otoritas_pengeluaran_table">Pengeluaran</td>
				</tr>	
			</thead>
			<tbody id="tabel_isi" style="overflow-y:scroll; width:99%px; height: 380px;">
				
			</tbody>
				

	</table>
	<br>
	<?php
		if($_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'TULIS' ){

	?>
	
	<button style="margin-right:10px; width:100px; height:25px;" id="buttonTambah" onclick="adminTambah()">Tambah</button>
	<button style="margin-right:10px; width:100px; height:25px;" id="buttonStatus" onclick="adminStatus()">Nonaktifkan</button>
	<button style="margin-right:10px; width:100px; height:25px;" id="buttonEdit" onclick="adminEdit()">Ubah</button>
	<?php
		}
	?>
	<button style="margin-right:10px; width:100px; height:25px;" id="buttonDetail" onclick="adminDetailPopUp('#detailAdmin')">Detail</button>
	</div>
	<br>
</div>


