<div id="divTitle">
		<label class="lblTitle">JURNAL MUTASI</label>
</div>

<div style="padding-left:30px;">
	<br>
	<form id="formJurnalMutasi" method="post">
	<table>
		<tr>
			<td>
				Tahun Pembukuan
			</td>
			<td class="kolomTitikDua"> : </td>
			<td colspan="3">
				<input id="thnJurnalMutasi" maxlength=4 style="width:172px;" type="number" min=2000 max=9999 placeholder='Tahun Pembukuan' required>
			</td>
		</tr>
		<tr>
			<td>Mulai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglMulaiJurnalMutasi" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required></td>
			<td ><input id="blnMulaiJurnalMutasi" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required></td>
			<td ><input id="thnMulaiJurnalMutasi" maxlength=4 class='thnMulaiLaporan' style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required></td>
			<td>&nbsp;&nbsp;</td>
			<td>Sampai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglSampaiJurnalMutasi" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required></td>
			<td ><input id="blnSampaiJurnalMutasi" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required></td>
			<td ><input id="thnSampaiJurnalMutasi" maxlength=4 class='thnSampaiLaporan' style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>
				<input type="submit" value="Tampilkan" style="margin-right:10px; width:100px; height:25px;" id="btnTampilJurnalMutasi" name="btnTampilJurnalMutasi">
			</td>
		</tr>

	</table>
	</form>
	</br>
	</br>
	<div>
		<label id="judulJurnalMutasi">JURNAL MUTASI</label>
	</div>
	<br>
	<div>
		<label id="perBulanJurnalMutasi">BULAN : </label>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<label id="perTahunJurnalMutasi" >TAHUN : </label>
	</div>
	</br>
	<table id="jurnal_mutasi">
	<style type="text/css">
		@media print {

		    table, td {
		    	border: 1px solid black;

			}

			table{
				margin-left: 60px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 80px;
			}
		}
	</style>
	</table>
		<br>
		<br>
		<button onclick="printData('jurnal_mutasi','judulJurnalMutasi','perBulanJurnalMutasi','perTahunJurnalMutiasi')" id='cetakLaporan'>Cetak</button>
		<label id="lblExportjurnal_mutasi">Export : </label>
		<select id='selectExportjurnal_mutasi' class="selectExportjurnal_mutasi" onchange="exportLaporan('jurnal_mutasi','selectExportjurnal_mutasi','judulJurnalMutasi','perBulanJurnalMutasi','perTahunJurnalMutiasi')">
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
		<br>
		<br>
		</div>