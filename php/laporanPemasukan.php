
<div id="divTitle">
		<label class="lblTitle">LAPORAN PEMASUKAN</label>
</div>

<div style="padding-left:30px;">
	<br>
	<form id="formLaporanPemasukan" method="post">
	<table>
		<tr>
			<td>Mulai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglMulaiPemasukan" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required></td>
			<td ><input id="blnMulaiPemasukan" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required></td>
			<td ><input id="thnMulaiPemasukan" maxlength=4 class='thnMulaiLaporan' style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>
				<input type="submit" value="Tampilkan" style="margin-right:10px; width:100px; height:25px;" id="btnTampilkanLapPemasukan" name="btnTampilkanLapPemasukan">
			</td>
		</tr>
		<tr>
			<td>Sampai Tanggal</td>
			<td class="kolomTitikDua"> : </td>
			<td ><input id="tglSampaiPemasukan" maxlength=2 style="width:50px;" type="number" min=1 max=31 placeholder='tgl' required></td>
			<td ><input id="blnSampaiPemasukan" maxlength=2 style="width:50px;" type="number" min=1 max=12 placeholder='bln' required></td>
			<td ><input id="thnSampaiPemasukan" maxlength=4 class='thnSampaiLaporan' style="width:50px;" type="number" min=2000 max=9999 placeholder='thn' required></td>
		</tr>
	</table>
	</form>
	<br>
	<table id="laporan_pemasukan">
	<style type="text/css">
		@media print {

		    table, td {
		    	border: 1px solid black;

			}

			table{
				margin-left: 60px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 80px;
			}
		}
	</style>
	</table>

		<br>
		<br>
		<button onclick="printData('laporan_pemasukan','lblTitle','','')" id='cetakLaporan'>Cetak</button>
		<label id="lblExportlaporan_pemasukan">Export : </label>
		<select id='selectExportlaporan_pemasukan' class="selectExportlaporan_pemasukan" onchange="exportLaporan('laporan_pemasukan','selectExportlaporan_pemasukan','lblTitle','','')">
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
		<br>
		<br>
		</div>