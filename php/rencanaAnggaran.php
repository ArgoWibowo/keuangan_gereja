<div id="divTitle">
		<label class="lblTitle">RENCANA ANGGARAN</label>
</div>
<div style="padding-left:30px;">
	<br>
	<form id="formTampilAnggaran" method="post">
	<tr>
			<td>Tahun Anggaran :</td>
			<td ><input id="thnRenAng" maxlength=4 style="width:55px;" type="number" min=2000 max=9999 value='<?php echo  $_SESSION['t4hun_P3m8uku4an']; ?>' required></td>
			<td>&nbsp;</td>
			<td><input type="submit" value="Tampilkan"  style="margin-right:10px; width:100px; height:25px;" id="btnTampilRencanaAnggaran" name="btnTampilRencanaAnggaran"></td>
			
	<tr>
	</form>
	</br>
	</br>
	<div id="keteranganLaporanHeader">
		<label id="noEditketeranganLaporanHeader" >RENCANA ANGGARAN TAHUN</label>
		<label id="editKeteranganLaporanHeader" >2000</label>
	</div>
	</br>
	<table id= "rencanaAnggaran">
	<style type="text/css">
		@media print {

		    table, td {
		    	border: 1px solid black;

			}

			table{
				margin-left: 60px;
			}
			label{
				margin-left: 60px;
			}
			table#kopPrint{
				border: 0px solid black;
				margin-left: 60px;
			}
		}
	</style>
		<thead>	
		<tr>
			<td id="rekPer">
				Nomer dan Nama Rekening
			</td>
			<td id="saldoPer">
				Rencana Rp
			</td>
		</tr>
		</thead>
		<tbody>
		</tbody>

	</table>
	</br>
	</br>
	<?php
	$admin = $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'TULIS';
	$pemasukan = $_SESSION['0t0rit4s_P3ma5uk4n_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_P3ma5uk4n_K3uan94n_G3r3j4'] == 'TULIS';
	$pengeluaran = $_SESSION['0t0rit4s_P3n93lu4ran_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_P3n93lu4ran_K3uan94n_G3r3j4'] == 'TULIS';

	if($admin && $pemasukan && $pengeluaran){
	?>
	<button id="tambahRencanaAnggaranBut" onclick="tambahRencanaAnggaranAkunBut()">Tambah</button>
	<button id="editRencanaAnggaranBut" onclick="editRencanaAnggaran('#formEditRencanaAnggaran')">Ubah</button>
	<button id="hapusRencanaAnggaranBut" onclick="hapusRencanaAnggaranAkun()">Hapus</button>
	<?php
	}
	?>
	<button id="printRencanaAnggaranBut" onclick="printDataRen('rencanaAnggaran','noEditketeranganLaporanHeader','editKeteranganLaporanHeader')">Cetak</button>
	<label id="lblExportRencanaAnggaran">Export : </label>
	<select id='selectExportDataRencanaAnggaran' class="selectExportDataRencanaAnggaran" onchange="exportRencanaAnggaran('rencanaAnggaran','noEditketeranganLaporanHeader','editKeteranganLaporanHeader')">
		<option value=''>Pilih</option>
	 	<option value='XLS'>XLS</option>
	 	<option value='PDF'>PDF</option>
	</select>
	</br>
	</br>
</div>