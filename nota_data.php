<?php  
	require_once("koneksi.php");
?>	
	<div id="navJumlahDataNota" class="navJumlahData">
		<label>Jumlah Data : </label> <label id="lblJumlahDataNota" class="lblJumlahDataNota" ></label>
	</div>
	<div id="navSearchNota" class="navSearch">
		<ul class="nav">
			<li class='search'>
				<div class='divSearch'>
					<input name ="kataKunciNota" onchange="searchNota()" onkeyup="searchNota()"  id='searchNota' class='searchNota' type='text' placeholder="Masukkan kata kunci">
				</div>
			</li>
		</ul>
	</div>
	<div id="navSortNota" class="navSort" >
		<label>Urutkan berdasarkan : </label>
		<select id='sortDataNota' class="sortData" onchange=searchNota()>
		 	<option value='kode_nota'>Kode Nota</option>
		 	<option value='nama_toko'>Nama Toko</option>
		 	<option value='kode_jenis_akun'>Rekening</option>
		 	<option value='tgl_pengeluaran'>Tanggal Pengeluaran</option>
		 	<option value='jumlah_pengeluaran'>Jumlah Pengeluaran</option>		 	
		</select>
	</div>
	<div id="datanota" class="dataTable">
		<table class="tabelData striped" id='tabelDataNota'style="padding :20px 20px; align:"center";">
			<thead>
				<tr >
					<th id="notabelnota" style="text-align:left;width: 30px;">No</th>					
					<th id="kodetabelnota" style="text-align:left;">Kode Nota</th>
					<th class="kodetokotabelnota"  style="text-align:left;">Kode Toko</th>
					<!-- <th class="kodetransaksitabelnota"  style="text-align:left;">Kode Transaksi</th> -->
					<th class="debettabelnota" style="text-align:left;">Debet</th>	<!-- aliasnya kode jenis akun -->
					<th class="tanggaltabelnota" style="text-align:left;">Tanggal Pengeluaran</th>
					<th class="jumlahtabelnota" style="text-align:left;">Jumlah</th>															
					<th class="pelaksanatabelnota" style="text-align:left;">Pelaksana</th>										
					<!-- <th class="statustabelnota" style="text-align:left;">Status nota</th> -->
					<th class="actiontabelnota" style="text-align:left;">Pilihan</th>
				</tr>	
			</thead>
			<tbody id ="isiTabelNota" class="isiTabel">
			<tbody>
		</table>
	</div>
	<div id="paginationNota" class="pagination" cellspacing="0">
	</div>
	<div style="margin-left:20px;">
		<button onclick="detailNotaData('#detailNota')"  style="width: 120px; height: 25px;">Detail</button>
		<!-- <button onclick="printDataKomisiNota('tabelDataNota')" id='cetakLaporan'  style="width: 120px; height: 25px;">Cetak</button> -->
		<label>Export : </label>
		<select id='selectExportDataNota' class="selectExportDataNota" onchange=exportNota()>
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
	</div>
	<br>


<div id="detailNota" class="detailNota" >
	<label id="labelDetailNota">DETAIL NOTA</label>
	<a href="#" class="close"><img src="image/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
	
	<form id="formDetailNota">
	<table border="0">

		<tr>
			<td style="width: 160px;">Kode Nota</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="kodeNotaDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Kode Toko</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="kodeTokoNotaDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Debet</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="jenisAkunNotaDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Tanggal Pengeluaran</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="tanggalPengeluaranNotaDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Jumlah</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="jumlahNotaDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Pelaksana</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="pelaksanaNotaDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Waktu Input</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="waktuInputNotaDetail" disabled></td>
		</tr>
		<!-- <tr style="vertical-align: top;">
			<td style="width: 160px;">Deskripsi</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td style="padding-bottom: 0px;"><textarea style="width: 170px; height: 60px;" name ="desNotaDetai" id ="desNotaDetail" disabled></textarea></td>
		</tr> -->
		<tr>
			<td style="width: 160px;">Kode Admin</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input type="text" id="kodeAdminNotaDetail" disabled></td>
		</tr>
	
	</table>
	</form>
	<br>
	
	<div id="buttonEditProfilAkunAllKomisiNota">
		<button type="button" id"buttNotaDetail" onclick="closeDetailNota();">Tutup</button>
	</div>
</div>
