<?php 
	if(isset($_POST['type'])){
		
	} else {
		getViewBuktiPenerimaan();
	}

	

	function getViewBuktiPenerimaan(){
		?>

		<link rel="stylesheet" type="text/css" href="css/buktipenerimaan.css">
		<script type="text/javascript" src="js/buktipenerimaan.js"></script>
		<form id="formbuktipenerimaan">
			<div class="fTitle">FORM BUKTI PENERIMAAN</div>
			<table id="buktiPenerimaanAtas">
				<tr>
					<td width="15%">Kode bukti penerimaan</td><td width="1%">:</td>
					<td width="30%" colspan=3><input id="kodeBuktiPenerimaan" maxlength=8 placeholder="max 8 karakter" required tabindex="1"></td>
					<td width="8%"class="separator"></td>
					<td width="15%">Bendahara</td><td width="1%">:</td>
					<td width="30%">
						<div class="relativeBox">
							<select class="selectBendaharaBuktiPenerimaan" required tabindex="7">
							<?php isiSelectPenanggungJawab("bendahara"); ?>
							</select><input class="trick" autocomplete="off" id="selectBendaharaBuktiPenerimaan" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Debet</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectdebetbuktipenerimaan" required tabindex="2">
							<?php isiSelectRekening(); ?>
							</select><input class="trick" autocomplete="off" id="selectdebetbuktipenerimaan" tabindex="-1">
						</div>
					</td>
					<td class="separator"></td>
					<td>Penerima</td><td>:</td>
					<td>
						<div class="relativeBox">
							<select class="selectPenerimaBuktiPenerimaan" required tabindex="8">
							<?php isiSelectPenanggungJawab("penerima"); ?>
							</select><input class="trick" autocomplete="off" id="selectPenerimaBuktiPenerimaan" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Tanggal penerimaan</td><td>:</td>
					<td class="customdate"><input type="number" class="ex" id="buktipenerimaan_tanggal" min=1 max=31 placeholder="tanggal" value=<?php echo date("d") ?> required tabindex="3"></td>
					<td class="customdate"><input type="number" class="ex" id="buktipenerimaan_bulan" min=1 max=12 placeholder="bulan" value=<?php echo date("m") ?> required tabindex="4"></td>
					<td class="customdate"><input type="number" class="ex" id="buktipenerimaan_tahun" min=2000 max=9999 placeholder="tahun" value=<?php echo date("y")+2000 ?> required tabindex="5"></td>
					<td class="separator"></td>
					<td>Penyetor</td><td>:</td>
					<td>
						<div class="relativeBox">
							<select class="selectPenyetorBuktiPenerimaan" required tabindex="9">
							<?php isiSelectPenanggungJawab("penyetor"); ?>
							</select><input class="trick" autocomplete="off" id="selectPenyetorBuktiPenerimaan" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Keterangan</td><td>:</td>
					<td colspan=7 id="boxKeteranganBuktiPenerimaan">
						<input id="keteranganBuktiPenerimaan" placeholder="max 150 karakter" maxlength=150 required tabindex="6">
					</td>
				</tr>
			</table>
			<br><hr>
			<table id="detailKredit">
				<thead>
					<tr>
						<th width="10%">Kode Penerimaan</th>
						<th width="10%">Kredit</th>
						<th width="60%">Uraian</th>
						<th colspan=2>Jumlah</th>
						<th width="8%"></th>
					</tr>
				</thead>
				<tbody id="detailKreditBody">
					<?php
						$data = isiDetailKredit(date('y-m-d'));
					?>
				</tbody>
				
		</form>
		<form id="tambahPenerimaan">
				<tbody id="detailKreditBody2">
					<tr>
			    		<td colspan=2 id="boxSelectKreditPenerimaanTambahan">
			    			<div class="relativeBox">
				    			<select class="selectKreditPenerimaanTambahan" required>
								<?php isiSelectRekeningNonIndividu(); ?>
								</select><input class="trick" autocomplete="off" id="selectKreditPenerimaanTambahan" tabindex="-1">
							</div>
						</td>
			    		<td id="boxUraianPenerimaanTambahan">
			    			<input id="uraianPenerimaanTambahan" placeholder="uraian penerimaan" required>
			    		</td>
			    		<td id="boxJumlahPenerimaanTambahan" colspan=2>
			    			<div class="relativeBox">
				    			<input value="Rp." class="rp ex" tabindex="-1" disabled>
								<input class="rptail" autocomplete="off" min=0 step=100 id="penerimaanTambahan_jumlah" required>
							</div>
			    		</td>
			    		<td>
			    			<div class="relativeBox">
			  	  				<input type="submit" form="tambahPenerimaan" value="tambah" style="height: 24px;">
			  	  			</div>
			    		</td>
			    	</tr>
				</tbody>
		</form>
				<tbody id="totalDetailKredit">
					<tr>
						<td>Jumlah</td>
						<td></td>
						<td></td>
						<td class="rpCol">Rp.</td>
						<td class="numerik" id="totalSementaraDetailPenerimaan"><?php echo $data; ?></td>
						<td></td>
					</tr>
				</tbody>
			</table><br>
			<div style="text-align:center;">
				<input type="reset" value="bersihkan" tabindex=-1 style="width: 120px; height: 35px;">
				<input type="submit" value="simpan" tabindex=10 style="width: 120px; height: 35px;">
			</div>
		<?php 
	} 
?>

