<?php 
	if(isset($_POST['type'])){
		if($_POST['type'] == 'isiDetailKredit'){
			isiDetailKredit($_POST['batas']);
		} else if($_POST['type'] == 'isiDetailDebet'){
			isiDetailDebet($_POST['batas']);
		} else if($_POST['type'] == 'isiDetailBon'){
			isiDetailBon();
		} else if($_POST['type'] == 'isiDetailMutasi'){
			isiDetailMutasi();
		} else if($_POST['type'] == "isiPenerimaanIndividu"){
			isiPenerimaanIndividu();
		} else if($_POST['type'] == "isiBuktiTransaksi"){
			isiBuktiTransaksi();
		} else if($_POST['type'] == "isiBuktiTransaksi2"){
			isiBuktiTransaksi2();
		}
	} 

	function formatDate($fromdb){
		$arr = explode('-', $fromdb);
		return $arr[2].'-'.$arr[1].'-'.$arr[0];
	}

	function isiDetailKredit($batas){
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';

		$query = "SELECT MAX(kode_transaksi)+1 FROM tbl_transaksi WHERE tahun_pembukuan = ?";
	    $statement = $conn->prepare($query);
	    $statement->bind_param("i", $_SESSION['t4hun_P3m8uku4an']);
	    $statement->bind_result($count);
	    if($statement->execute()){
	    	$statement->fetch();
	    	if(!isset($count)) $count = 1;
	    }
	    $statement->close();

	    $query = "SELECT kode_jenis_akun, nama_kode_jenis_akun, SUM(jumlah), MIN(tanggal_penerimaan), MAX(tanggal_penerimaan)
	    		  FROM tbl_penerimaan_individual NATURAL JOIN tbl_jenis_akun WHERE kode_transaksi IS NULL AND tanggal_penerimaan <= ? 
	    		  GROUP BY kode_jenis_akun";
	    $statement = $conn->prepare($query);
	    $statement->bind_param('s', $batas);
	    $statement->bind_result($kredit, $nama, $jumlah, $min, $max);
	    $statement->execute();

	    $totalSementara = 0;
	    while ($statement->fetch()) {
	    	$totalSementara = $totalSementara + $jumlah;
	    	?>
	    	<tr>
	    		<td class="calonKodeTransaksi"><?php echo $count; ?></td>
	    		<td id=<?php echo "'".str_replace(".", "_dot_", $kredit)."'"; ?>><?php echo $kredit; ?></td>
	    		<td><input class="uraianPenerimaan" value=
	    			<?php
		    			echo '"'.$nama.' '.formatDate($min);
		    			if($max != $min){
		    				echo ' s/d '.formatDate($max);
		    			}
		    			echo '"'; 
		    		?>>
		    	</td>
		    	<td class="rpCol">Rp.</td>
	    		<td class="numerik"><?php echo $jumlah; ?></td>
	    		<td><label class="delRow">x</label></td>
	    	</tr>
	    	<?php
	    	$count++;
	    }
	    $statement->close();
	    ?>
	    <tr hidden>
	    	<td><label id="lastCounterDetailPenerimaan" hidden><?php echo $count; ?></label></td>
	    	<td><label id="totalSementaraDetailPenerimaanHidden" hidden><?php echo $totalSementara; ?></label></td>
	    </tr>
	    <?php
	    return $totalSementara;
    }

    function isiDetailDebet($batas){
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';

		$query = "SELECT MAX(kode_transaksi)+1 FROM tbl_transaksi WHERE tahun_pembukuan = ?";
	    $statement = $conn->prepare($query);
	    $statement->bind_param("i", $_SESSION['t4hun_P3m8uku4an']);
	    $statement->bind_result($count);
	    if($statement->execute()){
	    	$statement->fetch();
	    	if(!isset($count)) $count = 1;
	    }
	    $statement->close();

	    $query = "SELECT kode_nota, nama_toko, tgl_pengeluaran, kode_jenis_akun, jumlah
	    		  FROM tbl_nota NATURAL JOIN tbl_toko WHERE kode_transaksi IS NULL AND tgl_pengeluaran <= ?";
	    $statement = $conn->prepare($query);
	    $statement->bind_param('s', $batas);
	    $statement->bind_result($nota, $toko, $tanggal, $debet, $jumlah);
	    $statement->execute();

	    $totalSementara = 0;
	    while ($statement->fetch()) {
	    	$totalSementara = $totalSementara + $jumlah;
	    	?>
	    	<tr>
	    		<td class="calonKodeTransaksi"><?php echo $count; ?></td>
	    		<td id=<?php echo "'".str_replace(".", "_dot_", $debet)."'"; ?>><?php echo $debet; ?></td>
	    		<td><input class="uraianPengeluaran" value=
	    			<?php
		    			echo '"Pembelian ... di toko '.$toko.' dengan nota '.$nota.' tanggal '.formatDate($tanggal).'"';
		    		?>>
		    	</td>
		    	<td class="rpCol">Rp.</td>
	    		<td class="numerik"><?php echo $jumlah; ?></td>
	    		<td><label class="delRow">x</label></td>
	    		<td hidden><?php echo $nota; ?></td>
	    	</tr>
	    	<?php
	    	$count++;
	    }
	    if($statement->num_rows() == 0){
	    	?>
	    	<tr>
	    		<td colspan=6><i>tidak ada nota baru</i></td>
	    	</tr>
	    	<?php
	    }
	    
	    $statement->close();
	    ?>
	    <tr hidden>
	    	<td><label id="lastCounterDetailPengeluaran" hidden><?php echo $count; ?></label></td>
	    	<td><label id="totalSementaraDetailPengeluaranHidden" hidden><?php echo $totalSementara; ?></label></td>
	    </tr>
	    <?php
	    return $totalSementara;
    }
	
	function isiDetailBon(){
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';

		$query = "SELECT MAX(kode_transaksi)+1 FROM tbl_transaksi WHERE tahun_pembukuan = ?";
	    $statement = $conn->prepare($query);
	    $statement->bind_param("i", $_SESSION['t4hun_P3m8uku4an']);
	    $statement->bind_result($count);
	    if($statement->execute()){
	    	$statement->fetch();
	    	if(!isset($count)) $count = 1;
	    }
	    $statement->close();

	    ?>
	    <tr hidden>
	    	<td><label id="lastCounterDetailBon" hidden><?php echo $count; ?></label></td>
	    </tr>
	    <?php
    }

    function isiDetailMutasi(){
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';

		$query = "SELECT MAX(kode_transaksi)+1 FROM tbl_transaksi WHERE tahun_pembukuan = ?";
	    $statement = $conn->prepare($query);
	    $statement->bind_param("i", $_SESSION['t4hun_P3m8uku4an']);
	    $statement->bind_result($count);
	    if($statement->execute()){
	    	$statement->fetch();
	    	if(!isset($count)) $count = 1;
	    }
	    $statement->close();

	    ?>
	    <tr hidden>
	    	<td><label id="lastCounterDetailMutasi" hidden><?php echo $count; ?></label></td>
	    </tr>
	    <?php
    }

    function isiPenerimaanIndividu(){
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';

		$query = "SELECT no_kwitansi, tanggal_penerimaan, p.kode_jenis_akun, nama_kode_jenis_akun, pemberi, nama_jemaat,
				  p.jumlah, bulan, tahun, p.penerima
	    		  FROM (tbl_jenis_akun r NATURAL JOIN tbl_penerimaan_individual p) INNER JOIN tbl_jemaat j
                  ON p.pemberi = j.kode_jemaat
                  WHERE kode_transaksi IS NULL ORDER BY 2";
	    $statement = $conn->prepare($query);
	    $statement->bind_result($kwitansi, $tanggal, $kredit, $namaKredit, $pemberi, $namaPemberi, $jumlah, $bulan, $tahun, $penerima);
	    $statement->execute();

	    while ($statement->fetch()) {
	    	?>
	    	<tr>
	    		<td><?php echo $kwitansi; ?></td>
	    		<td><?php echo formatDate($tanggal); ?></td>
	    		<td class="leftAlignTd"><?php echo $kredit.' - '.$namaKredit; ?></td>
	    		<td class="leftAlignTd"><?php echo $pemberi.' - '.$namaPemberi; ?></td>
		    	<td class="rpCol">Rp.</td>
	    		<td class="rightAlignTd"><?php echo $jumlah; ?></td>
	    		<td hidden><?php echo $penerima; ?></td>
	    		<td class="leftAlignTd"><?php
	    			if ($bulan != NULL) echo $bulan;
	    			if ($tahun != NULL) echo ' tahun '.$tahun; 
	    			?>
	    		</td>
	    		<td class="boxOptionDataPenerimaan">
	    			<select class="optionDataPenerimaan">
	    				<option value="" disabled selected style="display: none;"></option>
	    				<option value="ubah">ubah</option>
	    				<option value="hapus">hapus</option>
	    			</select>
	    		</td>
	    	</tr>
	    	<?php
	    }
	    if($statement->num_rows() == 0){
	    	?>
	    	<tr>
	    		<td colspan=9><i>tidak ada penerimaan individu yang belum masuk transaksi</i></td>
	    	</tr>
	    	<?php
	    }
	    $statement->close();
	}

	function isiBuktiTransaksi(){
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';

		$query = "SELECT kode_bukti_transaksi, tanggal_bukti_transaksi, tipe_transaksi, b.kode_jenis_akun,
				  nama_kode_jenis_akun, jumlah, keterangan, bendahara, p1.nama_penanggungjawab, penerima,
				  p2.nama_penanggungjawab, penyetor, p3.nama_penanggungjawab
				  FROM (((tbl_jenis_akun r NATURAL JOIN tbl_bukti_transaksi b) INNER JOIN tbl_penanggungjawab p1
				  ON b.bendahara = p1.kode_penanggungjawab) INNER JOIN tbl_penanggungjawab p2
				  ON b.penerima = p2.kode_penanggungjawab) INNER JOIN tbl_penanggungjawab p3
				  ON b.penyetor = p3.kode_penanggungjawab 
                  WHERE waktu_posting IS NULL ORDER BY 2";
	    $statement = $conn->prepare($query);
	    $statement->bind_result($kodebukti, $tanggal, $tipe, $rekening, $namarekening, $jumlah, $keterangan, $kodebendahara, $namabendahara, $kodepenerima, $namapenerima, $kodepenyetor, $namapenyetor);
	    $statement->execute();

	    while ($statement->fetch()) {
	    	?>
	    	<tr>
	    		<td><?php echo $kodebukti; ?></td>
	    		<td><?php echo formatDate($tanggal); ?></td>
	    		<td class="leftAlignTd"><?php echo $tipe.'<br>'.$rekening.' - '.$namarekening; ?></td>
	    		<td class="leftAlignTd">
	    			<?php
	    				echo $kodebendahara.' - '.$namabendahara.'<br>';
	    				echo $kodepenerima.' - '.$namapenerima.'<br>';
	    				echo $kodepenyetor.' - '.$namapenyetor; 
	    			 ?>
	    		</td>
		    	<td class="rpCol">Rp.</td>
	    		<td class="rightAlignTd"><?php echo $jumlah; ?></td>
	    		<td class="leftAlignTd"><?php echo $keterangan; ?></td>
	    		<td class="boxOptionDataBukti">
	    			<select class="optionDataBukti">
	    				<option value="" disabled selected style="display: none;"></option>
	    				<option value="post">post</option>
	    				<option value="hapus">hapus</option>
	    			</select>
	    		</td>
	    	</tr>
	    	<?php
	    }
	    if($statement->num_rows() == 0){
	    	?>
	    	<tr>
	    		<td colspan=9><i>tidak ada bukti transaksi yang belum di-posting</i></td>
	    	</tr>
	    	<?php
	    }
	    $statement->close();
	}

	function isiBuktiTransaksi2(){
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';

		$query = "SELECT kode_bukti_transaksi, tanggal_bukti_transaksi, tipe_transaksi, b.kode_jenis_akun,
				  nama_kode_jenis_akun, jumlah, keterangan, bendahara, p1.nama_penanggungjawab, penerima,
				  p2.nama_penanggungjawab, penyetor, p3.nama_penanggungjawab
				  FROM (((tbl_jenis_akun r NATURAL JOIN tbl_bukti_transaksi b) INNER JOIN tbl_penanggungjawab p1
				  ON b.bendahara = p1.kode_penanggungjawab) INNER JOIN tbl_penanggungjawab p2
				  ON b.penerima = p2.kode_penanggungjawab) INNER JOIN tbl_penanggungjawab p3
				  ON b.penyetor = p3.kode_penanggungjawab 
                  WHERE waktu_posting IS NOT NULL";
	    $statement = $conn->prepare($query);
	    $statement->bind_result($kodebukti, $tanggal, $tipe, $rekening, $namarekening, $jumlah, $keterangan, $kodebendahara, $namabendahara, $kodepenerima, $namapenerima, $kodepenyetor, $namapenyetor);
	    $statement->execute();

	    while ($statement->fetch()) {
	    	?>
	    	<tr>
	    		<td><?php echo $kodebukti; ?></td>
	    		<td><?php echo formatDate($tanggal); ?></td>
	    		<td class="leftAlignTd"><?php echo $tipe.'<br>'.$rekening.' - '.$namarekening; ?></td>
	    		<td class="leftAlignTd">
	    			<?php
	    				echo $kodebendahara.' - '.$namabendahara.'<br>';
	    				echo $kodepenerima.' - '.$namapenerima.'<br>';
	    				echo $kodepenyetor.' - '.$namapenyetor; 
	    			 ?>
	    		</td>
		    	<td class="rpCol">Rp.</td>
	    		<td class="rightAlignTd"><?php echo number_format($jumlah, 0, ".", "."); ?></td>
	    		<td class="leftAlignTd"><?php echo $keterangan; ?></td>
	    	</tr>
	    	<?php
	    }
	    if($statement->num_rows() == 0){
	    	?>
	    	<tr>
	    		<td colspan=9><i>tidak ada bukti transaksi yang sudah di-posting</i></td>
	    	</tr>
	    	<?php
	    }
	    $statement->close();
	}
 ?>