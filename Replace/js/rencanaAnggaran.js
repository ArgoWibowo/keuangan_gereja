$(document).ready(function() {
	
	uniform("formTambahRencanaAnggaran",300);
	uniform("formEditRencanaAnggaranAkun",180);
	$(document).on('click','#mask, .btn_close',function(){
		closeEditRencanaAnggaran();
    });
});

 $("form#formTambahRencanaAnggaran").submit(function(){


		   var jenisAkunRencanaDanaOptions = document.getElementById("pilihanRencanaKodeAkunRekening");
		   var jenisAkunRencanaDana = jenisAkunRencanaDanaOptions.options[jenisAkunRencanaDanaOptions.selectedIndex].value;

		   var jumlahRencanaAnggaran=$("#rencanaDanaJenisAkunRekening").val();
		   var tahunRencanaAnggaran=$("#tahunJenisAkunRekening").val();
		   
		   $.ajax({
		        url: "tambahRencanaAnggaranRek.php",
		        type: 'POST',
		        data: {
					"jenisAkunRencanaDana": jenisAkunRencanaDana,
					"jumlahRencanaAnggaran":jumlahRencanaAnggaran,
					"tahunRencanaAnggaran": tahunRencanaAnggaran
				},
		        async: false,
		        success: function (data) {
		            if(data == "1"){
		            	$("#thnRenAng").val(tahunRencanaAnggaran);
						alert('Rencana Anggaran Ditambahkan');
						loadRencanaAnggaran();
						$("div.tambahPerencanaan").slideUp("700");
						$("#errTambahRencanaAnggaran").html("<label>&nbsp;</label>");
						$("#right>div.perencanaanAnggaran").slideDown("700");
						$("#right>div.perencanaanAnggaran div").slideDown("700");
					}else if(data == "0"){
						$("#errTambahRencanaAnggaran").html("<label style='color:red; margin-left: 100px;'>Error: Perencanaan akun telah dibuat!</label>");
					

					}
		        }
		    });
	    return false;
	});

	$("form#formTampilAnggaran").submit(function(){


		   loadRencanaAnggaran();
	    return false;
	});

	$("form#formEditRencanaAnggaranAkun").submit(function(){


		   editKodeJenisAkunPopUp();
	    return false;
	});



// $("form#formTambahRencanaAnggaran").submit(function(){

// 	var jenisAkunRencanaDanaOptions = document.getElementById("pilihanRencanaKodeAkunRekening");
// 	var jenisAkunRencanaDana = jenisAkunRencanaDanaOptions.options[jenisAkunRencanaDanaOptions.selectedIndex].value;

// 	var jumlahRencanaAnggaran=$("#rencanaDanaJenisAkunRekening").val();
// 	var tahunRencanaAnggaran=$("#tahunJenisAkunRekening").val();
// 			$.ajax({
// 				type: "POST",
// 				url: "tambahRencanaAnggaranRek.php",
// 				data: {
// 					"jenisAkunRencanaDana": jenisAkunRencanaDana,
// 					"jumlahRencanaAnggaran":jumlahRencanaAnggaran,
// 					"tahunRencanaAnggaran": tahunRencanaAnggaran
// 				},
// 				async: false,
// 				success: function(data){
// 				if(data){
// 					if(data == "1"){
// 						alert('Rencana Anggaran Ditambahkan');
// 						loadRencanaAnggaran();
// 						$("div.tambahPerencanaan").slideUp("700");
// 						$("#right>div.perencanaanAnggaran").slideDown("700");
// 						$("#right>div.perencanaanAnggaran div").slideDown("700");
// 					}else if(data == "0"){
						
// 						$("#errTambahRencanaAnggaran").html("<label style='color:red; margin-left: 100px;'>Error: Perencanaan akun telah dibuat!</label>");
// 					}
					
// 				}
// 				}
// 			});
// 			return false;
			
// 	});

function batalTambahRencanaAnggaran(){
	$("#errTambahRencanaAnggaran").html("<label>&nbsp;</label>");
	$("div.tambahPerencanaan").slideUp("700");
	$("#right>div.perencanaanAnggaran").slideDown("700");
	$("#right>div.perencanaanAnggaran div").slideDown("700");
}

function closeEditRencanaAnggaran(){
	$('#mask').remove();  
    $('.editRencanaAnggaranJenisAkun').fadeOut(200); 
    $('.editRencanaAnggaranJenisAkun').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });
}

function loadRencanaAnggaran(){
	var tahunRencanaAnggaranDipilih = $("#thnRenAng").val();
	$.ajax({
		type: "POST",
		url: "loadRencanaAnggaran.php",
		data:{"tahunRencanaAnggaranDipilih": tahunRencanaAnggaranDipilih},
		success: function(data){
			if(data){
				$("#errTambahRencanaAnggaran").html("<label>&nbsp;</label>");
				$("#rencanaAnggaran tbody").html(data);
				$("#printrencanaAnggaran tbody").html(data);
				$("#editKeteranganLaporanHeader").html($("#thnRenAng").val());
			}
			
		}
	});
}

function loadKOdeJenisAkunRencanaAnggaran(){
	$.ajax({
			type: "POST",
			url: "loadJenisAkunAnggaran.php",
			success: function(data){
				if(data){
					$('#pilihanRencanaKodeAkunRekening').html(data);			
				}
			}
	});
}

function pilihKodeJenisAkun(){

	document.getElementById("editRencanaAnggaranBut").disabled = false;
	document.getElementById("hapusRencanaAnggaranBut").disabled = false;


	$("#rencanaAnggaran tbody tr").click(function(){
			
			   $(this).addClass('selected').siblings().removeClass('selected');			       
	});

}

function editRencanaAnggaran(div){
	if($('#rencanaAnggaran tbody tr.selected td:eq(1)').html()){
	var jumlahAnggaran = ($('#rencanaAnggaran tbody tr.selected td:eq(1)').html()).replace(/&nbsp;/gi,'');
	var editRencanaAnggaranAkun = $('#rencanaAnggaran tbody tr.selected').closest('tr').attr('id');
	//rubah 7
	var tahunAnggaranEdit = $('#rencanaAnggaran tbody tr.selected').closest('tr').attr('tahun');
	//rubah 1
	var editTahunAnggaranAkun = $("#editKeteranganLaporanHeader").text();
	$('#editKodeJenisAkunPopUp').html(editRencanaAnggaranAkun);
	$('#editTahunAkunPopUp').html(tahunAnggaranEdit);
	document.getElementById("editAnggaranKodeJenisAkunPopUp").value = jumlahAnggaran.substring(4);;
	var loginBox = div;
	var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    //  Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
    $(".editRencanaAnggaranJenisAkun").fadeIn(500);
    $(".editRencanaAnggaranJenisAkun").css({ 
        'visibility' :'visible',
        'display' :'block',
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    }else{
		alert('pilih dahulu rencana Anggaran!');
	}
}

function editKodeJenisAkunPopUp(){
	//rubah 2
	var editRencanaAnggaranAkun2 = $('#editKodeJenisAkunPopUp').text();
	
	var jumlahAnggaran2 = $('#editAnggaranKodeJenisAkunPopUp').val().replace(/\./g, "");

	//rubah 3
	var tahunEditRencanaAnggaran = $("#editTahunAkunPopUp").text();
	$.ajax({
			type: "POST",
			url: "editRencanaAnggaranAkunTerpilih.php",
			data: { "editRencanaAnggaranAkun2": editRencanaAnggaranAkun2,
					"jumlahAnggaran2": jumlahAnggaran2,
					"tahunEditRencanaAnggaran": tahunEditRencanaAnggaran },
			success: function(data){
				if(data == 1){
					$('#thnRenAng').val(tahunEditRencanaAnggaran);
					alert('data berhasil diubah!');
					loadRencanaAnggaran();
					closeEditRencanaAnggaran();			
				}else{
					alert('data tidak diubah!');
				}
			}
	});
}

function pilihBukanKodeJenisAkun(){
	document.getElementById("editRencanaAnggaranBut").disabled = true;
	document.getElementById("hapusRencanaAnggaranBut").disabled = true;
	$("#rencanaAnggaran tbody tr").click(function(){
			
			   $(this).addClass('selected').siblings().removeClass('selected');			       
	});
}

function hapusRencanaAnggaranAkun(){
	if($('#rencanaAnggaran tbody tr.selected td:eq(1)').html()){

	var hapusRencanaAnggaranAkun = $('#rencanaAnggaran tbody tr.selected').closest('tr').attr('id');
	
	//rubah 6
	var tahunAnggaranHapus = $('#rencanaAnggaran tbody tr.selected').closest('tr').attr('tahun');
	//rubah 4
	var tahunHapusRencanaAnggaran = tahunAnggaranHapus;

	//var tahunHapusRencanaAnggaran = $("#editKeteranganLaporanHeader").text();
	$.ajax({
			type: "POST",
			url: "hapusRencanaAnggaranAkunTerpilih.php",
			data: {"hapusRencanaAnggaranAkun": hapusRencanaAnggaranAkun,
					"tahunHapusRencanaAnggaran": tahunHapusRencanaAnggaran
				   },
			success: function(data){
				if(data == 1){
					$('#thnRenAng').val(tahunAnggaranHapus);
					alert('data berhasil dihapus!');
					loadRencanaAnggaran();		
				}else{
					alert('data tidak dihapus!');
				} 
			}
	});
	}else{
		alert('pilih dahulu rencana Anggaran!');
	}
}

function tambahRencanaAnggaranAkunBut(){
	loadKOdeJenisAkunRencanaAnggaran();
	bersihkanDataREncanaTambah();
	editTanggalLaporanRencana();
	$("#right>div.perencanaanAnggaran").slideUp("700");
	$("#right>div.perencanaanAnggaran div").slideUp("700");
	$("div.tambahPerencanaan").slideDown("700");
	

}

function bersihkanDataREncanaTambah(){
	document.getElementById("rencanaDanaJenisAkunRekening").value = "";
	document.getElementById("tahunJenisAkunRekening").value = "";
}

function editTanggalLaporanRencana(){
	$.ajax({
			type: "POST",
			url: "loadTanggalTahun.php",
			dataType: "json",
			success: function(data){
				if(data){
					//tahun pembukuan
					document.getElementById('thnRenAng').value = data[3];

				if (typeof $('form#formTampilBukuBesar') != 'undefined'){
					document.getElementById('thnPembukuanBukBes').value = data[3];
					document.getElementById('tglBukBes').value = data[0];
					document.getElementById('blnBukBes').value = data[1];
					document.getElementById('thnBukBes').value = data[2];
				}

					//daftar rekening saldo
				if (typeof document.getElementById('formTampilSaldoRekening') != 'undefined'){
					document.getElementById('thnPemRekSaldo').value = data[3];
					document.getElementById('tglSalRek').value = data[0];
					document.getElementById('blnSalRek').value = data[1];
					document.getElementById('thnSalRek').value = data[2];
				}

					// //Mutasi
				if (typeof document.getElementById('formTampilMutasiPerRek') != 'undefined'){
					document.getElementById('thnPembukuanMutasiPerRek').value = data[3];
					document.getElementById('tglMulaiMutasi').value = 1;
					document.getElementById('blnMulaiMutasi').value = 1;
					document.getElementById('thnMulaiMutasi').value = data[3];
					document.getElementById('tglSampaiMutasi').value = data[0];
					document.getElementById('blnSampaiMutasi').value = data[1];
					document.getElementById('thnSampaiMutasi').value = data[2];
				}

					// //lapKelRek
				if (typeof document.getElementById('formTampilLapPerRek') != 'undefined'){
					document.getElementById('thnPembukuanLapPerRek').value = data[3];
					document.getElementById('tglKelAK').value = data[0];
					document.getElementById('blnKelAK').value = data[1];
					document.getElementById('thnKelAK').value = data[2];
				}

					//laporan induvidu
				if (typeof document.getElementById('formTampilPenInd') != 'undefined'){
					document.getElementById('tglMulaiPenInd').value = 1;
					document.getElementById('blnMulaiPenInd').value = 1;
					document.getElementById('thnMulaiPenInd').value = data[3];
					document.getElementById('tglPenInd').value = data[0];
					document.getElementById('blnPenInd').value = data[1];
					document.getElementById('thnPenInd').value = data[2];
				}

					//persembahan jemaat
				if (typeof document.getElementById('formTampilPerJem') != 'undefined'){
					document.getElementById('tglMulaiPerJem').value = 1;
					document.getElementById('blnMulaiPerJem').value = 1;
					document.getElementById('thnMulaiPerJem').value = data[3];
					document.getElementById('tglPerJem').value = data[0];
					document.getElementById('blnPerJem').value = data[1];
					document.getElementById('thnPerJem').value = data[2];
				}

				if (typeof document.getElementById('tambahRencanaAnggaran') != 'undefined'){
					document.getElementById('tahunJenisAkunRekening').value = data[3];
				}

					
					
					

					//bukubesar
				

					
				}
			}
	});
}

var tableToExcelLap = (function() {
	
	  var uri = 'data:application/vnd.ms-excel;base64,'
	    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
	    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
	    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
	  return function(table, name, ket1, ket2, ket3) {

	  	var colspanKop = document.getElementById(table).rows[0].cells.length;
	  	if(table == 'LaporanPerRekening'){
	  		colspanKop +=2;
	  	}else if(table == 'mutasi_per_rek'){
	  		colspanKop +=1;
	  	}
		$.ajax({
				type: "POST",
				url: "loadKopTanpaLogo.php",
				data: {"colspanKop" : colspanKop},
				success: function(data){
					if(data){
		
						tamp1 = $('#'+ket1).text();
					  	tamp2 = $('#'+ket2).text();
					  	tamp3 = "";
					  	if(ket3 != ''){
					  		tamp3 = $('#'+ket3).text();	
					  	}

					  	var StringAdd;
					  	if(ket3==''){
					  		StringAdd = "<table border=0><tr><td colspan=3>"+tamp1+" "+tamp2+"</td></tr></table>";
					  	}else{
					  		StringAdd = "<table border=0><tr><td colspan=3>"+tamp1+"</td></tr><tr><td colspan=3>"+tamp2+" "+tamp3+"</td></tr></table>";
					  	}
					  	
					    if (!table.nodeType) table = document.getElementById(table)
					    var ctx = {worksheet: name || 'Worksheet', table: data +StringAdd+"<table border=1>"+table.innerHTML+ "</table>"}
					    window.location.href = uri + base64(format(template, ctx))

					}
				}
		});
	  	
	  }
})()

var tableToExcelBukuBesar = (function() {
	  var uri = 'data:application/vnd.ms-excel;base64,'
	    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
	    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
	    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
	  return function(table1, table2, name, ket1, ket2, ket3) {

	  	var tamp;
		$.ajax({
				type: "POST",
				url: "loadKopTanpaLogo.php",
				data: {"colspanKop" : 3},
				success: function(data){
					if(data){
						tamp1 = $('#'+ket1).text();
					  	tamp2 = $('#'+ket2).text();
					  	tamp3 = "";
					  	if(ket3 != ''){
					  		tamp3 = $('#'+ket3).text();	
					  	}

					  	var StringAdd;
					  	if(ket3==''){
					  		StringAdd = "<table border=0><tr><td colspan=3>"+tamp1+" "+tamp2+"</td></tr></table>";
					  	}else{
					  		StringAdd = "<table border=0><tr><td colspan=3>"+tamp1+"</td></tr><tr><td colspan=3>"+tamp2+" "+tamp3+"</td></tr></table>";
					  	}
					  	
					    if (!table1.nodeType) table1 = document.getElementById(table1)
					    if (!table2.nodeType) table2 = document.getElementById(table2)
					    var ctx = {worksheet: name || 'Worksheet', table: data +StringAdd+"<table border=1>"+table1.innerHTML+ "</table><table><tr><td><br></td></tr></table>"+"<table border=1>"+table2.innerHTML+ "</table>"}
					    window.location.href = uri + base64(format(template, ctx))


					}
				}
		});
	  }
})()



function exportRencanaAnggaran(table,ket1,ket2){
	 var valueExRenAng = $('#selectExportDataRencanaAnggaran option:selected').val();
	 if(valueExRenAng == "XLS"){
	 	tableToExcelLap('rencanaAnggaran', 'Table',ket1,ket2,'');
	 	// fnExcelReport();
	 }else if(valueExRenAng == "PDF"){
	 	// topdf('#rencanaAnggaran');
	 	convertToPdfData('#rencanaAnggaran',ket1,ket2,'')
	 }else if(valueExRenAng == "CSV"){
	 	$('#rencanaAnggaran').tableExport({type:'csv',escape:'false'});
	 }

	 $('#selectExportDataRencanaAnggaran').val('');
}

function fnExcelReport()
{	
	var tamp;
	$.ajax({
			type: "POST",
			url: "loadKop.php",
			success: function(data){
				if(data){
				  	var tab_text=data+"<table border='2px'><tr>";
				    var textRange; var j=0;
				    tab = document.getElementById('rencanaAnggaran'); // id of table

				    for(j = 0 ; j < tab.rows.length ; j++) 
				    {     
				        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
				        //tab_text=tab_text+"</tr>";
				    }

				    tab_text=tab_text+"</table>";
				    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
				    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
				    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

				    var ua = window.navigator.userAgent;
				    var msie = ua.indexOf("MSIE "); 

				    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
				    {
				        txtArea1.document.open("txt/html","replace");
				        txtArea1.document.write(tab_text);
				        txtArea1.document.close();
				        txtArea1.focus(); 
				        sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
				    }  
				    else                 //other browser not tested on IE 11
				        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

				    return (sa);
				}
			}
	});
    
}

$("#editAnggaranKodeJenisAkunPopUp").on('input', function(){
	var jumlah = $(this).val().replace(/\./g, "");
	var tampCeil = Math.ceil((jumlah.replace(/\./g, "")).length / 3);
	var tampFloor = Math.floor((jumlah.replace(/\./g, "")).length / 3);
	var tampLenght = (jumlah.replace(/\./g, "")).length;
	var hasil = "";
	var data = [];

	for(var i = 0; i < tampCeil; i++){
		if(tampFloor - i >= 0){
			var index = ((tampLenght%3) - 1)+((tampFloor - i)*3 );
			data[i] = jumlah.substring(index - 2, index+1);
		}else{
			data[i] = jumlah.substring(0, (tampLenght%3));
		}
		
		
	}

	for(var i = data.length -1 ; i >= 0; i--){
		hasil += data[i];
		if (i > 0){
			hasil+=".";
		}
	}
	$(this).val(hasil);

});

$("#editAnggaranKodeJenisAkunPopUp").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if (e.shiftKey || e.keyCode == 0){
        	e.preventDefault();
        }else if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)){
                 // let it happen, don't do anything
                 return;
        }else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) {
        	return;
        }else{
        	e.preventDefault();
        }
       		
       	
    });
