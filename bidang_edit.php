
<div id="divTitle">
	<label id="lblTitleEditBidang">Edit Bidang</label>
</div>
<form id="formEditBidang" name="formEditBidang" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td>Kode Bidang</td><td> : </td>
			<td><input type="text" id="editKodeBidang" name="editKodeBidang" readonly spellcheck="false" placeholder="Kode Bidang" disabled style="width: 246px;" required style="this.style.color='#f00'"/></td>
		</tr>
		<tr>
			<td>Nama Bidang</td><td> : </td>
			<td><input type="text" id="editNamaBidang" name="editNamaBidang" spellcheck="false" placeholder="Nama Bidang" style="width: 246px;" required /></td>
		</tr>
		<tr>
			<!-- <td class="kolomLabel">Jenis Akun</td><td> : </td>
			<td><input type="text" id="editJenisAkunBidang" name="editJenisAkunBidang" spellcheck="false" placeholder="Edit Kode Jenis Akun" style="width: 246px;" required/></td> -->
			<td class="kolomLabel">Jenis Akun</td><td> : </td>
				<td><select class='editJenisAkunBidang' name="editJenisAkunBidang" style ="width:250px;" value = ''>
				<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_jenis_akun,nama_kode_jenis_akun FROM  tbl_jenis_akun ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_jenis_akun"];
			                $str.="'>";
			                $str.=$baris["kode_jenis_akun"]." - ".$baris["nama_kode_jenis_akun"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>
		</tr>

		<tr>
			<td class="kolomLabel">Pengurus 1</td><td> : </td>
			<td><select class='editPengurus1Bidang' name="editPengurus1Bidang" style ="width:250px;" placeholder ="Pengurus 1" >
			 					<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>

		</tr>
		<tr>
			<td class="kolomLabel">Pengurus 2</td><td> : </td>
			<td><select class='editPengurus2Bidang' name="editPengurus2Bidang" style ="width:250px;" placeholder ="Pengurus 2" >
			 					<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>

		</tr>
		<tr>
			<td class="kolomLabel">Pengurus 3</td><td> : </td>
			<td><select class='editPengurus3Bidang' name="editPengurus3Bidang" style ="width:250px;" placeholder ="Pengurus 3" >
			 					<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>

		</tr>
		
		<tr>
			<td>Tanggal Dibentuk</td> <td> : </td>
			<td class="customdate"><input type="number" class="ex" id="bidangEdit_tanggal" name="bidangEdit_tanggal" min=1 max=31 placeholder="tanggal" required style="width: 72px;">
			<input type="number" class="ex" id="bidangEdit_bulan" name="bidangEdit_bulan" min=1 max=12 placeholder="bulan" required style="width: 72px;">
			<input type="number" class="ex" id="bidangEdit_tahun" name="bidangEdit_tahun" min=2000 max=9999 placeholder="tahun" required style="width: 72px;">
			</td>
		</tr>
		<tr style="vertical-align: top;">
			<td class="kolomLabel">Deskripsi</td><td> : </td>
			<td style="padding-bottom: 0px;"><textarea style="width: 244px; height: 70px;" name ="editDeskripsiBidang" id ="editDeskripsiBidang" placeholder="Edit Deskripsi"></textarea></td>
		</tr>

		<tr>
			<td class="kolomLabel">Status</td><td> : </td>
			<td><select id='editStatusBidang' name="editStatusBidang" style ="width:250px;">
			 	<option value='' disabled selected style="display:none;">Status</option>
				<option value='1'>Aktif</option>
			 	<option value='0'>Tidak Aktif</option>
				</select></td>
		</tr>
		
	</table>
	<input type="submit" value="Simpan" id="btnEditBidang" name="editBidang"  style="width: 120px; height: 25px;">
	<button id="btnBatalEditBidang" type ="button" name="btnBatalEditBidang"  style="width: 120px; height: 25px;">Batal</button>

	<input type="text" id="editKodeBidang2" name="editKodeBidang2" style="visibility:hidden";>
</form>
	<button id="btnKembaliBidang" name="btnKembaliBidang" style="visibility:hidden";  style="width: 120px; height: 25px;">Kembali</button>



<script type="text/javascript">
	$("#bidangEdit_tanggal").focusout(function(){
		var objectd = $("#bidangEdit_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#bidangEdit_bulan").focusout(function(){
		var m = $("#bidangEdit_bulan").val();
		if(m > 12 || m < 1) {
			$("#bidangEdit_bulan").val("");
			m = null;
		}
		if(m == null) $("#bidangEdit_tanggal").attr("max", 31);
		else if(m == 2) $("#bidangEdit_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#bidangEdit_tanggal").attr("max", 31);
			else $("#bidangEdit_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#bidangEdit_tanggal").attr("max", 31);
			else $("#bidangEdit_tanggal").attr("max", 30);
		}
		$("#bidangEdit_tahun").triggerHandler("focusout");
		$("#bidangEdit_tanggal").triggerHandler("focusout");
	});

	$("#bidangEdit_tahun").focusout(function(){
		var m = $("#bidangEdit_bulan").val(); 
		var y = $("#bidangEdit_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#bidangEdit_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#bidangEdit_tanggal").attr("max", 29);
				} else {
					$("#bidangEdit_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#bidangEdit_tanggal").attr("max", 29);
				} else {
					$("#bidangEdit_tanggal").attr("max", 28);
				}
			}
			$("#bidangEdit_tanggal").triggerHandler("focusout");
		}
	});

</script>