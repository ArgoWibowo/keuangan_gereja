<div id="divTitle">
	<label id="lblTitle">Tambah Jemaat</label>
</div>
<form id="formTambahJemaat" name="formTambahJemaat" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabel">Kode Jemaat</td>
			<td> : <input type="text" id="kodeJemaat" maxlength="8" name="kodeJemaat" spellcheck="false" placeholder="Masukan Kode Jemaat" style="width: 246px;" onKeyUp="checkKodeJemaat()" required/></td>
			<td class="warning"><img id="warningUsernameJemaat" src="image/warning.png" alt="warning" style ="width: 15px;height: 15px;"></td>
		</tr>
		<tr>
			<td class="kolomLabel">Nama Jemaat</td>
			<td> : <input type="text" id="namaJemaat" name="namaJemaat" spellcheck="false" placeholder="Masukan Nama Jemaat" style="width: 246px;" required/></td>
		</tr>
		<tr>
			<td class="kolomLabel">Wilayah</td>
			<td> : <input type="text" id="wilayahJemaat" name="wilayahJemaat" spellcheck="false" placeholder="Masukan Wilayah Jemaat" style="width: 246px;" required/></td>
		</tr>
		
	</table>
	<input type="submit" value="SIMPAN"  id="btnTambahJemaat" name="tambahJemaat" class="button"  style="width: 120px; height: 25px;">
</form>

	