<?php  
	require_once("koneksi.php");
?>	
	<div id="navJumlahDataJemaat" class="navJumlahData">
		<label>Jumlah Data : </label> <label id="lblJumlahDataJemaat" class="lblJumlahDataJemaat" ></label>
	</div>
	<div id="navSearchJemaat" class="navSearch">
		<ul class="nav">
			<li class='search'>
				<div class='divSearch'>
					<input name ="kataKunciJemaat" onchange="searchJemaat()" onkeyup="searchJemaat()"  id='searchJemaat' class='searchJemaat' type='text' placeholder="Masukkan kata kunci">
				</div>
			</li>
		</ul>
	</div>
	<div id="navSortJemaat" class="navSort" >
		<label>Urutkan berdasarkan : </label>
		<select id='sortDataJemaat' class="sortData" onchange=searchJemaat()>
		 	<!-- <option value='no_jemaat'>No</option>
 -->		 	<option value='nama_jemaat'>Nama</option>
		 	<option value='wilayah'>Wilayah</option>
		 	<option value='kode_jemaat'>Kode Jemaat</option>		 			 
		</select>
	</div>
	<div id="datajemaat" class="dataTable">
		<table class="tabelData striped" style="padding :20px 20px; align:"center";">
			<thead>
				<tr >
					<th id="notabeljemaat" style="text-align:left;width:35px;">No</th>
					<th class="namatabeljemaat"  style="text-align:left;">Nama Jemaat</th>
					<th class="wilayahtabeljemaat" style="text-align:left;">Wilayah</th>
					<th class="kodetabeljemaat"  style="text-align:left;">Kode Jemaat</th>					
					<?php
						$admin = $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'TULIS';
		 
						if ($admin){
					?>
		
					<th class="pilihantabeljemaat"  style="text-align:left;">Pilihan</th>
					<?php } ?>
				</tr>	
			</thead>
			<tbody id ="isiTabelJemaat" class="isiTabel">
			<tbody>
		</table>
	</div>
	<div id="paginationJemaat" class="pagination" cellspacing="0">
	</div>

