<div id="divTitle">
	<label id="lblTitle">Tambah Komisi</label>
</div>
<form id="formTambahKomisi" name="formTambahKomisi" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabel">Kode Komisi</td><td> : </td>
			<td><input type="text" id="kodeKomisi" maxlength=8 name="kodeKomisi" spellcheck="false" placeholder="Masukan Kode Komisi" style="width: 246px;" onKeyUp="checkKodeKomisi()" required/></td>
			<td class="warning"><img id="warningUsernameKomisi" src="image/warning.png" alt="warning" style ="width: 15px;height: 15px;"></td>
		</tr>
		<tr>
			<td class="kolomLabel">Nama Komisi</td><td> : </td>
			<td><input type="text" id="namaKomisi" name="namaKomisi" spellcheck="false" placeholder="Masukan Nama Komisi" style="width: 246px;" required/></td>
		</tr>
		<tr>
			<td class="kolomLabel">Kode Bidang</td><td> : </td>
				<td><select class='bidangKomisi' name="bidangKomisi" style ="width:250px;">
					<option value='' disabled selected style="display:none;">Bidang</option>
					
			 		<?php 
						include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_bidang,nama_bidang FROM  tbl_bidang ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_bidang"];
			                $str.="'>";
			                $str.=$baris["kode_bidang"]." - ".$baris["nama_bidang"];
			                $str.="</option>";

			                echo $str;
			                $str = "";
			            }	

				 ?>
			</select> </td>
		</tr>
		<tr>
			<td class="kolomLabel">Jenis Akun</td><td> : </td>
				<td><select class='jenisakunKomisi' name="jenisakunKomisi" style ="width:250px;">
					<option value='' disabled selected style="display:none;">Rekening</option>
			
			 		<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_jenis_akun,nama_kode_jenis_akun FROM  tbl_jenis_akun ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_jenis_akun"];
			                $str.="'>";
			                $str.=$baris["kode_jenis_akun"]." - ".$baris["nama_kode_jenis_akun"];
			                $str.="</option>";

			                echo $str;
			                $str = "";
			            }	

				 ?>
			</select> </td>
		</tr>

		<tr>
			<td class="kolomLabel">Pengurus 1</td><td> : </td>
				<td><select class='pengurus1Komisi' name="pengurus1Komisi" style ="width:250px;" placeholder ="Pengurus 1" >
			 	<option value='' disabled selected style="display:none;" >Pengurus 1</option>
				<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>

		</tr>
		<tr>
			<td class="kolomLabel">Pengurus 2</td><td> : </td>
				<td><select class='pengurus2Komisi' name="pengurus2Komisi" style ="width:250px;" placeholder ="Pengurus 2">
			 	<option value='' disabled selected style="display:none;">Pengurus 2</option>
				<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>

		</tr>
			<tr>
			<td class="kolomLabel">Pengurus 3</td><td> : </td>
				<td><select class='pengurus3Komisi' name="pengurus3Komisi" style ="width:250px;" placeholder ="Pengurus 3">
			 	<option value='' disabled selected style="display:none;" >Pengurus 3</option>
				<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>

		</tr>
		
		
		<tr>
			<td>Tanggal Dibentuk</td> <td> : </td>
			<td class="customdate"><input type="number" class="ex" id="komisi_tanggal" name="komisi_tanggal" min=1 max=31 placeholder="tanggal" required style="width: 72px;">
			<input type="number" class="ex" id="komisi_bulan" name="komisi_bulan" min=1 max=12 placeholder="bulan" required style="width: 72px;">
			<input type="number" class="ex" id="komisi_tahun" name="komisi_tahun" min=2000 max=9999 placeholder="tahun" required style="width: 72px;">
			</td>
		</tr>
		<tr style="vertical-align: top;">
			<td class="kolomLabel">Deskripsi</td><td> : </td>
			<td style="padding-bottom: 0px;"><textarea style="width: 244px; height: 70px;" name ="deskripsiKomisi" id ="deskripsiKomisi" placeholder="Masukan Deskripsi"></textarea></td>
		</tr>

			</table>
	<input type="submit" value="SIMPAN"  id="btnTambahKomisi" name="tambahKomisi" class="button"  style="width: 120px; height: 25px;">
</form>

	<script type="text/javascript">
	$("#komisi_tanggal").focusout(function(){
		var objectd = $("#komisi_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#komisi_bulan").focusout(function(){
		var m = $("#komisi_bulan").val();
		if(m > 12 || m < 1) {
			$("#komisi_bulan").val("");
			m = null;
		}
		if(m == null) $("#komisi_tanggal").attr("max", 31);
		else if(m == 2) $("#komisi_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#komisi_tanggal").attr("max", 31);
			else $("#komisi_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#komisi_tanggal").attr("max", 31);
			else $("#komisi_tanggal").attr("max", 30);
		}
		$("#komisi_tahun").triggerHandler("focusout");
		$("#komisi_tanggal").triggerHandler("focusout");
	});

	$("#komisi_tahun").focusout(function(){
		var m = $("#komisi_bulan").val(); 
		var y = $("#komisi_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#komisi_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#komisi_tanggal").attr("max", 29);
				} else {
					$("#komisi_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#komisi_tanggal").attr("max", 29);
				} else {
					$("#komisi_tanggal").attr("max", 28);
				}
			}
			$("#komisi_tanggal").triggerHandler("focusout");
		}
	});


</script>	