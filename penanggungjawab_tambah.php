<div id="divTitle">
	<label class="lblTitle">TAMBAH PENANGGUNG JAWAB</label>
</div>
<form id="formTambahPenanggungJawab" name="formTambahPenanggungJawab" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabel">Kode</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="kodePenanggungJawab" maxlength=8 name="kodePenanggungJawab" spellcheck="false" placeholder="Masukan Kode Penanggung Jawab" style="width: 246px;" onKeyUp="checkKodePenanggungJawab()" onFocusOut="checkKodePenanggungJawab()" required/></td>
			<td class="warning"><img id="warningKodePenanggungJawab" src="image/warning.png" alt="warning"></td>
		</tr>
		<tr>
			<td class="kolomLabel">Nama</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="namaPenanggungJawab" name="namaPenanggungJawab" spellcheck="false" placeholder="Masukan Nama Penanggung Jawab" style="width: 246px;" required/></td>
		</tr>
		<tr>
			<td class="kolomLabel">Jabatan</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="jabatanPenanggungJawab" name="jabatanPenanggungJawab" spellcheck="false" placeholder="Masukan Jabatan Penanggung Jawab" style="width: 246px;" required/></td>
		</tr>
		<tr style="vertical-align: top;">
			<td class="kolomLabel">Alamat</td>
			<td class="kolomTitikDua"> : </td>
			<td style="padding-bottom: 0px; vertical-align: text-top; text-align: top;"><textarea name ="alamatPenanggungJawab" id ="alamatPenanggungJawab" >Masukan Alamat Penanggung Jawab</textarea></td>
		</tr>
		<tr>
			<td class="kolomLabel">No Telepon</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="number" id="noTeleponPenanggungJawab" name="noTeleponPenanggungJawab" spellcheck="false" placeholder="Masukan No Telepon Penanggung Jawab" style="width: 246px;" required/></td>
		</tr>
	</table>
	<input type="submit" value="Tambah"  id="btnTambahPenanggungJawab" name="tambahPenanggungJawab" class="button" >
</form>

<div id="divTitle"><label class="lblTitle">IMPORT CSV</label></div>
<form name="formTambahPenanggungJawabImport" id="formTambahPenanggungJawabImport" method="post" enctype="multipart/form-data">
    	<input type="file" name="filePenanggungJawabImport" id="filePenanggungJawabImport" />
        <input type="submit" name="tambahPenanggungJawabImport" id="btnTambahPenanggungJawabImport" value="Import" />
        <input type="text" id="jenisfileImportPenanggungJawab" value="penanggungjawab" name="jenisfileImportPenanggungJawab" style="visibility:hidden";>
</form>



