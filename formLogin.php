<?php
	session_start();
	if(isset($_SESSION['u53r_4dm1n_K3uan94n_G3r3j4']) ){
	header("location: index.php");
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>	</title>
	<link rel="stylesheet" href="css/loginStyle.css" type="text/css">
	<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="js/login.js"></script>
</head>
<body>
		<div id="wrapper">	

				<div class="header">
					<h1>SISTEM KEUANGAN GEREJA</h1>
				</div>

				<div class="form">
						<div class="login">
							<form method="post">
								<input type="text" name="username" id="username" pLaceholder="Username" required></input>
								<input type="password" name="password" id="password" pLaceholder="Password" required></input>
								<input type="submit" id="login-button" name="Login" value="Login">
								<a href="#pemulihanPass1" onclick="pemulihanPasswordAwal('#pemulihanPass1')">Lupa Password?</a>
								<div id="err"></div>
							</form>
						</div>
						<div id="pemulihanPassword1">
								<?php include "php/form/editPassPemulihan1.php"; ?>
						</div>
						<div id="pemulihanPassword2">
								<?php include "php/form/editPassPemulihan2.php"; ?>
						</div>
			
				</div>
				<div class="footer">
				</div>
		</div>

</body>
</html>