<div id="divTitle">
	<label id="lblTitle">Edit Nota</label>
</div>
<form id="formEditNota" name="formEditNota" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td>Kode Nota</td><td> : </td>
			<td><input type="text" id="editKodeNota" name="editKodeNota" spellcheck="false" placeholder="Kode Nota" disabled style="width: 246px;" required style="this.style.color='#f00'"/></td>
		</tr>
		<tr>
			<td>Kode Toko</td><td> : </td>
<!-- 			<td><input type="text" id="editKodeTokoNota" name="editKodeTokoNota" spellcheck="false" placeholder="Kode Toko" style="width: 246px;" required /></td><td> : </td>
 -->			<td><select id ='editKodeTokoNota' class='editKodeTokoNota' name="editKodeTokoNota" style ="width:250px;" value = '' disabled>
				<?php 
					include "koneksi.php";

			           $cek_kode = mysql_query("SELECT kode_toko,nama_toko FROM  tbl_toko ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_toko"];
			                $str.="'>";
			                $str.=$baris["kode_toko"]." - ".$baris["nama_toko"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>		
		</tr>
		<tr>
			<td class="kolomLabel">Debet</td><td> : </td>
			<!-- <td><input type="text" id="editDebetNota" name="editDebetNota" spellcheck="false" placeholder="Edit Rekening" style="width: 246px;" required/></td> -->
			<td><select id ='editDebetNota' class='editDebetNota' name="editDebetNota" style ="width:250px;" value = ''>
				<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_jenis_akun,nama_kode_jenis_akun FROM  tbl_jenis_akun ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_jenis_akun"];
			                $str.="'>";
			                $str.=$baris["kode_jenis_akun"]." - ".$baris["nama_kode_jenis_akun"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>
		</tr>

		<tr>
			<td>Tanggal Pengeluaran</td> <td> : </td>
			<td class="customdate"><input type="number" class="ex" id="notaEdit_tanggal" name="notaEdit_tanggal" min=1 max=31 placeholder="tanggal" required style="width: 72px;">
			<input type="number" class="ex" id="notaEdit_bulan" name="notaEdit_bulan" min=1 max=12 placeholder="bulan" required style="width: 72px;">
			<input type="number" class="ex" id="notaEdit_tahun" name="notaEdit_tahun" min=2000 max=9999 placeholder="tahun" required style="width: 72px;">
			</td>
		</tr>
		
		<tr>
			<td class="kolomLabel">Jumlah</td><td> : </td>
			<td><input type="text" id="editJumlahNota" name="editJumlahNota" spellcheck="false" style="width: 246px;" /></td>
		</tr>

		<tr>
			<td class="kolomLabel">Pelaksana</td><td> : </td>
			<td><input type="text" id="editPelaksanaNota" name="editPelaksanaNota" spellcheck="false" placeholder="Edit Nama Pelaksana " style="width: 246px;" /></td>
		</tr>
		
	</table>
	<input type="submit" value="Simpan" id="btnEditNota" name="editNota"  style="width: 120px; height: 25px;">
	<button type="button" id="btnBatalEditNota" name="btnBatalEditNota"  style="width: 120px; height: 25px;">Batal</button>
	<input type="text" id="editKodeNota2" name="editKodeNota2" style="visibility:hidden";>
	<input type="text" id="editKodeTokoNota2" name="editKodeTokoNota2" style="visibility:hidden";>

</form>


<script type="text/javascript">
	$("#notaEdit_tanggal").focusout(function(){
		var objectd = $("#notaEdit_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#notaEdit_bulan").focusout(function(){
		var m = $("#notaEdit_bulan").val();
		if(m > 12 || m < 1) {
			$("#notaEdit_bulan").val("");
			m = null;
		}
		if(m == null) $("#notaEdit_tanggal").attr("max", 31);
		else if(m == 2) $("#notaEdit_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#notaEdit_tanggal").attr("max", 31);
			else $("#notaEdit_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#notaEdit_tanggal").attr("max", 31);
			else $("#notaEdit_tanggal").attr("max", 30);
		}
		$("#notaEdit_tahun").triggerHandler("focusout");
		$("#notaEdit_tanggal").triggerHandler("focusout");
	});

	$("#notaEdit_tahun").focusout(function(){
		var m = $("#notaEdit_bulan").val(); 
		var y = $("#notaEdit_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#notaEdit_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#notaEdit_tanggal").attr("max", 29);
				} else {
					$("#notaEdit_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#notaEdit_tanggal").attr("max", 29);
				} else {
					$("#notaEdit_tanggal").attr("max", 28);
				}
			}
			$("#notaEdit_tanggal").triggerHandler("focusout");
		}
	});

</script>