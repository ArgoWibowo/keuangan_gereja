<?php
	session_start();
	include 'koneksi2.php';
	include 'koneksi.php';
	include_once 'refreshSelectFunction.php';
	include_once 'refreshTableFunction.php';
	//$_SESSION['tahun_pembukuan'] = 2015;
	if($_SESSION['u53r_4dm1n_K3uan94n_G3r3j4']==""){
		header("location: formLogin.php");
	}
	
 ?>

<!DOCTYPE HTML>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/header.css">
	<link rel="stylesheet" type="text/css" href="css/datatoko_rekening_penanggungjawab.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<link rel="stylesheet" type="text/css" href="css/rencanaAnggaran.css">
	<link rel="stylesheet" type="text/css" href="css/laporan.css">
	<link rel="stylesheet" type="text/css" href="css/homepage.css">
	<link rel="stylesheet" type="text/css" href="css/admin_profil.css">
	<script type="text/javascript" src="jspdf/dist/jspdf.debug.js"></script>
	<script type="text/javascript" src="jspdf/dist/jspdf.min.js"></script>
	<script type="text/javascript" src="autotable/dist/jspdf.plugin.autotable.js"></script>
	<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="js/homepage.js"></script>
	<script type="text/javascript" src="js/datatoko.js"></script>	
	<script type="text/javascript" src="js/rekening.js"></script>
	<script type="text/javascript" src="js/penanggungjawab.js"></script>
	<script type="text/javascript" src="js/admin.js"></script>
	<script type="text/javascript" src="js/profilGereja.js"></script>
	<script type="text/javascript" src="js/laporan.js"></script>
	<script type="text/javascript" src="js/rencanaAnggaran.js"></script>
	<script type="text/javascript" src="js/index.js"></script>
	
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->

	<!-- Wawan tambah -->
	<link rel="stylesheet" type="text/css" href="css/bidang.css">
	<script type="text/javascript" src="js/databidang.js"></script>	
	<link rel="stylesheet" type="text/css" href="css/komisi.css">
	<script type="text/javascript" src="js/datakomisi.js"></script>	
	<link rel="stylesheet" type="text/css" href="css/nota.css">
	<script type="text/javascript" src="js/datanota.js"></script>	
	<script type="text/javascript" src="js/datajemaat.js"></script>		

</head>
<body style="overflow-y: scroll;">
	<?php
		$lapPemasukanOt = $_SESSION['0t0rit4s_P3ma5uk4n_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_P3ma5uk4n_K3uan94n_G3r3j4'] == 'BACA';
		$lapPengeluaranOt = $_SESSION['0t0rit4s_P3n93lu4ran_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_P3n93lu4ran_K3uan94n_G3r3j4'] == 'BACA';
		$adminOt = $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'TULIS';
		$pemasukanOt = $_SESSION['0t0rit4s_P3ma5uk4n_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_P3ma5uk4n_K3uan94n_G3r3j4'] == 'TULIS';
		$pengeluaranOt = $_SESSION['0t0rit4s_P3n93lu4ran_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_P3n93lu4ran_K3uan94n_G3r3j4'] == 'TULIS';
	?>	
	<div id="wrapper">
		<?php include "header.php";?>
		<div id="content">
			<div id="left">
				<ul class="menu">
					<li class="menuadmin"  onclick="loadTableAdmin();">Data Admin</li>
					<?php
					if($adminOt){
					?>
					<div class="menuadmin">
						<li class="tambahAdmin" onclick="checkConfirmasiPassTambah();gantiKode();">Tambah Admin</li>
					</div>
					<?php
					}
					?>

					<li class="datarekening" onclick="reloadDataRekening();">Rekening</li>
					
					<div class="datarekening">
					<?php 
					if ($adminOt){
					?>
							<li class="tambahrekening" onclick="refreshChecking();">Tambah Rekening</li>
					<?php 
					} 
					?>
					</div>
					<li class="perencanaanAnggaran" onclick="editTanggalLaporanRencana();loadRencanaAnggaran();">Perencanaan Anggaran</li>
					<?php
					if( $adminOt && $pemasukanOt && $pengeluaranOt){
					?>
					<div class="perencanaanAnggaran">
						<li class="tambahPerencanaan" onclick="loadKOdeJenisAkunRencanaAnggaran(); bersihkanDataREncanaTambah();editTanggalLaporanRencana();">Tambah Perencanaan</li>
					</div>
					<?php
					}
					?>
					
					<!-- Tambah Wawan -->
					<li class="databidang" onclick="reloadBidang();">Data Bidang</li>
					<div class="databidang">
						<?php 
						if ($adminOt){
						?>
						<li class="tambahbidang" onclick="reloadBidang();">Tambah Bidang</li>
						<?php 
						}
						?>
					</div>
					<li class="datakomisi" onclick="reloadKomisi();">Data Komisi</li>
					<div class="datakomisi">
						<?php 
						if ($adminOt){
						?>
						<li class="tambahkomisi" onclick="reloadKomisi();">Tambah Komisi</li>
						<?php 
						}
						?>
					</div>
					<li class="datajemaat" onclick="reloadJemaat();">Data Jemaat</li>
					<div class="datajemaat">
						<?php 
						if ($adminOt){
						?>
						<li class="tambahjemaat">Tambah Jemaat</li>
						<?php 
						}
						?>
					</div>
					<!-- ----------------------------------------------------------------------- -->


					<li class="datatoko" onclick="reloadToko();">Data Toko</li>
					<div class="datatoko">
					<?php 
					if ($adminOt){
					?>	
						<li class="tambahtoko">Tambah Toko</li>
					<?php 
					} 
					?>
					</div>
					
					<li class="datapenanggungjawab" onclick="reloadPenanggungJawab();">Penanggung Jawab</li>
					
					<div class="datapenanggungjawab">
					<?php 
					if ($adminOt){
					?>
							<li class="tambahpenanggungjawab">Tambah Penanggung Jawab</li>
					<?php 
					} 
					?>
					</div>

					<!-- bagian yosia -->
					<?php if($pemasukanOt || $pengeluaranOt){ ?>
						<li class="transaksi">Transaksi</li>
						<div class="transaksi">
							<?php if($pemasukanOt){ ?>
								<li class="penerimaan">Penerimaan</li>
								<div class="penerimaan">
									<li class="datapenerimaanindividu">Data Penerimaan Individual</li>
									<li class="penerimaanindividu">Input Penerimaan Individual</li>
									<li class="buktipenerimaan">Input Bukti Penerimaan</li>
								</div>
							<?php }
							if($pengeluaranOt){ ?>
								<li class="pengeluaran">Pengeluaran</li>
								<div class="pengeluaran">
									<li class="datanota" onclick="reloadNota();">Data nota</li>
									<li class="inputnota">Input Nota</li>
									<li class="buktipengeluaran">Input Bukti Pengeluaran</li>
								</div>
							<?php }
							if($pemasukanOt && $pengeluaranOt){ ?>
								<li class="bonsementara">Bon Sementara</li>
								<li class="mutasirekening">Mutasi Rekening</li>
								<!-- <li class="datatransaksi">Data Transaksi</li> -->
								<li class="databuktitransaksi">Data Bukti Transaksi</li>
							<?php } ?>	
						</div>
					<?php } ?>
					<!-- akhir bagian yosia -->

					<?php
					if( $lapPemasukanOt || $lapPengeluaranOt){
					?>
					<li class="laporan" onclick="editTanggalLaporanRencana();">Laporan</li>
					<div class="laporan" onclick="loadDataSaldoRekening();loadBukuBesarAktiva();loadBukuBesarPasiva();loadPenerimaanInduvidu();">
						<li class="bukubesar" onclick="editTanggalLaporanRencana();loadBukuBesarAktiva();loadBukuBesarPasiva();">Buku Besar</li>
						<li class="rekeningSaldo" onclick="editTanggalLaporanRencana();loadDataSaldoRekening();">Saldo Rekening</li>
						<li class="posisiSaldo" onclick="editTanggalLaporanRencana();loadDataPosisiSaldoRekening();">Posisi Saldo Rekening (Neraca)</li>
						<li class="rekapRekeningSaldo" onclick="editTanggalLaporanRencana();loadDataRekapSaldoRekening();">Rekap Saldo Rekening</li>
						<li class="mutasiRekening" onclick="editTanggalLaporanRencana();loadDataMutasiPerRekening();">Mutasi per Rekening</li>
						<li class="mutasiKas" onclick="editTanggalLaporanRencana();loadDataMutasiKas();">Mutasi Kas</li>
						<li class="jurnalMutasi" onclick="editTanggalLaporanRencana();loadDataJurnalMutasi();">Jurnal Mutasi</li>
						<li class="mutasiPerAkun" onclick="editTanggalLaporanRencana();loadselectTampilJenisAkun();loadDataMutasiPerAkun();">Mutasi per Akun</li>
						<li class="laporanPemasukan" onclick="editTanggalLaporanRencana();loadDataLaporanPemasukan();">Laporan Pemasukan</li>
						<li class="laporanPengeluaran" onclick="editTanggalLaporanRencana();loadDataLaporanPengeluaran();">Laporan Pengeluaran</li>
						<li class="laporanPemasukanThn" onclick="editTanggalLaporanRencana();loadDataLaporanPemasukanTahunan();">Laporan Pemasukan Tahunan</li>
						<li class="pengeluaranPerTahun" onclick="editTanggalLaporanRencana();loadPengeluaranPerTahun();">Laporan Pengeluaran Tahunan</li>
						<li class="laporanPerKelAkun" onclick="editTanggalLaporanRencana();loadselectTampilLapKelAkun();loadLaporanPerKelAkun();">Laporan Per Kelompok Akun</li>
						<li class="databuktitransaksi2">Laporan Bukti Transaksi</li>
						<?php
						if( $lapPemasukanOt){
						?>
						<li class="laporanPenInd" onclick="editTanggalLaporanRencana();loadPenerimaanInduvidu();">Laporan Penerimaan Individu</li>
						<li class="laporanPerJem" onclick="editTanggalLaporanRencana();loadLaporanPersembahanJemaat();loadselectJenisAkunPersembahanJemaat();loadselectPersembahanJemaat();">Laporan Persembahan Jemaat</li>
						<?php 
						} 
						?>
					</div>
					<?php 
					} 
					?>

					<?php 
					if ($adminOt){
					?>
							<li class="tambahpenanggungjawab">Tutup Buku</li>
					<?php 
					} 
					?>

					<li class="logout">Log Out</li>
				</ul>
			</div>
			<div id="right">
				<div class="homepage">
					<?php include 'homepage.php'; ?>
				</div>
				<div class="datatoko">
					<?php include "toko_data.php";?>
				</div>
				<div class="tambahtoko">
					<?php include "toko_tambah.php";?>
				</div>
				<div class="edittoko">
					<?php include "toko_edit.php";?>
				</div>
				<div class="datarekening">
					<?php include "rekening_data.php";?>
				</div>
				<div class="tambahrekening">
					<?php include "rekening_tambah.php" ?>
				</div>
				<div class="editrekening">
					<?php include "rekening_edit_jenis_rekening.php" ?>
				</div>
				<div class="datapenanggungjawab">
					<?php include "penanggungjawab_data.php";?>
				</div>
				<div class="tambahpenanggungjawab">
					<?php include "penanggungjawab_tambah.php" ?>
				</div>
				<div class="editpenanggungjawab">
					<?php include "penanggungjawab_edit.php" ?>
				</div>
				
				<div class="menuadmin">
					<?php include "php/admin.php"; ?>
				</div>		

				<?php
				if($adminOt){
				?>
				<div class="tambahAdmin">
					<?php include "php/form/tambahAdmin.php"; ?>
				</div>

				<div class="editAdmin">
					<?php include "php/form/editAdmin.php"; ?>
				</div>
				<?php
				}
				?>

				<!-- bagian yosia -->
				<?php if($pemasukanOt){ ?>
					<div class="datapenerimaanindividu">
						<?php include 'datapenerimaan.php'; ?>
					</div>
					<div class="penerimaanindividu">
						<?php include 'penerimaan.php'; ?>
					</div>
					<div class="buktipenerimaan">
						<?php include 'buktipenerimaan.php'; ?>
					</div>
				<?php }
				if($pengeluaranOt){ ?>
					<div class="datanota">
						<?php include "nota_data.php";?>
					</div>
					<div class="inputnota">
						<?php include 'inputnota.php'; ?>
					</div>
					<div class="buktipengeluaran">
						<?php include 'buktipengeluaran.php'; ?>
					</div>
				<?php }
				if($pemasukanOt && $pengeluaranOt){ ?>
					<div class="bonsementara">
						<?php include 'bonsementara.php'; ?>
					</div>
					<div class="mutasirekening">
						<?php include 'mutasirekening.php'; ?>
					</div>
					<div class="databuktitransaksi">
						<?php include 'databuktitransaksi.php'; ?>
					</div>
				<?php } ?>
				<!-- akhir bagian yosia -->
				

				<!-- tambah wawan -->
				<div class="databidang">
					<?php include "bidang_data.php";?>
				</div>
				<div class="tambahbidang">
					<?php include "bidang_tambah.php";?>
				</div>
				<div class="editbidang">
					<?php include "bidang_edit.php";?>
				</div>
				<div class="datakomisi">
					<?php include "komisi_data.php";?>
				</div>
				<div class="tambahkomisi">
					<?php include "komisi_tambah.php";?>
				</div>
				<div class="editkomisi">
					<?php include "komisi_edit.php";?>
				</div>
 				
				<!-- <div class="tambahnota"> -->
					<?php //include "nota_tambah.php";?>
				<!-- </div> -->
				<div class="editnota">
					<?php include "nota_edit.php";?>
				</div> 
				<div class="datajemaat">
					<?php include "jemaat_data.php";?>
				</div>
				<div class="tambahjemaat">
					<?php include "jemaat_tambah.php";?>
				</div>
				<div class="editjemaat">
					<?php include "jemaat_edit.php";?>
				</div>
				<!-- ---------------------------------------------------- -->

				<div class="perencanaanAnggaran">
					<?php include "php/rencanaAnggaran.php"; ?>
				</div>

				<?php
					if( $adminOt && $pemasukanOt && $pengeluaranOt){
				?>
				<div class="tambahPerencanaan">
					<?php include "php/form/tambahRencanaDana.php"; ?>
				</div>
				<?php
				}
				?>

				<?php
				if( $lapPemasukanOt || $lapPengeluaranOt){
				?>
				<div class="databuktitransaksi2">
					<?php include "databuktitransaksi2.php" ?>
				</div>
				<div class="rekeningSaldo">
					<?php include "php/dafarRekeningDanSaldo.php"; ?>
				</div>
				<div class="posisiSaldo">
					<?php include "php/PosisiSaldo.php"; ?>
				</div>
				<div class="rekapRekeningSaldo">
					<?php include "php/dafarRekapRekeningDanSaldo.php"; ?>
				</div>
				<div class="bukubesar">
					<?php include "php/bukuBesar.php"; ?>
				</div>
				<div class="mutasiRekening">
					<?php include "php/mutasiPerRek.php"; ?>
				</div>
				<div class="mutasiKas">
					<?php include "php/mutasiKas.php"; ?>
				</div>
				<div class="jurnalMutasi">
					<?php include "php/jurnalMutasi.php"; ?>
				</div>
				<div class="mutasiPerAkun">
					<?php include "php/mutasiPerAkun.php"; ?>
				</div>
				<div class="laporanPemasukan">
					<?php include "php/laporanPemasukan.php"; ?>
				</div>
				<div class="laporanPengeluaran">
					<?php include "php/laporanPengeluaran.php"; ?>
				</div>
				<div class="laporanPemasukanThn">
					<?php include "php/laporanPemasukanTahunan.php"; ?>
				</div>
				<div class="pengeluaranPerTahun">
					<?php include "php/pengeluaranPerTahun.php"; ?>
				</div>
				<div class="laporanPerKelAkun">
					<?php include "php/laporanPerRekening.php"; ?>
				</div>
				<?php
				}
				?>

				<?php
				if( $lapPemasukanOt){
				?>
				<div class="laporanPenInd">
					<?php include "php/laporanPenInduvidu.php"; ?>
				</div>
				<div class="laporanPerJem">
					<?php include "php/laporanPersembahanJemaat.php"; ?>
				</div>
				<?php
				}
				?>
				
			</div>
		</div>
		<?php include "footer.php";?>
	
	</div>

	<div class="detailAdmin">
		<?php include "php/form/detailAdmin.php"; ?>
	</div>
	<div class="editProfilGereja">
		<?php include "php/form/editProfilGereja.php"; ?>
	</div>

	<div class="editPengaturanAkun">
		<?php include "php/form/pengaturanAkun.php"; ?>
	</div>

	<div class="pengaturanAkunEdit">
		<?php include "php/form/akunEdit.php"; ?>
	</div>

	<div class="editPasswordAkun">
		<?php include "php/form/editPasswordAkun.php"; ?>
	</div>

	<div class="editRencanaAnggaranJenisAkun">
		<?php include "php/form/editRencanaAnggaranJenisAkun.php"; ?>
	</div>
	
	<link rel="stylesheet" type="text/css" href="css/datatoko_rekening_penanggungjawab.css">
	<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="js/datatoko.js"></script>	
	<script type="text/javascript" src="js/rekening.js"></script>
	<script type="text/javascript" src="js/penanggungjawab.js"></script>

		<!-- wawan tambah -->
	<script type="text/javascript" src="js/databidang.js"></script>	
	<script type="text/javascript" src="js/datakomisi.js"></script>	
	<script type="text/javascript" src="js/datanota.js"></script>	
	<script type="text/javascript" src="js/datajemaat.js"></script>		
	<script type="text/javascript" src="js/homepage.js"></script>

	<script type="text/javascript" src="js/admin.js"></script>
	<script type="text/javascript" src="js/profilGereja.js"></script>
	<script type="text/javascript" src="js/laporan.js"></script>
	<script type="text/javascript" src="js/rencanaAnggaran.js"></script>


</body>
</html>