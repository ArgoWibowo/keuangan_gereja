<?php 
	if(isset($_POST['type'])){
		
	} else {
		getViewNota();
	}

	function getViewNota(){
		?>
		<link rel="stylesheet" type="text/css" href="css/inputnota.css">
		<script type="text/javascript" src="js/inputnota.js"></script>
		<form id="formNota">
			<div class="fTitle">FORM INPUT NOTA</div>
			<table>
				<tr>
					<td>Kode nota</td><td>:</td>
					<td colspan=3><input id="kodeNota" maxlength=10 placeholder="max 10 karakter" required></td>
				</tr>
				<tr>
					<td>Kode toko</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectTokoNota" required>
							<?php isiSelectToko(); ?>
							</select><input class="trick" autocomplete="off" id="selectTokoNota" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Debet</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectDebetNota" required>
							<?php isiSelectRekening(); ?>
							</select><input class="trick" autocomplete="off" id="selectDebetNota" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Tanggal nota</td><td>:</td>
					<td class="customdate"><input type="number" class="ex" id="nota_tanggal" min=1 max=31 placeholder="tanggal" value=<?php echo date("d") ?> required></td>
					<td class="customdate"><input type="number" class="ex" id="nota_bulan" min=1 max=12 placeholder="bulan" value=<?php echo date("m") ?> required></td>
					<td class="customdate"><input type="number" class="ex" id="nota_tahun" min=2000 max=9999 placeholder="tahun" value=<?php echo date("y")+2000 ?> required></td>
				</tr>
				<tr>
					<td>Jumlah</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<input value="Rp." class="rp ex" tabindex="-1" disabled>
							<input class="rptail" autocomplete="off" min=0 step=100 id="nota_jumlah" required>
						</div>
					</td>
				</tr>
				<tr>
					<td>Pelaksana</td><td>:</td>
					<td colspan=3><input id="pelaksanaNota" placeholder="nama pelaksana" required></td>
				</tr>
				<tr>
					<td colspan=5 style="text-align:center;"><br><hr></td>
				</tr>
				<tr>
					<td colspan=5 style="text-align:center;">
						<input type="reset" value="bersihkan" tabindex=-1 style="width: 120px; height: 35px;">
						<input type="submit" value="simpan" style="width: 120px; height: 35px;">
					</td>
				</tr>
			</table>
		</form>

		<?php 
	}
?>
