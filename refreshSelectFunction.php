<?php 
	if(isset($_POST['type'])){
		if($_POST['type'] == 'rekening'){
			isiSelectRekening();
		} else if($_POST['type'] == 'rekeningindividu'){
			isiSelectRekeningIndividu();
		} else if($_POST['type'] == 'rekeningnonindividu'){
			isiSelectRekeningNonIndividu();
		} else if ($_POST['type'] == 'rekeningmut') {
			isiSelectRekeningMut();	
		} else if($_POST['type'] == 'jemaat'){
			isiSelectPemberiPenerimaan();
		} else if($_POST['type'] == 'pj_penerima'){
			isiSelectPenanggungJawab("penerima");
		} else if($_POST['type'] == 'pj_bendahara'){
			isiSelectPenanggungJawab("bendahara");
		} else if($_POST['type'] == 'pj_penyetor'){
			isiSelectPenanggungJawab("penyetor");
		} else if($_POST['type'] == 'toko'){
			isiSelectToko();
		} 
	} 
//tambahan
	function isiSelectRekening(){
	?>
		<option value="" disabled selected style="display: none;">pilih rekening</option>
	<?php 
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';
		$query = "SELECT kode_jenis_akun, nama_kode_jenis_akun FROM tbl_jenis_akun  WHERE jenis_partisipan IS NULL";
	    $statement = $conn->prepare($query);
	    $statement->execute();
	    $statement->bind_result($kode, $nama);
	    while ($statement->fetch()) {
	    	echo "<option value='".str_replace(".", "_dot_", $kode)."'>".$kode." - ".$nama."</option>";
	    }
	    $statement->close();
	}
	//akhir tambahan

	function isiSelectRekeningIndividu(){
	?>
		<option value="" disabled selected style="display: none;">pilih rekening</option>
	<?php 
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';
		$query = "SELECT kode_jenis_akun, nama_kode_jenis_akun FROM tbl_jenis_akun WHERE jenis_partisipan = 'Individu'";
	    $statement = $conn->prepare($query);
	    $statement->execute();
	    $statement->bind_result($kode, $nama);
	    while ($statement->fetch()) {
	    	echo "<option value='".str_replace(".", "_dot_", $kode)."'>".$kode." - ".$nama."</option>";
	    }
	    $statement->close();
	}

	function isiSelectRekeningNonIndividu(){
	?>
		<option value="" disabled selected style="display: none;">pilih rekening</option>
	<?php 
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';
		$query = "SELECT kode_jenis_akun, nama_kode_jenis_akun FROM tbl_jenis_akun WHERE jenis_partisipan <> 'Individu'";
	    $statement = $conn->prepare($query);
	    $statement->execute();
	    $statement->bind_result($kode, $nama);
	    while ($statement->fetch()) {
	    	echo "<option value='".str_replace(".", "_dot_", $kode)."'>".$kode." - ".$nama."</option>";
	    }
	    $statement->close();
	}
//tamahan
	function isiSelectRekeningMut(){
	?>
		<option value="" disabled selected style="display: none;">pilih rekening</option>
	<?php 
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';
		$query = "SELECT kode_jenis_akun, nama_kode_jenis_akun FROM tbl_jenis_akun";
	    $statement = $conn->prepare($query);
	    $statement->execute();
	    $statement->bind_result($kode, $nama);
	    while ($statement->fetch()) {
	    	echo "<option value='".str_replace(".", "_dot_", $kode)."'>".$kode." - ".$nama."</option>";
	    }
	    $statement->close();
	}
	//akhir tambahan

	function isiSelectPemberiPenerimaan(){
	?>
		<option value="" disabled selected style="display: none;">pilih pemberi</option>
	<?php 
		if(session_status() == PHP_SESSION_NONE) session_start();
		include "koneksi2.php";
		$query = "SELECT kode_jemaat, nama_jemaat FROM tbl_jemaat ORDER BY 2";
	    $statement = $conn->prepare($query);
	    $statement->execute();
	    $statement->bind_result($kode, $nama);
	    while ($statement->fetch()) {
	    	echo "<option value='".$kode."'>".$nama." - ".$kode."</option>";
	    }
	    $statement->close();		
	}

	function isiSelectPenanggungJawab($placeholder){
	?>
		<option value="" disabled selected style="display: none;">pilih <?php echo $placeholder ?></option>
	<?php 
		if(session_status() == PHP_SESSION_NONE) session_start();
		include "koneksi2.php";
		$query = "SELECT kode_penanggungjawab, nama_penanggungjawab FROM tbl_penanggungjawab";
	    $statement = $conn->prepare($query);
	    $statement->execute();
	    $statement->bind_result($kode, $nama);
	    while ($statement->fetch()) {
	    	echo "<option value='".$kode."'>".$kode." - ".$nama."</option>";
	    }
	    $statement->close();		
	}

	function isiSelectToko(){
	?>
		<option value="" disabled selected style="display: none;">pilih toko</option>
	<?php 
		if(session_status() == PHP_SESSION_NONE) session_start();
		include 'koneksi2.php';
		$query = "SELECT kode_toko, nama_toko FROM tbl_toko";
	    $statement = $conn->prepare($query);
	    $statement->execute();
	    $statement->bind_result($kode, $nama);
	    while ($statement->fetch()) {
	    	echo "<option value='".$kode."'>".$kode." - ".$nama."</option>";
	    }
	    $statement->close();
	}
 
	