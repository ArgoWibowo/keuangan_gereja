<div id="divTitle">
	<label class="lblTitle">TAMBAH TOKO</label>
</div>
<form id="formTambahToko" name="formTambahToko" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabel">Kode Toko</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="kodeToko" maxlength=8 name="kodeToko" spellcheck="false" placeholder="Masukan Kode Toko" style="width: 246px;" onKeyUp="checkKodeToko()" onFocusOut="checkKodeToko()" required/></td>
			<td class="warning"><img id="warningUsername" src="image/warning.png" alt="warning"></td>
			<td>
				<?php
					include "koneksi.php";

					$cek_kode = "SELECT kode_toko FROM tbl_toko WHERE waktu_tambah >= (SELECT DATE(MAX(waktu_tambah)) FROM tbl_toko) ORDER BY waktu_tambah DESC LIMIT 1";
					$cek_kode2= mysql_query($cek_kode);
					$num_rows = mysql_num_rows($cek_kode2);
					if($num_rows >= 1){
						$row = mysql_fetch_assoc($cek_kode2);
						echo $row['kode_toko'];
					}
				?>
			</td>
		</tr>
		<tr>
			<td class="kolomLabel">Nama Toko</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="namaToko" name="namaToko" spellcheck="false" placeholder="Masukan Nama Toko" style="width: 246px;" required/></td>
		</tr>
		<tr style="vertical-align: top;">
			<td class="kolomLabel">Alamat Toko</td>
			<td class="kolomTitikDua"> : </td>
			<td style="padding-bottom: 0px;"><textarea style="width: 244px; height: 70px;" name ="alamatToko" id ="alamatToko" >Masukan Alamat Toko</textarea></td>
		</tr>
		<tr>
			<td class="kolomLabel">No Telepon</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="number" id="noTelepon" name="noTelepon" spellcheck="false" placeholder="Masukan No Telepon" style="width: 246px;" required/></td>
		</tr>
		<tr>
			<td class="kolomLabel">CP Toko</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="cpToko" name="cpToko" spellcheck="false" placeholder="Masukan CP Toko" style="width: 246px;" required/></td>
		</tr>
	</table>
	<input type="submit" value="Tambah"  id="btnTambahToko" name="tambahToko" class="button" >
</form>

<div id="divTitle"><label id="lblTitle">Import CSV</label></div>
<form name="formTambahTokoImport" id="formTambahTokoImport" method="post" enctype="multipart/form-data">
    	<input type="file" name="fileTokoImport" id="fileTokoImport" />
        <input type="submit" name="tambahTokoImport" id="btnTambahTokoImport" value="Import" />
        <input type="text" id="jenisfileImportToko" value="toko" name="jenisfileImportToko" style="visibility:hidden";>
</form>



