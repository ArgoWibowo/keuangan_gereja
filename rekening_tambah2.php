<div id="divTitle">
	<label id="lblTitle">Golongan Akun</label>
</div>
<form id="formTambahGolonganRekening" name="formTambahGolonganRekening" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabelRekening">Kelompok Akun</td>
			<td> : <select id='pilihanKelompokAkunRekening' name="pilihanKelompokAkunRekening">
			 	<option value='' disabled selected style="display:none;">Kelompok</option>
				<option value='D'>Debit</option>
			 	<option value='K'>Kredit</option>
				</select></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Kode Golongan</td>
			<td> : <input type="text" id="kodeGolongan" name="kodeGolongan" spellcheck="false" onKeyUp="checkKodeGolongan()" placeholder="Masukan Kode Golongan" required/></td>
			<td class="warning"><img id="warningKodeGolongan" src="image/warning.png" alt="warning"></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Nama Golongan</td>
			<td> : <input type="text" id="namaGolongan" name="namaGolongan" spellcheck="false" placeholder="Masukan Nama Golongan" required/></td>
		</tr>
	</table>
	<div class="divKeteranganTambahRekening" id="divKeteranganTambahGolongan">
		<label id="lblKeteranganTambahGolongan" class="lblKeteranganTambahRekening">Sukses</label>
	</div>
	<input type="submit" value="Tambah"  id="btnTambahGolonganRekening" name="tambahGolonganRekening" class="button" >
</form>

<div id="divTitle">
	<label id="lblTitle">Sub Golongan Akun</label>
</div>
<form id="formTambahSubGolonganRekening" name="formTambahSubGolonganRekening" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabelRekening">Kode Golongan</td>
			<td> : <select id='pilihanKodeGolongan' name="pilihanKodeGolongan" >

				</select></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Kode Sub Golongan</td>
			<td> : <input type="text" id="kodeSubGolongan" name="kodeSubGolongan" onKeyUp="checkKodeSubGolongan()" spellcheck="false" placeholder="Masukan Kode Sub Golongan" required/></td>
			<td class="warning"><img id="warningKodeSubGolongan" src="image/warning.png" alt="warning"></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Nama Sub Golongan</td>
			<td> : <input type="text" id="namaSubGolongan" name="namaSubGolongan" spellcheck="false" placeholder="Masukan Nama Sub Golongan" required/></td>
		</tr>
	</table>
	<div class="divKeteranganTambahRekening" id="divKeteranganTambahSubGolongan">
		<label id="lblKeteranganTambahSubGolongan" class="lblKeteranganTambahRekening">Sukses</label>
	</div>
	<input type="submit" value="Tambah"  id="btnTambahSubGolonganRekening" name="tambahSubGolonganRekening" class="button" >
</form>

<div id="divTitle">
	<label id="lblTitle">Jenis Akun</label>
</div>
<form id="formTambahJenisAkunRekening" name="formTambahJenisAkunRekening" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabelRekening">Kode Sub Golongan</td>
			<td> : <select id='pilihanKodeSubRekening' name="pilihanKodeSubRekening">
			 	
				</select></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Kode Jenis Akun</td>
			<td> : <input type="text" id="kodeJenisAkunRekening" name="kodeJenisAkunRekening" onKeyUp="checkKodeJenis()" spellcheck="false" placeholder="Masukan Kode Jenis Golongan" required/></td>
			<td class="warning"><img id="warningKodeJenisAkunRekening" src="image/warning.png" alt="warning"></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Nama Rekening</td>
			<td> : <input type="text" id="namaJenisAkunRekening" name="namaSubAkunRekening" spellcheck="false" placeholder="Masukan Nama Jenis Akun" required/></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Jenis Partisipan</td>
			<td> : <select id='pilihanPartisipanAkunRekening' name="pilihanPartisipanAkunRekening">
			 	<option value='' disabled selected style="display:none;">Jenis Partisipan</option>
				<option value='Individu'>Individu</option>
			 	<option value='Non Individu'>Non Individu</option>
				</select></td>
		</tr>
	</table>
	<div class="divKeteranganTambahRekening" id="divKeteranganTambahJenisRekening">
		<label id="lblKeteranganTambahRekening" class="lblKeteranganTambahRekening">Sukses</label>
	</div>
	<input type="submit" value="Tambah"  id="btnTambahJenisAkunRekening" name="tambahJenisAkunRekening" class="button" >
</form>
