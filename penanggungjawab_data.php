<?php  
	require_once("koneksi.php");
?>	
	<div id="divTitle">
		<label class="lblTitle">DATA PENANGGUNG JAWAB</label>
	</div>
	<div id="navJumlahDataToko" class="navJumlahData">
		<label>Jumlah Data : </label> <label id="lblJumlahDataPenanggungJawab" class="lblJumlahDataPenanggungJawab" ></label>
	</div>
	<div id="navSearchPenanggungJawab" class="navSearch">
		<ul class="nav">
			<li class='search'>
				<div class='divSearch'>
					<input name ="kataKunciPenanggungJawab" onchange="searchPenanggungJawab()" onkeyup="searchPenanggungJawab()"  id='searchPenanggungJawab' class='searchPenanggungJawab' type='text' placeholder="Cari Nama Penanggung Jawab">
				</div>
			</li>
			<li class="search">
				<div class='divSearch'>
					<button id="btnDropPencarianPenanggungJawab">Pencarian Lanjutan</button>
				</div>
			</li>
		</ul>
		
	</div>
	<div id="navSortPenanggungJawab" class="navSort" >
		<label>Urutkan berdasarkan : </label>
		<select id='sortDataPenanggungJawab' class="sortData" onchange=searchPenanggungJawab()>
		 	<option value='kode_penanggungjawab'>Kode</option>
		 	<option value='nama_penanggungjawab'>Nama</option>
		 	<option value='jabatan'>Jabatan</option>
		 	<option value='alamat_penanggungjawab'>ALamat</option>
		 	<option value='no_telepon_penanggungjawab'>No Telepon</option>
		</select>
	</div>
	<div id="divBoxPencarian" class="divBoxPencarian" >
		<form id="formCariPenanggungJawab" name="formCariPenanggungJawab" method="post" enctype="multipart/form-data">
			<table class="tabelData" id="tablePencarian">
				<tr><td colspan="2"><label>Pencarian Berdasarkan</label></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td class="tdLabelTablePencarian"><label class="inputPencarian">Kode</label></td>
					<td class="tdInputTablePencarian"><input name ="kataKunciKodePenanggungJawab"  id='kataKunciKodePenanggungJawab' class='searchPenanggungJawab' type='text' placeholder="Cari Kode"></td>
					<td class="tdLabelTablePencarian"><label class="inputPencarian">Nama</label></td>
					<td class="tdInputTablePencarian"><input name ="kataKunciNamaPenanggungJawab"  id='kataKunciNamaPenanggungJawab' class='searchPenanggungJawab' type='text' placeholder="Cari Nama"></td>
					<td class="tdLabelTablePencarian"><label class="inputPencarian">Jabatan</label></td>
					<td class="tdInputTablePencarian"><input name ="kataKunciJabatanPenanggungJawab"  id='kataKunciJabatanPenanggungJawab' class='searchPenanggungJawab' type='text' placeholder="Cari Jabatan"></td>
					
				</tr>
				<tr>
					<td class="tdLabelTablePencarian"><label class="inputPencarian">Alamat</label></td>
					<td class="tdInputTablePencarian"><input name ="kataKunciAlamatPenanggungJawab"  id='kataKunciAlamatPenanggungJawab' class='searchPenanggungJawab' type='text' placeholder="Cari Alamat"></td>
					<td class="tdLabelTablePencarian"><label class="inputPencarian">No Telepon</label></td>
					<td class="tdInputTablePencarian"><input name ="kataKunciNoTeleponPenanggungJawab"  id='kataKunciNoTeleponPenanggungJawab' class='searchPenanggungJawab' type='text' placeholder="Cari No Telepon"></td>
					<td></td>
					<td><input type="submit" value="Cari"  id="btnPencarianPenanggungJawab" name="cariPenanggungJawab" ></td>
				</tr>
			</table>
		</form>
	</div>
	
	<div id="datapenanggungjawab" class="dataTable">
		<table id="tablepenanggungjawab" class="tabelData striped" style="padding :20px 20px; align:"center";">
			<thead>
				<tr >
					<th id="nomortabelpenanggungjawab" style="text-align:left">No</th>
					<th id="kodetabelpenanggungjawab" class="kodetabelpenanggungjawab"  style="text-align:left">Kode</th>
					<th id="namatabelpenanggungjawab" class="namatabelpenanggungjawab"  style="text-align:left">Nama</th>
					<th id="jabatantabelpenanggungjawab" class="jabatantabelpenanggungjawab" style="text-align:left">Jabatan</th>
					<th id="alamattabelpenanggungjawab" class="alamattabelpenanggungjawab" style="text-align:left">Alamat</th>
					<th id="telepontabelpenanggungjawab" class="telepontabelpenanggungjawab" style="text-align:left">No Telepon</th>
					<?php 
						if ($_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == "BACATULIS" || $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == "TULIS"){
					?>
						<th id="actiontabelpenanggungjawab" class="actiontabelpenanggungjawab" style="text-align:left">Pilihan</th>
					<?php 
						} 
					?>

				</tr>	
			</thead>
			<tbody id ="isiTabelPenanggungJawab" class="isiTabel">
			<tbody>
		</table>
	</div>
	<div id="paginationPenanggungJawab" class="pagination" cellspacing="0">
	</div>

	<div id="divOptionDataPenanggungJawab" class="divOptionDataPenanggungJawab">
		<?php 
			if ($_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == "BACATULIS" || $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == "TULIS"){
		?>
			<button id="btnTambahPenanggungJawab2" onclick="tambahPenanggungJawab();">Tambah Penanggung Jawab</button>
		<?php 
			} 
		?>

		<input type="button"  class="submit_button" onclick="printDiv2('penanggungjawab')" value="Cetak" style="margin-left:0px; width:100px; height:25px;" />
		<label id="lblExportPenanggungJawab">Export : </label>
		<select id='selectExportDataPenanggungJawab' class="selectExportDataPenanggungJawab" onchange=exportPenanggungJawab()>
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='CSV'>CSV</option>
		 	<option value='PDF'>PDF</option>
		</select>
	</div>