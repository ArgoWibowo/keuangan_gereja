<?php 
	if(isset($_POST['type'])){
		
	} else {
		getViewBonSementara();
	}

	function getViewBonSementara(){
		?>

		<link rel="stylesheet" type="text/css" href="css/bonsementara.css">
		<script type="text/javascript" src="js/bonsementara.js"></script>
		<form id="formBonSementara">
			<div class="fTitle">FORM BON SEMENTARA</div>
			<table id="bonSementaraAtas">
				<tr>
					<td width="15%">Kode bukti bon</td><td width="1%">:</td>
					<td width="30%" colspan=3><input id="kodeBonSementara" maxlength=8 placeholder="max 8 karakter" required tabindex="1"></td>
					<td width="8%"class="separator"></td>
					<td width="15%">Bendahara</td><td width="1%">:</td>
					<td width="30%">
						<div class="relativeBox">
							<select class="selectBendaharaBonSementara" required tabindex="7">
							<?php isiSelectPenanggungJawab("bendahara"); ?>
							</select><input class="trick" autocomplete="off" id="selectBendaharaBonSementara" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Kredit</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectKreditBonSementara" required tabindex="2">
							<?php isiSelectRekening() ?>
							</select><input class="trick" autocomplete="off" id="selectKreditBonSementara" tabindex="-1">
						</div>
					</td>
					<td class="separator"></td>
					<td>Penerima</td><td>:</td>
					<td>
						<div class="relativeBox">
							<select class="selectPenerimaBonSementara" required tabindex="8">
							<?php isiSelectPenanggungJawab("penerima"); ?>
							</select><input class="trick" autocomplete="off" id="selectPenerimaBonSementara" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Tanggal bon</td><td>:</td>
					<td class="customdate"><input type="number" class="ex" id="bonSementara_tanggal" min=1 max=31 placeholder="tanggal" value=<?php echo date("d") ?> required tabindex="3"></td>
					<td class="customdate"><input type="number" class="ex" id="bonSementara_bulan" min=1 max=12 placeholder="bulan" value=<?php echo date("m") ?> required tabindex="4"></td>
					<td class="customdate"><input type="number" class="ex" id="bonSementara_tahun" min=2000 max=9999 placeholder="tahun" value=<?php echo date("y")+2000 ?> required tabindex="5"></td>
					<td class="separator"></td>
					<td>Penyetor</td><td>:</td>
					<td>
						<div class="relativeBox">
							<select class="selectPenyetorBonSementara" required tabindex="9">
							<?php isiSelectPenanggungJawab("penyetor"); ?>
							</select><input class="trick" autocomplete="off" id="selectPenyetorBonSementara" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Keterangan</td><td>:</td>
					<td colspan=7 id="boxKeteranganBonSementara">
						<input id="keteranganBonSementara" placeholder="max 150 karakter" maxlength=150 required tabindex="6">
					</td>
				</tr>
			</table>
			<br><hr>
			<table id="detailBon">
				<thead>
					<tr>
						<th width="10%">Kode Bon</th>
						<th width="10%">Debet</th>
						<th width="60%">Uraian</th>
						<th colspan=2>Jumlah</th>
						<th width="8%"></th>
					</tr>
				</thead>
				<tbody id="detailBonBody">
					<?php
						isiDetailBon();
					?>
				</tbody>
				
		</form>
		<form id="tambahBon">
				<tbody id="detailBonBody2">
					<tr>
			    		<td colspan=2 id="boxSelectDebetBonTambahan">
			    			<div class="relativeBox">
				    			<select class="selectDebetBonTambahan" required>
								<?php isiSelectRekening(); ?>
								</select><input class="trick" autocomplete="off" id="selectDebetBonTambahan" tabindex="-1">
							</div>
						</td>
			    		<td id="boxUraianBonTambahan">
			    			<input id="uraianBonTambahan" placeholder="uraian bon sementara" required>
			    		</td>
			    		<td id="boxJumlahBonTambahan" colspan=2>
			    			<div class="relativeBox">
				    			<input value="Rp." class="rp ex" tabindex="-1" disabled>
								<input class="rptail" autocomplete="off" min=0 step=100 id="bonTambahan_jumlah" required>
							</div>
			    		</td>
			    		<td>
			    			<div class="relativeBox">
			  	  				<input type="submit" form="tambahBon" value="tambah" style="height: 24px;">
			  	  			</div>
			    		</td>
			    	</tr>
				</tbody>
		</form>
				<tbody id="totalDetailBon">
					<tr>
						<td>Jumlah</td>
						<td></td>
						<td></td>
						<td class="rpCol">Rp.</td>
						<td class="numerik" id="totalSementaraDetailBon">0</td>
						<td></td>
					</tr>
				</tbody>
			</table><br>
			<div style="text-align:center;">
				<input type="reset" value="bersihkan" tabindex=-1 style="width: 120px; height: 35px;">
				<input type="submit" value="simpan" tabindex=10 style="width: 120px; height: 35px;">
			</div>
		<?php 
	} 
?>

