<div id="divTitle">
	<label class="lblTitle">TAMBAH GOLONGAN, SUB, DAN JENIS REKENING</label>
</div>

<form id="formTambahGolonganRekening" name="formTambahGolonganRekening" method="post" enctype="multipart/form-data">
	<div id="divSubTitle">
		<label class="lblSubTitle">GOLONGAN AKUN</label>
	</div>
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabelRekening">Kelompok Akun</td>
			<td> : <select id='pilihanKelompokAkunRekening' name="pilihanKelompokAkunRekening">
			 	<option value='' disabled selected style="display:none;">Kelompok</option>
				<option value='D'>Debit</option>
			 	<option value='K'>Kredit</option>
				</select></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Kode Golongan</td>
			<td> : <input type="text" id="fixKodeKelompokDisabled" name="fixKodeKelompokDisabled" style="width:20px;" disabled><input type="text" id="kodeGolongan" name="kodeGolongan" maxlength=2 spellcheck="false" onKeyUp="checkKodeGolongan()" placeholder="1 ( contoh )" style="width:145px;" required/><input type="text" id="fixKodeKelompokHidden" name="fixKodeKelompokHidden" style="width:20px; visibility:hidden;" ></td>
			<td class="warning"><img id="warningKodeGolongan" src="image/warning.png" alt="warning"></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Nama Golongan</td>
			<td> : <input type="text" id="namaGolongan" name="namaGolongan" spellcheck="false" placeholder="Masukan Nama Golongan" required/></td>
		</tr>
	</table>
	<div class="divKeteranganTambahRekening" id="divKeteranganTambahGolongan">
		<label id="lblKeteranganTambahGolongan" class="lblKeteranganTambahRekening">Sukses</label>
	</div>
	<input type="submit" value="Tambah"  id="btnTambahGolonganRekening" name="tambahGolonganRekening" class="button" >
</form>

<form id="formTambahSubGolonganRekening" name="formTambahSubGolonganRekening" method="post" enctype="multipart/form-data">
		<div id="divSubTitle">
		<label class="lblSubTitle">SUB GOLONGAN AKUN</label>
	</div>
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabelRekening">Kode Golongan</td>
			<td> : <select id='pilihanKodeGolongan' name="pilihanKodeGolongan" >

				</select></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Kode Sub Golongan</td>
			<td> : <input type="text" id="fixKodeGolonganDisabled" name="fixKodeGolonganDisabled" style="width:27px;" disabled><input type="text" id="kodeSubGolongan" name="kodeSubGolongan" onKeyUp="checkKodeSubGolongan()" maxlength=2 spellcheck="false" placeholder="01 ( contoh )" style="width:145px;" required/><input type="text" id="fixKodeGolonganHidden" name="fixKodeGolonganHidden" style="width:20px; visibility:hidden;" ></td>
			<td class="warning"><img id="warningKodeSubGolongan" src="image/warning.png" alt="warning"></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Nama Sub Golongan</td>
			<td> : <input type="text" id="namaSubGolongan" name="namaSubGolongan" spellcheck="false" placeholder="Masukan Nama Sub Golongan" required/></td>
		</tr>
	</table>
	<div class="divKeteranganTambahRekening" id="divKeteranganTambahSubGolongan">
		<label id="lblKeteranganTambahSubGolongan" class="lblKeteranganTambahRekening">Sukses</label>
	</div>
	<input type="submit" value="Tambah"  id="btnTambahSubGolonganRekening" name="tambahSubGolonganRekening" class="button" >
</form>


<form id="formTambahJenisAkunRekening" name="formTambahJenisAkunRekening" method="post" enctype="multipart/form-data">
	<div id="divSubTitle">
		<label class="lblSubTitle">JENIS AKUN</label>
	</div>
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabelRekening">Kode Sub Golongan</td>
			<td> : <select id='pilihanKodeSubRekening' name="pilihanKodeSubRekening">
			 	
				</select></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Kode Jenis Akun</td>
			<td> : <input type="text" id="fixKodeSubGolonganDisabled" name="fixKodeSubGolonganDisabled" style="width:43px;" disabled><input type="text" id="kodeJenisAkunRekening" name="kodeJenisAkunRekening" onKeyUp="checkKodeJenis()" maxlength=2 spellcheck="false" placeholder="01 ( contoh )" style="width:125px;" required/><input type="text" id="fixKodeSubGolonganHidden" name="fixKodeSubGolonganHidden" style="width:20px; visibility:hidden;" ></td>
			<td class="warning"><img id="warningKodeJenisAkunRekening" src="image/warning.png" alt="warning"></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Nama Rekening</td>
			<td> : <input type="text" id="namaJenisAkunRekening" name="namaSubAkunRekening" spellcheck="false" placeholder="Masukan Nama Jenis" required/></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Jenis Partisipan</td>
			<td> : <select id='pilihanPartisipanAkunRekening' name="pilihanPartisipanAkunRekening">
			 	<option value='' disabled selected style="display:none;">Khusus Persembahan</option>
			 	<option value=''>-</option>
				<option value='Individu'>Individu</option>
			 	<option value='Non Individu'>Non Individu</option>
				</select></td>
		</tr>
	</table>
	<div class="divKeteranganTambahRekening" id="divKeteranganTambahJenisRekening">
		<label id="lblKeteranganTambahRekening" class="lblKeteranganTambahRekening">Sukses</label>
	</div>
	<input type="submit" value="Tambah"  id="btnTambahJenisAkunRekening" name="tambahJenisAkunRekening" class="button" >
</form>
