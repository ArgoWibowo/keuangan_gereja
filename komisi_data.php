<?php  
	require_once("koneksi.php");
?>	
	<div id="navJumlahDataKomisi" class="navJumlahData">
		<label>Jumlah Data : </label> <label id="lblJumlahDataKomisi" class="lblJumlahDataKomisi" ></label>
	</div>
	<div id="navSearchKomisi" class="navSearch">
		<ul class="nav">
			<li class='search'>
				<div class='divSearch'>
					<input name ="kataKunciKomisi" onchange="searchKomisi()" onkeyup="searchKomisi()"  id='searchKomisi' class='searchKomisi' type='text' placeholder="Masukkan kata kunci">
				</div>
			</li>
		</ul>
	</div>
	<div id="navSortKomisi" class="navSort" >
		<label>Urutkan berdasarkan : </label>
		<select id='sortDataKomisi' class="sortData" onchange=searchKomisi()>
		 	<option value='kode_komisi'>Kode</option>
		 	<option value='nama_komisi'>Nama Komisi</option>
		 	<option value='kode_bidang'>Nama Bidang</option>		 	
		 	<option value='kode_jenis_akun'>Jenis Akun</option>
		 	<option value='tgl_dibentuk'>Tanggal</option>
		 	<option value='status'>Status Komisi</option>		 	
		</select>
	</div>
	<div id="datakomisi" class="dataTable">
		<table class="tabelData striped" id='tabeldatakomisi' style="padding :20px 20px; align:"center";">
			<thead>
				<tr >
					<th id="nomortabelkomisi" style="text-align:left;width: 30px;">No</th>
					<th class="kodetabelkomisi"  style="text-align:left;">Kode komisi</th>
					<th class="namatabelkomisi"  style="text-align:left;">Nama komisi</th>
					<th class="namabidangtabelkomisi"  style="text-align:left;">Nama Bidang</th>
					<th class="jenisakuntabelkomisi" style="text-align:left;">Jenis Akun</th>
					<th class="tanggaltabelkomisi" style="text-align:left;">Tanggal Dibentuk</th>
					<th class="statustabelkomisi" style="text-align:left;">Status komisi</th>
					<?php
						$admin = $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'TULIS';
		 
						if ($admin){
					?>
		
					<th class="actiontabelkomisi" style="text-align:left;">Pilihan</th>
					<?php } ?>
				</tr>	
			</thead>
			<tbody id ="isiTabelKomisi" class="isiTabel">
			<tbody>
		</table>
	</div>
	<div id="paginationKomisi" class="pagination" cellspacing="0">
	</div>
	<div style="margin-left:20px;">
		<button onclick="detailKomisiData('#detailKomisi')"  style="width: 120px; height: 25px;">Detail</button>
		<!-- <button onclick="printDataKomisiBidang('tabeldatakomisi')" id='cetakLaporan'  style="width: 120px; height: 25px;">Cetak</button>
 -->		<label id="lblExportBukuBesar">Export : </label>
		<select id='selectExportDataKomisi' class="selectExportDataKomisi" onchange=exportKomisi()>
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
		
	</div>
	</br>
<div id="detailKomisi" class="detailKomisi" >
	<label id="labelDetailKomisi">DETAIL KOMISI</label>
	<a href="#" class="close"><img src="image/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
	
	<form id="formDetailKomisi">
	<table border="0">

		<tr>
			<td style="width: 160px;">Kode Komisi</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="kodeKomisiDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Nama Komisi</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="namaKomisiDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Nama Bidang</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="bidangKomisiDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Jenis Akun</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="jenisAkunKomisiDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Pengurus 1</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="pengurus1KomisiDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Pengurus 2</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="pengurus2KomisiDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Pengurus 3</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="pengurus3KomisiDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Tgl dibentuk</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="tglDibentukKomisiDetail" disabled></td>
		</tr>
		<tr style="vertical-align: top;">
			<td style="width: 160px;">Deskripsi</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td style="padding-bottom: 0px;"><textarea style="width: 170px; height: 60px;" name ="desKomisiDetai" id ="desKomisiDetail" disabled></textarea></td>
		</tr>
		<tr>
			<td style="width: 160px;">Status</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input type="text" id="statusKomisiDetail" disabled></td>
		</tr>
	
	</table>
	</form>
	<br>
	
	<div id="buttonEditProfilAkunAllKomisiBidang">
		<button type="button" id"buttKomisiDetail" onclick="closeDetailKomisi();">Tutup</button>
	</div>
</div>

