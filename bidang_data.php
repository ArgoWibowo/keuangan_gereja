<?php  
	require_once("koneksi.php");
		

?>	
	<div id="navJumlahDataBidang" class="navJumlahData">
		<label>Jumlah Data : </label> <label id="lblJumlahDataBidang" class="lblJumlahDataBidang" ></label>
	</div>
	<div id="navSearchBidang" class="navSearch">
		<ul class="nav">
			<li class='search'>
				<div class='divSearch'>
					<input name ="kataKunciBidang" onchange="searchBidang()" onkeyup="searchBidang()"  id='searchBidang' class='searchBidang' type='text' placeholder="Masukkan kata kunci">
				</div>
			</li>
		</ul>
	</div>
	<div id="navSortBidang" class="navSort" >
		<label>Urutkan berdasarkan : </label>
		<select id='sortDataBidang' class="sortData" onchange=searchBidang()>
		 	<option value='kode_bidang'>Kode</option>
		 	<option value='nama_bidang'>Nama</option>
		 	<option value='kode_jenis_akun'>Jenis Akun</option>
		 	<option value='tgl_dibentuk'>Tanggal</option>
		 	<option value='status'>Status Bidang</option>		 	
		</select>
	</div>
	<div id="databidang" class="dataTable" style="width:100%;">
		<table class="tabelData striped" id='tabeldataBidang' style="padding :20px 20px; align:center;">
			<thead>
				<tr >
					<th id="nomortabelbidang" style="text-align:left;width: 30px;">No</th>
					<th class="kodetabelbidang"  style="text-align:left;">Kode Bidang</th>
					<th class="namatabelbidang"  style="text-align:left;">Nama Bidang</th>
					<th class="jenisakuntabelbidang" style="text-align:left;">Jenis Akun</th>
					<th class="tanggaltabelbidang" style="text-align:left;">Tanggal Dibentuk</th>
					<th class="statustabelbidang" style="text-lign:left;">Status Bidang</th>
					<?php
						//$admin = ;
		 
						if ($_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'TULIS'){
					?>
		
					<th class="actiontabelbidang" style="text-align:left;">Pilihan</th>
					<?php 
						} 
					?>
				</tr>	
			</thead>
			<tbody id ="isiTabelBidang" class="isiTabel">
			<tbody>
		</table>

	</div>

	<div id="paginationBidang" class="pagination" cellspacing="0">
	</div>
		<div style="margin-left:20px;">
		<button onclick="detailBidangData('#detailBidang')"  style="width: 120px; height: 25px;">Detail</button>
		<!-- <button onclick="printDataKomisiBidang('tabeldataBidang')" id='cetakLaporan'  style="width: 120px; height: 25px;">Cetak</button> -->
		<label id="lblExportBidang">Export : </label>
		<select id='selectExportDataBidang' class="selectExportDataBidang" onchange=exportBidang()>
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='PDF'>PDF</option>
		</select>
	</div>
	</br>

<div id="detailBidang" class="detailBidang" >
	<label id="labelDetailBidang">DETAIL BIDANG</label>
	<a href="#" class="close"><img src="image/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
	
	<form id="formDetailBidang">
	<table border="0">

		<tr>
			<td style="width: 160px;">Kode Bidang</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="kodeBidangDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Nama Bidang</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="namaBidangDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Jenis Akun</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="jenisAkunBidangDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Pengurus 1</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="pengurus1BidangDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Pengurus 2</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="pengurus2BidangDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Pengurus 3</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="pengurus3BidangDetail" disabled></td>
		</tr>
		<tr>
			<td style="width: 160px;">Tgl dibentuk</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input	type="text" id="tglDibentukBidangDetail" disabled></td>
		</tr>
		<tr style="vertical-align: top;">
			<td style="width: 160px;">Deskripsi</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td style="padding-bottom: 0px;"><textarea style="width: 170px; height: 60px;" name ="desBidangDetai" id ="desBidangDetail" disabled></textarea></td>
		</tr>
		<tr>
			<td style="width: 160px;">Status</td>
			<td class="kolomTitikDuaPopUpKeuangan"> : </td>
			<td ><input type="text" id="statusBidangDetail" disabled></td>
		</tr>
	
	</table>
	</form>
	<br>
	
	<div id="buttonEditProfilAkunAllKomisiBidang">
		<button type="button" id"buttKomisiDetail" onclick="closeDetailBidang();">Tutup</button>
	</div>
</div>

