<?php
// require('fpdf.php');
require('FPDF/WriteHTML.php');
require('koneksi2.php');
require('koneksi.php');

$sql_profil = "select * from tbl_profil_gereja";
$hasil_profil = mysql_query($sql_profil);
$baris_profil = mysql_fetch_array($hasil_profil);
$adaTidak = mysql_num_rows($hasil_profil);

$filePath = "PDF_Data.pdf";
$pdf = new PDF_HTML();
$pdf->AddPage('p');
$pdf->SetAutoPageBreak(false);
$pdf->SetFont('Arial', '', 10);
$pdf->Image('image/gkj.png',18,13,20);

//Nama Gereja
$pdf->Ln(-10);
$pdf->Cell(35, 30, '', 0);
$pdf->Cell(115, 30, $baris_profil['nama_gereja'], 0);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(40, 30, 'Tanggal: '.date('d-m-Y').'', 0);
$pdf->Ln(5);
//Alamat Gereja
$pdf->Cell(35, 30, '', 0);
$pdf->Cell(100, 30, $baris_profil['alamat_gereja'], 0);
$pdf->Ln(5);
//email Gereja
$pdf->Cell(35, 30, '', 0);
$pdf->Cell(100, 30, $baris_profil['email_gereja'], 0);
$pdf->Ln(5);
//nomor telepon Gereja
$pdf->Cell(35, 30, '', 0);
$pdf->Cell(100, 30, $baris_profil['nomer_telepon_gereja'], 0);
$pdf->Ln(20);

if(isset($_GET["pdffor"])){
	$pdffor = $_GET['pdffor'];
	if ($pdffor == "toko"){
		//set TH
		$pdf->SetFont('Arial', 'B', 11);
		$pdf->Cell(70, 8, '', 0);
		$pdf->Cell(100, 8, 'Data Toko', 0);
		$pdf->Ln(10);
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(15, 8, 'No', 0);
		$pdf->Cell(20, 8, 'Kode Toko', 0);
		$pdf->Cell(30, 8, 'Nama Toko', 0);
		$pdf->Cell(75, 8, 'Alamat Toko', 0);
		$pdf->Cell(28, 8, 'No Telepon Toko', 0);
		$pdf->Cell(20, 8, 'CP Toko', 0);
		$pdf->Ln(8);
		$pdf->SetFont('Arial', '', 8);

		//Set untuk pagebreak
		$height_of_cell = 30; // mm

		//SET CONTENT
		$query = mysql_query("SELECT * FROM tbl_toko");
		$item = 0;
		$pdf->SetFillColor(255,255,255);	
		$fill = false;
		while($hasil = mysql_fetch_array($query)){
			$item = $item+1;
			$x = $pdf->GetX();
			$y = $pdf->GetY();

			if (($y + $height_of_cell) >= 297) {
			    $pdf->AddPage();
			    $y = 10; 		// should be your top margin
			}

			$pdf->MultiCell(15, 8, $item,0,'L',$fill);
			$pdf->SetXY($x + 15, $y);
			$pdf->MultiCell(20, 8,$hasil['kode_toko'],0,'L',$fill);
			$pdf->SetXY($x + 35, $y);
			$pdf->MultiCell(30, 8, $hasil['nama_toko'],0,'L',$fill);
			$pdf->SetXY($x + 65, $y);
			$pdf->MultiCell(75, 8, $hasil['alamat_toko'],0,'L',$fill);
			$pdf->SetXY($x + 140, $y);
			$pdf->MultiCell(28, 8, $hasil['no_telepon_toko'],0,'L',$fill);
			$pdf->SetXY($x + 168, $y);
			$pdf->MultiCell(25, 8, $hasil['cp_toko'],0,'L',$fill);
			$pdf->SetXY($x + 183, $y);
			$pdf->Ln(16);
			$fill = !$fill;
		}	
	} else if ($pdffor == "penanggungjawab"){
		//set TH
		$pdf->SetFont('Arial', 'B', 11);
		$pdf->Cell(70, 8, '', 0);
		$pdf->Cell(100, 8, 'Data Penanggung Jawab', 0);
		$pdf->Ln(10);
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(15, 8, 'No', 0);
		$pdf->Cell(15, 8, 'Kode', 0);
		$pdf->Cell(30, 8, 'Nama', 0);
		$pdf->Cell(30, 8, 'Jabatan', 0);
		$pdf->Cell(75, 8, 'Alamat', 0);
		$pdf->Cell(28, 8, 'No Telepon', 0);
		$pdf->Ln(8);
		$pdf->SetFont('Arial', '', 8);

		//Set untuk pagebreak
		$height_of_cell = 30; // mm

		//SET Query
		$query 		= mysql_query("SELECT * FROM tbl_penanggungjawab");
		
		$item = 0;
		$pdf->SetFillColor(255,255,255);	
		$fill = false;
		while($hasil = mysql_fetch_array($query)){
			$item = $item+1;
			$x = $pdf->GetX();
			$y = $pdf->GetY();

			if (($y + $height_of_cell) >= 297) {
			    $pdf->AddPage();
			    $y = 10; 		// should be your top margin
			}

			$pdf->MultiCell(15, 8, $item,0,'L',$fill);
			$pdf->SetXY($x + 15, $y);
			$pdf->MultiCell(15, 8,$hasil['kode_penanggungjawab'],0,'L',$fill);
			$pdf->SetXY($x + 30, $y);
			$pdf->MultiCell(30, 8, $hasil['nama_penanggungjawab'],0,'L',$fill);
			$pdf->SetXY($x + 60, $y);
			$pdf->MultiCell(30, 8, $hasil['jabatan'],0,'L',$fill);
			$pdf->SetXY($x + 90, $y);
			$pdf->MultiCell(75, 8, $hasil['alamat_penanggungjawab'],0,'L',$fill);
			$pdf->SetXY($x + 165, $y);
			$pdf->MultiCell(28, 8, $hasil['no_telepon_penanggungjawab'],0,'L',$fill);
			$pdf->SetXY($x + 193, $y);
			$pdf->Ln(16);
			$fill = !$fill;
		}	
	}
}

$pdf->Output($filePath,'I');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . $filePath);
header('location:'.$filePath);	

?>