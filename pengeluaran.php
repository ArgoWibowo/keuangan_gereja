<form id="formpengeluaran" name="formpengeluaran" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data">
	<table id="tabelpengeluaran" style="padding :20px 20px; ">
		<tr>
			<td class="tdlabel"><label id="lblnamatoko">Nama Toko</label></td>
			<td> : <select class="namatokopengeluaran" style="width: 250px;" required>
				   		<option value="1">Canceria Corp</option>
				   </select>
				   <input class="trick" id="namatokopengeluaran" tabindex="-1">
			</td> 
		</tr>
		<tr>
			<td class="tdlabel"><label id="lbltanggaltransaksi">Tanggal Transaksi</label></td>
			<td> : <input type="date" id="tanggaltransaksipengeluaran" style="width: 245px;"></td>
		</tr>
		<tr>
			<td class="tdlabel"><label id="lblnamaadmin">Nama Admin</label></td>
			<td class="nama"> : <input type="text" id="namaadminpengeluaran" name="namaadmin" spellcheck="false" placeholder="Nama Admin"/></td>
			</td> 
		</tr>
		<tr>
			<td class="tdlabel"><label id="lblpenanggungjawab">Penanggung Jawab</label></td>
			<td> : <select class="penanggungjawabpengeluaran" style="width: 250px;" required>
				   </select><input class="trick" id="penanggungjawabpengeluaran" tabindex="-1">
			</td> 
		</tr>
		<tr>
			<td class="tdlabel"><label id="lblkategoripengeluaran">Kategori</label></td>
			<td> : <select class="kategori" style="width: 250px;" required>
				   <option value="1">Pengeluaran Rutin</option>
				   <option value="2">Pengeluaran Kegiatan</option>
				   <option value="3">Pengeluaran Tenaga</option>
				   <option value="4">Pengeluaran Subsidi dan Bantuan</option>
				   <option value="5">Pengeluaran Penyusutan dan Perawatan Peralatan</option>
				   <option value="6">Pengeluaran Pembelian Peralatan</option>
				   </select><input class="trick" id="kategoripengeluaran" tabindex="1">
			</td> 
		</tr>
		<tr>
			<td class="tdlabel"><label id="lbljeniskategori">Jenis Kategori</label></td>
			<td> : <select class="jeniskategoripengeluaran" style="width: 250px;" required>
				   <option value="1">Natal</option>
				   </select><input class="trick" id="jeniskategoripengeluaran" tabindex="-1">
			</td> 
		</tr>
		<tr>
			<td class="tdlabel"><label id="lbldetailkategori">Detail Kategori</label></td>
			<td> : <select class="detailkategoripengeluaran" style="width: 250px;" required>
				   <option value="1">Hias Pohon Natal</option>
				   </select><input class="trick" id="detailkategoripengeluaran" tabindex="-1">
			</td> 
		</tr>
	</table>
	<input type="submit" value="Tambah" id="btntambahpengeluaran" style="width: 100px; height:20px;" class="btn btn-default btn-success">
	<button value="Batal" id="btnbatalpengeluaran" style="width: 100px;height:20px;" class="btn btn-default btn-success">Batal</Button>	
</form>
