<div id="divTitle">
	<label class="lblTitle">EDIT TOKO</label>
</div>
<form id="formEditToko" name="formEditToko" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabel">Kode Toko</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="editKodeToko" name="editKodeToko" spellcheck="false" placeholder="Kode Toko" disabled style="width: 246px;" required style="this.style.color='#f00'"/></td>
		</tr>
		<tr>
			<td class="kolomLabel">Nama Toko</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="editNamaToko" name="editNamaToko" spellcheck="false" placeholder="Nama Toko" style="width: 246px;" required /></td>
		</tr>
		<tr style="vertical-align: top;">
			<td class="kolomLabel">Alamat Toko</td>
			<td class="kolomTitikDua"> : </td>
			<td style="padding-bottom: 0px;"><textarea style="width: 244px; height: 70px;" id ="editAlamatToko" name ="editAlamatToko" ></textarea></td>
		</tr>
		<tr>
			<td class="kolomLabel">No Telepon</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="number" id="editNoTelepon" name="editNoTelepon" spellcheck="false" placeholder="Edit No Telepon" style="width: 246px;" required/></td>
		</tr>
		<tr>
			<td class="kolomLabel">CP Toko</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="editCpToko" name="editCpToko" spellcheck="false" placeholder="Edit CP Toko" style="width: 246px;" required/></td>
		</tr>
	</table>
	<input type="submit" value="Edit" id="btnEditToko" name="editToko" >
	<button id="btnBatalEditToko" name="btnBatalEditToko" >Batal</button>
	<input type="text" id="editKodeToko2" name="editKodeToko2" style="visibility:hidden";>
</form>

