<?php
// require('fpdf.php');
require('FPDF/WriteHTML.php');
require('koneksi2.php');
require('koneksi.php');

$sql_profil = "select * from tbl_profil_gereja";
$hasil_profil = mysql_query($sql_profil);
$baris_profil = mysql_fetch_array($hasil_profil);
$adaTidak = mysql_num_rows($hasil_profil);

$filePath = "PDF_Data.pdf";
$pdf = new PDF_HTML();
$pdf->AddPage('l');
$pdf->SetAutoPageBreak(false);
$pdf->SetFont('Arial', '', 10);
$pdf->Image('image/'.$baris_profil['logo_gereja'],18,13,20);

//Nama Gereja
$pdf->Ln(-10);
$pdf->Cell(35, 30, '', 0);
$pdf->Cell(115, 30, $baris_profil['nama_gereja'], 0);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(40, 30, 'Tanggal: '.date('d-m-Y').'', 0);
$pdf->Ln(5);
//Alamat Gereja
$pdf->Cell(35, 30, '', 0);
$pdf->Cell(100, 30, $baris_profil['alamat_gereja'], 0);
$pdf->Ln(5);
//email Gereja
$pdf->Cell(35, 30, '', 0);
$pdf->Cell(100, 30, $baris_profil['email_gereja'], 0);
$pdf->Ln(5);
//nomor telepon Gereja
$pdf->Cell(35, 30, '', 0);
$pdf->Cell(100, 30, $baris_profil['nomer_telepon_gereja'], 0);
$pdf->Ln(20);

if(isset($_GET["pdffor"])){
	$pdffor = $_GET['pdffor'];
	if ($pdffor == "bidang"){
		//set TH
		$pdf->SetFont('Arial', 'B', 11);
		$pdf->Cell(70, 8, '', 0);
		$pdf->Cell(100, 8, 'Data Bidang', 0);
		$pdf->Ln(10);
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(10, 8, 'No', 0,0,'C');
		$pdf->Cell(25, 8, 'Kode Bidang', 0,0,'C');
		$pdf->Cell(30, 8, 'Nama Bidang', 0,0,'C');
		$pdf->Cell(30, 8, 'Rekening', 0,0,'C');
		$pdf->Cell(25, 8, 'Pengurus 1', 0,0,'C');
		$pdf->Cell(25, 8, 'Pengurus 2', 0,0,'C');
		$pdf->Cell(25, 8, 'Pengurus 3', 0,0,'C');
		$pdf->Cell(20, 8, 'Tgl dibentuk', 0,0,'C');
		$pdf->Cell(70, 8, 'Deskripsi', 0,0,'C');
		$pdf->Cell(10, 8, 'Status', 0,0,'C');




		$pdf->Ln(8);
		$pdf->SetFont('Arial', '', 8);

		//Set untuk pagebreak
		$height_of_cell = 30; // mm

		//SET CONTENT
		$query = mysql_query("SELECT * FROM tbl_bidang");
		$item = 0;
		$pdf->SetFillColor(255,255,255);	
		$fill = false;
		while($hasil = mysql_fetch_array($query)){
			$item = $item+1;
			$x = $pdf->GetX();
			$y = $pdf->GetY();

			if (($y + $height_of_cell) >= 297) {
			    $pdf->AddPage();
			    $y = 10; 		// should be your top margin
			}

			$pdf->MultiCell(10, 8, $item,0,'C',$fill);
			$pdf->SetXY($x + 10, $y);
			$pdf->MultiCell(25, 8,$hasil['kode_bidang'],0,'C',$fill);
			$pdf->SetXY($x + 35, $y);
			$pdf->MultiCell(30, 8, $hasil['nama_bidang'],0,'C',$fill);
			$pdf->SetXY($x + 65, $y);
			$pdf->MultiCell(30, 8, $hasil['kode_jenis_akun'],0,'C',$fill);
			$pdf->SetXY($x + 95, $y);
			
			$p1a = mysql_query("SELECT nama_penanggungjawab FROM tbl_penanggungjawab  where kode_penanggungjawab = '".$hasil['pengurus_1']."'");
        	$p1aa = mysql_fetch_array($p1a); 
        	$p1 = $p1aa['nama_penanggungjawab'];
        	if($p1==""){
        		$p1="Tidak ada";
        	}		 
			$p2a = mysql_query("SELECT nama_penanggungjawab FROM tbl_penanggungjawab  where kode_penanggungjawab = '".$hasil['pengurus_2']."'");
        	$p2aa = mysql_fetch_array($p2a); 
        	$p2 = $p2aa['nama_penanggungjawab'];

			if($p2==""){
        		$p2="Tidak ada";
        	}		 
			$p3a = mysql_query("SELECT nama_penanggungjawab FROM tbl_penanggungjawab  where kode_penanggungjawab = '".$hasil['pengurus_3']."'");
        	$p3aa = mysql_fetch_array($p3a); 
        	$p3 = $p3aa['nama_penanggungjawab'];
        	if($p3==""){
        		$p3="Tidak ada";
        	}
		 
			
			
			$pdf->MultiCell(25, 8, $p1,0,'C',$fill);
			$pdf->SetXY($x + 120, $y);
			$pdf->MultiCell(25, 8, $p2,0,'C',$fill);
			$pdf->SetXY($x + 145, $y);
			$pdf->MultiCell(25, 8, $p3,0,'C',$fill);
			$pdf->SetXY($x + 170, $y);
			$tgl = substr($hasil['tgl_dibentuk'], 8);
            $bulan = substr($hasil['tgl_dibentuk'], 5,2);
            $tahun = substr($hasil['tgl_dibentuk'], 0,4);
            $date = $tgl."-".$bulan."-".$tahun;
   						
			$pdf->MultiCell(20, 8, $date,0,'C',$fill);
			$pdf->SetXY($x + 190, $y);
			//$pdf->SetXY($x + 300, $y);
			
			$pdf->MultiCell(70, 8, $hasil['deskripsi'],0,'C',$fill);
			$pdf->SetXY($x + 260, $y);
			if($hasil['status'] == 1) $status = "Aktif";
			else $status = "Tidak Aktif";
			$pdf->MultiCell(10, 8, $status,0,'C',$fill);
			$pdf->SetXY($x + 270, $y);
			
			$pdf->Ln(16);
			$fill = !$fill;
		}	
	} 
	else if($pdffor == "komisi"){
			//set TH
		$pdf->SetFont('Arial', 'B', 11);
		$pdf->Cell(70, 8, '', 0);
		$pdf->Cell(100, 8, 'Data Komisi', 0);
		$pdf->Ln(10);
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(10, 8, 'No', 0,0,'C');
		$pdf->Cell(30-10, 8, 'Kode Komisi', 0,0,'C');
		$pdf->Cell(40, 8, 'Nama Komisi', 0,0,'C');
		$pdf->Cell(30-10, 8, 'Kode Bidang', 0,0,'C');		
		$pdf->Cell(30, 8, 'Rekening', 0,0,'C');
		$pdf->Cell(25, 8, 'Pengurus 1', 0,0,'C');
		$pdf->Cell(25, 8, 'Pengurus 2', 0,0,'C');
		$pdf->Cell(25, 8, 'Pengurus 3', 0,0,'C');
		$pdf->Cell(20, 8, 'Tgl dibentuk', 0,0,'C');
		$pdf->Cell(50, 8, 'Deskripsi', 0,0,'C');
		$pdf->Cell(10, 8, 'Status', 0,0,'C');




		$pdf->Ln(8);
		$pdf->SetFont('Arial', '', 8);

		//Set untuk pagebreak
		$height_of_cell = 30; // mm

		//SET CONTENT
		$query = mysql_query("SELECT * FROM tbl_komisi");
		$item = 0;
		$pdf->SetFillColor(255,255,255);	
		$fill = false;
		while($hasil = mysql_fetch_array($query)){
			$item = $item+1;
			$x = $pdf->GetX();
			$y = $pdf->GetY();

			if (($y + $height_of_cell) >= 297) {
			    $pdf->AddPage();
			    $y = 10; 		// should be your top margin
			}

		
		
			$pdf->MultiCell(10, 8, $item,0,'C',$fill);
			$pdf->SetXY($x + 10, $y);
			$pdf->MultiCell(30-10, 8,$hasil['kode_komisi'],0,'C',$fill);
			$pdf->SetXY($x + 40-10, $y);
			$pdf->MultiCell(40, 8, $hasil['nama_komisi'],0,'C',$fill);
			$pdf->SetXY($x + 80-10, $y);
			$pdf->MultiCell(30-10, 8,$hasil['kode_bidang'],0,'C',$fill);
			$pdf->SetXY($x + 110-10-10, $y);
			
			$pdf->MultiCell(30, 8, $hasil['kode_jenis_akun'],0,'C',$fill);
			$pdf->SetXY($x + 140-10-10, $y);
			
			$p1a = mysql_query("SELECT nama_penanggungjawab FROM tbl_penanggungjawab  where kode_penanggungjawab = '".$hasil['pengurus_1']."'");
        	$p1aa = mysql_fetch_array($p1a); 
        	$p1 = $p1aa['nama_penanggungjawab'];
        	if($p1==""){
        		$p1="Tidak ada";
        	}		 
			$p2a = mysql_query("SELECT nama_penanggungjawab FROM tbl_penanggungjawab  where kode_penanggungjawab = '".$hasil['pengurus_2']."'");
        	$p2aa = mysql_fetch_array($p2a); 
        	$p2 = $p2aa['nama_penanggungjawab'];

			if($p2==""){
        		$p2="Tidak ada";
        	}		 
			$p3a = mysql_query("SELECT nama_penanggungjawab FROM tbl_penanggungjawab  where kode_penanggungjawab = '".$hasil['pengurus_3']."'");
        	$p3aa = mysql_fetch_array($p3a); 
        	$p3 = $p3aa['nama_penanggungjawab'];
        	if($p3==""){
        		$p3="Tidak ada";
        	}
		 
			
			$pdf->MultiCell(25, 8, $p1,0,'C',$fill);
			$pdf->SetXY($x + 165-10-10, $y);
			$pdf->MultiCell(25, 8, $p2,0,'C',$fill);
			$pdf->SetXY($x + 190-10-10, $y);
			$pdf->MultiCell(25, 8, $p3,0,'C',$fill);
			$pdf->SetXY($x + 215-10-10, $y);
			$tgl = substr($hasil['tgl_dibentuk'], 8);
            $bulan = substr($hasil['tgl_dibentuk'], 5,2);
            $tahun = substr($hasil['tgl_dibentuk'], 0,4);
            $date = $tgl."-".$bulan."-".$tahun;
   						
			$pdf->MultiCell(20, 8, $date,0,'C',$fill);
			$pdf->SetXY($x + 235-10-10, $y);
			//$pdf->SetXY($x + 300, $y);
			
			$pdf->MultiCell(50, 8, $hasil['deskripsi'],0,'C',$fill);
			$pdf->SetXY($x + 265, $y);
			if($hasil['status'] == 1) $status = "Aktif";
			else $status = "Tidak Aktif";
			$pdf->MultiCell(10, 8, $status,0,'C',$fill);
			$pdf->SetXY($x + 275, $y);
			
			$pdf->Ln(16);
			$fill = !$fill;
		}	
	}
		else if($pdffor == "nota"){
			//set TH
		$pdf->SetFont('Arial', 'B', 11);
		$pdf->Cell(70, 8, '', 0);
		$pdf->Cell(100, 8, 'Data Nota', 0);
		$pdf->Ln(10);
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(10, 8, 'No', 0,0,'C');
		$pdf->Cell(30, 8, 'Kode Nota', 0,0,'C');
		$pdf->Cell(30, 8, 'Kode Toko', 0,0,'C');
		$pdf->Cell(30+10, 8, 'Debet', 0,0,'C');		
		$pdf->Cell(30, 8, 'Tgl Pengeluaran', 0,0,'C');
		$pdf->Cell(30, 8, 'Jumlah', 0,0,'C');
		$pdf->Cell(40, 8, 'Pelaksana', 0,0,'C');
		$pdf->Cell(30+10, 8, 'Waktu Input', 0,0,'C');
		$pdf->Cell(30, 8, 'Kode Admin', 0,0,'C');
		



		$pdf->Ln(8);
		$pdf->SetFont('Arial', '', 8);

		//Set untuk pagebreak
		$height_of_cell = 30; // mm

		//SET CONTENT
		$query = mysql_query("SELECT * FROM tbl_nota");
		$item = 0;
		$pdf->SetFillColor(255,255,255);	
		$fill = false;
		while($hasil = mysql_fetch_array($query)){
			$item = $item+1;
			$x = $pdf->GetX();
			$y = $pdf->GetY();

			if (($y + $height_of_cell) >= 297) {
			    $pdf->AddPage();
			    $y = 10; 		// should be your top margin
			}

		
		
				
			$pdf->MultiCell(10, 8, $item,0,'C',$fill);
			$pdf->SetXY($x + 10, $y);
			$pdf->MultiCell(30, 8,$hasil['kode_nota'],0,'C',$fill);
			$pdf->SetXY($x + 40, $y);
			$pdf->MultiCell(30, 8, $hasil['kode_toko'],0,'C',$fill);
			$pdf->SetXY($x + 70, $y);
			$pdf->MultiCell(30+10, 8,$hasil['kode_jenis_akun'],0,'C',$fill);
			$pdf->SetXY($x + 100+10, $y);
			
			$tgl = substr($hasil['tgl_pengeluaran'], 8);
            $bulan = substr($hasil['tgl_pengeluaran'], 5,2);
            $tahun = substr($hasil['tgl_pengeluaran'], 0,4);
            $date = $tgl."-".$bulan."-".$tahun;
   						
			$pdf->MultiCell(30, 8, $date,0,'C',$fill);
			$pdf->SetXY($x + 130+10, $y);			
			
			$pdf->MultiCell(30, 8, $hasil['jumlah'],0,'C',$fill);
			$pdf->SetXY($x + 160+10, $y);
			$pdf->MultiCell(40, 8, $hasil['pelaksana'],0,'C',$fill);
			$pdf->SetXY($x + 200+10, $y);
			
			$tgl = substr($hasil['waktu_input'], 8,2);
            $bulan = substr($hasil['waktu_input'], 5,2);
            $tahun = substr($hasil['waktu_input'], 0,4);
            $waktu = substr($hasil['waktu_input'], 10);
            $waktuInput = $tgl."-".$bulan."-".$tahun."".$waktu;

   
			$pdf->MultiCell(30+10, 8, $waktuInput,0,'C',$fill);
			$pdf->SetXY($x + 230+10+10, $y);
			//$pdf->SetXY($x + 300, $y);
			
			$pdf->MultiCell(30, 8, $hasil['kode_admin'],0,'C',$fill);
			$pdf->SetXY($x + 260+10+10, $y);
			
			$pdf->Ln(16);
			$fill = !$fill;
		}	
	}

}

$pdf->Output($filePath,'I');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . $filePath);
header('location:'.$filePath);	

?>