<?php
session_start();
include("koneksi.php");

		$sql;
		$jmlKredit = 0;
		$jmlDebet = 0;

		
			$sql = "select * from ((select  (select MAX(kode_transaksi) FROM tbl_transaksi t3 WHERE t3.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi) hidden,999999999 kode_transaksi, tbl_bukti_transaksi.kode_bukti_transaksi, tbl_bukti_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_bukti_transaksi.keterangan, tbl_bukti_transaksi.jumlah, tbl_bukti_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
								   from tbl_bukti_transaksi
								   inner join tbl_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi)
								   UNION
								   (select  tbl_transaksi.kode_transaksi hidden, tbl_transaksi.kode_transaksi, tbl_transaksi.kode_bukti_transaksi, tbl_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_transaksi.uraian, tbl_transaksi.jumlah, tbl_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
							 	   from tbl_transaksi
								   inner join tbl_bukti_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi))
					AS transaksi_data
					NATURAL JOIN tbl_jenis_akun
					NATURAL JOIN tbl_gol_akun
					NATURAL JOIN tbl_sub_gol_akun
					WHERE tanggal_bukti_transaksi <= '".$_POST['sampaiJurnalMutasi']."'
					AND transaksi_data.waktu_posting != 'NULL'
					order by hidden ASC, kode_transaksi ASC, kode_bukti_transaksi ASC";

			echo "<thead>";
			echo "<tr>";
				echo "<td id='tipeMutasi' colspan='2' rowspan='1' >";
					echo "Mutasi";
				echo "</td>";
				echo "<td id='noRekDK' rowspan='2' >";
					echo "No.Rek D/K";
				echo "</td>";
				echo "<td id='uraianJurnalMutasi' rowspan='2' >";
					echo "Uraian";
				echo "</td>";
				echo "<td id='debet' rowspan='2' >";
					echo "Debet Rp";
				echo "</td>";
				echo "<td id='kredit' rowspan='2' >";
					echo "Kredit Rp";
				echo "</td>";
			echo "</tr>";
			echo "<tr>";
					echo "<td id='noWarkat'>";
						echo "No.Warkat";
					echo "</td>";
					echo "<td id='tglWarkat'>";
						echo "Tgl.Warkat";
					echo "</td>";
				echo "</tr>";

			echo "</thead>";

			$hasil = mysql_query($sql);
			$saldo = 0 ;
			
			if($hasil == FALSE) { 
		   	 	die(mysql_error());
			}
		
		echo "<tbody>";
		$noUrut = 1;


			while ($row3 = mysql_fetch_array($hasil)){
				if($row3['tipe_transaksi'] == 'DEBET'){
					$saldo -= $row3['jumlah'];
									
					if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mulaiJurnalMutasi']))){
						echo "<tr>";
						echo "<td id= 'noWarkat' >";
							echo $row3['kode_bukti_transaksi'];
						echo "</td>";
						echo "<td id= 'tglWarkat' >";
							$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
							echo $dateTransaksi;
						echo "</td>";
						echo "<td id= 'noRekDK' >";
							echo $row3['kode_jenis_akun'];
						echo "</td>";
						echo "<td id='uraianJurnalMutasi'>";
							echo $row3['keterangan'];
						echo "</td>";
						echo "<td id='debet''>";
							echo "Rp. ".number_format($row3['jumlah'], 0, ".", ".");
						echo "</td>";
							echo "<td id='kredit'>";
							echo "&nbsp;";
						echo "</td>";
						echo "</tr>";
						$noUrut += 1;
					}
				}else if($row3['tipe_transaksi'] == 'KREDIT'){
					$saldo += $row3['jumlah'];
					
					if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mulaiJurnalMutasi']))){				
						echo "<tr>";
						echo "<td id= 'noWarkat' >";
							echo $row3['kode_bukti_transaksi'];
						echo "</td>";
						echo "<td id= 'tglWarkat' >";
							$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
							echo $dateTransaksi;
						echo "</td>";
						echo "<td id= 'noRekDK' >";
							echo $row3['kode_jenis_akun'];
						echo "</td>";
						echo "<td id='uraianJurnalMutasi'>";
							echo $row3['keterangan'];
						echo "</td>";
						echo "<td id='debet''>";
							echo "&nbsp;";
						echo "</td>";
						echo "<td id='kredit'>";
							echo "Rp. ".number_format($row3['jumlah'], 0, ".", ".");
						echo "</td>";
						echo "</tr>";
						$noUrut += 1;
					}
				}
			}
		echo "</tbody>";
?>