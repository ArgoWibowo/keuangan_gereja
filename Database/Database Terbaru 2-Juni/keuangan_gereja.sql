-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2016 at 08:52 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `keuangan_gereja`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `kode_admin` char(8) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `kode_terpilih` enum('TIDAKADA','KOMISI','BIDANG') NOT NULL,
  `kode` char(8) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `pertanyaan_pemulihan` enum('Siapa nama ayah ada?','Siapa nama ibu anda?','Siapa guru Sekolah Minggu yang paling anda sukai?','Siapa nama paman yang paling anda sukai?','Dimana anda lahir?','Berapa 3 angka terakhir nomer telepon pertama anda?','Dimana ayah anda lahir?','Ayat paling anda sukai?','Tokoh Alkitab yang paling menginspirasi?') NOT NULL,
  `jawaban_pertanyaan_pemulihan` varchar(30) NOT NULL,
  `alamat_admin` varchar(90) NOT NULL,
  `no_telepon_admin` varchar(15) NOT NULL,
  `email_admin` varchar(50) NOT NULL,
  `otoritas_administrasi` enum('TIDAKADA','BACA','TULIS','BACATULIS') NOT NULL,
  `otoritas_pemasukan` enum('TIDAKADA','BACA','TULIS','BACATULIS') NOT NULL,
  `otoritas_pengeluaran` enum('TIDAKADA','BACA','TULIS','BACATULIS') NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`kode_admin`, `nama_admin`, `kode_terpilih`, `kode`, `password`, `pertanyaan_pemulihan`, `jawaban_pertanyaan_pemulihan`, `alamat_admin`, `no_telepon_admin`, `email_admin`, `otoritas_administrasi`, `otoritas_pemasukan`, `otoritas_pengeluaran`, `status`) VALUES
('admin', 'admin', 'TIDAKADA', NULL, '4153a94abe5b151f0dbd700632515979ed3a00359305c21863a39cf82ccaa3e9', 'Dimana anda lahir?', 'yogyakarta', 'Jln Wahidin Sudirohusodo No 40 Yogyakarta', '0274513570', 'gkj.gondokusuman@yahoo.com', 'BACATULIS', 'BACATULIS', 'BACATULIS', 1),
('Bumega', 'Ersti Wati Meganingsih', 'TIDAKADA', NULL, 'ff254e19c243d43cd2c94486c51b380073087f5dcecd6b17e1da3004b42ef18f', 'Dimana anda lahir?', 'Yogyakarta', 'Klitren Lor GK.III/472\nYogyakarta\n', '085292381387', 'gkjgondokusuman@yahoo.com', 'BACATULIS', 'BACATULIS', 'BACATULIS', 1),
('hantoro', 'tri hantoro djoko sp', 'TIDAKADA', NULL, 'e966687d4370e57efa947468044c3e1d32b88d875ff5e071c984a7c3fbbe4339', 'Dimana anda lahir?', 'klaten', 'Nanggulan RT 07/RW 17 No. 55A\nMaguwoharjo, Depok Sleman\nYogyakarta', '081328896443', 'handenavan@gmail.com', 'BACATULIS', 'BACATULIS', 'BACATULIS', 1),
('lina', 'fransiska herlina sri hargiyanti', 'TIDAKADA', NULL, '8a890a242ca3d022a1865e4ae1894ac6c8a8335362cef38bffe0e6f2a33fc9bb', 'Dimana anda lahir?', 'sleman', 'mancasan pandowoharjo sleman\n', '08175457464', 'lina_gtuloh@yahoo.com', 'BACATULIS', 'BACATULIS', 'BACATULIS', 1),
('wawan', 'Tjandrayana Setiawan', 'TIDAKADA', NULL, '9eb25a26cf84f6204a71eda95fc548b07c3538c339a5c0eebb95fd0e4751320b', 'Tokoh Alkitab yang paling menginspirasi?', 'daniel', 'UKDW', '089647875840', 'tjandrayana.setiawan@ti.ukdw.ac.id', 'TIDAKADA', 'BACATULIS', 'TIDAKADA', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bidang`
--

CREATE TABLE IF NOT EXISTS `tbl_bidang` (
  `kode_bidang` char(7) NOT NULL,
  `nama_bidang` varchar(50) NOT NULL,
  `kode_jenis_akun` char(7) DEFAULT NULL,
  `pengurus_1` char(8) DEFAULT NULL,
  `pengurus_2` char(8) DEFAULT NULL,
  `pengurus_3` char(8) DEFAULT NULL,
  `tgl_dibentuk` date NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bukti_transaksi`
--

CREATE TABLE IF NOT EXISTS `tbl_bukti_transaksi` (
  `kode_bukti_transaksi` char(8) NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `tipe_transaksi` enum('DEBET','KREDIT') NOT NULL,
  `jumlah` int(15) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `tanggal_bukti_transaksi` date NOT NULL,
  `bendahara` char(8) NOT NULL,
  `penerima` char(8) NOT NULL,
  `penyetor` char(8) NOT NULL,
  `waktu_posting` timestamp NULL DEFAULT NULL,
  `waktu_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kode_admin` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_bukti_transaksi`
--

INSERT INTO `tbl_bukti_transaksi` (`kode_bukti_transaksi`, `kode_jenis_akun`, `tipe_transaksi`, `jumlah`, `keterangan`, `tanggal_bukti_transaksi`, `bendahara`, `penerima`, `penyetor`, `waktu_posting`, `waktu_input`, `kode_admin`) VALUES
('1012016', 'D101.03', 'KREDIT', 61600, 'Sisa KC2', '2016-01-09', 'B102', 'B101', 'ADM 2', NULL, '2016-05-16 03:21:44', 'admin'),
('KC3', 'D101.01', 'KREDIT', 2000000, 'BON KC 3', '2016-01-09', 'B101', 'B102', 'ADM 2', NULL, '2016-05-16 03:15:42', 'admin'),
('M0002', 'D101.02', 'DEBET', 77802200, 'Persembahan Malam Tahun Baru 31/12/2015, Tahun Baru 1/1/2016 & Persembahan Sabtu-Minggu tgl 2-3 Januari 2016', '2016-01-04', 'B101', 'BANK', 'B104', '2016-06-02 03:34:23', '2016-05-19 04:41:23', 'Bumega'),
('M01/1/16', 'D203.02', 'DEBET', 420000, 'Persmbahan Mirunggan via rek giro tgl.1-1-16', '2016-01-01', 'B103', 'ADM 2', 'B103', '2016-05-16 04:01:57', '2016-05-16 04:00:53', 'Bumega');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gol_akun`
--

CREATE TABLE IF NOT EXISTS `tbl_gol_akun` (
  `kelompok_akun` enum('D','K') NOT NULL,
  `kode_golongan` char(2) NOT NULL,
  `nama_golongan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gol_akun`
--

INSERT INTO `tbl_gol_akun` (`kelompok_akun`, `kode_golongan`, `nama_golongan`) VALUES
('D', 'D1', 'K A S'),
('D', 'D2', 'BANK'),
('D', 'D3', 'PINJAMAN'),
('D', 'D4', 'UANG MUKA'),
('D', 'D5', 'INVENTARIS'),
('D', 'D8', 'KEGIATAN BADAN PEMBANTU MAJELIS'),
('D', 'D9', 'KEGIATAN KANTOR GEREJA'),
('K', 'K1', 'TITIPAN DAN CADANGAN'),
('K', 'K7', 'AKTIVA BERSIH'),
('K', 'K9', 'KEGIATAN PERSEMBAHAN');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jemaat`
--

CREATE TABLE IF NOT EXISTS `tbl_jemaat` (
  `kode_jemaat` char(8) NOT NULL,
  `nama_jemaat` varchar(50) NOT NULL,
  `wilayah` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jemaat`
--

INSERT INTO `tbl_jemaat` (`kode_jemaat`, `nama_jemaat`, `wilayah`) VALUES
('01', 'I Trasip', 'I'),
('02', 'Jantan Vibrianto', 'VIII'),
('02 TIM A', 'Sri Astuti Suratno', 'TIM A'),
('02 VII', 'Totok Mujoto', 'VII'),
('03', 'Wahyuningsih', 'X'),
('100', 'Darmawan', 'VII'),
('102', 'Wibowo Hadi', 'IX'),
('102 W.B', 'Edi Asri Handoko', 'W.B'),
('21', 'Wasito Hadi A.', 'WII Sel'),
('24', 'E. Retno Watyas', 'X'),
('25', 'Rukmono W', 'XI'),
('26', 'Tri Budiono', 'V'),
('28', 'Tasmidi', 'TIM A'),
('33 VIII', 'Dyah Wakitoningsih', 'VIII'),
('33 X', 'Kel. Soehadi', 'X'),
('35', 'Retni Madu H.', 'III'),
('38', 'Dewi Paramitasari', 'VII'),
('4', 'Endang Sri P.', 'W. Sel'),
('45', 'Prabowo Hadi', 'IX'),
('52', 'Tri Murdiyati', 'I'),
('56', 'Stevanus Danan N.', 'W. Sel'),
('57', 'Hana Dewi Pratiwi', 'W. Sel'),
('58', 'Eko Subagyo', 'V'),
('58 W.Sel', 'Gideon Rizal G', 'W. Sel'),
('63', 'Ris Kusbianto', 'V'),
('66', 'S. Rini S', 'TA'),
('71', 'Ester Rika Kasiyem', 'XIV'),
('73', 'Suparmi Soenarno', 'X'),
('78', 'Ery Utami', '10'),
('78 TIM A', 'Nurcahyo Nugroho', 'TIM A'),
('82', 'Arvina', 'Brt'),
('83', 'Dadit C', 'Brt'),
('87', 'Budianto', 'I'),
('92', 'Hendrayana Mandala', 'TB'),
('95', 'Rigen Pratitisari', 'VII'),
('95 W. B', 'Widiatmo', 'W. B'),
('Jemaat1', 'NN', 'NN'),
('Jemaat2', 'Tri mardiyati', '1'),
('Jemaat3', 'ANT', 'TA'),
('Jemaat4', 'YT', 'TA'),
('Jemaat5', 'Madiani Dyah Natalia', 'BRT'),
('M/US', 'M/US', 'NN'),
('U.S.A.', 'U.S.A.', 'U.S.A.'),
('US', 'US', 'NN');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_akun`
--

CREATE TABLE IF NOT EXISTS `tbl_jenis_akun` (
  `kode_sub_gol_akun` char(4) NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `nama_kode_jenis_akun` varchar(100) NOT NULL,
  `jenis_partisipan` enum('Individu','Non Individu') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jenis_akun`
--

INSERT INTO `tbl_jenis_akun` (`kode_sub_gol_akun`, `kode_jenis_akun`, `nama_kode_jenis_akun`, `jenis_partisipan`) VALUES
('D101', 'D101.01', 'Kas Bendahara', NULL),
('D101', 'D101.02', 'Kas Setoran Persembahan', NULL),
('D101', 'D101.03', 'Kas Kecil', NULL),
('D101', 'D101.04', 'Biaya Kegiatan Bidang/Komisi Lainnya', NULL),
('D201', 'D201.01', 'Bank MANDIRI', NULL),
('D201', 'D201.02', 'BRI', NULL),
('D201', 'D201.03', 'Bank Pembanguan Daerah', NULL),
('D201', 'D201.04', 'Bank Mandiri, No.AD449238 Tgl.20-02-2014 Dana Tanah Belger', NULL),
('D201', 'D201.05', 'BCA, No.AH667747 Tgl.11-02-2014 Dana Tanah Belger', NULL),
('D201', 'D201.06', 'BCA, No.AH667748 Tgl.11-02-2014 Dana Tanah Belger', NULL),
('D201', 'D201.07', 'BNI, No.AA276470 David Rubingan, Dana Abadi Reksa Putra', NULL),
('D202', 'D202.01', 'Bank Mandiri, Cadangan Dana Kemandirian (Pdt. David R.)', NULL),
('D202', 'D202.02', 'Tabungan Bank BRI Tampung Bunga Deposito (Pdt. Siswadi, S.Si)', NULL),
('D202', 'D202.03', 'Bank Mandiri, Pdt.Yudo Aster Daniel, Dana Kemandirian', NULL),
('D202', 'D202.05', 'Bank Mandiri, Cadangan Dana Emiritus', NULL),
('D202', 'D202.06', 'Bank Mandiri, Cadangan Dana Tanah Belger', NULL),
('D202', 'D202.07', 'Bank BPD DIY Senopati-Tampung Bunga Deposito', NULL),
('D202', 'D202.08', 'Bank BPD DIY Senopati, penampungan gaji karyawan', NULL),
('D202', 'D202.09', 'GKJ Gondokusuman, Tabungan BRI BritAma', NULL),
('D202', 'D202.10', 'BRI Dana Sawo Kembar Emergency (Pdt.Seno AN)', NULL),
('D202', 'D202.11', 'BCA, Cadangan Dana Pembangunan di Tanah Belger', NULL),
('D202', 'D202.12', 'BRI Tabungan BANSOSKOM', NULL),
('D202', 'D202.13', 'BRI -Tanah Makam, Rudjito QQ GKJ Gondokusuman', NULL),
('D202', 'D202.14', ' BRI -SOEWONDO QQ PANTI WERDA*DON*GKJ GONDOKUSUMAN', NULL),
('D202', 'D202.15', 'BRI -Soewondo QQ Panti Werda GKJ GK*Persembahan*', NULL),
('D203', 'D203.02', 'Giro BRI Katamso', NULL),
('D301', 'D301.01', 'Modal Pengelola Pinjaman', NULL),
('D301', 'D301.02', 'Christina Wijayanti, Pinjm.Penyelesaian Urusan Tanah di Wonosobo', 'Individu'),
('D301', 'D301.03', 'Christina Wijayanti, pinjam Persembhn Tanah BelGer', 'Individu'),
('D302', 'D302.02', 'Pinjaman dana kpd STAK MARTURIA via Bapelklas XXVIII Yk.Selatan', NULL),
('D302', 'D302.10', 'Pinjaman Lainnya', NULL),
('D401', 'D401.01', 'Uang Muka Untuk Pelayanan Diakonia', NULL),
('D401', 'D401.05', 'Uang Muka Bayar Listrik dan Air beban Kantor dan Pendeta', NULL),
('D401', 'D401.06', 'Uang Muka Bayar Telepon beban Kantor dan Pendeta', NULL),
('D401', 'D401.10', 'Uang muka biaya-biaya keperluan Kantor Gereja', NULL),
('D401', 'D401.11', 'Uang muka Biaya Hidup dan Gaji Tenaga GKJ', NULL),
('D401', 'D401.13', 'Uang Muka Kegiatan Bidang/Komis', NULL),
('D501', 'D501.01', 'Tanah Kantor & Toko TPK', NULL),
('D501', 'D501.02', 'Tanah Gedung Gereja', NULL),
('D501', 'D501.03', 'Tanah Rumah Pastori', NULL),
('D501', 'D501.04', 'Tanah Rumah Asrama', NULL),
('D501', 'D501.05', 'Tanah Kosong', NULL),
('D501', 'D501.06', 'Tanah Makam', NULL),
('D502', 'D502.01', 'Bangunan kantor & Toko TPK', NULL),
('D502', 'D502.02', 'Bangunan Gereja', NULL),
('D502', 'D502.03', 'Bangunan Rmh Pastori', NULL),
('D502', 'D502.04', 'Bangunan Rumah Asrama', NULL),
('D503', 'D503.01', 'Kendaraan Mobil', NULL),
('D503', 'D503.02', 'Sepeda Motor', NULL),
('D504', 'D504.01', 'Mesin Foto Copy', NULL),
('D504', 'D504.02', 'Mesin Risograph EZ-231 No.76740224, Kwits.18-7-2013', NULL),
('D504', 'D504.03', 'Komputer & Perlengkapannya', NULL),
('D504', 'D504.04', 'Air Condotion', NULL),
('D505', 'D505.01', 'Meja', NULL),
('D505', 'D505.02', 'Kursi', NULL),
('D505', 'D505.03', 'Almari', NULL),
('D506', 'D506.01', 'Peralatan Sound system', NULL),
('D506', 'D506.02', 'Alat Musik dan Perlengkapannya', NULL),
('D506', 'D506.03', 'Alat Musik Keroncong', NULL),
('D506', 'D506.04', 'Alat Musik Gamelan', NULL),
('D507', 'D507.01', 'Kamera', NULL),
('D507', 'D507.02', 'Komputer', 'Individu'),
('D507', 'D507.03', 'Handy & Walky Talky', NULL),
('D507', 'D507.04', 'Tripod dan Monopod', NULL),
('D507', 'D507.05', 'Power Supply', NULL),
('D507', 'D507.06', 'TV Monitor', NULL),
('D507', 'D507.07', 'Video-Audio Splitter', NULL),
('D507', 'D507.08', 'Video Switcher, Splitter, Converter', NULL),
('D507', 'D507.09', 'Tool Box', NULL),
('D507', 'D507.10', 'Kabel', NULL),
('D507', 'D507.11', 'Televisi & LCD Projector', NULL),
('D507', 'D507.12', 'Perlengkapan Multimedia Lainnya', NULL),
('D508', 'D508.01', 'Perlengkapan Persembahan & Busana', NULL),
('D508', 'D508.02', 'Perlengkapan Perjamuan Kudus', NULL),
('D508', 'D508.03', 'Perlengkapan Kotak Salib', NULL),
('D508', 'D508.04', ' Hiasan Dinding', NULL),
('D508', 'D508.05', 'Perlengkapan Meja', NULL),
('D508', 'D508.06', 'Perlengkapan Bunga', NULL),
('D508', 'D508.07', 'Perlengkapan Mimbar', NULL),
('D508', 'D508.08', 'Peralatan Makan & Dapur', NULL),
('D508', 'D508.99', 'Perlengkapan & Peralatan Lain-lain', NULL),
('D801', 'D801.01', 'Komisi Kebaktian', NULL),
('D801', 'D801.02', 'Komisi Pendukung Kebaktian', NULL),
('D801', 'D801.03', 'Komisi Seni & Budaya', NULL),
('D801', 'D801.04', 'Koordinasi dan Adm Bidang', NULL),
('D802', 'D802.01', 'Komisi Pekabaran Injil dan Komunikasi', NULL),
('D802', 'D802.02', 'Komisi Pendidikan', NULL),
('D802', 'D802.03', 'Komisi Pralenan', NULL),
('D802', 'D802.04', 'Komisi Sawokembar Emergency', NULL),
('D802', 'D802.05', 'BANSOSKOM', NULL),
('D802', 'D802.06', 'Koordinasi dan Administrasi Bidang', NULL),
('D803', 'D803.01', 'Komisi Pengkaderan', NULL),
('D803', 'D803.02', 'Komisi Anak', NULL),
('D803', 'D803.03', 'Komisi Remaja', NULL),
('D803', 'D803.04', 'Komisi Pemuda', NULL),
('D803', 'D803.05', 'Komisi Dewasa Muda', NULL),
('D803', 'D803.06', 'Komisi Pemberdayaan Kel. (PKM)', NULL),
('D803', 'D803.07', 'Komisi Warga Dewasa Wanita Jemaat', NULL),
('D803', 'D803.08', 'Komisi Adiyuswa', NULL),
('D803', 'D803.09', 'Koordinasi dan Adm. Bidang', NULL),
('D804', 'D804.01', 'Komisi Pembangunan', NULL),
('D804', 'D804.02', 'Komisi Pemeliharaan & Inventaris Gereja', NULL),
('D804', 'D804.03', 'Koordinasi dan Administrasi Bidang', NULL),
('D805', 'D805.01', 'Komisi Perencanaan Program dan Anggaran', NULL),
('D805', 'D805.02', 'Komisi Pengendalian Program', NULL),
('D805', 'D805.03', 'Komisi Litbang dan Evaluasi', NULL),
('D805', 'D805.04', 'Koordinasi dan Administrasi Bidang', NULL),
('D806', 'D806.01', 'Komisi Pengawasan dan Pemeriksaan Administrasi', NULL),
('D806', 'D806.02', 'Komisi Pengawasan dan Pemeriksaan Keuangan dan Logistik', NULL),
('D806', 'D806.03', 'Koordinasi dan Aministrasi Bidang Wasrik', NULL),
('D807', 'D807.01', 'PA Rekso Putra Bag Putra', NULL),
('D807', 'D807.02', 'PA Rekso Putra Bag Putri', NULL),
('D807', 'D807.03', 'Bantuan biaya hidup di Surabaya unt 2 org', 'Individu'),
('D808', 'D808.01', 'Biaya Pengurusan Surat Tanah', NULL),
('D808', 'D808.02', 'Biaya Kegiatan Bidang/Komisi Lainnya', NULL),
('D809', 'D809.01', 'Panitia Pemanggilan Pendeta', NULL),
('D809', 'D809.02', 'Tanah Makam', NULL),
('D809', 'D809.03', 'Pembangunan Tanah Belger', NULL),
('D809', 'D809.04', 'Peringatan HUT GKJ Gondokusuman', NULL),
('D809', 'D809.06', 'Bantuan untuk semua wilayah', NULL),
('D809', 'D809.08', '', NULL),
('D809', 'D809.09', '', NULL),
('D810', 'D810.01', 'Komisi Sosial', NULL),
('D810', 'D810.02', 'Komisi Bantuan dan Santunan', NULL),
('D810', 'D810.03', 'Komisi Panti Werda', NULL),
('D810', 'D810.04', 'Komisi Pastoral', NULL),
('D810', 'D810.05', 'Komisi Transformatif & PEJ', NULL),
('D810', 'D810.06', 'Koordinasi dan Adm Bidang Diakonia', NULL),
('D811', 'D811.01', 'Pemeliharaan Iman', NULL),
('D811', 'D811.02', 'Pastoral', NULL),
('D811', 'D811.03', 'Konseling', NULL),
('D811', 'D811.04', 'Nilai/Karakter', NULL),
('D811', 'D811.05', 'Koordinasi dan Administrasi Bidang', NULL),
('D901', 'D901.01', 'Alat Tulis Kantor', NULL),
('D901', 'D901.02', 'Alkitab, Nyanyian Rohani, Buku2 Lainnya', NULL),
('D901', 'D901.03', 'Biaya Surat Menyurat', NULL),
('D901', 'D901.04', 'Kertas Berbagai ukuran dan jenis', NULL),
('D901', 'D901.05', 'Biaya Fotocopy di luar kantor', NULL),
('D901', 'D901.06', 'Cetak Formulir dan Kalender', NULL),
('D901', 'D901.07', 'Cetak Warta Jemaat', NULL),
('D901', 'D901.08', 'Surat Kabar untuk Kantor dan Pendeta', NULL),
('D902', 'D902.01', 'Listrik', NULL),
('D902', 'D902.02', 'Telepon', NULL),
('D902', 'D902.03', 'Pajak Bumi dan Bangunan', NULL),
('D902', 'D902.04', 'Pajak Kendaraan Bermotor (STNK)', NULL),
('D902', 'D902.05', 'Bensin Kendaraan Dinas', NULL),
('D902', 'D902.06', 'Rumah Tangga Kantor Gereja', NULL),
('D902', 'D902.07', 'Tugas Pengamanan', NULL),
('D902', 'D902.08', 'Biaya Lembur Pegawai', NULL),
('D902', 'D902.09', 'Bunga Mimbar', NULL),
('D903', 'D903.01', 'Iuran Dana Kemandirian Klasis', NULL),
('D903', 'D903.02', 'Iuran Dana Abadi Klasis/ Sinode', NULL),
('D903', 'D903.03', 'Iuran PGI Wilayah/ Kotamadya', NULL),
('D903', 'D903.04', 'Perjamuan Kudus dan Kelengkapannya', NULL),
('D903', 'D903.05', 'Honor Organis Pemberkatan Nikah', NULL),
('D903', 'D903.06', 'Bantuan Antar Gereja', NULL),
('D903', 'D903.07', 'Bantuan Untuk Lembaga Kristen', NULL),
('D903', 'D903.08', 'Pelayanan Kasih Tanda Kasih', NULL),
('D903', 'D903.09', 'Sidang Majelis Gereja Terbuka (SMGT)', NULL),
('D903', 'D903.10', 'Rapat-rapat', NULL),
('D903', 'D903.11', 'Pisah Sambut Majelis', NULL),
('D903', 'D903.12', 'Retreat Majelis/ Karyawan', NULL),
('D904', 'D904.01', 'Biaya Hidup Pendeta', NULL),
('D904', 'D904.02', 'Gaji Tenaga Non Pendeta', NULL),
('D904', 'D904.03', 'Transport Khotbah Pendeta Tamu', NULL),
('D904', 'D904.04', 'Tukar Mimbar Klasis Sinode/PGI', NULL),
('D904', 'D904.05', 'Bantuan Transport', NULL),
('D904', 'D904.06', 'Bantuan untuk Pamulang', NULL),
('D904', 'D904.07', 'Bantuan Transport Guru Agama', NULL),
('D904', 'D904.08', 'Perjalanan Dinas', NULL),
('D904', 'D904.09', 'Honorarium Tenaga Tidak Tetap', 'Individu'),
('D905', 'D905.01', 'Pembayaran Uang Pensiun Pendeta', NULL),
('D905', 'D905.02', 'Pembayaran Uang Pensiun Non Pendeta', NULL),
('D905', 'D905.03', 'Voucher HP', 'Individu'),
('D905', 'D905.04', 'Perawatan Kesehatan', 'Individu'),
('D905', 'D905.05', 'Tunjangan Cuti Tahunan, Tunjangan Khusus', 'Individu'),
('D905', 'D905.06', 'Tunjangan Hari Raya Natal', 'Individu'),
('D905', 'D905.07', 'Pakaian dan Sepatu', 'Individu'),
('D905', 'D905.08', 'Biaya Pendidikan Anak Pendeta', 'Individu'),
('D905', 'D905.09', 'Pembayaran Premi Pensiun', NULL),
('D905', 'D905.10', 'Pengadaan Buku untuk Pendeta', NULL),
('D905', 'D905.11', 'Studi Lanjut Pendeta/Pelatihan/Seminar Pendeta', NULL),
('D905', 'D905.12', 'Asuransi bagi Pendeta yng Aktif', NULL),
('D906', 'D906.01', 'Pemeliharaan Gedung oleh Kantor', NULL),
('D906', 'D906.02', 'Pemeliharaan Inventaris', NULL),
('D906', 'D906.03', 'Pemeliharaan Taman', NULL),
('D906', 'D906.04', 'Perawatan Kendaraan Dinas', NULL),
('D906', 'D906.05', 'Perawatan Peralatan Kantor', NULL),
('D906', 'D906.06', 'Sewa Tenda dan Kursi', 'Individu'),
('D906', 'D906.07', 'Pengadaan Peralatan Kantor ', NULL),
('D906', 'D906.08', 'Kegiatan Kebersihan Lingkungan Gereja', NULL),
('D907', 'D907.01', 'Biaya Simpan Dana di Bank', NULL),
('D907', 'D907.02', 'Pajak Jasa Tabungan Bank', NULL),
('D908', 'D908.01', 'Cadangan Tabungan Kemandirian', NULL),
('D908', 'D908.02', 'Cadangan Tabungan Emiritus Pendeta', NULL),
('D908', 'D908.03', '', NULL),
('D999', 'D999.01', 'Dana Penyangga', NULL),
('D999', 'D999.99', 'Biaya Kegiatan Kantor Lainnya', NULL),
('K101', 'K101.01', 'Titipan Persembahan Peduli Rekso Putro', NULL),
('K101', 'K101.02', 'Titipan Diakonia', NULL),
('K101', 'K101.03', 'Titipan Lainnya/Umum', NULL),
('K101', 'K101.05', 'Dana Pembayaran Pensiun dari Sinode', NULL),
('K101', 'K101.07', 'Titipan Persembahan Bea Siswa', NULL),
('K101', 'K101.14', 'Titipan Uang Iuran Pensiun Pdt. Fendi S.', NULL),
('K101', 'K101.90', 'Selisih Lebih Kas', NULL),
('K102', 'K102.01', 'Cadangan Dana Emiritus', NULL),
('K102', 'K102.02', 'Cadangan Dana Kemandirian', NULL),
('K701', 'K701.01', 'Aktiva Bersih', NULL),
('K701', 'K701.02', 'Kenaikan Aktiva Bersih', NULL),
('K901', 'K901.01', 'Persembahan Bulanan', 'Individu'),
('K901', 'K901.02', 'Persembahan Mingguan', 'Non Individu'),
('K901', 'K901.03', 'Persembahan Perjamuan Kudus', 'Non Individu'),
('K901', 'K901.04', 'Persembahan Mirunggan', 'Individu'),
('K901', 'K901.05', 'Persembahan Persepuluhan', 'Individu'),
('K901', 'K901.06', 'Persembahan Baptis, Sidhi, Pertobatan', 'Non Individu'),
('K901', 'K901.07', 'Persembahan Pemberkatan Pernikahan', 'Non Individu'),
('K901', 'K901.08', 'Persembahan Hari Besar Kristen', 'Non Individu'),
('K901', 'K901.09', 'Persembahan Dana Kemandirian', 'Non Individu'),
('K901', 'K901.10', 'Persembahan Dana Emeritus', 'Non Individu'),
('K901', 'K901.11', 'Persembahan Pemakaian Gd Pertemuan', 'Individu'),
('K901', 'K901.12', 'Persembahan Tanah Pastori', 'Individu'),
('K901', 'K901.13', 'Persembahan untuk Bea Siswa', 'Individu'),
('K901', 'K901.14', 'Persembahan untuk Diakonia / Panti Wreda', 'Individu'),
('K901', 'K901.15', 'Persembahan Peduli Reksa Putra', 'Individu'),
('K901', 'K901.16', 'Persembahan untuk Tanah Belger', 'Individu'),
('K901', 'K901.99', 'Persembahan Lain-lain', 'Individu'),
('K902', 'K902.01', 'Persembahan Yayasan Sawo Kembar', 'Individu'),
('K903', 'K903.01', 'Bunga Deposito', 'Individu'),
('K903', 'K903.02', 'Bunga Tabungan dan Giro Bank', 'Individu'),
('K999', 'K999.01', 'Hasil Penjualan Kalender', 'Individu'),
('K999', 'K999.02', 'Persembahan untuk Tanah Belger', 'Individu'),
('K999', 'K999.99', 'Penerimaan Lain-lain', 'Individu');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_komisi`
--

CREATE TABLE IF NOT EXISTS `tbl_komisi` (
  `kode_komisi` char(8) NOT NULL,
  `nama_komisi` varchar(50) NOT NULL,
  `kode_bidang` char(8) NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `pengurus_1` char(8) DEFAULT NULL,
  `pengurus_2` char(8) DEFAULT NULL,
  `pengurus_3` char(8) DEFAULT NULL,
  `tgl_dibentuk` date NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota`
--

CREATE TABLE IF NOT EXISTS `tbl_nota` (
  `kode_nota` char(8) NOT NULL,
  `kode_toko` char(8) NOT NULL,
  `kode_transaksi` int(6) DEFAULT NULL,
  `tgl_pengeluaran` date NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `jumlah` int(15) NOT NULL,
  `pelaksana` varchar(50) NOT NULL,
  `waktu_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kode_admin` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penanggungjawab`
--

CREATE TABLE IF NOT EXISTS `tbl_penanggungjawab` (
  `kode_penanggungjawab` char(8) NOT NULL,
  `nama_penanggungjawab` varchar(30) NOT NULL,
  `jabatan` varchar(20) NOT NULL,
  `alamat_penanggungjawab` varchar(90) NOT NULL,
  `no_telepon_penanggungjawab` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_penanggungjawab`
--

INSERT INTO `tbl_penanggungjawab` (`kode_penanggungjawab`, `nama_penanggungjawab`, `jabatan`, `alamat_penanggungjawab`, `no_telepon_penanggungjawab`) VALUES
('ADM 1', 'DESSY NATALIA', 'KEPALA KANTOR', 'PURBONEGARAN', '089671448774'),
('ADM 2', 'ASTUTI WIDAYATI', 'ADMINISTRASI', 'KLITREN LOR', '0274513573'),
('B101', 'ANTONOV HENDRATMOKO', 'KETUA KOMISI KAS', 'Komp Balapan', '081804212990'),
('B102', 'RUDJITO', 'KETUA KOMISI AKUTANS', 'Mino Martani', '085729255799'),
('B103', 'TRI HANTORO', 'KETUA BID. KEHARTAAN', 'Yogyakarta', '081328896443'),
('B104', 'Fransiska Herlina Sri Hargiyan', 'Anggota Komisi Akunt', 'mancasan pandowoharjo sleman\r\n', '08175457464'),
('BANK', 'BANK', 'BANK', '-', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penerimaan_individual`
--

CREATE TABLE IF NOT EXISTS `tbl_penerimaan_individual` (
  `no_kwitansi` char(10) NOT NULL,
  `kode_transaksi` int(6) DEFAULT NULL,
  `tanggal_penerimaan` date NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `pemberi` char(8) NOT NULL,
  `jumlah` int(15) NOT NULL,
  `penerima` char(8) NOT NULL,
  `bulan` varchar(15) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  `waktu_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kode_admin` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_penerimaan_individual`
--

INSERT INTO `tbl_penerimaan_individual` (`no_kwitansi`, `kode_transaksi`, `tanggal_penerimaan`, `kode_jenis_akun`, `pemberi`, `jumlah`, `penerima`, `bulan`, `tahun`, `waktu_input`, `kode_admin`) VALUES
('M01/I/16', 3, '2016-01-01', 'K901.04', 'Jemaat1', 420000, 'ADM 2', NULL, NULL, '2016-05-16 03:52:43', 'Bumega'),
('PERBUL20', NULL, '2016-01-10', 'K901.01', '02 TIM A', 120000, 'B101', 'JAN ', 2016, '2016-06-02 04:30:19', 'Bumega'),
('PERSBG1', 10, '2016-01-03', 'K901.16', '52', 50000, 'B101', NULL, NULL, '2016-05-19 04:07:42', 'Bumega'),
('PERSBS1', 7, '2016-01-03', 'K901.13', 'Jemaat1', 2657000, 'B101', NULL, NULL, '2016-05-19 03:31:12', 'Bumega'),
('PERSBS2', 7, '2016-01-03', 'K901.13', '52', 60000, 'B101', NULL, NULL, '2016-05-17 05:41:43', 'Bumega'),
('PERSBUL1', 4, '2016-01-03', 'K901.01', '82', 1200000, 'B101', 'JAN - DES', 2015, '2016-05-16 04:58:15', 'Bumega'),
('PERSBUL10', 4, '2016-01-03', 'K901.01', '71', 50000, 'B101', 'JAN - FEB', 2016, '2016-05-16 05:11:25', 'Bumega'),
('PERSBUL11', 4, '2016-01-03', 'K901.01', '25', 100000, 'B101', 'JAN', 2016, '2016-05-16 05:12:17', 'Bumega'),
('PERSBUL12', 4, '2016-01-03', 'K901.01', 'Jemaat5', 100000, 'B101', 'JAN', 2016, '2016-05-16 05:13:15', 'Bumega'),
('PERSBUL13', NULL, '2016-01-10', 'K901.01', '63', 50000, 'B101', 'Januari', 2016, '2016-06-02 03:50:15', 'Bumega'),
('PERSBUL14', NULL, '2016-01-10', 'K901.01', '58 W.Sel', 50000, 'B101', 'JAN-MEI', 2016, '2016-06-02 03:54:05', 'Bumega'),
('PERSBUL15', NULL, '2016-01-10', 'K901.01', '78 TIM A', 150000, 'B101', 'JAN ', 2016, '2016-06-02 04:21:41', 'Bumega'),
('PERSBUL16', NULL, '2016-01-10', 'K901.01', '24', 50000, 'B101', 'JAN ', 2016, '2016-06-02 04:23:01', 'Bumega'),
('PERSBUL17', NULL, '2016-01-10', 'K901.01', '33 VIII', 20000, 'B101', 'JAN ', 2016, '2016-06-02 04:23:46', 'Bumega'),
('PERSBUL18', NULL, '2016-01-10', 'K901.01', '33 X', 100000, 'B101', 'JAN - APR', 2017, '2016-06-02 04:24:33', 'Bumega'),
('PERSBUL19', NULL, '2016-01-10', 'K901.01', '73', 50000, 'B101', 'DES', 2015, '2016-06-02 04:29:17', 'Bumega'),
('PERSBUL2', 4, '2016-01-03', 'K901.01', '83', 1200000, 'B101', 'JAN - DES ', 2015, '2016-05-16 04:59:34', 'Bumega'),
('PERSBUL21', NULL, '2016-01-10', 'K901.01', '28', 100000, 'B101', 'JAN ', 2016, '2016-06-02 04:30:47', 'Bumega'),
('PERSBUL22', NULL, '2016-01-10', 'K901.01', '21', 150000, 'B101', 'JAN ', 2016, '2016-06-02 04:31:22', 'Bumega'),
('PERSBUL23', NULL, '2016-01-10', 'K901.01', '02', 40000, 'B101', 'NOV - DES', 2015, '2016-06-02 04:32:39', 'Bumega'),
('PERSBUL24', NULL, '2016-01-10', 'K901.01', '02 VII', 40000, 'B101', 'NOV - DES', 2015, '2016-06-02 04:33:21', 'Bumega'),
('PERSBUL25', NULL, '2016-01-10', 'K901.01', '26', 50000, 'B101', 'JAN', 2016, '2016-06-02 04:33:58', 'Bumega'),
('PERSBUL26', NULL, '2016-01-10', 'K901.01', '102 W.B', 140000, 'B101', 'JUN-DES', 2015, '2016-06-02 04:53:57', 'Bumega'),
('PERSBUL27', NULL, '2016-01-10', 'K901.01', '01', 20000, 'B101', 'JAN', 2016, '2016-06-02 06:06:55', 'Bumega'),
('PERSBUL28', NULL, '2016-01-10', 'K901.01', '87', 10000, 'B101', 'JAN', 2016, '2016-06-02 06:07:44', 'Bumega'),
('PERSBUL29', NULL, '2016-01-10', 'K901.01', '100', 50000, 'B101', 'JAN', 2016, '2016-06-02 06:08:17', 'Bumega'),
('PERSBUL3', 4, '2016-01-03', 'K901.01', '78 TIM A', 20000, 'B101', 'DES', 2015, '2016-05-16 05:00:45', 'Bumega'),
('PERSBUL30', NULL, '2016-01-10', 'K901.01', '03', 50000, 'B101', 'DES', 2015, '2016-06-02 06:09:00', 'Bumega'),
('PERSBUL31', NULL, '2016-01-10', 'K901.01', '35', 100000, 'B101', 'JAN', 2016, '2016-06-02 06:09:31', 'Bumega'),
('PERSBUL32', NULL, '2016-01-10', 'K901.01', '56', 120000, 'B101', 'JAN - FEB', 2016, '2016-06-02 06:11:18', 'Bumega'),
('PERSBUL33', NULL, '2016-01-10', 'K901.01', '4', 100000, 'B101', 'JAN - MEI', 2016, '2016-06-02 06:11:48', 'Bumega'),
('PERSBUL34', NULL, '2016-01-10', 'K901.01', '57', 50000, 'B101', 'JAN - MEI', 2016, '2016-06-02 06:12:20', 'Bumega'),
('PERSBUL35', NULL, '2016-01-10', 'K901.01', '58', 100000, 'B101', 'JAN - JUN', 2016, '2016-06-02 06:13:32', 'Bumega'),
('PERSBUL36', NULL, '2016-01-10', 'K901.01', '95 W. B', 50000, 'B101', 'JAN', 2016, '2016-06-02 06:14:15', 'Bumega'),
('PERSBUL37', NULL, '2016-01-10', 'K901.01', '66', 50000, 'B101', 'JAN', 2016, '2016-06-02 06:14:45', 'Bumega'),
('PERSBUL4', 4, '2016-01-03', 'K901.01', '92', 50000, 'B101', 'JAN', 2016, '2016-05-16 05:01:54', 'Bumega'),
('PERSBUL5', 4, '2016-01-03', 'K901.01', '95', 50000, 'B101', 'JAN', 2016, '2016-05-16 05:03:08', 'Bumega'),
('PERSBUL6', 4, '2016-01-03', 'K901.01', '38', 30000, 'B101', 'JAN', 2016, '2016-05-16 05:04:14', 'Bumega'),
('PERSBUL7', 4, '2016-01-03', 'K901.01', '45', 210000, 'B101', 'JUN - DES ', 2015, '2016-05-16 05:05:55', 'Bumega'),
('PERSBUL8', 4, '2016-01-03', 'K901.01', '102 W.B', 1000000, 'B101', 'JAN', 2016, '2016-05-16 05:05:43', 'Bumega'),
('PERSBUL9', 4, '2016-01-03', 'K901.01', '52', 20000, 'B101', 'DES', 2015, '2016-05-16 05:06:46', 'Bumega'),
('PERSD1', 8, '2016-01-03', 'K901.14', 'Jemaat1', 100000, 'B101', NULL, NULL, '2016-05-19 03:37:08', 'Bumega'),
('PERSD2', 8, '2016-01-03', 'K901.14', 'Jemaat1', 1145000, 'B101', '	    		', NULL, '2016-05-19 03:38:01', 'Bumega'),
('PERSEM1', 5, '2016-01-03', 'K901.04', 'Jemaat2', 50000, 'B101', NULL, NULL, '2016-05-16 04:27:08', 'Bumega'),
('PERSEM2', 5, '2016-01-03', 'K901.04', 'Jemaat1', 14551000, 'B101', NULL, NULL, '2016-05-16 04:28:49', 'Bumega'),
('PERSEP1', 6, '2016-01-03', 'K901.05', 'Jemaat3', 350000, 'B101', NULL, NULL, '2016-05-16 04:42:10', 'Bumega'),
('PERSEP2', 6, '2016-01-03', 'K901.05', 'Jemaat4', 70000, 'B101', NULL, NULL, '2016-05-16 04:43:16', 'Bumega'),
('PERSEP3', 6, '2016-01-03', 'K901.05', 'Jemaat1', 8745000, 'B101', NULL, NULL, '2016-05-16 04:45:05', 'Bumega'),
('PERSLL1', 11, '2016-01-03', 'K901.99', 'Jemaat1', 1111000, 'B101', '	    		', NULL, '2016-05-19 03:46:36', 'Bumega'),
('PERSRP1', 9, '2016-01-03', 'K901.15', 'Jemaat1', 1200000, 'B101', NULL, NULL, '2016-05-19 03:43:33', 'Bumega'),
('PMIR1', NULL, '2016-01-10', 'K901.04', 'US', 50000, 'B101', NULL, NULL, '2016-06-02 06:34:42', 'Bumega'),
('PMIR10', NULL, '2016-01-10', 'K901.04', 'US', 20000, 'B101', NULL, NULL, '2016-06-02 06:40:59', 'Bumega'),
('PMIR11', NULL, '2016-01-10', 'K901.04', 'M/US', 10000, 'B101', NULL, NULL, '2016-06-02 06:41:25', 'Bumega'),
('PMIR12', NULL, '2016-01-10', 'K901.04', 'M/US', 10000, 'B101', NULL, NULL, '2016-06-02 06:41:42', 'Bumega'),
('PMIR2', NULL, '2016-01-10', 'K901.04', 'M/US', 10000, 'B101', NULL, NULL, '2016-06-02 06:36:28', 'Bumega'),
('PMIR3', NULL, '2016-01-10', 'K901.04', 'M/US', 5000, 'B101', NULL, NULL, '2016-06-02 06:36:49', 'Bumega'),
('PMIR4', NULL, '2016-01-10', 'K901.04', 'M/US', 20000, 'B101', NULL, NULL, '2016-06-02 06:38:48', 'Bumega'),
('PMIR5', NULL, '2016-01-10', 'K901.04', 'M/US', 50000, 'B101', NULL, NULL, '2016-06-02 06:39:07', 'Bumega'),
('PMIR6', NULL, '2016-01-10', 'K901.04', 'M/US', 10000, 'B101', NULL, NULL, '2016-06-02 06:39:27', 'Bumega'),
('PMIR7', NULL, '2016-01-10', 'K901.04', 'M/US', 5000, 'B101', NULL, NULL, '2016-06-02 06:39:54', 'Bumega'),
('PMIR8', NULL, '2016-01-10', 'K901.04', 'M/US', 5000, 'B101', NULL, NULL, '2016-06-02 06:40:07', 'Bumega'),
('PMIR9', NULL, '2016-01-10', 'K901.04', 'M/US', 2000, 'B101', NULL, NULL, '2016-06-02 06:40:29', 'Bumega');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profil_gereja`
--

CREATE TABLE IF NOT EXISTS `tbl_profil_gereja` (
  `nama_gereja` varchar(100) NOT NULL,
  `alamat_gereja` varchar(100) NOT NULL,
  `email_gereja` varchar(30) NOT NULL,
  `nomer_telepon_gereja` varchar(40) NOT NULL,
  `logo_gereja` varchar(20) NOT NULL,
  `tahun_pembukuan` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_profil_gereja`
--

INSERT INTO `tbl_profil_gereja` (`nama_gereja`, `alamat_gereja`, `email_gereja`, `nomer_telepon_gereja`, `logo_gereja`, `tahun_pembukuan`) VALUES
('GEREJA KRISTEN JAWA GONDOKUSUMAN', 'JL. DR. WAHIDIN SUDIROHUSODO 40 YOGYAKARTA 55222', 'gkj.gondokusuman@yahoo.com', '(0274) 513570', 'logo-gereja.png', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rencana_anggaran`
--

CREATE TABLE IF NOT EXISTS `tbl_rencana_anggaran` (
  `kode_jenis_akun` char(7) NOT NULL,
  `rencana_anggaran` bigint(20) NOT NULL,
  `tahun_perencanaan` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rencana_anggaran`
--

INSERT INTO `tbl_rencana_anggaran` (`kode_jenis_akun`, `rencana_anggaran`, `tahun_perencanaan`) VALUES
('D801.01', 103000000, 2016),
('D801.02', 85080000, 2016),
('D801.03', 37200000, 2016),
('D801.04', 1000000, 2016),
('D802.01', 26400000, 2016),
('D802.02', 24000000, 2016),
('D802.03', 10000000, 2016),
('D802.04', 7500000, 2016),
('D802.05', 12600000, 2016),
('D802.06', 1000000, 2016),
('D803.01', 12000000, 2016),
('D803.02', 24200000, 2016),
('D803.03', 12400000, 2016),
('D803.04', 24900000, 2016),
('D803.05', 3500000, 2016),
('D803.06', 4500000, 2016),
('D803.07', 21525000, 2016),
('D803.08', 33610000, 2016),
('D803.09', 1000000, 2016),
('D804.01', 179100000, 2016),
('D804.02', 50000000, 2016),
('D804.03', 1000000, 2016),
('D805.01', 1000000, 2016),
('D805.02', 1000000, 2016),
('D805.03', 1500000, 2016),
('D805.04', 25200000, 2016),
('D806.01', 0, 2016),
('D807.01', 10000000, 2016),
('D807.02', 12000000, 2016),
('D807.03', 12000000, 2016),
('D809.01', 107000000, 2016),
('D809.08', 17000000, 2016),
('D809.09', 18000000, 2016),
('D810.01', 18000000, 2016),
('D810.02', 140897000, 2016),
('D810.03', 19200000, 2016),
('D810.04', 3600000, 2016),
('D810.05', 10000000, 2016),
('D810.06', 4000000, 2016),
('D901.01', 10000000, 2016),
('D901.02', 6000000, 2016),
('D901.03', 3500000, 2016),
('D901.04', 10000000, 2016),
('D901.05', 2000000, 2016),
('D901.06', 10000000, 2016),
('D901.07', 45000000, 2016),
('D901.08', 5000000, 2016),
('D902.01', 30000000, 2016),
('D902.02', 15000000, 2016),
('D902.03', 5000000, 2016),
('D902.04', 5000000, 2016),
('D902.05', 6000000, 2016),
('D902.06', 23000000, 2016),
('D902.07', 5000000, 2016),
('D902.08', 10000000, 2016),
('D902.09', 15000000, 2016),
('D903.01', 65000000, 2016),
('D903.02', 14000000, 2016),
('D903.03', 600000, 2016),
('D903.04', 75000000, 2016),
('D903.05', 2000000, 2016),
('D903.06', 10000000, 2016),
('D903.07', 10000000, 2016),
('D903.08', 15000000, 2016),
('D903.09', 10000000, 2016),
('D903.10', 60000000, 2016),
('D903.11', 15000000, 2016),
('D903.12', 55000000, 2016),
('D904.01', 316000000, 2016),
('D904.02', 240000000, 2016),
('D904.03', 22500000, 2016),
('D904.04', 10000000, 2016),
('D904.05', 25000000, 2016),
('D904.08', 25000000, 2016),
('D904.09', 96000000, 2016),
('D905.01', 100000000, 2016),
('D905.02', 24000000, 2016),
('D905.03', 11000000, 2016),
('D905.04', 50000000, 2016),
('D905.05', 84000000, 2016),
('D905.06', 60000000, 2016),
('D905.07', 20000000, 2016),
('D905.09', 30000000, 2016),
('D905.10', 10000000, 2016),
('D905.11', 25000000, 2016),
('D905.12', 45000000, 2016),
('D906.04', 12000000, 2016),
('D906.05', 6000000, 2016),
('D906.06', 20000000, 2016),
('D906.07', 10000000, 2016),
('D906.08', 6000000, 2016),
('D907.01', 3500000, 2016),
('D907.02', 5000000, 2016),
('D908.01', 32000000, 2016),
('D908.02', 147000000, 2016),
('D908.03', 90000000, 2016),
('K901.01', 350000000, 2016),
('K901.02', 1500000000, 2016),
('K901.03', 175000000, 2016),
('K901.04', 240000000, 2016),
('K901.05', 225000000, 2016),
('K901.06', 55000000, 2016),
('K901.07', 70000000, 2016),
('K901.08', 125000000, 2016),
('K901.09', 147000000, 2016),
('K901.10', 90000000, 2016),
('K901.11', 3500000, 2016),
('K901.14', 24000000, 2016),
('K901.99', 80512000, 2016),
('K903.01', 55000000, 2016);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sub_gol_akun`
--

CREATE TABLE IF NOT EXISTS `tbl_sub_gol_akun` (
  `kode_golongan` char(2) NOT NULL,
  `kode_sub_gol_akun` char(4) NOT NULL,
  `nama_sub_gol_akun` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sub_gol_akun`
--

INSERT INTO `tbl_sub_gol_akun` (`kode_golongan`, `kode_sub_gol_akun`, `nama_sub_gol_akun`) VALUES
('D1', 'D101', 'Kas'),
('D2', 'D201', 'BANK DEPOSITO'),
('D2', 'D202', 'BANK TABUNGAN'),
('D2', 'D203', 'GIRO BANK'),
('D3', 'D301', 'PINJAMAN PEGAWAI'),
('D3', 'D302', 'PINJAMAN LAINNYA'),
('D4', 'D401', 'UANG MUKA'),
('D5', 'D501', 'INVENTARIS TANAH'),
('D5', 'D502', 'INVENTARIS BANGUNAN'),
('D5', 'D503', 'INVENTARIS KENDARAAN'),
('D5', 'D504', 'INVENTARIS ALAT PERKANTORAN'),
('D5', 'D505', 'INVENTARIS MEBELAIR'),
('D5', 'D506', 'SOUND SYSTEM DAN ALAT MUSIK'),
('D5', 'D507', 'INVENTARIS MULTIMEDIA'),
('D5', 'D508', 'INVENTARIS PERLENGKAPAN & PERALATAN'),
('D8', 'D801', 'BIDANG IBADAH'),
('D8', 'D802', 'BIDANG KESAKSIAN / PELAYANAN'),
('D8', 'D803', 'BIDANG PEMBINAAN WARGA GEREJA'),
('D8', 'D804', 'BIDANG PEMBANGUNAN DAN PEMELIHARAAN'),
('D8', 'D805', 'BADAN PERENCANAAN PROGRAM DAN LITBANG'),
('D8', 'D806', 'BADAN PENGAWASAN DAN PEMERIKSAAN'),
('D8', 'D807', 'BADAN SOSIAL KRISTEN'),
('D8', 'D808', 'LAIN-LAIN'),
('D8', 'D809', 'BADAN ADHOCK'),
('D8', 'D810', 'BIDANG DIAKONIA'),
('D8', 'D811', 'BADAN PEMELIHARAAN IMAN, PASTORAL DAN KONSELING'),
('D9', 'D901', 'PENGELOLAAN SEKRETARIAT'),
('D9', 'D902', 'PENGELOLAAN RUMAH TANGGA'),
('D9', 'D903', 'PERSEKUTUAN KESAKSIAN DAN PELAYANAN'),
('D9', 'D904', 'BIAYA HIDUP, GAJI DAN HONORARIUM'),
('D9', 'D905', 'PEMBERIAN TUNJANGAN'),
('D9', 'D906', 'PEMELIHARAAN & PENGADAAN INVENTARIS'),
('D9', 'D907', 'BIAYA BANK'),
('D9', 'D908', 'DANA CADANGAN'),
('D9', 'D999', 'KEGIATAN KANTOR LAINNYA'),
('K1', 'K101', 'SIMPANAN TITIPAN'),
('K1', 'K102', 'CADANGAN'),
('K7', 'K701', 'AKTIVA BERSIH'),
('K9', 'K901', 'KEGIATAN PERSEMBAHAN'),
('K9', 'K902', 'YAYASAN'),
('K9', 'K903', 'BUNGA BANK'),
('K9', 'K999', 'PENERIMAAN LAINNYA');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_toko`
--

CREATE TABLE IF NOT EXISTS `tbl_toko` (
  `kode_toko` char(8) NOT NULL,
  `nama_toko` varchar(20) NOT NULL,
  `alamat_toko` varchar(50) NOT NULL,
  `no_telepon_toko` varchar(15) NOT NULL,
  `cp_toko` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi`
--

CREATE TABLE IF NOT EXISTS `tbl_transaksi` (
  `kode_transaksi` int(6) NOT NULL,
  `tahun_pembukuan` year(4) NOT NULL,
  `kode_bukti_transaksi` char(8) NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `tipe_transaksi` enum('DEBET','KREDIT') NOT NULL,
  `jumlah` int(15) NOT NULL,
  `uraian` varchar(150) DEFAULT NULL,
  `waktu_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kode_admin` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`kode_transaksi`, `tahun_pembukuan`, `kode_bukti_transaksi`, `kode_jenis_akun`, `tipe_transaksi`, `jumlah`, `uraian`, `waktu_input`, `kode_admin`) VALUES
(1, 2016, 'KC3', 'D101.03', 'DEBET', 2000000, 'Bon KC3', '2016-05-16 03:15:42', 'admin'),
(2, 2016, '1012016', 'D101.01', 'DEBET', 61600, 'Sisa KC2', '2016-05-16 03:21:44', 'admin'),
(3, 2016, 'M01/1/16', 'K901.04', 'KREDIT', 420000, 'Persembahan Mirunggan 01-01-2016', '2016-05-16 04:00:53', 'Bumega'),
(4, 2016, 'M0002', 'K901.01', 'KREDIT', 4030000, 'Persembahan Bulanan 03-01-2016', '2016-05-19 04:41:23', 'Bumega'),
(5, 2016, 'M0002', 'K901.04', 'KREDIT', 14601000, 'Persembahan Mirunggan 03-01-2016', '2016-05-19 04:41:23', 'Bumega'),
(6, 2016, 'M0002', 'K901.05', 'KREDIT', 9165000, 'Persembahan Persepuluhan 03-01-2016', '2016-05-19 04:41:23', 'Bumega'),
(7, 2016, 'M0002', 'K901.13', 'KREDIT', 2717000, 'Persembahan untuk Bea Siswa 03-01-2016', '2016-05-19 04:41:23', 'Bumega'),
(8, 2016, 'M0002', 'K901.14', 'KREDIT', 1245000, 'Persembahan untuk Diakonia / Panti Wreda 03-01-2016', '2016-05-19 04:41:23', 'Bumega'),
(9, 2016, 'M0002', 'K901.15', 'KREDIT', 1200000, 'Persembahan Peduli Reksa Putra 03-01-2016', '2016-05-19 04:41:23', 'Bumega'),
(10, 2016, 'M0002', 'K901.16', 'KREDIT', 50000, 'Persembahan untuk Tanah Belger 03-01-2016', '2016-05-19 04:41:23', 'Bumega'),
(11, 2016, 'M0002', 'K901.99', 'KREDIT', 1111000, 'Persembahan Lain-lain 03-01-2016', '2016-05-19 04:41:23', 'Bumega'),
(12, 2016, 'M0002', 'K901.02', 'KREDIT', 15630500, 'Kebaktian Sabtu, Kantong Merah', '2016-05-19 04:41:23', 'Bumega'),
(13, 2016, 'M0002', 'K901.08', 'KREDIT', 10049200, 'Kebaktian Malam Tahun Baru, Tahun Baru, Ucapan Syukur 2015/2016', '2016-05-19 04:41:23', 'Bumega'),
(14, 2016, 'M0002', 'K901.10', 'KREDIT', 12534500, 'Kantong Kuning Emeritus', '2016-05-19 04:41:23', 'Bumega'),
(15, 2016, 'M0002', 'K901.07', 'KREDIT', 5469000, 'Pemberkatan Manten Wilayah 12', '2016-05-19 04:41:23', 'Bumega');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
 ADD PRIMARY KEY (`kode_admin`), ADD UNIQUE KEY `kode_admin` (`kode_admin`), ADD KEY `kode` (`kode`);

--
-- Indexes for table `tbl_bidang`
--
ALTER TABLE `tbl_bidang`
 ADD PRIMARY KEY (`kode_bidang`), ADD KEY `pengurus_1` (`pengurus_1`,`pengurus_2`,`pengurus_3`), ADD KEY `pengurus_2` (`pengurus_2`), ADD KEY `pengurus_3` (`pengurus_3`);

--
-- Indexes for table `tbl_bukti_transaksi`
--
ALTER TABLE `tbl_bukti_transaksi`
 ADD PRIMARY KEY (`kode_bukti_transaksi`), ADD KEY `kode_admin` (`kode_admin`), ADD KEY `kode_jenis_akun` (`kode_jenis_akun`), ADD KEY `bendahara` (`bendahara`,`penerima`,`penyetor`,`kode_admin`), ADD KEY `penerima` (`penerima`), ADD KEY `penyetor` (`penyetor`);

--
-- Indexes for table `tbl_gol_akun`
--
ALTER TABLE `tbl_gol_akun`
 ADD PRIMARY KEY (`kode_golongan`), ADD KEY `kode_golongan` (`kode_golongan`);

--
-- Indexes for table `tbl_jemaat`
--
ALTER TABLE `tbl_jemaat`
 ADD PRIMARY KEY (`kode_jemaat`);

--
-- Indexes for table `tbl_jenis_akun`
--
ALTER TABLE `tbl_jenis_akun`
 ADD PRIMARY KEY (`kode_jenis_akun`), ADD KEY `kode_sub_gol_akun` (`kode_sub_gol_akun`);

--
-- Indexes for table `tbl_komisi`
--
ALTER TABLE `tbl_komisi`
 ADD PRIMARY KEY (`kode_komisi`), ADD KEY `kode_bidang` (`kode_bidang`,`pengurus_1`,`pengurus_2`,`pengurus_3`), ADD KEY `pengurus_1` (`pengurus_1`), ADD KEY `pengurus_2` (`pengurus_2`), ADD KEY `pengurus_3` (`pengurus_3`);

--
-- Indexes for table `tbl_nota`
--
ALTER TABLE `tbl_nota`
 ADD PRIMARY KEY (`kode_nota`,`kode_toko`), ADD KEY `kode_jenis_akun` (`kode_jenis_akun`), ADD KEY `kode_transaksi` (`kode_transaksi`,`kode_admin`), ADD KEY `kode_admin` (`kode_admin`), ADD KEY `kode_toko` (`kode_toko`);

--
-- Indexes for table `tbl_penanggungjawab`
--
ALTER TABLE `tbl_penanggungjawab`
 ADD PRIMARY KEY (`kode_penanggungjawab`);

--
-- Indexes for table `tbl_penerimaan_individual`
--
ALTER TABLE `tbl_penerimaan_individual`
 ADD PRIMARY KEY (`no_kwitansi`), ADD KEY `kode_transaksi` (`kode_transaksi`,`kode_jenis_akun`,`pemberi`,`penerima`,`kode_admin`), ADD KEY `kode_jenis_akun` (`kode_jenis_akun`), ADD KEY `pemberi` (`pemberi`), ADD KEY `penerima` (`penerima`), ADD KEY `kode_admin` (`kode_admin`);

--
-- Indexes for table `tbl_rencana_anggaran`
--
ALTER TABLE `tbl_rencana_anggaran`
 ADD PRIMARY KEY (`kode_jenis_akun`,`tahun_perencanaan`), ADD KEY `kode_jenis_akun` (`kode_jenis_akun`), ADD KEY `kode_jenis_akun_2` (`kode_jenis_akun`);

--
-- Indexes for table `tbl_sub_gol_akun`
--
ALTER TABLE `tbl_sub_gol_akun`
 ADD PRIMARY KEY (`kode_sub_gol_akun`), ADD KEY `kode_sub_gol_akun` (`kode_sub_gol_akun`), ADD KEY `kode_golongan` (`kode_golongan`);

--
-- Indexes for table `tbl_toko`
--
ALTER TABLE `tbl_toko`
 ADD PRIMARY KEY (`kode_toko`);

--
-- Indexes for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
 ADD PRIMARY KEY (`kode_transaksi`,`tahun_pembukuan`), ADD KEY `kode_bukti_transaksi` (`kode_bukti_transaksi`), ADD KEY `kode_admin` (`kode_admin`), ADD KEY `kode_jenis_akun` (`kode_jenis_akun`), ADD KEY `kode_bukti_transaksi_2` (`kode_bukti_transaksi`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_bidang`
--
ALTER TABLE `tbl_bidang`
ADD CONSTRAINT `tbl_bidang_ibfk_1` FOREIGN KEY (`pengurus_1`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
ADD CONSTRAINT `tbl_bidang_ibfk_2` FOREIGN KEY (`pengurus_2`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
ADD CONSTRAINT `tbl_bidang_ibfk_3` FOREIGN KEY (`pengurus_3`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`);

--
-- Constraints for table `tbl_bukti_transaksi`
--
ALTER TABLE `tbl_bukti_transaksi`
ADD CONSTRAINT `tbl_bukti_transaksi_ibfk_1` FOREIGN KEY (`kode_jenis_akun`) REFERENCES `tbl_jenis_akun` (`kode_jenis_akun`),
ADD CONSTRAINT `tbl_bukti_transaksi_ibfk_2` FOREIGN KEY (`kode_admin`) REFERENCES `tbl_admin` (`kode_admin`),
ADD CONSTRAINT `tbl_bukti_transaksi_ibfk_3` FOREIGN KEY (`bendahara`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
ADD CONSTRAINT `tbl_bukti_transaksi_ibfk_4` FOREIGN KEY (`penerima`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
ADD CONSTRAINT `tbl_bukti_transaksi_ibfk_5` FOREIGN KEY (`penyetor`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`);

--
-- Constraints for table `tbl_jenis_akun`
--
ALTER TABLE `tbl_jenis_akun`
ADD CONSTRAINT `tbl_jenis_akun_ibfk_1` FOREIGN KEY (`kode_sub_gol_akun`) REFERENCES `tbl_sub_gol_akun` (`kode_sub_gol_akun`);

--
-- Constraints for table `tbl_komisi`
--
ALTER TABLE `tbl_komisi`
ADD CONSTRAINT `tbl_komisi_ibfk_1` FOREIGN KEY (`kode_bidang`) REFERENCES `tbl_bidang` (`kode_bidang`),
ADD CONSTRAINT `tbl_komisi_ibfk_2` FOREIGN KEY (`pengurus_1`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
ADD CONSTRAINT `tbl_komisi_ibfk_3` FOREIGN KEY (`pengurus_2`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
ADD CONSTRAINT `tbl_komisi_ibfk_4` FOREIGN KEY (`pengurus_3`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`);

--
-- Constraints for table `tbl_nota`
--
ALTER TABLE `tbl_nota`
ADD CONSTRAINT `tbl_nota_ibfk_1` FOREIGN KEY (`kode_jenis_akun`) REFERENCES `tbl_jenis_akun` (`kode_jenis_akun`),
ADD CONSTRAINT `tbl_nota_ibfk_2` FOREIGN KEY (`kode_transaksi`) REFERENCES `tbl_transaksi` (`kode_transaksi`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_nota_ibfk_3` FOREIGN KEY (`kode_admin`) REFERENCES `tbl_admin` (`kode_admin`),
ADD CONSTRAINT `tbl_nota_ibfk_4` FOREIGN KEY (`kode_toko`) REFERENCES `tbl_toko` (`kode_toko`);

--
-- Constraints for table `tbl_penerimaan_individual`
--
ALTER TABLE `tbl_penerimaan_individual`
ADD CONSTRAINT `tbl_penerimaan_individual_ibfk_2` FOREIGN KEY (`pemberi`) REFERENCES `tbl_jemaat` (`kode_jemaat`),
ADD CONSTRAINT `tbl_penerimaan_individual_ibfk_1` FOREIGN KEY (`kode_jenis_akun`) REFERENCES `tbl_jenis_akun` (`kode_jenis_akun`),
ADD CONSTRAINT `tbl_penerimaan_individual_ibfk_3` FOREIGN KEY (`penerima`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
ADD CONSTRAINT `tbl_penerimaan_individual_ibfk_4` FOREIGN KEY (`kode_admin`) REFERENCES `tbl_admin` (`kode_admin`),
ADD CONSTRAINT `tbl_penerimaan_individual_ibfk_5` FOREIGN KEY (`kode_transaksi`) REFERENCES `tbl_transaksi` (`kode_transaksi`) ON DELETE SET NULL;

--
-- Constraints for table `tbl_rencana_anggaran`
--
ALTER TABLE `tbl_rencana_anggaran`
ADD CONSTRAINT `tbl_rencana_anggaran_ibfk_1` FOREIGN KEY (`kode_jenis_akun`) REFERENCES `tbl_jenis_akun` (`kode_jenis_akun`);

--
-- Constraints for table `tbl_sub_gol_akun`
--
ALTER TABLE `tbl_sub_gol_akun`
ADD CONSTRAINT `tbl_sub_gol_akun_ibfk_1` FOREIGN KEY (`kode_golongan`) REFERENCES `tbl_gol_akun` (`kode_golongan`);

--
-- Constraints for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
ADD CONSTRAINT `tbl_transaksi_ibfk_1` FOREIGN KEY (`kode_bukti_transaksi`) REFERENCES `tbl_bukti_transaksi` (`kode_bukti_transaksi`) ON DELETE CASCADE,
ADD CONSTRAINT `tbl_transaksi_ibfk_2` FOREIGN KEY (`kode_jenis_akun`) REFERENCES `tbl_jenis_akun` (`kode_jenis_akun`),
ADD CONSTRAINT `tbl_transaksi_ibfk_3` FOREIGN KEY (`kode_admin`) REFERENCES `tbl_admin` (`kode_admin`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
