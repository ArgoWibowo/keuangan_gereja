<?PHP
  // Original PHP code by Chirp Internet: www.chirp.com.au
  // Please acknowledge use of this code by including this header.
  require_once("koneksi.php");
  if (isset($_GET['exp'])){
    $data = array();
    $result;
    if ($_GET['exp'] == "datatoko"){
        $result = mysql_query("SELECT * FROM tbl_toko");    
    }else if ($_GET['exp'] == "penanggungjawab"){
        $result = mysql_query("SELECT * FROM tbl_penanggungjawab");    
    }
    while ( $baris = mysql_fetch_assoc($result)){
        $data[] = $baris;
    }

    function cleanData(&$str)
    {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }

    // file name for download
    

    if ($_GET['exp'] == "datatoko"){
        $filename = "DataToko" . date('Ymd') . ".xls";  
    }else if ($_GET['exp'] == "penanggungjawab"){
        $filename = "DataPenanggungJawab" . date('Ymd') . ".xls";   
    }

    header("Content-Disposition: attachment; filename=\"$filename\"");
    header("Content-Type: application/vnd.ms-excel");

    $flag = false;
    foreach($data as $row) {
      if(!$flag) {
        // display field/column names as first row
        echo implode("\t", array_keys($row)) . "\n";
        $flag = true;
      }
      array_walk($row, 'cleanData');
      echo implode("\t", array_values($row)) . "\n";
    }
    // echo $filename;
    exit;
  }
  
  
?>