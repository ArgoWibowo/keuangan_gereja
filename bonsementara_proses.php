<?php
	session_start(); 
	include 'koneksi2.php';

	$kodebukti = $_POST["kodebukti"];
	$kredit = $_POST["kredit"];
	$tanggalBonSementara = $_POST["tanggalBonSementara"];
	$bendahara = $_POST["bendahara"];
	$penerima = $_POST["penerima"];
	$penyetor = $_POST["penyetor"];
	$keterangan = $_POST["keterangan"];
	$jumlah = $_POST["jumlah"];
	$detail = json_decode($_POST["detail"]);
	$admin = $_SESSION['u53r_4dm1n_K3uan94n_G3r3j4'];

	$conn->autocommit(false);
	try {
		$sql = "INSERT INTO `tbl_bukti_transaksi` (`kode_bukti_transaksi`, `kode_jenis_akun`, `tipe_transaksi`, `jumlah`, `keterangan`, `tanggal_bukti_transaksi`, `bendahara`, `penerima`, `penyetor`, `kode_admin`) VALUES (?, ?, 'KREDIT', ?, ?, ?, ?, ?, ?, ?);";

		$statement = $conn->prepare($sql);
	    $statement->bind_param("ssissssss", $kodebukti, $kredit, $jumlah, $keterangan, $tanggalBonSementara, $bendahara, $penerima, $penyetor, $admin);
	    if($statement->execute() == false) throw new Exception("Gagal insert bukti transaksi");
    	$statement->close();    	

	    $count = sizeof($detail);
	    for ($i=0; $i < $count; $i++) {
	    	$sql = "INSERT INTO `tbl_transaksi`(`kode_transaksi`, `tahun_pembukuan`, `kode_bukti_transaksi`, `kode_jenis_akun`, `tipe_transaksi`, `jumlah`, `uraian`, `kode_admin`) VALUES (?, ?, ?, ?, 'DEBET', ?, ?, ?);";

	    	$statement = $conn->prepare($sql);
		    $statement->bind_param("sississ", $detail[$i][0], $_SESSION['t4hun_P3m8uku4an'], $kodebukti, $detail[$i][1], $detail[$i][3], $detail[$i][2], $admin);
		 	 
		    if($statement->execute() == false) throw new Exception("Gagal insert transaksi ke-".$i);
	    	$statement->close();
	    }

	    $conn->commit();
	    echo true;
	} catch (Exception $e) {
		$conn->rollback();
		echo $e->getMessage();
	}
    
    $conn->autocommit(true);
 ?>