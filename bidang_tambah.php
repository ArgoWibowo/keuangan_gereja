<div id="divTitle">
	<label id="lblTitle">Tambah Bidang</label>
</div>
<form id="formTambahBidang" name="formTambahBidang" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabel">Kode Bidang</td> <td> : </td>
			<td>  <input type="text" id="kodeBidang" maxlength=8  name="kodeBidang" spellcheck="false" placeholder="Masukan Kode Bidang" style="width: 246px;" onKeyUp="checkKodeBidang()" required/></td>
			<td class="warning"><img id="warningUsernameBidang" src="image/warning.png" alt="warning" style ="width: 15px;height: 15px;"></td>
		</tr>
		<tr>
			<td class="kolomLabel">Nama Bidang</td> <td> : </td>
			<td>  <input type="text" id="namaBidang" name="namaBidang" spellcheck="false" placeholder="Masukan Nama Bidang" style="width: 246px;" required/></td>
		</tr>
		<tr>
			<td class="kolomLabel">Jenis Akun</td> <td> : </td>
				<td>  <select class='jenisakunBidang' name="jenisakunBidang" style ="width:253px;">
					<option value='' disabled selected style="display:none;">Rekening</option>
			
			 		<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_jenis_akun,nama_kode_jenis_akun FROM  tbl_jenis_akun ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_jenis_akun"];
			                $str.="'>";
			                $str.=$baris["kode_jenis_akun"]." - ".$baris["nama_kode_jenis_akun"];
			                $str.="</option>";

			                echo $str;
			                $str = "";
			            }	

				 ?>
			</select> <!-- <input class="trick" id="jenisakunBidang" tabindex="-1"> --></td>
		</tr>

		<tr>
			<td class="kolomLabel">Pengurus 1</td> <td> : </td>
				<td>  <select class='pengurus1Bidang' name="pengurus1Bidang" style ="width:253px;" placeholder ="Pengurus 1" >
			 	<option value='' disabled selected style="display:none;" >Pengurus 1</option>
				<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> <!-- <input class="trick" id="pengurus1Bidang" tabindex="-1"> --></td>

		</tr>
		<tr>
			<td class="kolomLabel">Pengurus 2</td> <td> : </td>
				<td>  <select class='pengurus2Bidang' name="pengurus2Bidang" style ="width:253px;" placeholder ="Pengurus 2">
			 	<option value='' disabled selected style="display:none;">Pengurus 2</option>
				<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> <!-- <input class="trick" id="pengurus2Bidang" tabindex="-1"> --></td>

		</tr>
			<tr>
			<td class="kolomLabel">Pengurus 3</td> <td> : </td>
				<td>  <select class='pengurus3Bidang' name="pengurus3Bidang" style ="width:253px;" placeholder ="Pengurus 3">
			 	<option value='' disabled selected style="display:none;" >Pengurus 3</option>
				<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> <!-- <input class="trick" id="pengurus3Bidang" tabindex="-1"> --></td>

		</tr>
		
		<tr>
			<td>Tanggal Dibentuk</td> <td> : </td>
			<td class="customdate"><input type="number" class="ex" id="bidang_tanggal" name="bidang_tanggal" min=1 max=31 placeholder="tanggal" required style="width: 72px;">
			<input type="number" class="ex" id="bidang_bulan" name="bidang_bulan" min=1 max=12 placeholder="bulan" required style="width: 72.5px;">
			<input type="number" class="ex" id="bidang_tahun" name="bidang_tahun" min=2000 max=9999 placeholder="tahun" required style="width: 72.5px;">
			</td>
		</tr>
		
		<!-- <tr>
			<td class="kolomLabel">Tanggal Dibentuk</td> <td> : </td>
			<td>  <input type="date" id="tanggal" name="tanggal" spellcheck="false" placeholder="Masukan Tanggal" style="width: 246px;" required/></td>
		</tr> -->
		<tr style="vertical-align: top;">
			<td class="kolomLabel">Deskripsi</td> <td> : </td>
			<td style="padding-bottom: 0px;">  <textarea style="width: 244px; height: 70px;" name ="deskripsiBidang" id ="deskripsiBidang" placeholder="Masukan Deskripsi"></textarea></td>
		</tr>

			</table>
	<input type="submit" value="SIMPAN"  id="btnTambahBidang" name="tambahBidang" class="button"  style="width: 120px; height: 25px;">
</form>

<script type="text/javascript">
	$("#bidang_tanggal").focusout(function(){
		var objectd = $("#bidang_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#bidang_bulan").focusout(function(){
		var m = $("#bidang_bulan").val();
		if(m > 12 || m < 1) {
			$("#bidang_bulan").val("");
			m = null;
		}
		if(m == null) $("#bidang_tanggal").attr("max", 31);
		else if(m == 2) $("#bidang_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#bidang_tanggal").attr("max", 31);
			else $("#bidang_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#bidang_tanggal").attr("max", 31);
			else $("#bidang_tanggal").attr("max", 30);
		}
		$("#bidang_tahun").triggerHandler("focusout");
		$("#bidang_tanggal").triggerHandler("focusout");
	});

	$("#bidang_tahun").focusout(function(){
		var m = $("#bidang_bulan").val(); 
		var y = $("#bidang_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#bidang_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#bidang_tanggal").attr("max", 29);
				} else {
					$("#bidang_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#bidang_tanggal").attr("max", 29);
				} else {
					$("#bidang_tanggal").attr("max", 28);
				}
			}
			$("#bidang_tanggal").triggerHandler("focusout");
		}
	});

</script>	