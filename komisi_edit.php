<div id="divTitle">
	<label id="lblTitleKomisi">Edit Komisi</label>
</div>
<form id="formEditKomisi" name="formEditKomisi" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td>Kode Komisi</td><td> : </td>
			<td><input type="text" id="editKodeKomisi" name="editKodeKomisi" readonly spellcheck="false" placeholder="Kode Komisi" disabled style="width: 246px;" required style="this.style.color='#f00'"/></td>
		</tr>
		<tr>
			<td>Nama Komisi</td><td> : </td>
			<td><input type="text" id="editNamaKomisi" name="editNamaKomisi" spellcheck="false" placeholder="Nama Komisi" style="width: 246px;" required /></td>
		</tr>
		<tr>
			<td class="kolomLabel">Kode Bidang</td><td> : </td>
				<td><select class='editBidangKomisi' name="editBidangKomisi" style ="width:250px;">
					<option value='' disabled selected style="display:none;">Bidang</option>
			
			 		<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_bidang,nama_bidang FROM  tbl_bidang ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_bidang"];
			                $str.="'>";
			                $str.=$baris["kode_bidang"]." - ".$baris["nama_bidang"];
			                $str.="</option>";

			                echo $str;
			                $str = "";
			            }	

				 ?>
			</select> </td>
		</tr>

		<tr>
			<!-- <td class="kolomLabel">Jenis Akun</td><td> : </td>
			<td><input type="text" id="editJenisAkunKomisi" name="editJenisAkunKomisi" spellcheck="false" placeholder="Edit Kode Jenis Akun" style="width: 246px;" required/></td> -->
			<td class="kolomLabel">Jenis Akun</td><td> : </td>
				<td><select class='editJenisAkunKomisi' name="editJenisAkunKomisi" style ="width:250px;" value = ''>
				<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_jenis_akun,nama_kode_jenis_akun FROM  tbl_jenis_akun ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_jenis_akun"];
			                $str.="'>";
			                $str.=$baris["kode_jenis_akun"]." - ".$baris["nama_kode_jenis_akun"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>
		</tr>

		<tr>
			<td class="kolomLabel">Pengurus 1</td><td> : </td>
			<td><select class='editPengurus1Komisi' name="editPengurus1Komisi" style ="width:250px;" placeholder ="Pengurus 1" >
			 					<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>

		</tr>
		<tr>
			<td class="kolomLabel">Pengurus 2</td><td> : </td>
			<td><select class='editPengurus2Komisi' name="editPengurus2Komisi" style ="width:250px;" placeholder ="Pengurus 2" >
			 					<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>

		</tr>
		<tr>
			<td class="kolomLabel">Pengurus 3</td><td> : </td>
			<td><select class='editPengurus3Komisi' name="editPengurus3Komisi" style ="width:250px;" placeholder ="Pengurus 3" >
			 					<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_penanggungjawab,nama_penanggungjawab FROM  tbl_penanggungjawab ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_penanggungjawab"];
			                $str.="'>";
			                $str.=$baris["kode_penanggungjawab"]." - ".$baris["nama_penanggungjawab"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> </td>

		</tr>
		
		<tr>
			<td>Tanggal Dibentuk</td> <td> : </td>
			<td class="customdate"><input type="number" class="ex" id="komisiEdit_tanggal" name="komisiEdit_tanggal" min=1 max=31 placeholder="tanggal" required style="width: 72px;">
			<input type="number" class="ex" id="komisiEdit_bulan" name="komisiEdit_bulan" min=1 max=12 placeholder="bulan" required style="width: 72px;">
			<input type="number" class="ex" id="komisiEdit_tahun" name="komisiEdit_tahun" min=2000 max=9999 placeholder="tahun" required style="width: 72px;">
			</td>
		</tr>
	
		<tr style="vertical-align: top;">
			<td class="kolomLabel">Deskripsi</td><td> : </td>
			<td style="padding-bottom: 0px;"><textarea style="width: 244px; height: 70px;" name ="editDeskripsiKomisi" id ="editDeskripsiKomisi" placeholder="Edit Deskripsi"></textarea></td>
		</tr>
		<tr>
			<td class="kolomLabel">Status</td><td> : </td>
			<td><select id='editStatusKomisi' name="editStatusKomisi" style ="width:250px;">
			 	<option value='' disabled selected style="display:none;">Status</option>
				<option value='1'>Aktif</option>
			 	<option value='0'>Tidak Aktif</option>
				</select></td>
		</tr>
	</table>
	<input type="submit" value="Simpan" id="btnEditKomisi" name="editKomisi"  style="width: 120px; height: 25px;">
	<button type="button" id="btnBatalEditKomisi" name="btnBatalEditKomisi"  style="width: 120px; height: 25px;">Batal</button>
	<input type="text" id="editKodeKomisi2" name="editKodeKomisi2" style="visibility:hidden";>
</form>
	<button id="btnKembaliKomisi" name="btnKembaliKomisi" style="visibility:hidden";  style="width: 120px; height: 25px;">Kembali</button>


<script type="text/javascript">
	$("#komisiEdit_tanggal").focusout(function(){
		var objectd = $("#komisiEdit_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#komisiEdit_bulan").focusout(function(){
		var m = $("#komisiEdit_bulan").val();
		if(m > 12 || m < 1) {
			$("#komisiEdit_bulan").val("");
			m = null;
		}
		if(m == null) $("#komisiEdit_tanggal").attr("max", 31);
		else if(m == 2) $("#komisiEdit_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#komisiEdit_tanggal").attr("max", 31);
			else $("#komisiEdit_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#komisiEdit_tanggal").attr("max", 31);
			else $("#komisiEdit_tanggal").attr("max", 30);
		}
		$("#komisiEdit_tahun").triggerHandler("focusout");
		$("#komisiEdit_tanggal").triggerHandler("focusout");
	});

	$("#komisiEdit_tahun").focusout(function(){
		var m = $("#komisiEdit_bulan").val(); 
		var y = $("#komisiEdit_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#komisiEdit_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#komisiEdit_tanggal").attr("max", 29);
				} else {
					$("#komisiEdit_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#komisiEdit_tanggal").attr("max", 29);
				} else {
					$("#komisiEdit_tanggal").attr("max", 28);
				}
			}
			$("#komisiEdit_tanggal").triggerHandler("focusout");
		}
	});

</script>