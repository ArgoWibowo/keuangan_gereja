<?php 
	if(isset($_POST['type'])){
		
	} else {
		getViewBuktiPengeluaran();
	}

	function getViewBuktiPengeluaran(){
		?>

		<link rel="stylesheet" type="text/css" href="css/buktipengeluaran.css">
		<script type="text/javascript" src="js/buktipengeluaran.js"></script>
		<form id="formBuktiPengeluaran">
			<div class="fTitle">FORM BUKTI PENGELUARAN</div>
			<table id="buktiPengeluaranAtas">
				<tr>
					<td width="15%">Kode bukti pengeluaran</td><td width="1%">:</td>
					<td width="30%" colspan=3><input id="kodeBuktiPengeluaran" maxlength=8 placeholder="max 8 karakter" required tabindex="1"></td>
					<td width="8%"class="separator"></td>
					<td width="15%">Bendahara</td><td width="1%">:</td>
					<td width="30%">
						<div class="relativeBox">
							<select class="selectBendaharaBuktiPengeluaran" required tabindex="7">
							<?php isiSelectPenanggungJawab("bendahara"); ?>
							</select><input class="trick" autocomplete="off" id="selectBendaharaBuktiPengeluaran" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Kredit</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectKreditBuktiPengeluaran" required tabindex="2">
							<?php isiSelectRekening() ?>
							</select><input class="trick" autocomplete="off" id="selectKreditBuktiPengeluaran" tabindex="-1">
						</div>
					</td>
					<td class="separator"></td>
					<td>Penerima</td><td>:</td>
					<td>
						<div class="relativeBox">
							<select class="selectPenerimaBuktiPengeluaran" required tabindex="8">
							<?php isiSelectPenanggungJawab("penerima"); ?>
							</select><input class="trick" autocomplete="off" id="selectPenerimaBuktiPengeluaran" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Tanggal pengeluaran</td><td>:</td>
					<td class="customdate"><input type="number" class="ex" id="buktiPengeluaran_tanggal" min=1 max=31 placeholder="tanggal" value=<?php echo date("d") ?> required tabindex="3"></td>
					<td class="customdate"><input type="number" class="ex" id="buktiPengeluaran_bulan" min=1 max=12 placeholder="bulan" value=<?php echo date("m") ?> required tabindex="4"></td>
					<td class="customdate"><input type="number" class="ex" id="buktiPengeluaran_tahun" min=2000 max=9999 placeholder="tahun" value=<?php echo date("y")+2000 ?> required tabindex="5"></td>
					<td class="separator"></td>
					<td>Penyetor</td><td>:</td>
					<td>
						<div class="relativeBox">
							<select class="selectPenyetorBuktiPengeluaran" required tabindex="9">
							<?php isiSelectPenanggungJawab("penyetor"); ?>
							</select><input class="trick" autocomplete="off" id="selectPenyetorBuktiPengeluaran" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Guna keperluan</td><td>:</td>
					<td colspan=7 id="boxKeteranganBuktiPengeluaran">
						<input id="keteranganBuktiPengeluaran" placeholder="max 150 karakter" maxlength=150 required tabindex="6">
					</td>
				</tr>
			</table>
			<br><hr>
			<table id="detailDebet">
				<thead>
					<tr>
						<th width="10%">Kode Pengeluaran</th>
						<th width="10%">Debet</th>
						<th width="60%">Uraian</th>
						<th colspan=2>Jumlah</th>
						<th width="8%"></th>
					</tr>
				</thead>
				<tbody id="detailDebetBody">
					<?php
						$data = isiDetailDebet(date('y-m-d'));
					?>
				</tbody>
				<tbody id="totalDetailDebet">
					<tr>
						<td>Jumlah</td>
						<td></td>
						<td></td>
						<td class="rpCol">Rp.</td>
						<td class="numerik" id="totalSementaraDetailPengeluaran"><?php echo $data; ?></td>
						<td></td>
					</tr>
				</tbody>
			</table><br>
			<div style="text-align:center;">
				<input type="reset" value="bersihkan" tabindex=-1 style="width: 120px; height: 35px;">
				<input type="submit" value="simpan" tabindex=10 style="width: 120px; height: 35px;">
			</div>
		</form>
		<?php 
	} 
?>

