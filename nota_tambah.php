<div id="divTitle">
	<label id="lblTitle">Tambah Nota</label>
</div>
<form id="formTambahNota" name="formTambahNota" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabel">Kode Nota</td><td> : </td>
			<td><input type="text" id="kodeNota" name="kodeNota" spellcheck="false" placeholder="Masukan Kode Nota" style="width: 246px;" onKeyUp="checkKodeNota()" required/></td>
			<td class="warning"><img id="warningToko" src="image/warning.png" alt="warning" style ="width: 15px;height: 15px;"></td>
		</tr>
		<tr>
			<td class="kolomLabel">Kode Toko</td><td> : </td>
			<!-- <td><input type="text" id="kodeTokoNota" name="kodeTokoNota" spellcheck="false" placeholder="Masukan Kode Toko" style="width: 246px;" required/></td> -->
 				<td><select class='kodeTokoNota' name="kodeTokoNota" style ="width:250px;" value = ''>
 					<option value='' disabled selected style="display:none;">Toko</option>
			
<!--  			 	<option value='' disabled selected style="display:none;">Kode Toko</option> -->
 				<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_toko,nama_toko FROM  tbl_toko ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_toko"];
			                $str.="'>";
			                $str.=$baris["kode_toko"]." - ".$baris["nama_toko"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			 </select> <input class="trick" id="kodeTokoNota" tabindex="-1"></td>
	
		</tr>
		<tr>
			<!-- <td class="kolomLabel">Debet</td><td> : </td>
			<td><input type="text" id="debetNota" name="debetNota" spellcheck="false" placeholder="Pilih Rekening" style="width: 246px;" required/></td> -->
			<td class="kolomLabel">Jenis Akun</td><td> : </td>
				<td><select class='debetNota' name="debetNota" style ="width:250px;" value = ''>
					<option value='' disabled selected style="display:none;">Rekening</option>
			
			 		<?php 
					include "koneksi.php";

			            $cek_kode = mysql_query("SELECT kode_jenis_akun,nama_kode_jenis_akun FROM  tbl_jenis_akun ");
			            while ( $baris = mysql_fetch_array($cek_kode)){
			                $str.="<option value='";
			                $str.=$baris["kode_jenis_akun"];
			                $str.="'>";
			                $str.=$baris["kode_jenis_akun"]." - ".$baris["nama_kode_jenis_akun"];
			                $str.="</option>";

			                echo $str;
			                $str = "";

			            }	

				 ?>
			</select> <input class="trick" id="debetNota" tabindex="-1"></td>
		</tr>
		<tr>
			<td>Tanggal Pengeluaran</td> <td> : </td>
			<td class="customdate"><input type="number" class="ex" id="nota_tanggal" name="nota_tanggal" min=1 max=31 placeholder="tanggal" required style="width: 72px;">
			<input type="number" class="ex" id="nota_bulan" name="nota_bulan" min=1 max=12 placeholder="bulan" required style="width: 72px;">
			<input type="number" class="ex" id="nota_tahun" name="nota_tahun" min=2000 max=9999 placeholder="tahun" required style="width: 72px;">
			</td>
		</tr>
		
		<tr>
			<td class="kolomLabel">Jumlah</td><td> : </td>
			<td><input type="text" id="jumlahNota" name="jumlahNota" spellcheck="false" placeholder="Masukan Jumlah Pengeluaran " style="width: 246px;" required/></td>
		</tr>

		<tr>
			<td class="kolomLabel">Pelaksana</td><td> : </td>
			<td><input type="text" id="pelaksanaNota" name="pelaksanaNota" spellcheck="false" placeholder="Masukan Nama Pelaksana " style="width: 246px;" required/></td>
		</tr>
				
	</table>
	<input type="submit" value="SIMPAN"  id="btnTambahNota" name="tambahNota" class="button"  style="width: 120px; height: 25px;">
</form>

<script type="text/javascript">
	$("#nota_tanggal").focusout(function(){
		var objectd = $("#nota_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#nota_bulan").focusout(function(){
		var m = $("#nota_bulan").val();
		if(m > 12 || m < 1) {
			$("#nota_bulan").val("");
			m = null;
		}
		if(m == null) $("#nota_tanggal").attr("max", 31);
		else if(m == 2) $("#nota_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#nota_tanggal").attr("max", 31);
			else $("#nota_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#nota_tanggal").attr("max", 31);
			else $("#nota_tanggal").attr("max", 30);
		}
		$("#nota_tahun").triggerHandler("focusout");
		$("#nota_tanggal").triggerHandler("focusout");
	});

	$("#nota_tahun").focusout(function(){
		var m = $("#nota_bulan").val(); 
		var y = $("#nota_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#nota_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#nota_tanggal").attr("max", 29);
				} else {
					$("#nota_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#nota_tanggal").attr("max", 29);
				} else {
					$("#nota_tanggal").attr("max", 28);
				}
			}
			$("#nota_tanggal").triggerHandler("focusout");
		}
	});

</script>	