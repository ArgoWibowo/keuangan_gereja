-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2017 at 06:26 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `keuangan_gereja`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `kode_admin` char(8) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `kode_terpilih` enum('TIDAKADA','KOMISI','BIDANG') NOT NULL,
  `kode` char(8) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `pertanyaan_pemulihan` enum('Siapa nama ayah ada?','Siapa nama ibu anda?','Siapa guru Sekolah Minggu yang paling anda sukai?','Siapa nama paman yang paling anda sukai?','Dimana anda lahir?','Berapa 3 angka terakhir nomer telepon pertama anda?','Dimana ayah anda lahir?','Ayat paling anda sukai?','Tokoh Alkitab yang paling menginspirasi?') NOT NULL,
  `jawaban_pertanyaan_pemulihan` varchar(30) NOT NULL,
  `alamat_admin` varchar(90) NOT NULL,
  `no_telepon_admin` varchar(15) NOT NULL,
  `email_admin` varchar(50) NOT NULL,
  `otoritas_administrasi` enum('TIDAKADA','BACA','TULIS','BACATULIS') NOT NULL,
  `otoritas_pemasukan` enum('TIDAKADA','BACA','TULIS','BACATULIS') NOT NULL,
  `otoritas_pengeluaran` enum('TIDAKADA','BACA','TULIS','BACATULIS') NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`kode_admin`, `nama_admin`, `kode_terpilih`, `kode`, `password`, `pertanyaan_pemulihan`, `jawaban_pertanyaan_pemulihan`, `alamat_admin`, `no_telepon_admin`, `email_admin`, `otoritas_administrasi`, `otoritas_pemasukan`, `otoritas_pengeluaran`, `status`) VALUES
('admin', 'admin', 'TIDAKADA', NULL, '4153a94abe5b151f0dbd700632515979ed3a00359305c21863a39cf82ccaa3e9', 'Dimana anda lahir?', 'yogyakarta', 'Jln Wahidin Sudirohusodo No 40 Yogyakarta', '0274513570', 'gkj.gondokusuman@yahoo.com', 'BACATULIS', 'BACATULIS', 'BACATULIS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bidang`
--

CREATE TABLE `tbl_bidang` (
  `kode_bidang` char(7) NOT NULL,
  `nama_bidang` varchar(50) NOT NULL,
  `kode_jenis_akun` char(7) DEFAULT NULL,
  `pengurus_1` char(8) DEFAULT NULL,
  `pengurus_2` char(8) DEFAULT NULL,
  `pengurus_3` char(8) DEFAULT NULL,
  `tgl_dibentuk` date NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bukti_transaksi`
--

CREATE TABLE `tbl_bukti_transaksi` (
  `kode_bukti_transaksi` char(8) NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `tipe_transaksi` enum('DEBET','KREDIT') NOT NULL,
  `jumlah` int(15) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `tanggal_bukti_transaksi` date NOT NULL,
  `bendahara` char(8) NOT NULL,
  `penerima` char(8) NOT NULL,
  `penyetor` char(8) NOT NULL,
  `waktu_posting` timestamp NULL DEFAULT NULL,
  `waktu_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kode_admin` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gol_akun`
--

CREATE TABLE `tbl_gol_akun` (
  `kelompok_akun` enum('D','K') NOT NULL,
  `kode_golongan` char(2) NOT NULL,
  `nama_golongan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gol_akun`
--

INSERT INTO `tbl_gol_akun` (`kelompok_akun`, `kode_golongan`, `nama_golongan`) VALUES
('D', 'D1', 'K A S'),
('D', 'D2', 'BANK'),
('D', 'D3', 'PINJAMAN'),
('D', 'D4', 'UANG MUKA'),
('D', 'D5', 'INVENTARIS'),
('D', 'D8', 'KEGIATAN BADAN PEMBANTU MAJELIS'),
('D', 'D9', 'KEGIATAN KANTOR GEREJA'),
('K', 'K1', 'TITIPAN DAN CADANGAN'),
('K', 'K5', 'SALDO AWAL'),
('K', 'K7', 'AKTIVA BERSIH'),
('K', 'K9', 'KEGIATAN PERSEMBAHAN');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jemaat`
--

CREATE TABLE `tbl_jemaat` (
  `kode_jemaat` char(8) NOT NULL,
  `nama_jemaat` varchar(50) NOT NULL,
  `wilayah` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jemaat`
--

INSERT INTO `tbl_jemaat` (`kode_jemaat`, `nama_jemaat`, `wilayah`) VALUES
('--TB', 'Abimantara', 'TB'),
('--V', 'Surya Handayani', 'V'),
('--VIII', 'Kel. ibu Harlimpat Dwi R', 'VIII'),
('--XII', 'Banar', 'XII'),
('01-I', 'I Trasip', 'I'),
('01-WB', 'Purwanto', 'WB'),
('02-I', 'Tri mardiyati', 'I'),
('02-TA', 'Sri Astuti Suratno', 'TA'),
('02-VII', 'Totok Mujoto', 'VII'),
('02-VIII', 'Jantan Vibrianto', 'VIII'),
('03-TA', 'ANT', 'TA'),
('03-X', 'Wahyuningsih', 'X'),
('04-TA', 'YT', 'TA'),
('05-TA', 'Manto Wardoyo', 'TA'),
('05-WB', 'Madiani Dyah Natalia', 'WB'),
('08-IX', 'Hartini Djoko Setyarso', 'IX'),
('100-I', 'Ibu Sumarningsih', 'I'),
('100-VII', 'Darmawan', 'VII'),
('101-VII', 'Pdt. Yudo Aster Daniel', 'VII'),
('102-IX', 'Wibowo Hadi', 'IX'),
('102-WB', 'Edi Asri Handoko', 'WB'),
('104-TA', 'Sigit Widi Permana', 'TA'),
('105-XIV', 'Ibu Soegiyono', 'XIV'),
('106-XIV', 'Ibu Soegiyono', 'XIV'),
('107-XIV', 'Ibu Soegiyono', 'XIV'),
('108-WB', 'Madiani Dyah Natalia', 'WB'),
('109-XIV', 'Ibu Sugiarti', 'XIV'),
('11-II', 'Kristianto', 'II'),
('111-XII', 'Putra Roman', 'XII'),
('111-XIV', 'Ibu Soegiyono', 'XIV'),
('112-XII', 'Candra M', 'XII'),
('114-XIV', 'Ibu Soegiyono', 'XIV'),
('12-VII', 'Hendro Martono', 'VII'),
('12-XIII', 'Suswanto', 'XIII'),
('121-VII', 'Sukini', 'VII'),
('124-IX', 'Poniyem', 'IX'),
('125-IX', 'Ibu Suharti', 'IX'),
('125-XII', 'Saud & Nurendah', 'XII'),
('13-WB', 'Harwantiningsih', 'WB'),
('131-IX', 'Augustin Hascaryo', 'IX'),
('132-IX', 'Audy Swari', 'IX'),
('133-IX', 'Brian Ganang', 'IX'),
('134-IX', 'Hascaryo Budi P.', 'IX'),
('14-III', 'Dr. Gapong', 'III'),
('14-WB', 'Maharani', 'WB'),
('144-XII', 'Priyo Hadi Nurjanto', 'XII'),
('15-X', 'Margareta', 'X'),
('16-III', 'Ibu Dulngajib', 'III'),
('16-IV', 'Basuki', 'IV'),
('16-XII', 'Endang Mulyaningsing', 'XII'),
('16-XIII', 'Tukijo Brojosumarto', 'XIII'),
('160-XII', 'Pdt. Seno Adhi Noegroho', 'XII'),
('161-XII', 'Kel. Edi Budi S.', 'XII'),
('166-XII', 'Sundari', 'XII'),
('170-XII', 'Christian Muryati', 'XII'),
('171-XII', 'Dorkas Natalia', 'XII'),
('18-IV', 'Aditya Prasta', 'IV'),
('18-IX', 'Partini Suherman', 'IX'),
('18-XI', 'Susiana Sahetapi', 'XI'),
('186-XIV', 'Ibu Sugiarti', 'XIV'),
('19-XI', 'Ibu Sugihardjo', 'XI'),
('2-TB', 'Pdt. Em. David Rubingan', 'TB'),
('2-X', 'Reso Pamarto', 'X'),
('20-IV', 'Gerson Tri Z', 'IV'),
('21-VII', 'Sukini', 'VII'),
('21-WS', 'Wasito Hadi A.', 'WS'),
('22-TA', 'Ibu Sutopo', 'TA'),
('22-VI', 'Sudaryanto', 'VI'),
('22-VIII', 'Djayusman', 'VIII'),
('22-X', 'J. Darmadi', 'X'),
('23-II', 'Djemingun', 'II'),
('23-IX', 'Erstanto', 'IX'),
('23-TA', 'Eni Rostiana', 'TA'),
('23-VIII', 'Djayusman', 'VIII'),
('23-XII', 'Kel. Edi Budi S', 'XII'),
('24-IX', 'Widodo', 'IX'),
('24-TA', 'Lukas Dody Setiawan', 'TA'),
('24-WB', 'Kel. Marsudi', 'WB'),
('24-X', 'E. Retno Watyas', 'X'),
('24-XI', 'Djumariah Santosa', 'XI'),
('24-XII', 'Kel. Edi Budi S.', 'XII'),
('25-WB', 'Susy Marwulan', 'Wb'),
('25-XI', 'Rukmono W', 'XI'),
('25-XIV', 'Sabar Sanyoto', 'XIV'),
('26-V', 'Tri Budiono', 'V'),
('26-XI', 'Tri Sutrapti', 'XI'),
('26-XIV', 'Ibu Sugiarti', 'XIV'),
('27-TA', 'Ery Huthomo', 'TA'),
('27-WB', 'Erlina Widya', 'WB'),
('27-X', 'Sulistyo Budi Purwoko', 'X'),
('27-XII', 'Ani Soemarto', 'XII'),
('28-II', 'Sudiman', 'II'),
('28-TA', 'Tasmidi', 'TA'),
('29-I', 'Sutaryono', 'I'),
('29-V', 'Maharyono', 'V'),
('3-TB', 'Herwi David Rubingan', 'TB'),
('3-X', 'Wahyuningsih', 'X'),
('3-XII', 'Suryantoro. MS', 'XII'),
('30-VII', 'Daniel Sunardjo', 'VII'),
('32-X', 'Christina Mulyaniaty', 'X'),
('32-XII', 'Ruhoro Wimbo Prakoso', 'XII'),
('33-III', 'Bernard Dwi C', 'III'),
('33-VIII', 'Dyah Wakitoningsih', 'VIII'),
('33-X', 'Kel. Soehadi', 'X'),
('34-XII', 'Eriantana', 'XII'),
('35-III', 'Retni Madu H.', 'III'),
('35-X', 'Maju Singarimbun', 'X'),
('37-WB', 'Sigit Widiatmoko', 'WB'),
('38-VII', 'Dewi Paramitasari', 'VII'),
('39-I', 'Hani Widiyanto', 'I'),
('39-II', 'Sugianto', 'II'),
('39-III', 'Suparyono', 'III'),
('4-VI', 'Yohanes Sutrisna', 'VI'),
('4-WS', 'Endang Sri P.', 'WS'),
('4-X', 'Step. Surono', 'X'),
('40-IX', 'Hariroh', 'IX'),
('40-XII', 'Ibu Bambang Mintarto', 'XII'),
('41-X', 'Suryo Widiarso', 'X'),
('42-I', 'Ediyanto', 'I'),
('42-III', 'Seno Adi Lukito', 'III'),
('42-VIII', 'Wenny Hartanto', 'VIII'),
('42-X', 'Dewi P', 'X'),
('43-IX', 'Endro Wahyono', 'IX'),
('44-IX', 'Bayu S', 'IX'),
('44-XII', 'Eunike Set Satyarini', 'XII'),
('45-II', 'Sadjiati', 'II'),
('45-IX', 'Prabowo Hadi', 'IX'),
('47-VI', 'Sudaryanto', 'VI'),
('48-II', 'Ny. Sunarti', 'II'),
('49-VI', 'Siswanto Pramoedyo', 'VI'),
('5-III', 'Alfitranto K. Hadi', 'III'),
('5-IX', 'Winarti', 'IX'),
('50-WS', 'Lies Aryanti', 'WS'),
('51-V', 'Soehardjinah Soemanto', 'V'),
('52-I', 'Tri Murdiyati', 'I'),
('55-VIII', 'NN', 'VIII'),
('56-TA', 'Ny. Ery Guthomo', 'TA'),
('56-WS', 'Stevanus Danan N.', 'WS'),
('57-WS', 'Hana Dewi Pratiwi', 'WS'),
('58-TA', 'Yonathan Pudjo', 'TA'),
('58-V', 'Eko Subagyo', 'V'),
('58-WS', 'Gideon Rizal G', 'WS'),
('59-VI', 'Pdt. Em R. S. Sungkono', 'VI'),
('60-V', 'Hartoyo', 'V'),
('62-XI', 'Kelik Mulyono', 'XI'),
('63-TA', 'Johny Sianipar', 'TA'),
('63-V', 'Ris Kusbianto', 'V'),
('63-XI', 'Aria Kurniawati', 'XI'),
('64-XI', 'Krisbianto', 'XI'),
('65-III', 'Merry Tini', 'III'),
('66-TA', 'S. Rini S', 'TA'),
('66-TB', 'Graciades Hadi Herdanto', 'TB'),
('66-V', 'Ibu Sarimin', 'V'),
('67-IX', 'Tutik Winarti', 'IX'),
('67-TB', 'Nugroho Agung', 'TB'),
('67-V', 'Purwantini Yuli Astuti', 'V'),
('67-VII', 'Puspa Rahayu', 'VII'),
('69-X', 'Adhitya Wisnu Jatmiko', 'X'),
('7-II', 'Edi Winarti', 'II'),
('7-III', 'Budi Prihantoro', 'III'),
('70-III', 'Sastro Diharjo', 'III'),
('71-III', 'Ruri Setyawan', 'III'),
('71-VII', 'Madijana', 'VII'),
('71-XIV', 'Ester Rika Kasiyem', 'XIV'),
('72-XI', 'Antonius Nugroho', 'XI'),
('72-XII', 'Inti Aris Rinanto', 'XII'),
('73-X', 'Suparmi Soenarno', 'X'),
('76-I', 'Apri Nurgroho', 'I'),
('76-IX', 'Sri Yulianingrum', 'IX'),
('77-I', 'Iriani Setyanawati', 'I'),
('77-VII', 'Daniel Tri', 'VII'),
('78-I', 'I. Kartini M.', 'I'),
('78-TA', 'Nurcahyo Nugroho', 'TA'),
('78-X', 'Ery Utami', 'X'),
('79-I', 'Eny Rusnoto', 'I'),
('79-XII', 'Sundari', 'XII'),
('8-I', 'Djatmiko', 'I'),
('80-II', 'Kurniawati', 'II'),
('80-IX', 'Suratijah', 'IX'),
('80-XII', 'Pranata Widagdo', 'XII'),
('81-IX', 'Partini Suherman', 'IX'),
('82-I', 'Sumarto', 'I'),
('82-WB', 'Arvina', 'WB'),
('83-TA', 'Joko Pamungkas', 'TA'),
('83-WB', 'Dadit C', 'WB'),
('85-II', 'Rani Sugiarti', 'II'),
('86-I', 'Rendiyanto', 'I'),
('86-II', 'Ellyandra', 'II'),
('87-I', 'Budianto', 'I'),
('9-I', 'Budi Prasetyo', 'I'),
('9-II', 'Septi Suwarsi', 'II'),
('9-X', 'Retno Wijayanti', 'X'),
('9-XII', 'Sukesmi', 'XII'),
('90-TB', 'Pdt. Kristi', 'TB'),
('91-III', 'Rinentah Dwi Yulianti', 'III'),
('91-TB', 'Daniel Krismantoro', 'Tb'),
('92-TB', 'Hendrayana Mandala', 'TB'),
('95-I', 'Mulyadi', 'I'),
('95-VII', 'Rigen Pratitisari', 'VII'),
('95-WB', 'Widiatmo', 'WB'),
('96-WB', 'Marfuah', 'WB'),
('97-WB', 'Ismarti Andari', 'WB'),
('98-TA', 'Suyud', 'TA'),
('AAW-NN', 'AAW', 'NN'),
('AD7-NN', 'AD7', 'NN'),
('AH-X', 'Antonov Hendratmoko', 'X'),
('AP-XII', 'Agus Prasmanto', 'XII'),
('AS-XI', 'Ari Sudarsono', 'Xi'),
('AY-X', 'Aditya + Yuda', 'X'),
('B46-V', 'B46', 'V'),
('BWC-NN', 'BWC', 'NN'),
('D-IX', 'Daniel', 'IX'),
('D-VI', 'Djohan', 'VI'),
('DA-VIII', 'Daniel Aditya', 'VIII'),
('DKR-NN', 'kel. Dr. Kunto', 'NN'),
('DR-TB', 'Pdt. Em. David Rubingan', 'TB'),
('DS-XII', 'DS', 'XII'),
('E-TA', 'Elisia', 'TA'),
('ES-NN', 'Endang Saptini', 'NN'),
('GK--', 'GKI KLATEN', '-'),
('GKJC-NN', 'GKJ Cawas', 'NN'),
('GKJM-NN', 'GKJ Manissrenggo', 'NN'),
('H-WB', 'Harwantiningsih', 'WB'),
('KM--', 'Keluarga Marsudi', '-'),
('M/US-NN', 'M/US', 'NN'),
('MS15-NN', 'MS15', 'NN'),
('NN-I', 'NN', 'I'),
('NN-II', 'NN', 'II'),
('NN-III', 'NN', 'III'),
('NN-IV', 'NN', 'IV'),
('NN-IX', 'NN', 'IX'),
('NN-NN', 'NN', 'NN'),
('NN-TA', 'NN', 'TA'),
('NN-TB', 'NN', 'TB'),
('NN-V', 'NN', 'V'),
('NN-VI', 'NN', 'VI'),
('NN-VII', 'NN', 'VII'),
('NN-VIII', 'NN', 'VIII'),
('NN-WB', 'NN WB', 'WB'),
('NN-WS', 'NN', 'WS'),
('NN-X', 'NN', 'X'),
('NN-XI', 'NN', 'XI'),
('NN-XII', 'NN XII', 'XII'),
('NN-XIII', 'NN', 'XIII'),
('NN-XIV', 'NN', 'XIV'),
('PB-NN', 'PB', 'NN'),
('PB-V', 'P. Merapi B46', 'V'),
('PP-III', 'PP', 'III'),
('PP-NN', 'PP', 'NN'),
('PP-VII', 'PP', 'VII'),
('PW-NN', 'PW', 'NN'),
('PW-XI', 'Panti Wredha', 'XI'),
('S-X', 'Sarwoko', 'X'),
('S-XIII', 'Suswanto', 'XIII'),
('SH-V', 'Suya Handayani', 'V'),
('SK-XII', 'Sugeng Kiswanto', 'XII'),
('U.S.A.', 'U.S.A.', 'U.S.A.'),
('UKR--', 'Umat Kristiani RSUPS', '-'),
('US-NN', 'US', 'NN'),
('W12-XII', 'Wilayah 12', 'XII'),
('W4-IV', 'Wilayah 4', 'IV'),
('YT-XII', 'YT', 'XII');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_akun`
--

CREATE TABLE `tbl_jenis_akun` (
  `kode_sub_gol_akun` char(4) NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `nama_kode_jenis_akun` varchar(100) NOT NULL,
  `jenis_partisipan` enum('Individu','Non Individu') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jenis_akun`
--

INSERT INTO `tbl_jenis_akun` (`kode_sub_gol_akun`, `kode_jenis_akun`, `nama_kode_jenis_akun`, `jenis_partisipan`) VALUES
('D101', 'D101.01', 'Kas Bendahara', NULL),
('D101', 'D101.02', 'Kas Setoran Persembahan', NULL),
('D101', 'D101.03', 'Kas Kecil', NULL),
('D101', 'D101.04', 'Biaya Kegiatan Bidang/Komisi Lainnya', NULL),
('D201', 'D201.01', 'Bank MANDIRI', NULL),
('D201', 'D201.02', 'BRI', NULL),
('D201', 'D201.03', 'Bank Pembanguan Daerah', NULL),
('D201', 'D201.04', 'Bank Mandiri, No.AD449238 Tgl.20-02-2014 Dana Tanah Belger', NULL),
('D201', 'D201.05', 'BCA, No.AH667747 Tgl.11-02-2014 Dana Tanah Belger', NULL),
('D201', 'D201.06', 'BCA, No.AH667748 Tgl.11-02-2014 Dana Tanah Belger', NULL),
('D201', 'D201.07', 'BNI, No.AA276470 David Rubingan, Dana Abadi Reksa Putra', NULL),
('D202', 'D202.01', 'Bank Mandiri, Cadangan Dana Kemandirian (Pdt. David R.)', NULL),
('D202', 'D202.02', 'Tabungan Bank BRI Tampung Bunga Deposito (Pdt. Siswadi, S.Si)', NULL),
('D202', 'D202.03', 'Bank Mandiri, Pdt.Yudo Aster Daniel, Dana Kemandirian', NULL),
('D202', 'D202.05', 'Bank Mandiri, Cadangan Dana Emiritus', NULL),
('D202', 'D202.06', 'Bank Mandiri, Cadangan Dana Tanah Belger', NULL),
('D202', 'D202.07', 'Bank BPD DIY Senopati-Tampung Bunga Deposito', NULL),
('D202', 'D202.08', 'Bank BPD DIY Senopati, penampungan gaji karyawan', NULL),
('D202', 'D202.09', 'GKJ Gondokusuman, Tabungan BRI BritAma', NULL),
('D202', 'D202.10', 'BRI Dana Sawo Kembar Emergency (Pdt.Seno AN)', NULL),
('D202', 'D202.11', 'BCA, Cadangan Dana Pembangunan di Tanah Belger', NULL),
('D202', 'D202.12', 'BRI Tabungan BANSOSKOM', NULL),
('D202', 'D202.13', 'BRI -Tanah Makam, Rudjito QQ GKJ Gondokusuman', NULL),
('D202', 'D202.14', ' BRI -SOEWONDO QQ PANTI WERDA*DON*GKJ GONDOKUSUMAN', NULL),
('D202', 'D202.15', 'BRI -Soewondo QQ Panti Werda GKJ GK*Persembahan*', NULL),
('D202', 'D202.16', 'BRI PW*Persembahan', NULL),
('D203', 'D203.02', 'Giro BRI Katamso', NULL),
('D301', 'D301.01', 'Modal Pengelola Pinjaman', NULL),
('D301', 'D301.02', 'Christina Wijayanti, Pinjm.Penyelesaian Urusan Tanah di Wonosobo', 'Individu'),
('D301', 'D301.03', 'Christina Wijayanti, pinjam Persembhn Tanah BelGer', 'Individu'),
('D302', 'D302.02', 'Pinjaman dana kpd STAK MARTURIA via Bapelklas XXVIII Yk.Selatan', NULL),
('D302', 'D302.10', 'Pinjaman Lainnya', NULL),
('D401', 'D401.01', 'Uang Muka Untuk Pelayanan Diakonia', NULL),
('D401', 'D401.05', 'Uang Muka Bayar Listrik dan Air beban Kantor dan Pendeta', NULL),
('D401', 'D401.06', 'Uang Muka Bayar Telepon beban Kantor dan Pendeta', NULL),
('D401', 'D401.10', 'Uang muka biaya-biaya keperluan Kantor Gereja', NULL),
('D401', 'D401.11', 'Uang muka Biaya Hidup dan Gaji Tenaga GKJ', NULL),
('D401', 'D401.13', 'Uang Muka Kegiatan Bidang/Komis', NULL),
('D401', 'D401.14', 'Uang Muka Komisi', NULL),
('D401', 'D401.99', 'UM KR dan Minggu Pagi', NULL),
('D501', 'D501.01', 'Tanah Kantor & Toko TPK', NULL),
('D501', 'D501.02', 'Tanah Gedung Gereja', NULL),
('D501', 'D501.03', 'Tanah Rumah Pastori', NULL),
('D501', 'D501.04', 'Tanah Rumah Asrama', NULL),
('D501', 'D501.05', 'Tanah Kosong', NULL),
('D501', 'D501.06', 'Tanah Makam', NULL),
('D502', 'D502.01', 'Bangunan kantor & Toko TPK', NULL),
('D502', 'D502.02', 'Bangunan Gereja', NULL),
('D502', 'D502.03', 'Bangunan Rmh Pastori', NULL),
('D502', 'D502.04', 'Bangunan Rumah Asrama', NULL),
('D503', 'D503.01', 'Kendaraan Mobil', NULL),
('D503', 'D503.02', 'Sepeda Motor', NULL),
('D504', 'D504.01', 'Mesin Foto Copy', NULL),
('D504', 'D504.02', 'Mesin Risograph EZ-231 No.76740224, Kwits.18-7-2013', NULL),
('D504', 'D504.03', 'Komputer & Perlengkapannya', NULL),
('D504', 'D504.04', 'Air Condotion', NULL),
('D505', 'D505.01', 'Meja', NULL),
('D505', 'D505.02', 'Kursi', NULL),
('D505', 'D505.03', 'Almari', NULL),
('D506', 'D506.01', 'Peralatan Sound system', NULL),
('D506', 'D506.02', 'Alat Musik dan Perlengkapannya', NULL),
('D506', 'D506.03', 'Alat Musik Keroncong', NULL),
('D506', 'D506.04', 'Alat Musik Gamelan', NULL),
('D507', 'D507.01', 'Kamera', NULL),
('D507', 'D507.02', 'Komputer', 'Individu'),
('D507', 'D507.03', 'Handy & Walky Talky', NULL),
('D507', 'D507.04', 'Tripod dan Monopod', NULL),
('D507', 'D507.05', 'Power Supply', NULL),
('D507', 'D507.06', 'TV Monitor', NULL),
('D507', 'D507.07', 'Video-Audio Splitter', NULL),
('D507', 'D507.08', 'Video Switcher, Splitter, Converter', NULL),
('D507', 'D507.09', 'Tool Box', NULL),
('D507', 'D507.10', 'Kabel', NULL),
('D507', 'D507.11', 'Televisi & LCD Projector', NULL),
('D507', 'D507.12', 'Perlengkapan Multimedia Lainnya', NULL),
('D508', 'D508.01', 'Perlengkapan Persembahan & Busana', NULL),
('D508', 'D508.02', 'Perlengkapan Perjamuan Kudus', NULL),
('D508', 'D508.03', 'Perlengkapan Kotak Salib', NULL),
('D508', 'D508.04', ' Hiasan Dinding', NULL),
('D508', 'D508.05', 'Perlengkapan Meja', NULL),
('D508', 'D508.06', 'Perlengkapan Bunga', NULL),
('D508', 'D508.07', 'Perlengkapan Mimbar', NULL),
('D508', 'D508.08', 'Peralatan Makan & Dapur', NULL),
('D508', 'D508.99', 'Perlengkapan & Peralatan Lain-lain', NULL),
('D801', 'D801.01', 'Komisi Kebaktian', NULL),
('D801', 'D801.02', 'Komisi Pendukung Kebaktian', NULL),
('D801', 'D801.03', 'Komisi Seni & Budaya', NULL),
('D801', 'D801.04', 'Koordinasi dan Adm Bidang', NULL),
('D802', 'D802.01', 'Komisi Pekabaran Injil dan Komunikasi', NULL),
('D802', 'D802.02', 'Komisi Pendidikan', NULL),
('D802', 'D802.03', 'Komisi Pralenan', NULL),
('D802', 'D802.04', 'Komisi Sawokembar Emergency', NULL),
('D802', 'D802.05', 'BANSOSKOM', NULL),
('D802', 'D802.06', 'Koordinasi dan Administrasi Bidang', NULL),
('D803', 'D803.01', 'Komisi Pengkaderan', NULL),
('D803', 'D803.02', 'Komisi Anak', NULL),
('D803', 'D803.03', 'Komisi Remaja', NULL),
('D803', 'D803.04', 'Komisi Pemuda', NULL),
('D803', 'D803.05', 'Komisi Dewasa Muda', NULL),
('D803', 'D803.06', 'Komisi Pemberdayaan Kel. (PKM)', NULL),
('D803', 'D803.07', 'Komisi Warga Dewasa Wanita Jemaat', NULL),
('D803', 'D803.08', 'Komisi Adiyuswa', NULL),
('D803', 'D803.09', 'Koordinasi dan Adm. Bidang', NULL),
('D804', 'D804.01', 'Komisi Pembangunan', NULL),
('D804', 'D804.02', 'Komisi Pemeliharaan & Inventaris Gereja', NULL),
('D804', 'D804.03', 'Koordinasi dan Administrasi Bidang', NULL),
('D805', 'D805.01', 'Komisi Perencanaan Program dan Anggaran', NULL),
('D805', 'D805.02', 'Komisi Pengendalian Program', NULL),
('D805', 'D805.03', 'Komisi Litbang dan Evaluasi', NULL),
('D805', 'D805.04', 'Koordinasi dan Administrasi Bidang', NULL),
('D806', 'D806.01', 'Komisi Pengawasan dan Pemeriksaan Administrasi', NULL),
('D806', 'D806.02', 'Komisi Pengawasan dan Pemeriksaan Keuangan dan Logistik', NULL),
('D806', 'D806.03', 'Koordinasi dan Aministrasi Bidang Wasrik', NULL),
('D807', 'D807.01', 'PA Rekso Putra Bag Putra', NULL),
('D807', 'D807.02', 'PA Rekso Putra Bag Putri', NULL),
('D807', 'D807.03', 'Bantuan biaya hidup di Surabaya unt 2 org', NULL),
('D807', 'D807.3', 'Bantuan', NULL),
('D808', 'D808.01', 'Biaya Pengurusan Surat Tanah', NULL),
('D808', 'D808.02', 'Biaya Kegiatan Bidang/Komisi Lainnya', NULL),
('D808', 'D808.03', 'Biaya balik nama', NULL),
('D808', 'D808.04', '-', NULL),
('D809', 'D809.01', 'Panitia Pemanggilan Pendeta', NULL),
('D809', 'D809.02', 'Tanah Makam', NULL),
('D809', 'D809.03', 'Pembangunan Tanah Belger', NULL),
('D809', 'D809.04', 'Peringatan HUT GKJ Gondokusuman', NULL),
('D809', 'D809.06', 'Bantuan untuk semua wilayah', NULL),
('D809', 'D809.08', '', NULL),
('D809', 'D809.09', '', NULL),
('D810', 'D810.01', 'Komisi Sosial', NULL),
('D810', 'D810.02', 'Komisi Bantuan dan Santunan', NULL),
('D810', 'D810.03', 'Komisi Panti Werda', NULL),
('D810', 'D810.04', 'Komisi Pastoral', NULL),
('D810', 'D810.05', 'Komisi Transformatif & PEJ', NULL),
('D810', 'D810.06', 'Koordinasi dan Adm Bidang Diakonia', NULL),
('D811', 'D811.01', 'Pemeliharaan Iman', NULL),
('D811', 'D811.02', 'Pastoral', NULL),
('D811', 'D811.03', 'Konseling', NULL),
('D811', 'D811.04', 'Nilai/Karakter', NULL),
('D811', 'D811.05', 'Koordinasi dan Administrasi Bidang', NULL),
('D901', 'D901.01', 'Alat Tulis Kantor', NULL),
('D901', 'D901.02', 'Alkitab, Nyanyian Rohani, Buku2 Lainnya', NULL),
('D901', 'D901.03', 'Biaya Surat Menyurat', NULL),
('D901', 'D901.04', 'Kertas Berbagai ukuran dan jenis', NULL),
('D901', 'D901.05', 'Biaya Fotocopy di luar kantor', NULL),
('D901', 'D901.06', 'Cetak Formulir dan Kalender', NULL),
('D901', 'D901.07', 'Cetak Warta Jemaat', NULL),
('D901', 'D901.08', 'Surat Kabar untuk Kantor dan Pendeta', NULL),
('D902', 'D902.01', 'Listrik', NULL),
('D902', 'D902.02', 'Telepon', NULL),
('D902', 'D902.03', 'Pajak Bumi dan Bangunan', NULL),
('D902', 'D902.04', 'Pajak Kendaraan Bermotor (STNK)', NULL),
('D902', 'D902.05', 'Bensin Kendaraan Dinas', NULL),
('D902', 'D902.06', 'Rumah Tangga Kantor Gereja', NULL),
('D902', 'D902.07', 'Tugas Pengamanan', NULL),
('D902', 'D902.08', 'Biaya Lembur Pegawai', NULL),
('D902', 'D902.09', 'Bunga Mimbar', NULL),
('D903', 'D903.01', 'Iuran Dana Kemandirian Klasis', NULL),
('D903', 'D903.02', 'Iuran Dana Abadi Klasis/ Sinode', NULL),
('D903', 'D903.03', 'Iuran PGI Wilayah/ Kotamadya', NULL),
('D903', 'D903.04', 'Perjamuan Kudus dan Kelengkapannya', NULL),
('D903', 'D903.05', 'Honor Organis Pemberkatan Nikah', NULL),
('D903', 'D903.06', 'Bantuan Antar Gereja', NULL),
('D903', 'D903.07', 'Bantuan Untuk Lembaga Kristen', NULL),
('D903', 'D903.08', 'Pelayanan Kasih Tanda Kasih', NULL),
('D903', 'D903.09', 'Sidang Majelis Gereja Terbuka (SMGT)', NULL),
('D903', 'D903.10', 'Rapat-rapat', NULL),
('D903', 'D903.11', 'Pisah Sambut Majelis', NULL),
('D903', 'D903.12', 'Retreat Majelis/ Karyawan', NULL),
('D904', 'D904.01', 'Biaya Hidup Pendeta', NULL),
('D904', 'D904.02', 'Gaji Tenaga Non Pendeta', NULL),
('D904', 'D904.03', 'Transport Khotbah Pendeta Tamu', NULL),
('D904', 'D904.04', 'Tukar Mimbar Klasis Sinode/PGI', NULL),
('D904', 'D904.05', 'Bantuan Transport', NULL),
('D904', 'D904.06', 'Bantuan untuk Pamulang', NULL),
('D904', 'D904.07', 'Bantuan Transport Guru Agama', NULL),
('D904', 'D904.08', 'Perjalanan Dinas', NULL),
('D904', 'D904.09', 'Honorarium Tenaga Tidak Tetap', NULL),
('D905', 'D905.01', 'Pembayaran Uang Pensiun Pendeta', NULL),
('D905', 'D905.02', 'Pembayaran Uang Pensiun Non Pendeta', NULL),
('D905', 'D905.03', 'Voucher HP', NULL),
('D905', 'D905.04', 'Perawatan Kesehatan', NULL),
('D905', 'D905.05', 'Tunjangan Cuti Tahunan, Tunjangan Khusus', NULL),
('D905', 'D905.06', 'Tunjangan Hari Raya Natal', 'Individu'),
('D905', 'D905.07', 'Pakaian dan Sepatu', 'Individu'),
('D905', 'D905.08', 'Biaya Pendidikan Anak Pendeta', 'Individu'),
('D905', 'D905.09', 'Pembayaran Premi Pensiun', NULL),
('D905', 'D905.10', 'Pengadaan Buku untuk Pendeta', NULL),
('D905', 'D905.11', 'Studi Lanjut Pendeta/Pelatihan/Seminar Pendeta', NULL),
('D905', 'D905.12', 'Asuransi bagi Pendeta yng Aktif', NULL),
('D906', 'D906.01', 'Pemeliharaan Gedung oleh Kantor', NULL),
('D906', 'D906.02', 'Pemeliharaan Inventaris', NULL),
('D906', 'D906.03', 'Pemeliharaan Taman', NULL),
('D906', 'D906.04', 'Perawatan Kendaraan Dinas', NULL),
('D906', 'D906.05', 'Perawatan Peralatan Kantor', NULL),
('D906', 'D906.06', 'Sewa Tenda dan Kursi', 'Individu'),
('D906', 'D906.07', 'Pengadaan Peralatan Kantor ', NULL),
('D906', 'D906.08', 'Kegiatan Kebersihan Lingkungan Gereja', NULL),
('D907', 'D907.01', 'Biaya Simpan Dana di Bank', NULL),
('D907', 'D907.02', 'Pajak Jasa Tabungan Bank', NULL),
('D908', 'D908.01', 'Cadangan Tabungan Kemandirian', NULL),
('D908', 'D908.02', 'Cadangan Tabungan Emiritus Pendeta', NULL),
('D908', 'D908.03', '', NULL),
('D999', 'D999.01', 'Dana Penyangga', NULL),
('D999', 'D999.99', 'Biaya Kegiatan Kantor Lainnya', NULL),
('K101', 'K101.01', 'Titipan Persembahan Peduli Rekso Putro', NULL),
('K101', 'K101.02', 'Titipan Diakonia', NULL),
('K101', 'K101.03', 'Titipan Lainnya/Umum', NULL),
('K101', 'K101.05', 'Dana Pembayaran Pensiun dari Sinode', NULL),
('K101', 'K101.07', 'Titipan Persembahan Bea Siswa', NULL),
('K101', 'K101.14', 'Titipan Uang Iuran Pensiun Pdt. Fendi S.', NULL),
('K101', 'K101.90', 'Selisih Lebih Kas', NULL),
('K102', 'K102.01', 'Cadangan Dana Emiritus', NULL),
('K102', 'K102.02', 'Cadangan Dana Kemandirian', NULL),
('K501', 'K501.01', 'SALDO AWAL TAHUN 2016', 'Non Individu'),
('K701', 'K701.01', 'Aktiva Bersih', NULL),
('K701', 'K701.02', 'Kenaikan Aktiva Bersih', NULL),
('K901', 'K901.01', 'Persembahan Bulanan', 'Individu'),
('K901', 'K901.02', 'Persembahan Mingguan', 'Non Individu'),
('K901', 'K901.03', 'Persembahan Perjamuan Kudus', 'Non Individu'),
('K901', 'K901.04', 'Persembahan Mirunggan', 'Individu'),
('K901', 'K901.05', 'Persembahan Persepuluhan', 'Individu'),
('K901', 'K901.06', 'Persembahan Baptis, Sidhi, Pertobatan', 'Non Individu'),
('K901', 'K901.07', 'Persembahan Pemberkatan Pernikahan', 'Non Individu'),
('K901', 'K901.08', 'Persembahan Hari Besar Kristen', 'Non Individu'),
('K901', 'K901.09', 'Persembahan Dana Kemandirian', 'Non Individu'),
('K901', 'K901.10', 'Persembahan Dana Emeritus', 'Non Individu'),
('K901', 'K901.11', 'Persembahan Pemakaian Gd Pertemuan', 'Individu'),
('K901', 'K901.12', 'Persembahan Tanah Pastori', 'Individu'),
('K901', 'K901.13', 'Persembahan untuk Bea Siswa', 'Individu'),
('K901', 'K901.14', 'Persembahan untuk Diakonia / Panti Wreda', 'Individu'),
('K901', 'K901.15', 'Persembahan Peduli Reksa Putra', 'Individu'),
('K901', 'K901.16', 'Persembahan untuk Tanah Belger', 'Individu'),
('K901', 'K901.99', 'Persembahan Lain-lain', 'Non Individu'),
('K902', 'K902.01', 'Persembahan Yayasan Sawo Kembar', 'Non Individu'),
('K902', 'K902.02', 'Rekening bunga bansoskom', NULL),
('K903', 'K903.01', 'Bunga Deposito', NULL),
('K903', 'K903.02', 'Bunga Tabungan dan Giro Bank', NULL),
('K999', 'K999.01', 'Hasil Penjualan Kalender', 'Individu'),
('K999', 'K999.02', 'Persembahan untuk Tanah Belger', 'Individu'),
('K999', 'K999.99', 'Penerimaan Lain-lain', 'Individu');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_komisi`
--

CREATE TABLE `tbl_komisi` (
  `kode_komisi` char(8) NOT NULL,
  `nama_komisi` varchar(50) NOT NULL,
  `kode_bidang` char(8) NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `pengurus_1` char(8) DEFAULT NULL,
  `pengurus_2` char(8) DEFAULT NULL,
  `pengurus_3` char(8) DEFAULT NULL,
  `tgl_dibentuk` date NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota`
--

CREATE TABLE `tbl_nota` (
  `kode_nota` char(10) NOT NULL,
  `kode_toko` char(8) NOT NULL,
  `kode_transaksi` int(6) DEFAULT NULL,
  `tgl_pengeluaran` date NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `jumlah` int(15) NOT NULL,
  `pelaksana` varchar(50) NOT NULL,
  `waktu_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kode_admin` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penanggungjawab`
--

CREATE TABLE `tbl_penanggungjawab` (
  `kode_penanggungjawab` char(8) NOT NULL,
  `nama_penanggungjawab` varchar(30) NOT NULL,
  `jabatan` varchar(20) NOT NULL,
  `alamat_penanggungjawab` varchar(90) NOT NULL,
  `no_telepon_penanggungjawab` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_penanggungjawab`
--

INSERT INTO `tbl_penanggungjawab` (`kode_penanggungjawab`, `nama_penanggungjawab`, `jabatan`, `alamat_penanggungjawab`, `no_telepon_penanggungjawab`) VALUES
('99', 'Personal Khusus', 'Pelaksana', '-', '0'),
('ADM 1', 'DESSY NATALIA', 'KEPALA KANTOR', 'PURBONEGARAN', '089671448774'),
('ADM 2', 'ASTUTI WIDAYATI', 'ADMINISTRASI', 'KLITREN LOR', '0274513573'),
('B101', 'ANTONOV HENDRATMOKO', 'KETUA KOMISI KAS', 'Komp Balapan', '081804212990'),
('B102', 'RUDJITO', 'KETUA KOMISI AKUTANS', 'Mino Martani', '085729255799'),
('B103', 'TRI HANTORO', 'KETUA BID. KEHARTAAN', 'Yogyakarta', '081328896443'),
('B104', 'Fransiska Herlina Sri Hargiyan', 'Anggota Komisi Akunt', 'mancasan pandowoharjo sleman\r\n', '08175457464'),
('B105', 'Siwi Pujianti. W.', 'Anggota komisi', '-', '0'),
('BANK', 'BANK', 'BANK', '-', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penerimaan_individual`
--

CREATE TABLE `tbl_penerimaan_individual` (
  `no_kwitansi` char(12) NOT NULL,
  `kode_transaksi` int(6) DEFAULT NULL,
  `tanggal_penerimaan` date NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `pemberi` char(8) NOT NULL,
  `jumlah` int(15) NOT NULL,
  `penerima` char(8) NOT NULL,
  `bulan` varchar(15) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  `waktu_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kode_admin` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profil_gereja`
--

CREATE TABLE `tbl_profil_gereja` (
  `nama_gereja` varchar(100) NOT NULL,
  `alamat_gereja` varchar(100) NOT NULL,
  `email_gereja` varchar(30) NOT NULL,
  `nomer_telepon_gereja` varchar(40) NOT NULL,
  `logo_gereja` varchar(20) NOT NULL,
  `tahun_pembukuan` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_profil_gereja`
--

INSERT INTO `tbl_profil_gereja` (`nama_gereja`, `alamat_gereja`, `email_gereja`, `nomer_telepon_gereja`, `logo_gereja`, `tahun_pembukuan`) VALUES
('GEREJA KRISTEN JAWA GONDOKUSUMAN', 'JL. DR. WAHIDIN SUDIROHUSODO 40 YOGYAKARTA 55222', 'gkj.gondokusuman@yahoo.com', '(0274) 513570', 'logo-gereja.png', 2017);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rencana_anggaran`
--

CREATE TABLE `tbl_rencana_anggaran` (
  `kode_jenis_akun` char(7) NOT NULL,
  `rencana_anggaran` bigint(20) NOT NULL,
  `tahun_perencanaan` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rencana_anggaran`
--

INSERT INTO `tbl_rencana_anggaran` (`kode_jenis_akun`, `rencana_anggaran`, `tahun_perencanaan`) VALUES
('D201.02', 0, 2016),
('D201.03', 0, 2016),
('D801.01', 103000000, 2016),
('D801.02', 85080000, 2016),
('D801.03', 37200000, 2016),
('D801.04', 1000000, 2016),
('D802.01', 26400000, 2016),
('D802.02', 24000000, 2016),
('D802.03', 10000000, 2016),
('D802.04', 7500000, 2016),
('D802.05', 12600000, 2016),
('D802.06', 1000000, 2016),
('D803.01', 12000000, 2016),
('D803.02', 24200000, 2016),
('D803.03', 12400000, 2016),
('D803.04', 24900000, 2016),
('D803.05', 3500000, 2016),
('D803.06', 4500000, 2016),
('D803.07', 21525000, 2016),
('D803.08', 33610000, 2016),
('D803.09', 1000000, 2016),
('D804.01', 179100000, 2016),
('D804.02', 50000000, 2016),
('D804.03', 1000000, 2016),
('D805.01', 1000000, 2016),
('D805.02', 1000000, 2016),
('D805.03', 1500000, 2016),
('D805.04', 25200000, 2016),
('D806.01', 0, 2016),
('D807.01', 10000000, 2016),
('D807.02', 12000000, 2016),
('D807.03', 12000000, 2016),
('D809.01', 107000000, 2016),
('D809.08', 17000000, 2016),
('D809.09', 18000000, 2016),
('D810.01', 18000000, 2016),
('D810.02', 140897000, 2016),
('D810.03', 19200000, 2016),
('D810.04', 3600000, 2016),
('D810.05', 10000000, 2016),
('D810.06', 4000000, 2016),
('D901.01', 10000000, 2016),
('D901.02', 6000000, 2016),
('D901.03', 3500000, 2016),
('D901.04', 10000000, 2016),
('D901.05', 2000000, 2016),
('D901.06', 10000000, 2016),
('D901.07', 45000000, 2016),
('D901.08', 5000000, 2016),
('D902.01', 30000000, 2016),
('D902.02', 15000000, 2016),
('D902.03', 5000000, 2016),
('D902.04', 5000000, 2016),
('D902.05', 6000000, 2016),
('D902.06', 23000000, 2016),
('D902.07', 5000000, 2016),
('D902.08', 10000000, 2016),
('D902.09', 15000000, 2016),
('D903.01', 65000000, 2016),
('D903.02', 14000000, 2016),
('D903.03', 600000, 2016),
('D903.04', 75000000, 2016),
('D903.05', 2000000, 2016),
('D903.06', 10000000, 2016),
('D903.07', 10000000, 2016),
('D903.08', 15000000, 2016),
('D903.09', 10000000, 2016),
('D903.10', 60000000, 2016),
('D903.11', 15000000, 2016),
('D903.12', 55000000, 2016),
('D904.01', 316000000, 2016),
('D904.02', 240000000, 2016),
('D904.03', 22500000, 2016),
('D904.04', 10000000, 2016),
('D904.05', 25000000, 2016),
('D904.08', 25000000, 2016),
('D904.09', 96000000, 2016),
('D905.01', 100000000, 2016),
('D905.02', 24000000, 2016),
('D905.03', 11000000, 2016),
('D905.04', 50000000, 2016),
('D905.05', 84000000, 2016),
('D905.06', 60000000, 2016),
('D905.07', 20000000, 2016),
('D905.09', 30000000, 2016),
('D905.10', 10000000, 2016),
('D905.11', 25000000, 2016),
('D905.12', 45000000, 2016),
('D906.04', 12000000, 2016),
('D906.05', 6000000, 2016),
('D906.06', 20000000, 2016),
('D906.07', 10000000, 2016),
('D906.08', 6000000, 2016),
('D907.01', 3500000, 2016),
('D907.02', 5000000, 2016),
('D908.01', 32000000, 2016),
('D908.02', 147000000, 2016),
('D908.03', 90000000, 2016),
('K901.01', 350000000, 2016),
('K901.02', 1500000000, 2016),
('K901.03', 175000000, 2016),
('K901.04', 240000000, 2016),
('K901.05', 225000000, 2016),
('K901.06', 55000000, 2016),
('K901.07', 70000000, 2016),
('K901.08', 125000000, 2016),
('K901.09', 147000000, 2016),
('K901.10', 90000000, 2016),
('K901.11', 3500000, 2016),
('K901.14', 24000000, 2016),
('K901.99', 80512000, 2016),
('K903.01', 55000000, 2016);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sub_gol_akun`
--

CREATE TABLE `tbl_sub_gol_akun` (
  `kode_golongan` char(2) NOT NULL,
  `kode_sub_gol_akun` char(4) NOT NULL,
  `nama_sub_gol_akun` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sub_gol_akun`
--

INSERT INTO `tbl_sub_gol_akun` (`kode_golongan`, `kode_sub_gol_akun`, `nama_sub_gol_akun`) VALUES
('D1', 'D101', 'Kas'),
('D2', 'D201', 'BANK DEPOSITO'),
('D2', 'D202', 'BANK TABUNGAN'),
('D2', 'D203', 'GIRO BANK'),
('D3', 'D301', 'PINJAMAN PEGAWAI'),
('D3', 'D302', 'PINJAMAN LAINNYA'),
('D4', 'D401', 'UANG MUKA'),
('D5', 'D501', 'INVENTARIS TANAH'),
('D5', 'D502', 'INVENTARIS BANGUNAN'),
('D5', 'D503', 'INVENTARIS KENDARAAN'),
('D5', 'D504', 'INVENTARIS ALAT PERKANTORAN'),
('D5', 'D505', 'INVENTARIS MEBELAIR'),
('D5', 'D506', 'SOUND SYSTEM DAN ALAT MUSIK'),
('D5', 'D507', 'INVENTARIS MULTIMEDIA'),
('D5', 'D508', 'INVENTARIS PERLENGKAPAN & PERALATAN'),
('D8', 'D801', 'BIDANG IBADAH'),
('D8', 'D802', 'BIDANG KESAKSIAN / PELAYANAN'),
('D8', 'D803', 'BIDANG PEMBINAAN WARGA GEREJA'),
('D8', 'D804', 'BIDANG PEMBANGUNAN DAN PEMELIHARAAN'),
('D8', 'D805', 'BADAN PERENCANAAN PROGRAM DAN LITBANG'),
('D8', 'D806', 'BADAN PENGAWASAN DAN PEMERIKSAAN'),
('D8', 'D807', 'BADAN SOSIAL KRISTEN'),
('D8', 'D808', 'LAIN-LAIN'),
('D8', 'D809', 'BADAN ADHOCK'),
('D8', 'D810', 'BIDANG DIAKONIA'),
('D8', 'D811', 'BADAN PEMELIHARAAN IMAN, PASTORAL DAN KONSELING'),
('D9', 'D901', 'PENGELOLAAN SEKRETARIAT'),
('D9', 'D902', 'PENGELOLAAN RUMAH TANGGA'),
('D9', 'D903', 'PERSEKUTUAN KESAKSIAN DAN PELAYANAN'),
('D9', 'D904', 'BIAYA HIDUP, GAJI DAN HONORARIUM'),
('D9', 'D905', 'PEMBERIAN TUNJANGAN'),
('D9', 'D906', 'PEMELIHARAAN & PENGADAAN INVENTARIS'),
('D9', 'D907', 'BIAYA BANK'),
('D9', 'D908', 'DANA CADANGAN'),
('D9', 'D999', 'KEGIATAN KANTOR LAINNYA'),
('K1', 'K101', 'SIMPANAN TITIPAN'),
('K1', 'K102', 'CADANGAN'),
('K5', 'K501', 'SALDO AWAL TAHUN'),
('K7', 'K701', 'AKTIVA BERSIH'),
('K9', 'K901', 'KEGIATAN PERSEMBAHAN'),
('K9', 'K902', 'YAYASAN'),
('K9', 'K903', 'BUNGA BANK'),
('K9', 'K999', 'PENERIMAAN LAINNYA');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_toko`
--

CREATE TABLE `tbl_toko` (
  `kode_toko` char(8) NOT NULL,
  `nama_toko` varchar(20) NOT NULL,
  `alamat_toko` varchar(50) NOT NULL,
  `no_telepon_toko` varchar(15) NOT NULL,
  `cp_toko` varchar(50) NOT NULL,
  `waktu_tambah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_toko`
--

INSERT INTO `tbl_toko` (`kode_toko`, `nama_toko`, `alamat_toko`, `no_telepon_toko`, `cp_toko`, `waktu_tambah`) VALUES
('TOKO1', 'Bank Mandiri', '-', '0', '0', '0000-00-00 00:00:00'),
('TOKO10', 'BJ baut', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO11', 'HJs Alat Listrik', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO12', 'Kedaulatan Rakyat', 'Jl.P.Mangkubumi 40-46', '565683', '-', '0000-00-00 00:00:00'),
('TOKO13', 'Honda', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO14', 'toko andajaya', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO15', 'Restaurant Ngudi Rej', 'jalan magelang km.5,5', '0', '-', '0000-00-00 00:00:00'),
('TOKO16', 'RM. andalas raya', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO17', 'exodus', 'klitren lor gk3/375', '55222', '-', '0000-00-00 00:00:00'),
('TOKO18', 'ayam goreng co-de', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO19', 'PARAHITA', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO2', 'Servis Fotocopy', '-', '0', '0', '0000-00-00 00:00:00'),
('TOKO20', 'toko plastik demanga', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO21', 'Pandan Leaf Bakery a', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO22', 'Wasantara', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO24', 'Kompas', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO25', 'TB. Taman Pustaka Kr', 'Jl. dr. Wahidin No. 38 A Yogyakarta', '274512449', '-', '0000-00-00 00:00:00'),
('TOKO26', 'Sinar Mataram', 'Jl. Mataram no 31', '515937', '-', '0000-00-00 00:00:00'),
('TOKO27', 'Pos Indonesia', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO28', 'SPBU', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO29', 'Berkat Listrik', 'Jl. Juminahan 3 ', '554739', '-', '0000-00-00 00:00:00'),
('TOKO3', 'ayam bakar taliwang', 'Jl.Dr Wahidin', '0', '-', '0000-00-00 00:00:00'),
('TOKO30', 'Duta Minang', 'Jl. Laksda Adisucipto 153', '544546', '-', '0000-00-00 00:00:00'),
('TOKO31', 'Sahara', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO32', 'UD. Caroline', 'Jl. Janti Gang Bakung NO.11A', '0', '-', '0000-00-00 00:00:00'),
('TOKO33', 'AG CARWASH', 'JL.MOJO 10 BACIRO YOGYA', '0', '-', '0000-00-00 00:00:00'),
('TOKO34', 'PERTAMINA OK-LIMART', 'JL.KYAI MOJO 65 YOGYAKARTA', '274511421', '-', '0000-00-00 00:00:00'),
('TOKO35', 'Bank Mandiri', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO36', 'Piramida Agency', 'Jl. Tompeyan No 204 Tegalrejo', '510267', '-', '0000-00-00 00:00:00'),
('TOKO37', 'Ayam Goreng Bu Tini', 'Jl. Sultan Agung No. 17', '7490233', '-', '0000-00-00 00:00:00'),
('TOKO38', 'RS Bethesda', 'Jl. Jendral Sudirman 70', '562246', '-', '0000-00-00 00:00:00'),
('TOKO39', 'Toko Kaca Lima Satu', 'Jl. P. Diponegoro 49-51', '566552', '-', '0000-00-00 00:00:00'),
('TOKO4', 'ELS', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO40', 'Putra Sehat', '-', '585294', '-', '0000-00-00 00:00:00'),
('TOKO41', 'Goodwill', 'Jl. Kusbini NO 2', '0', '-', '0000-00-00 00:00:00'),
('TOKO42', 'Red Studio', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO43', 'BANK BNI', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO44', 'UKDW', 'JL.DR.WAHIDIN S NO.5-25 YOGYAKARTA', '274563929', '-', '0000-00-00 00:00:00'),
('TOKO45', 'BANK BRI', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO46', 'HEBAT KOMPUTAMA', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO47', 'PRUDENTIAL', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO48', 'BE LIVING', 'SUDIRMAN 48 C', '811257555', '-', '0000-00-00 00:00:00'),
('TOKO49', 'TOKO ROTI MURNI', 'JL.BINTARAN KIDUL 30', '376768', '-', '0000-00-00 00:00:00'),
('TOKO5', 'Ero Lampu', 'Jl. C. Simanjuntak No.44 Yogyakarta', '274566633', '-', '0000-00-00 00:00:00'),
('TOKO50', 'BU BAGYO COLOMBO', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO51', 'TOKO MIRASA', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO52', 'AMBARUKMO PLAZA', 'JL.LAKSDA ADISUCIPTO KM6', '0', '-', '0000-00-00 00:00:00'),
('TOKO53', 'PT TRANS RETAIL INDO', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO54', 'Mirota Kampus', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO55', 'RM. Mbah Gito', 'Jl.Nyi Ageng nis no.9 rejowinangun', '85228408800', '-', '2016-10-24 04:15:11'),
('TOKO56', 'lain - lain', '-', '0', '0', '2016-10-24 04:19:00'),
('TOKO57', 'Duta Minang', 'JL.Laksda Adisucipto 153', '274544546', '-', '2016-11-16 05:31:47'),
('TOKO58', 'Gita Anjana', 'JL.P.Diponegoro no 48 -50', '274515050', '-', '2016-11-18 04:46:56'),
('TOKO59', 'Zaara Textile', 'Jl.Urip Sumoharjo no 33', '2749158333', '-', '2016-11-18 04:59:36'),
('TOKO6', 'SPBU', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO60', 'Pukis Bikang', '-', '0', '-', '2016-11-18 05:01:50'),
('TOKO61', 'Cemerlang Kemindo', '-', '0', '-', '2016-11-18 05:19:17'),
('TOKO62', 'Karya Anugerah', '-', '0', '-', '2016-11-18 05:34:36'),
('TOKO63', 'Apotek K24', '-', '0', '-', '2016-11-21 01:56:13'),
('TOKO64', 'Virgo Tenda', '-', '0', '-', '2016-11-21 02:29:18'),
('TOKO65', 'Prabu Motor', '-', '0', '-', '2016-11-21 03:41:23'),
('TOKO66', 'Sami Jaya', '-', '0', '-', '2016-11-21 03:41:42'),
('TOKO67', 'Mie Bandung', 'kompleks Kridosono', '0', '-', '2016-11-21 04:29:35'),
('TOKO68', 'RK Studio', '-', '0', '-', '2016-11-21 04:39:25'),
('TOKO69', 'Metro Fotocopi', '-', '0', '-', '2016-11-22 05:13:39'),
('TOKO7', 'goodwill', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO70', 'Code RM', '-', '0', '-', '2016-11-22 06:30:22'),
('TOKO71', 'Bale Ayu', '-', '0', '-', '2016-11-23 05:06:37'),
('TOKO72', 'Bakmi Mbah Gito', '-', '0', '-', '2016-11-23 05:10:57'),
('TOKO73', 'Superindo', '-', '0', '-', '2016-11-23 05:40:12'),
('TOKO74', 'Sinar Mataram', '-', '0', '-', '2016-12-06 03:47:28'),
('TOKO75', 'Hypermart', '-', '0', '-', '2016-12-06 05:44:49'),
('TOKO76', 'Gardena', '-', '0', '-', '2016-12-06 05:49:29'),
('TOKO77', 'Mirota Bakery', '-', '0', '-', '2016-12-06 06:03:46'),
('TOKO78', 'Nusantara', '-', '0', '-', '2016-12-07 05:04:51'),
('TOKO8', 'toko merah ', '-', '0', '-', '0000-00-00 00:00:00'),
('TOKO9', 'Lembaga pembinaan da', '-', '0', '-', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `kode_transaksi` int(6) NOT NULL,
  `tahun_pembukuan` year(4) NOT NULL,
  `kode_bukti_transaksi` char(8) NOT NULL,
  `kode_jenis_akun` char(7) NOT NULL,
  `tipe_transaksi` enum('DEBET','KREDIT') NOT NULL,
  `jumlah` int(15) NOT NULL,
  `uraian` varchar(150) DEFAULT NULL,
  `waktu_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kode_admin` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`kode_admin`),
  ADD UNIQUE KEY `kode_admin` (`kode_admin`),
  ADD KEY `kode` (`kode`);

--
-- Indexes for table `tbl_bidang`
--
ALTER TABLE `tbl_bidang`
  ADD PRIMARY KEY (`kode_bidang`),
  ADD KEY `pengurus_1` (`pengurus_1`,`pengurus_2`,`pengurus_3`),
  ADD KEY `pengurus_2` (`pengurus_2`),
  ADD KEY `pengurus_3` (`pengurus_3`);

--
-- Indexes for table `tbl_bukti_transaksi`
--
ALTER TABLE `tbl_bukti_transaksi`
  ADD PRIMARY KEY (`kode_bukti_transaksi`),
  ADD KEY `kode_admin` (`kode_admin`),
  ADD KEY `kode_jenis_akun` (`kode_jenis_akun`),
  ADD KEY `bendahara` (`bendahara`,`penerima`,`penyetor`,`kode_admin`),
  ADD KEY `penerima` (`penerima`),
  ADD KEY `penyetor` (`penyetor`);

--
-- Indexes for table `tbl_gol_akun`
--
ALTER TABLE `tbl_gol_akun`
  ADD PRIMARY KEY (`kode_golongan`),
  ADD KEY `kode_golongan` (`kode_golongan`);

--
-- Indexes for table `tbl_jemaat`
--
ALTER TABLE `tbl_jemaat`
  ADD PRIMARY KEY (`kode_jemaat`);

--
-- Indexes for table `tbl_jenis_akun`
--
ALTER TABLE `tbl_jenis_akun`
  ADD PRIMARY KEY (`kode_jenis_akun`),
  ADD KEY `kode_sub_gol_akun` (`kode_sub_gol_akun`);

--
-- Indexes for table `tbl_komisi`
--
ALTER TABLE `tbl_komisi`
  ADD PRIMARY KEY (`kode_komisi`),
  ADD KEY `kode_bidang` (`kode_bidang`,`pengurus_1`,`pengurus_2`,`pengurus_3`),
  ADD KEY `pengurus_1` (`pengurus_1`),
  ADD KEY `pengurus_2` (`pengurus_2`),
  ADD KEY `pengurus_3` (`pengurus_3`);

--
-- Indexes for table `tbl_nota`
--
ALTER TABLE `tbl_nota`
  ADD PRIMARY KEY (`kode_nota`,`kode_toko`),
  ADD KEY `kode_jenis_akun` (`kode_jenis_akun`),
  ADD KEY `kode_transaksi` (`kode_transaksi`,`kode_admin`),
  ADD KEY `kode_admin` (`kode_admin`),
  ADD KEY `kode_toko` (`kode_toko`);

--
-- Indexes for table `tbl_penanggungjawab`
--
ALTER TABLE `tbl_penanggungjawab`
  ADD PRIMARY KEY (`kode_penanggungjawab`);

--
-- Indexes for table `tbl_penerimaan_individual`
--
ALTER TABLE `tbl_penerimaan_individual`
  ADD PRIMARY KEY (`no_kwitansi`),
  ADD KEY `kode_transaksi` (`kode_transaksi`,`kode_jenis_akun`,`pemberi`,`penerima`,`kode_admin`),
  ADD KEY `kode_jenis_akun` (`kode_jenis_akun`),
  ADD KEY `pemberi` (`pemberi`),
  ADD KEY `penerima` (`penerima`),
  ADD KEY `kode_admin` (`kode_admin`);

--
-- Indexes for table `tbl_rencana_anggaran`
--
ALTER TABLE `tbl_rencana_anggaran`
  ADD PRIMARY KEY (`kode_jenis_akun`,`tahun_perencanaan`),
  ADD KEY `kode_jenis_akun` (`kode_jenis_akun`),
  ADD KEY `kode_jenis_akun_2` (`kode_jenis_akun`);

--
-- Indexes for table `tbl_sub_gol_akun`
--
ALTER TABLE `tbl_sub_gol_akun`
  ADD PRIMARY KEY (`kode_sub_gol_akun`),
  ADD KEY `kode_sub_gol_akun` (`kode_sub_gol_akun`),
  ADD KEY `kode_golongan` (`kode_golongan`);

--
-- Indexes for table `tbl_toko`
--
ALTER TABLE `tbl_toko`
  ADD PRIMARY KEY (`kode_toko`);

--
-- Indexes for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`kode_transaksi`,`tahun_pembukuan`),
  ADD KEY `kode_bukti_transaksi` (`kode_bukti_transaksi`),
  ADD KEY `kode_admin` (`kode_admin`),
  ADD KEY `kode_jenis_akun` (`kode_jenis_akun`),
  ADD KEY `kode_bukti_transaksi_2` (`kode_bukti_transaksi`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_bidang`
--
ALTER TABLE `tbl_bidang`
  ADD CONSTRAINT `tbl_bidang_ibfk_1` FOREIGN KEY (`pengurus_1`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
  ADD CONSTRAINT `tbl_bidang_ibfk_2` FOREIGN KEY (`pengurus_2`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
  ADD CONSTRAINT `tbl_bidang_ibfk_3` FOREIGN KEY (`pengurus_3`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`);

--
-- Constraints for table `tbl_bukti_transaksi`
--
ALTER TABLE `tbl_bukti_transaksi`
  ADD CONSTRAINT `tbl_bukti_transaksi_ibfk_1` FOREIGN KEY (`kode_jenis_akun`) REFERENCES `tbl_jenis_akun` (`kode_jenis_akun`),
  ADD CONSTRAINT `tbl_bukti_transaksi_ibfk_2` FOREIGN KEY (`kode_admin`) REFERENCES `tbl_admin` (`kode_admin`),
  ADD CONSTRAINT `tbl_bukti_transaksi_ibfk_3` FOREIGN KEY (`bendahara`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
  ADD CONSTRAINT `tbl_bukti_transaksi_ibfk_4` FOREIGN KEY (`penerima`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
  ADD CONSTRAINT `tbl_bukti_transaksi_ibfk_5` FOREIGN KEY (`penyetor`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`);

--
-- Constraints for table `tbl_jenis_akun`
--
ALTER TABLE `tbl_jenis_akun`
  ADD CONSTRAINT `tbl_jenis_akun_ibfk_1` FOREIGN KEY (`kode_sub_gol_akun`) REFERENCES `tbl_sub_gol_akun` (`kode_sub_gol_akun`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_komisi`
--
ALTER TABLE `tbl_komisi`
  ADD CONSTRAINT `tbl_komisi_ibfk_1` FOREIGN KEY (`kode_bidang`) REFERENCES `tbl_bidang` (`kode_bidang`),
  ADD CONSTRAINT `tbl_komisi_ibfk_2` FOREIGN KEY (`pengurus_1`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
  ADD CONSTRAINT `tbl_komisi_ibfk_3` FOREIGN KEY (`pengurus_2`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
  ADD CONSTRAINT `tbl_komisi_ibfk_4` FOREIGN KEY (`pengurus_3`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`);

--
-- Constraints for table `tbl_nota`
--
ALTER TABLE `tbl_nota`
  ADD CONSTRAINT `tbl_nota_ibfk_1` FOREIGN KEY (`kode_jenis_akun`) REFERENCES `tbl_jenis_akun` (`kode_jenis_akun`),
  ADD CONSTRAINT `tbl_nota_ibfk_2` FOREIGN KEY (`kode_transaksi`) REFERENCES `tbl_transaksi` (`kode_transaksi`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_nota_ibfk_3` FOREIGN KEY (`kode_admin`) REFERENCES `tbl_admin` (`kode_admin`),
  ADD CONSTRAINT `tbl_nota_ibfk_4` FOREIGN KEY (`kode_toko`) REFERENCES `tbl_toko` (`kode_toko`);

--
-- Constraints for table `tbl_penerimaan_individual`
--
ALTER TABLE `tbl_penerimaan_individual`
  ADD CONSTRAINT `tbl_penerimaan_individual_ibfk_1` FOREIGN KEY (`kode_jenis_akun`) REFERENCES `tbl_jenis_akun` (`kode_jenis_akun`),
  ADD CONSTRAINT `tbl_penerimaan_individual_ibfk_2` FOREIGN KEY (`pemberi`) REFERENCES `tbl_jemaat` (`kode_jemaat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_penerimaan_individual_ibfk_3` FOREIGN KEY (`penerima`) REFERENCES `tbl_penanggungjawab` (`kode_penanggungjawab`),
  ADD CONSTRAINT `tbl_penerimaan_individual_ibfk_4` FOREIGN KEY (`kode_admin`) REFERENCES `tbl_admin` (`kode_admin`),
  ADD CONSTRAINT `tbl_penerimaan_individual_ibfk_5` FOREIGN KEY (`kode_transaksi`) REFERENCES `tbl_transaksi` (`kode_transaksi`) ON DELETE SET NULL;

--
-- Constraints for table `tbl_rencana_anggaran`
--
ALTER TABLE `tbl_rencana_anggaran`
  ADD CONSTRAINT `tbl_rencana_anggaran_ibfk_1` FOREIGN KEY (`kode_jenis_akun`) REFERENCES `tbl_jenis_akun` (`kode_jenis_akun`);

--
-- Constraints for table `tbl_sub_gol_akun`
--
ALTER TABLE `tbl_sub_gol_akun`
  ADD CONSTRAINT `tbl_sub_gol_akun_ibfk_1` FOREIGN KEY (`kode_golongan`) REFERENCES `tbl_gol_akun` (`kode_golongan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD CONSTRAINT `tbl_transaksi_ibfk_1` FOREIGN KEY (`kode_bukti_transaksi`) REFERENCES `tbl_bukti_transaksi` (`kode_bukti_transaksi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_transaksi_ibfk_2` FOREIGN KEY (`kode_jenis_akun`) REFERENCES `tbl_jenis_akun` (`kode_jenis_akun`),
  ADD CONSTRAINT `tbl_transaksi_ibfk_3` FOREIGN KEY (`kode_admin`) REFERENCES `tbl_admin` (`kode_admin`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
