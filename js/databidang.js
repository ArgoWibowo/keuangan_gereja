 $(document).ready(function() {
    // Setup - add a text input to each footer cell
   

    uniform("formDetailBidang",180);

    $(document).on('click','#mask, .btn_close, .close',function(){
        closeDetailBidang();
    });

});

function closeDetailBidang(){
    reloadBidang();
    $('#mask').remove();  
    $('#detailBidang').fadeOut(200); 
    $('#detailBidang').css({ 
        'visibility' :'hidden',
        'display' :'none'
    }); 
}

 reloadBidang();
 function reloadBidang(){
  //  alert("masuk");

	$.ajax({
        type	: 'POST',
        url		: 'ajax_databidang.php',
        data 	: 'actionfunction=showData&page=1',
        cache 	: false,
        success: function(result) {
     //       alert(result);
        	    
            var a = result.indexOf("<");
        	var rows = result.substring(0,a);
        	$('#lblJumlahDataBidang').text(rows);
        	result = result.substring(a);
			$('#isiTabelBidang').html(result);
			$('#jumlahData').text($('#jumlahDataBidang').val());
			$.ajax({
			    url:"ajax_databidang.php",
		        type:"POST",	
		        data:"actionfunction=pagination&page=1",
		        cache: false,
		        success: function(response){
					$('#paginationBidang').html(response);                    

				}
	   		});
        },
    });
//alert("masuk3");

    $('#paginationBidang').on('click','.page-numbers',function(){
       $page = $(this).attr('href');
	   $pageind = $page.indexOf('page=');
	   $page = $page.substring(($pageind+5));
       
	   $.ajax({
		    url:"ajax_databidang.php",
	        type:"POST",
	        data:"actionfunction=showData&page="+$page,
	        cache: false,
	        success: function(response1){
			   $.ajax({
			    url:"ajax_databidang.php",
		        type:"POST",
		        data:"actionfunction=pagination&page="+$page,
		        cache: false,
		        success: function(response){
		        	$('#isiTabelBidang').html(response1);
					$('#paginationBidang').html(response);
				}
	   		});
			 
			}
	   });
	return false;
	});
  //  alert("masuk2");

}  


function dipilih(e){
   $(e).addClass('selected').siblings().removeClass('selected');  
}



function pilihanDataBidang(index, bidang_kode){
	reloadBidang();

    
    if(index == "0") {
        $.ajax({
        type    : 'POST',
        url     : 'ajax_editbidang.php',
        data    : {'kode':bidang_kode},
        dataType: 'json',
        success: function(result) {
                document.getElementById("lblTitleEditBidang").innerHTML = "Detail Bidang";               
                document.getElementById("btnBatalEditBidang").style.visibility = "hidden";
                document.getElementById("btnEditBidang").style.visibility = "hidden";
                document.getElementById("btnKembaliBidang").style.visibility = "visible";
                                

                $('#editKodeBidang').val(result[0]);
                $('#editKodeBidang2').val(result[0]);
                
                $('#editNamaBidang').val(result[1]);
                $('.editJenisAkunBidang').val(result[2]);
                $('.editPengurus1Bidang').val(result[3]);
                $('.editPengurus2Bidang').val(result[4]);
                $('.editPengurus3Bidang').val(result[5]);
                $('#editTanggal').val(result[6]);
                $('#editDeskripsiBidang').val(result[7]);     
                $('#editStatusBidang').val(result[8]);                                    
                
                $( "#editKodeBidang" ).prop( "disabled", true );
                $( "#editNamaBidang" ).prop( "disabled", true );
                $( "#editTanggal" ).prop( "disabled", true );
                $( "#editDeskripsiBidang" ).prop( "disabled", true );
                $( "#editStatusBidang" ).prop( "disabled", true );                
                $( ".editPengurus1Bidang" ).prop( "disabled", true );
                $( ".editPengurus2Bidang" ).prop( "disabled", true );
                $( ".editPengurus3Bidang" ).prop( "disabled", true );
                $( ".editJenisAkunBidang" ).prop( "disabled", true );

                $("div.databidang").slideUp("700");
                $("div.editbidang").slideDown("700");                
            },

        });
     
    }
    else if (index == "1"){
		
        //$('#formEditBidang').html(result);

        $.ajax({
        type	: 'POST',
        url		: 'ajax_editbidang.php',
        data 	: {'kode':bidang_kode.replace(/_spasi_/g, " ")},
        dataType: 'json',
        success: function(result) {
                document.getElementById("lblTitleEditBidang").innerHTML = "Edit Bidang";               
                document.getElementById("btnBatalEditBidang").style.visibility = "visible";
                document.getElementById("btnEditBidang").style.visibility = "visible";
                document.getElementById("btnKembaliBidang").style.visibility = "hidden";
              
                $('#editKodeBidang').val(result[0]);
                $('#editKodeBidang2').val(result[0]);
                
                $('#editNamaBidang').val(result[1]);
                $('.editJenisAkunBidang').val(result[2]);
                $('.editPengurus1Bidang').val(result[3]);
                $('.editPengurus2Bidang').val(result[4]);
                $('.editPengurus3Bidang').val(result[5]);
                //$('#editTanggal').val(result[6]);
                var tanggal = result[6].substr(8,2);
                var bulan = result[6].substr(5,2);
                var tahun = result[6].substr(0,4);
                
                $('#bidangEdit_tanggal').val(parseInt(tanggal));
                $('#bidangEdit_bulan').val(parseInt(bulan));
                $('#bidangEdit_tahun').val(parseInt(tahun));


                $('#editDeskripsiBidang').val(result[7]);     
                $('#editStatusBidang').val(result[8]);                                    
                

                $( "#editKodeBidang" ).prop( "disabled", false );
                $( "#editNamaBidang" ).prop( "disabled", false );
                $( "#editTanggal" ).prop( "disabled", false );
                $( "#editDeskripsiBidang" ).prop( "disabled", false );
                $( "#editStatusBidang" ).prop( "disabled", false );
                $( ".editPengurus1Bidang" ).prop( "disabled", false );
                $( ".editPengurus2Bidang" ).prop( "disabled", false );
                $( ".editPengurus3Bidang" ).prop( "disabled", false );
                $( ".editJenisAkunBidang" ).prop( "disabled", false );
                placeholderselect(); 

            	$("div.databidang").slideUp("700");
            	$("div.editbidang").slideDown("700");                
	        },

	    });

	}else{
        var cek = confirm("Apakah yakin akan menghapus bidang ini ?");
		if (cek){
            $.ajax({
            type    : 'POST',
            url     : 'ajax_hapusbidang.php',
            data    : {'kode':bidang_kode.replace(/_spasi_/g, " ")},
            success: function(result) {
                    if (result == true){
                        $('pilihanDataBidang').val("");
                        alert("Data bidang terpilih berhasil dihapus");
                        reloadBidang();
                    }else{
                        alert("Data bidang terpilih tidak dapat dihapus." + '\n' + "Silahkan cek apakah data tersebut masih berhubungan dengan tabel komisi atau tidak.");
              
                    }
                    
                },
            });
        }else{
            $('pilihanDataBidang').val(""); 
            reloadBidang();
        }
        
	}
}


$("#btnKembaliBidang").click(function(){
    reloadBidang();        
                    $("div.editbidang").slideUp("700");
                $("#right>div.databidang").slideDown("700");

});

$("#btnBatalEditBidang").click(function(){
    reloadBidang();        
    $("div.editbidang").slideUp("700");
    $("#right>div.databidang").slideDown("700");

});


$("form#formTambahBidang").submit(function(){
  
  var formData2 = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_tambahbidang.php",
        type: 'POST',
        data: formData2,
        success: function (data) {
            //alert(data);     
            if ( data == true){
            	
                $('#kodeBidang').val("");
				$('#namaBidang').val("");
				$('.jenisakunBidang').val("");
				$('.pengurus2Bidang').val("");
				$('.pengurus3Bidang').val("");
                $('.pengurus1Bidang').val("");
                $('#bidang_tanggal').val("");
                $('#bidang_bulan').val("");
                $('#bidang_tahun').val("");
                
                $('#deskripsiBidang').val("");                

                /*$("div.tambahbidang").slideUp("700");
				$("#right>div.databidang").slideDown("700");
				$("#right>div.databidang div").slideDown("700");*/
                reloadBidang();
				alert("Bidang berhasil ditambahkan");                
          }
          else{
            alert("Gagal menambahkan bidang");
          }
          reloadBidang();  
          //location.reload();        

        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
});

$("form#formEditBidang").submit(function(){
  var formData3 = new FormData($(this)[0]);

   $.ajax({
        url: "ajax_editbidang.php",
        type: 'POST',
        data: formData3,
        success: function (data3) {            

             if (data3 == true){
            	$('#editKodeBidang').val("");
            	$('#editNamaBidang').val("");
				$('.editJenisAkunBidang').val("");
				$('.editPengurus1Bidang').val("");
                $('.editPengurus2Bidang').val("");
                $('.editPengurus3Bidang').val("");
                $('#bidangEdit_tanggal').val("");
                $('#bidangEdit_bulan').val("");
                $('#bidangEdit_tahun').val("");
             
                $('#editStatusBidang').val("");
                $('#editDeskripsiBidang').val("");
                
                alert("Data bidang berhasil diedit");
        		
                $("div.editbidang").slideUp("700");
				$("#right>div.databidang").slideDown("700");
				//$("#right>div.databidang div").slideDown("700");                    
				
            }            
            reloadBidang(); 
            //location.reload();  
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

function searchBidang(){
	var keyword = $('#searchBidang').val();
	var sortby  = $('#sortDataBidang option:selected').val();
  	$.ajax({
        type	: 'POST',
        url		: 'ajax_databidang.php',
        data 	: 'actionfunction=showData&page=1&keyword='+keyword+'&sortby='+sortby,
        cache 	: false,
        success: function(result) {
        	var a = result.indexOf("<");
        	var rows = result.substring(0,a);
        	$('#lblJumlahDataBidang').text(rows);
        	result = result.substring(a);
			$('#isiTabelBidang').html(result);
			$.ajax({
			    url:"ajax_databidang.php",
		        type:"POST",
		        data:"actionfunction=pagination&page=1&keyword="+keyword+'&sortby='+sortby,
		        cache: false,
		        success: function(response){
					$('#paginationBidang').html(response);
				}
	   		});
	   		$("#right>div.databidang").slideDown("700");
				$("#right>div.databidang div").slideDown("700");
        },
    });
}

function checkKodeBidang(){
      var kodeInput = document.getElementById("kodeBidang").value;
      $.ajax({
            url: "ajax_tambahbidang.php",
            type: "POST",
            data: {
                "kode_bidang_check": kodeInput
            },
            success: function(data){

            	if(kodeInput != "" && data == true){
					$("#warningUsernameBidang").attr("src","image/check.png");
                    document.getElementById("#warningUsernameBidang").style.visibility = "visible";
                }
                else {
                    document.getElementById("warningUsernameBidang").style.visibility = "visible";
                    $("#warningUsernameBidang").attr("src","image/warning.png");
                }   
            }
        });
        return false;
}

function detailBidangData(div){

    var loginBox = div;

    window.location.href= div;

    var kodeBidangDetail = String($('#databidang table tr.selected td:eq(1)').html()).trim();

    $.ajax({
                type: "POST",
                url: "loadDetailBidang.php",
                dataType: "json",
                data: {"kodeBidangDetail": kodeBidangDetail},
                success: function(data){
                    if(data){

                            
                            document.getElementById("kodeBidangDetail").value = data[0];
                            document.getElementById("namaBidangDetail").value = data[1];
                            document.getElementById("jenisAkunBidangDetail").value = data[2];
                            document.getElementById("pengurus1BidangDetail").value = data[3];
                            document.getElementById("pengurus2BidangDetail").value = data[4];
                            document.getElementById("pengurus3BidangDetail").value = data[5];
                            document.getElementById("tglDibentukBidangDetail").value = data[6];
                            document.getElementById("desBidangDetail").value = data[7];
                            document.getElementById("statusBidangDetail").value = data[8];

                            var popMargTop = ($(loginBox).height() + 24) / 2; 
                            var popMargLeft = ($(loginBox).width() + 24) / 2; 
                            //  Add the mask to body
                            $('body').append('<div id="mask"></div>');
                            $('#mask').fadeIn(300);
                            $("#detailBidang").fadeIn(500);
                            $("#detailBidang").css({ 
                                'visibility' :'visible',
                                'display' :'block',
                                'margin-top' : -popMargTop,
                                'margin-left' : -popMargLeft
                            }); 
                            
                    }
                    
                }
    });
}

function printDataKomisiBidang(div)
{
    $.ajax({
            type: "POST",
            url: "loadKop.php",
            success: function(data){
                if(data){
                   var divToPrint=document.getElementById(div);
                   newWin= window.open("");
                   newWin.document.write(data+"<br>"+"<br>"+divToPrint.outerHTML);
                   newWin.print();
                   newWin.close();
                }
            }
    });
   
}



var tableToExcel = (function() {
    var tamp;
    $.ajax({
            type: "POST",
            url: "loadKopTanpaLogo2.php",
            success: function(data){
                if(data){
                    tamp = data;
                }
            }
    });
      var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
      return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: tamp + "<table border=1>" + table.innerHTML+ "</table>"}
        window.location.href = uri + base64(format(template, ctx))

      }
})()



function exportBidang(){
    var value = $('#selectExportDataBidang option:selected').val();
    /*if (value == "CSV"){
        window.location='exportCSV.php?exp=datatoko';
        $('#selectExportDataToko').val(''); 
    }*/if (value =="XLS"){
        
         var originalContents = $("#databidang").html();
         $("#databidang").find("tr th:nth-child(" + 7 + ")").remove();
         $("#databidang").find("tr td:nth-child(" + 7 + ")").remove();
         $("#databidang table tbody tr td select").remove();
         
         tableToExcel("databidang", 'bidang');
         $('#selectExportDataBidang').val(''); 

         $("#databidang").html(originalContents);
         reloadBidang();
    }else if (value == "PDF"){
        window.location='exportPDFBidang.php/?pdffor=bidang';   
        $('#selectExportDataBidang').val(''); 
        // convertToPdfDataTokoPenanggungjawab("#tabletoko");
    }
}
