$(document).ready(function() {
    $("#divBoxPencarian").hide();
    reloadToko();
    uniform("formTambahToko",300);
    uniform("formEditToko",300);
});


//reloadToko();
 function reloadToko(){
	$.ajax({
        type	: 'POST',
        url		: 'ajax_datatoko.php',
        data 	: 'actionfunction=showData&page=1',
        cache 	: false,
        success: function(result) {
        	var a = result.indexOf("<");
        	var rows = result.substring(0,a);
        	$('#lblJumlahDataToko').text(rows);
        	result = result.substring(a);
			$('#isiTabelToko').html(result);
			$('#jumlahData').text($('#jumlahDataToko').val());
			$.ajax({
			    url:"ajax_datatoko.php",
		        type:"POST",	
		        data:"actionfunction=pagination&page=1",
		        cache: false,
		        success: function(response){
					$('#paginationToko').html(response);


				}
	   		});
        },
    });

    $('#paginationToko').on('click','.page-numbers',function(){
       $page = $(this).attr('href');
	   $pageind = $page.indexOf('page=');
	   $page = $page.substring(($pageind+5));
       
	   $.ajax({
		    url:"ajax_datatoko.php",
	        type:"POST",
	        data:"actionfunction=showData&page="+$page,
	        cache: false,
	        success: function(response1){
			   $.ajax({
			    url:"ajax_datatoko.php",
		        type:"POST",
		        data:"actionfunction=pagination&page="+$page,
		        cache: false,
		        success: function(response){
		        	$('#isiTabelToko').html(response1);
					$('#paginationToko').html(response);
                    $('#selectExportDataToko').val(""); 
				}
	   		});
			 
			}
	   });
	return false;
	});
}  


function dipilih(e){
   $(e).addClass('selected').siblings().removeClass('selected');  
}

function pilihanDataToko(index, toko_kode){
	if (index == "1"){
		$.ajax({
        type	: 'POST',
        url		: 'ajax_edittoko.php',
        data 	: {'kode':toko_kode.replace("_spasi_", " ")},
        dataType: 'json',
        success: function(result) {
                $('pilihanDataToko').val("");
        		$('#editKodeToko').val(result[0]);
                $('#editKodeToko2').val(result[0]);
				$('#editNamaToko').val(result[1]);
				$('#editAlamatToko').val(result[2]);
				$('#editNoTelepon').val(result[3]);
				$('#editCpToko').val(result[4]);

				$("div.datatoko").slideUp("700");
				$("div.edittoko").slideDown("700");
                
	        },
	    });
	}else{
        var cek = confirm("Apakah yakin akan menghapus toko ini ?");
		if (cek){
            $.ajax({
            type    : 'POST',
            url     : 'ajax_hapustoko.php',
            data    : {'kode':toko_kode.replace("_spasi_", " ")},
            success: function(result) {
                    if (result == true){
                        $('pilihanDataToko').val("");
                        alert("Data toko terpilih berhasil dihapus");
                        reloadToko();
                    }else{
                        alert(result);
                    }
                    
                },
            });
        }else{
            $('pilihanDataToko').val(""); 
            reloadToko();
        }
        reloadToko();

	}
}

$("form#formTambahToko").submit(function(){
  var formData = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_tambahtoko.php",
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
            if ( data == true){
            	$('#kodeToko').val("");
				$('#namaToko').val("");
				$('#alamatToko').val("");
				$('#noTelepon').val("");
				$('#cpToko').val("");

              	$("div.tambahtoko").slideUp("700");
				$("#right>div.datatoko").slideDown("700");
				$("#right>div.datatoko div").slideDown("700");
				reloadToko();
          }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

$("form#formEditToko").submit(function(){

  var formData = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_edittoko.php",
        type: 'POST',
        data: formData,
        async: false,
        success: function (data2eee) {
             if (data2eee == true){
            	$('#editKodeToko').val("");
            	$('#editNamaToko').val("");
				$('#editAlamatToko').val("");
				$('#editNoTelepon').val("");
				$('#editCpToko').val("");

              	$("div.edittoko").slideUp("700");
				$("#right>div.datatoko").slideDown("700");
				$("#right>div.datatoko div").slideDown("700");
				reloadToko();
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

function searchToko(){
	var keyword = $('#searchToko').val();
	var sortby  = $('#sortDataToko option:selected').val();
  	$.ajax({
        type	: 'POST',
        url		: 'ajax_datatoko.php',
        data 	: 'actionfunction=showData&page=1&keyword='+keyword+'&sortby='+sortby,
        cache 	: false,
        success: function(result) {
        	var a = result.indexOf("<");
        	var rows = result.substring(0,a);
        	$('#lblJumlahDataToko').text(rows);
        	result = result.substring(a);
			$('#isiTabelToko').html(result);
			$.ajax({
			    url:"ajax_datatoko.php",
		        type:"POST",
		        data:"actionfunction=pagination&page=1&keyword="+keyword+'&sortby='+sortby,
		        cache: false,
		        success: function(response){
					$('#paginationToko').html(response);
				}
	   		});
	   		$("#right>div.datatoko").slideDown("700");
				$("#right>div.datatoko div").slideDown("700");
        },
    });
}

$("form#formCariToko").submit(function(){
    var formData        = new FormData($(this)[0]);
    var sortby          = $('#sortDataToko option:selected').val();
    var search_kode     = $('#kataKunciKodeToko').val();
    var search_nama     = $('#kataKunciNamaToko').val();
    var search_alamat   = $('#kataKunciAlamatToko').val();
    var search_telepon  = $('#kataKunciNoTeleponToko').val();
    var search_cp       = $('#kataKunciCPToko').val();
    $.ajax({
        type    : 'POST',
        url     : 'ajax_datatoko.php',
        data    : 'actionfunction=showData&page=1&search_kode='+search_kode+'&search_nama='+search_nama+'&search_alamat='+search_alamat+'&search_telepon='+search_telepon+'&search_cp='+search_cp+'&sortby='+sortby,
        cache   : false,
        success: function(result) {
            var a = result.indexOf("<");
            var rows = result.substring(0,a);
            $('#lblJumlahDataToko').text(rows);
            result = result.substring(a);
            $('#isiTabelToko').html(result);
            $.ajax({
                url:"ajax_datatoko.php",
                type:"POST",
                data:'actionfunction=pagination&page=1&search_kode='+search_kode+'&search_nama='+search_nama+'&search_alamat='+search_alamat+'&search_telepon='+search_telepon+'&search_cp='+search_cp+'&sortby='+sortby,
                cache: false,
                success: function(response){
                    $('#paginationToko').html(response);
                }
            });
            $("#right>div.datatoko").slideDown("700");
                $("#right>div.datatoko div").slideDown("700");
        },
    });
 return false;
});

function checkKodeToko(){
      var kodeInput = document.getElementById("kodeToko").value;
      $.ajax({
            url: "ajax_tambahtoko.php",
            type: "POST",
            data: {
                "kode_toko_check": kodeInput
            },
            success: function(data){
            	if(kodeInput != "" && data == true){
					$("#warningUsername").attr("src","image/check.png");
                    document.getElementById("warningUsername").style.visibility = "visible";
                }
                else {
                    document.getElementById("warningUsername").style.visibility = "visible";
                    $("#warningUsername").attr("src","image/warning.png");
                }   
            }
        });
        return false;
}

$("#btnDropPencarian").click(function(){
    $("#formCariToko").toggle(1000);
});

function tambahToko(){
    $("div.datatoko").slideUp("700");
    $("div.tambahtoko").slideDown("700");
}

var tableToExcel2Toko = (function() {
    var tamp;
    $.ajax({
            type: "POST",
            url: "loadKopTanpaLogo2.php",
            success: function(data){
                if(data){
                    tamp = data;
                }
            }
    });
      var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
      return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        $.ajax({
         type    : 'POST',
         url     : 'ajax_print.php',
         data    : {'tentang':"toko"},
         success: function(result) {
                var originalContents = "";
                originalContents = result;
                var ctx = {worksheet: name || 'Worksheet', table: tamp + originalContents}
                window.location.href = uri + base64(format(template, ctx))
            },
         });    
       
      }
})()

function exportToko(){
    var value = $('#selectExportDataToko option:selected').val();
    if (value == "CSV"){
        window.location='exportCSV.php?exp=datatoko';
        $('#selectExportDataToko').val(''); 
    }else if (value =="XLS"){
         tableToExcel2Toko("datatoko", 'toko');
         $('#selectExportDataToko').val(''); 
         reloadToko();
    }else if (value == "PDF"){
        window.location='exportpdf.php/?pdffor=toko';
        $('#selectExportDataToko').val(''); 
        // convertToPdfDataTokoPenanggungjawab("#tabletoko");
    }
}

$("form#formTambahTokoImport").submit(function(){
    var formData        = new FormData($(this)[0]);
    if ($('#fileTokoImport').get(0).files.length === 0) {
        alert("Pilih file CSV terlebih dahulu");
    }else{
        $.ajax({
        type    : 'POST',
        url     : 'importCSV.php',
        data    : formData,
        async: false,
        success: function(result) {
            
            if (result == "false"){
                alert("Import gagal. Keseluruhan data sebelumnya tidak bisa dihapus karena masih berelasi dengan data lain dan tidak ada data baru yang berbeda");
            }else if (result == "filenotfound"){
                alert("Pilih file CSV terlebih dahulu")
            }else {
                var hasil = result.split('-');
                if (hasil[0] == true){
                    alert(hasil[2]+" gagal import karena data sebelumnya sudah ada. Import berhasil untuk "+hasil[1]+" data");                    
                }else{
                    alert("Terdapat data yang tidak bisa dihapus karena masih berelasi dengan data lain. "+hasil[2]+" gagal import karena data sebelumnya sudah ada. Import berhasil untuk "+hasil[1]+" data");                    
                }

                // alert("Import berhasil sebanyak "+ hasil[0]+" data dan tidak perlu import sebanyak "+hasil[1]+" data");
                $("div.tambahtoko").slideUp("700");
                $("#right>div.datatoko").slideDown("700");
                $("#right>div.datatoko div").slideDown("700");
                reloadToko();
            }
        },
        cache: false,
        contentType: false,
        processData: false
        })      
    }
    
    return false;
});

// function convertToPdfDataTokoPenanggungjawab(div){

//     tamp1 = "$('#'+ket1).text();"
//     tamp2 = "$('#'+ket2).text();"
//     tamp3 = "";

//     var columns = [];
//     var rows = [];
//     var img = new Image();
//     img.src = 'image/logoukdw.png';

//     if(div == '#tabletoko'){
//             columns = ["No","Kode Toko","Nama Toko","Alamat Toko","No Telepon Toko","CP Toko"];
//             $.each( $(div+' tr'), function (i, row){
//                 var tamp = [];
//                 $.each( $(row).find("td, th"), function(j, cell){
//                     var txt = $(cell).text();
//                     tamp[j] = txt
//                 });
//                 if(i >= 2){
//                     rows[i] = tamp; 
//                 }
                 
//             });
//     }
        


//     var doc = new jsPDF('p', 'pt');

//     var status;
//     var namaGereja;
//     var alamatGereja;
//     var teleponGereja;
//     var emailGereja;
//     var logoGereja;
//     $.ajax({
//         type: "POST",
//         url: "loadUntukKopPdf.php",
//         dataType: "json",
//         success: function(data){

//             if(data){
//                     namaGereja = String(data[0][0]);
//                     alamatGereja = data[0][1];
//                     teleponGereja = data[0][2];
//                     emailGereja = data[0][3];
//                     logoGereja = 'data:image/png;base64,'+ data[0][4];

                    
//                     doc.setFontSize(10);
//                     doc.setTextColor(40);
//                     doc.setFontStyle('bold');
//                     doc.addImage(logoGereja, 'PNG', 40, 40, 80, 80);
//                     doc.text(namaGereja, 40 + 100, 50);
//                     doc.text(alamatGereja, 40 + 100, 70);
//                     doc.text(emailGereja, 40 + 100, 90);
//                     doc.text(teleponGereja, 40 + 100, 110);


//                     if(div == '#rencanaAnggaran'){
//                         doc.text(tamp1+" "+tamp2, 40, 180);
                        
//                     }else{
//                         doc.text(tamp1, 40, 160);
//                         doc.text(tamp2 +" "+ tamp3, 40, 180);

//                     }

//                     doc.setFontStyle('normal');
//                     doc.autoTable(columns, rows, {startY: 200,theme: 'grid'});
//                     alert("fak");

//                     doc.save(div.substr(1)+'.pdf');
//             }else{

//                     namaGereja = "NAMA GEREJA";
//                     alamatGereja = "ALAMAT GEREJA";
//                     teleponGereja = "TELEPON GEREJA";
//                     emailGereja = "EMAIL GEREJA";
//                     logoGereja = "image/logoukdw.png";
                
//                     doc.setFontSize(10);
//                     doc.setTextColor(40);
//                     doc.setFontStyle('bold');
//                     doc.addImage(img, 'PNG', data.settings.margin.left, 40, 100, 100);
//                     doc.text(namaGereja, data.settings.margin.left + 110, 50);
//                     doc.text(alamatGereja, data.settings.margin.left + 110, 70);
//                     doc.text(emailGereja, data.settings.margin.left + 110, 90);
//                     doc.text(teleponGereja, data.settings.margin.left + 110, 110);
                    
                    
//                     if(div == '#rencanaAnggaran'){
//                         doc.text(tamp1+" "+tamp2, 40, 180);
                        
//                     }else{
//                         doc.text(tamp1, 40, 160);
//                         doc.text(tamp2 +" "+ tamp3, 40, 180);
//                     }

//                     doc.setFontStyle('normal');
//                     doc.autoTable(columns, rows, {startY: 200,theme: 'grid'});


//                     doc.save(div.substr(1)+'.pdf');
//             }       
//         }
//     });

    
// }