reloadSetOptionGolongan();
reloadSetOptionSubGolongan();
reloadDataRekening();
refreshChecking();

function refreshChecking(){
    document.getElementById("warningKodeGolongan").style.visibility = "hidden";
    document.getElementById("warningKodeSubGolongan").style.visibility = "hidden";
    document.getElementById("warningKodeJenisAkunRekening").style.visibility = "hidden";
    $("#warningKodeGolongan").attr("src","image/warning.png");
    $("#warningKodeSubGolongan").attr("src","image/warning.png");
    $("#warningKodeJenisAkunRekening").attr("src","image/warning.png");
    $('.divKeteranganTambahRekening').css({ 
        'visibility' :'hidden'
    }); 
    
}
function reloadSetOptionGolongan(){
    $.ajax({
        type    : 'POST',
        url     : 'ajax_tambahrekening.php',
        data    : 'actionfunction=getKodeGolongan',
        cache   : false,
        success: function(result) {
            $('#pilihanKodeGolongan').html(result);
            //$("#kodeSubGolongan").val($("#pilihanKodeGolongan").val());
            $("#fixKodeGolonganDisabled").val($("#pilihanKodeGolongan").val());
            $("#fixKodeGolonganHidden").val($("#pilihanKodeGolongan").val());
        },
    });
}

function reloadSetOptionSubGolongan(){
    $.ajax({
        type    : 'POST',
        url     : 'ajax_tambahrekening.php',
        data    : 'actionfunction=getKodeSubGolongan',
        cache   : false,
        success: function(result) {
            $('#pilihanKodeSubRekening').html(result);
            $("#fixKodeSubGolonganDisabled").val($("#pilihanKodeSubRekening").val()+".");
            $("#fixKodeSubGolonganHidden").val($("#pilihanKodeSubRekening").val()+".");
        },
    });
}

function reloadEditSetOptionSubGolongan(){
    $.ajax({
        type    : 'POST',
        url     : 'ajax_tambahrekening.php',
        data    : 'actionfunction=getKodeSubGolongan',
        cache   : false,
        success: function(result) {
            $('#pilihanEditKodeSubRekening').html(result);
            // $('#pilihanEditKodeSubRekening').val('D202');
        },
    });
}

$(document).ready(function (){

    $("#pilihanKodeGolongan").change(function () {
        $("#fixKodeGolonganDisabled").val($("#pilihanKodeGolongan").val());
        $("#fixKodeGolonganHidden").val($("#pilihanKodeGolongan").val());
        
    });

    $("#pilihanKodeSubRekening").change(function () {
        $("#fixKodeSubGolonganDisabled").val($("#pilihanKodeSubRekening").val()+".");
        $("#fixKodeSubGolonganHidden").val($("#pilihanKodeSubRekening").val()+".");

    });

    $("#pilihanKelompokAkunRekening").change(function () {
        $("#fixKodeKelompokDisabled").val($("#pilihanKelompokAkunRekening").val());
        $("#fixKodeKelompokHidden").val($("#pilihanKelompokAkunRekening").val());
    });

    $(document).on('click','#mask, .btn_close, .close',function(){
        closePopUp();
    });
});

function closePopUp(){
    reloadDataRekening();
    $('#mask').remove();  
    $('#divGantiNama').fadeOut(200); 
    $('#divGantiNama').css({ 
        'visibility' :'hidden',
        'display' :'none'
    }); 
}
function editNamaPopupMuncul(div, val, tipe){
    var loginBox = div;
    $.ajax({
        type    : 'POST',
        url     : 'ajax_editrekening.php',
        data    : {'kode_akun':val,'tipe':tipe},
        dataType: 'json',
        success: function(result) {
            reloadDataRekening();
                $("#inputGantiNama").val(result[1]);
                $("#idGantiNama").val(result[0]);
                $("#tipeGantiNama").val(result[2]);
                $("#tipeGantiNamaGol").val(result[2]);
                if(result[2] == 'gol' || result[2] == 'sub'){
                    $(".divLblFormGantiTipe").css({ 
                        'visibility' :'visible',
                        'display' :'none',
                        'margin-top' : -popMargTop,
                        'margin-left' : -popMargLeft
                    });
                }else{
                    $(".divLblFormGantiTipe").css({ 
                        'visibility' :'visible',
                        'display' :'block',
                        'margin-top' : -popMargTop,
                        'margin-left' : -popMargLeft
                    });
                }

                var popMargTop = ($(loginBox).height() + 24) / 2; 
                var popMargLeft = ($(loginBox).width() + 24) / 2; 
                //  Add the mask to body
                $('body').append('<div id="mask"></div>');
                $('#mask').fadeIn(300);
                $(loginBox).fadeIn(500);
                $(loginBox).css({ 
                    'visibility' :'visible',
                    'display' :'block',
                    'margin-top' : -popMargTop,
                    'margin-left' : -popMargLeft
                });
            },
    });
    return false;
}

function checkKodeGolongan(){
  var kodefix = document.getElementById("fixKodeKelompokHidden").value;
  var kodeInput = document.getElementById("kodeGolongan").value;
  kodeInput = kodefix+kodeInput;
    $("#lblKeteranganTambahGolongan").html("");
    $.ajax({
        url: "ajax_tambahrekening.php",
        type: "POST",
        data: {
            "kode_golongan_check": kodeInput
        },
        success: function(data){
            if(kodeInput != "" && kodeInput != "D"  && data == true && kodeInput.length <= 3){
                $("#warningKodeGolongan").attr("src","image/check.png");
                document.getElementById("warningKodeGolongan").style.visibility = "visible";
                document.getElementById("divKeteranganTambahGolongan").style.visibility = "visible";
                 $("#lblKeteranganTambahGolongan").html("Kode dapat ditambahkan");
                    $("#lblKeteranganTambahGolongan").css({ 
                        'color' :'green'
                    });
            }
            else {
                document.getElementById("warningKodeGolongan").style.visibility = "visible";
                document.getElementById("divKeteranganTambahGolongan").style.visibility = "visible";
                $("#warningKodeGolongan").attr("src","image/warning.png");
                 $("#lblKeteranganTambahGolongan").html("Kode sudah ada");
                    $("#lblKeteranganTambahGolongan").css({ 
                        'color' :'red'
                    });
            }   
        }
    });  
    return false;
}

function checkKodeSubGolongan(){
    var kodefix = document.getElementById("fixKodeGolonganHidden").value;
    var kodeInput = document.getElementById("kodeSubGolongan").value;
    kodeInput = kodefix+kodeInput;
     $("#lblKeteranganTambahSubGolongan").html("");
    $.ajax({
        url: "ajax_tambahrekening.php",
        type: "POST",
        data: {
            "kode_sub_golongan_check": kodeInput
        },
        success: function(data){
            if(kodeInput != "" && kodeInput != kodefix && data == true && kodeInput.length <= 5){
                $("#warningKodeSubGolongan").attr("src","image/check.png");
                document.getElementById("warningKodeSubGolongan").style.visibility = "visible";
                document.getElementById("divKeteranganTambahSubGolongan").style.visibility = "visible";
                $("#lblKeteranganTambahSubGolongan").html("Kode dapat ditambahkan");
                $("#lblKeteranganTambahSubGolongan").css({ 
                        'color' :'green'
                    });
            }
            else {
                document.getElementById("warningKodeSubGolongan").style.visibility = "visible";
                document.getElementById("divKeteranganTambahSubGolongan").style.visibility = "visible";
                $("#warningKodeSubGolongan").attr("src","image/warning.png");
                $("#lblKeteranganTambahSubGolongan").html("kode sudah ada");
                $("#lblKeteranganTambahSubGolongan").css({ 
                        'color' :'red'
                    });
            }   
        }
    });
    return false;
}

function checkKodeJenis(){
    var kodefix = document.getElementById("fixKodeSubGolonganHidden").value;
    var kodeInput = document.getElementById("kodeJenisAkunRekening").value;
    kodeInput = kodefix+kodeInput;
        $("#lblKeteranganTambahRekening").html("");
    $.ajax({
        url: "ajax_tambahrekening.php",
        type: "POST",
        data: {
            "kode_jenis_check": kodeInput
        },
        success: function(data){
            if(kodeInput != "" && kodeInput != kodefix && data == true&& kodeInput.length <= 8){
                $("#warningKodeJenisAkunRekening").attr("src","image/check.png");
                document.getElementById("warningKodeJenisAkunRekening").style.visibility = "visible";
                document.getElementById("divKeteranganTambahJenisRekening").style.visibility = "visible";
                $("#lblKeteranganTambahRekening").html("Kode dapat ditambahkan");
                $("#lblKeteranganTambahRekening").css({ 
                        'color' :'green'
                    });
            }
            else {
                document.getElementById("warningKodeJenisAkunRekening").style.visibility = "visible";
                document.getElementById("divKeteranganTambahJenisRekening").style.visibility = "visible";
                $("#warningKodeJenisAkunRekening").attr("src","image/warning.png");
                $("#lblKeteranganTambahRekening").html("kode sudah ada");
                $("#lblKeteranganTambahRekening").css({ 
                        'color' :'red'
                    });
            }   
        }
    });
    return false;
}

$("form#formTambahGolonganRekening").submit(function(){
  var formData = new FormData($(this)[0]);
  // var test = document.getElementById("fixKodeKelompokHidden").value;
  // alert(test);
  var kodeKelompok = document.getElementById("pilihanKelompokAkunRekening").value;
  if (kodeKelompok !=""){
        $.ajax({
            url: "ajax_tambahrekening.php",
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {
                if ( data == true){
                    document.getElementById("divKeteranganTambahGolongan").style.visibility = "visible";
                    $("#lblKeteranganTambahGolongan").html("Sukses ditambahkan");
                    $("#lblKeteranganTambahGolongan").css({ 
                        'color' :'green'
                    });
                    $("#kodeGolongan").val("");
                    $("#namaGolongan").val("");
                    $("#fixKodeKelompokDisabled").val("");
                    $("#fixKodeKelompokHidden").val("");
                    reloadSetOptionGolongan();
                    reloadDataRekening();
                    reloadSetOptionSubGolongan();
                    reloadDataRekening();
                    closePopUp();
                }else if ( data == false){
                    checkKodeGolongan();
                    document.getElementById("divKeteranganTambahGolongan").style.visibility = "visible";
                    $("#lblKeteranganTambahGolongan").html("Gagal ditambahkan");
                    $("#lblKeteranganTambahGolongan").css({ 
                        'color' :'red'
                    });

                }
            },
            cache: false,
            contentType: false,
            processData: false
        }); 
  }else{
        document.getElementById("divKeteranganTambahGolongan").style.visibility = "visible";
        $("#lblKeteranganTambahGolongan").html("Pilih Debit atau Kredit");
  }
  return false;
});

$("form#formTambahSubGolonganRekening").submit(function(){
  var formData = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_tambahrekening.php",
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
            if ( data == true){
                document.getElementById("divKeteranganTambahSubGolongan").style.visibility = "visible";
                $("#lblKeteranganTambahSubGolongan").html("Sukses ditambahkan");
                $("#lblKeteranganTambahGolongan").css({ 
                        'color' :'green'
                    });
                $("#kodeSubGolongan").val("");
                $("#namaSubGolongan").val("");
                $("#fixKodeGolonganDisabled").val("");
                $("#fixKodeGolonganHidden").val("");
                    
                reloadDataRekening();
                reloadSetOptionGolongan();
                reloadSetOptionSubGolongan();
                closePopUp();
            }else if ( data == false){
                checkKodeSubGolongan();
                document.getElementById("divKeteranganTambahSubGolongan").style.visibility = "visible";
                $("#lblKeteranganTambahSubGolongan").html("Gagal ditambahkan");
                $("#lblKeteranganTambahGolongan").css({ 
                        'color' :'red'
                    });
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

$("form#formTambahJenisAkunRekening").submit(function(){
  var formData = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_tambahrekening.php",
        type: 'POST',
        data: formData,
        async: false,
        success: function (data1) {
            if ( data1 == true){
                document.getElementById("divKeteranganTambahJenisRekening").style.visibility = "visible";
                $("#lblKeteranganTambahRekening").html("Sukses ditambahkan");
                $("#lblKeteranganTambahGolongan").css({ 
                        'color' :'green'
                    });
                $("#kodeJenisAkunRekening").val("");
                $("#namaJenisAkunRekening").val("");
                $("#fixKodeSubGolonganDisabled").val("");
                $("#fixKodeSubGolonganHidden").val("");
                reloadDataRekening();
                reloadSetOptionSubGolongan();
                closePopUp();
            }else if ( data1 == false){
                checkKodeJenis();
                document.getElementById("divKeteranganTambahJenisRekening").style.visibility = "visible";
                $("#lblKeteranganTambahRekening").html("Gagal ditambahkan");
                $("#lblKeteranganTambahGolongan").css({ 
                        'color' :'red'
                    });
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

 reloadDataRekening();
 function reloadDataRekening(){
    $.ajax({
        type    : 'POST',
        url     : 'ajax_datarekening.php',
        data    : 'actionfunction=showDataRekening&page=1',
        cache   : false,
        success: function(result) {
            var a = result.indexOf("<");
            var rows = result.substring(0,a);
            $('#lblJumlahDataRekening').text(rows);
            result = result.substring(a);
            $('#datarekening').html(result);
        },
    });
}  

function hapusrekening(val, tipe){
var cek = confirm("Apakah yakin akan menghapus rekening ini ? Menghapus golongan / sub rekening akan menghapus sub data didalamnya");
    if (cek){
        $.ajax({
            type    : 'POST',
            url     : 'ajax_hapusrekening.php',
            data    : {'kode_akun':val,
                        'tipe':tipe},
            success: function(result) {
                if (result == true){
                        alert("Rekening terpilih berhasil dihapus !");
                        reloadDataRekening();
                }else{
                    alert(result);
                }
            },
        });
    }
}

$("form#formGantiNama").submit(function(){
  var formData = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_editrekening.php",
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
            if ( data == true){
                alert("Ganti Nama Rekening Sukses");
                reloadDataRekening();
                $('#mask').remove();  
                $('#divGantiNama').fadeOut(200); 
                $('#divGantiNama').css({ 
                    'visibility' :'hidden',
                    'display' :'none',
                }); 
                closePopUp();
            }   else if ( data == false){
                alert("Ganti Nama Rekening Gagal");
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

function printDiv2(divName) {
     var originalContents = $("#"+divName+" a").html();
     var originalContents = $("#"+divName).html();
     $("#"+divName+" a").remove();
     $("#"+divName+" select").remove();
     $("#"+divName+" .actiontabeltoko").remove();
     
     //var printContents = $("#"+divName).html();
     if (divName!= "datarekening"){
         var printContents = "";
         $.ajax({
         type    : 'POST',
         url     : 'ajax_print.php',
         data    : {'tentang':divName},
         success: function(result) {
                printContents = result;
                var contents = printContents;
                var frame1 = $('<iframe />');
                frame1[0].name = "frame1";
                frame1.css({ "position": "absolute", "top": "-1000000px" });
                $("body").append(frame1);
                var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
                frameDoc.document.open();
                //Create a new HTML document.
                if (divName == "toko"){
                    frameDoc.document.write('<html><head><title>Data Toko</title>');    
                } else if (divName == "penanggungjawab"){
                    frameDoc.document.write('<html><head><title>Data Penanggungjawab</title>');
                } else{
                    frameDoc.document.write('<html><head><title>Data Rekening</title>');
                }
                
                frameDoc.document.write('</head><body>');
                //Append the external CSS file.
                frameDoc.document.write('<link href="css/datatoko_rekening_penanggungjawab.css" rel="stylesheet" type="text/css" />');
                //Append the DIV contents.
                frameDoc.document.write(contents);
                frameDoc.document.write('</body></html>');
                frameDoc.document.close();
                setTimeout(function () {
                    window.frames["frame1"].focus();
                    window.frames["frame1"].print();
                    frame1.remove();
                }, 500);

                 // document.body.innerHTML = originalContents;
                 $("#"+divName).html(originalContents);
                 reloadDataRekening();
            },
         });

    }else {
        var printContents = $("#"+divName).html();
        var contents = printContents;
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>Data Rekening</title>');
        frameDoc.document.write('</head><body>');
        //Append the external CSS file.
        frameDoc.document.write('<link href="css/datatoko_rekening_penanggungjawab.css" rel="stylesheet" type="text/css" />');
        //Append the DIV contents.
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);

         // document.body.innerHTML = originalContents;
         $("#"+divName).html(originalContents);
         reloadDataRekening();
    }
}
