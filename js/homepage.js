function reloadHomePage(){
	$.ajax({
		type: "POST",
		url: "loadDetailAdmin.php",
		dataType: "json",
		success: function(data){
			if(data){
					if (data[0][2]=="-"){
						$("#lblNamaDivisi").html("");
					}else if (data[0][2]==null || data[0][2]=="null" ){
						$("#lblNamaDivisi").html("");
					}else{
						if (data[0][1]=="BIDANG"){
							$("#lblNamaDivisi").html("Bidang "+data[0][2]);
						}else if (data[0][1]=="KOMISI"){
							$("#lblNamaDivisi").html("Komisi "+data[0][2]);
						}
						
					}
					$("#lblNamaAdminHomePage").html(data[0][0]);
					$("#lblDivisiAdminHomePage").html(data[0][2]);
					$("#lblAlamatAdminHomePage").html(data[0][3]);
					$("#lblTeleponAdminHomePage").html(data[0][4]);
					$("#lblEmailAdminHomePage").html(data[0][5]);
					
			}
		}
	});
}