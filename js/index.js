var timer;
var flag;

$(document).ready(function() {
	$("#wrapper").height($(window).height() - $("body").offset().top*2);
	$("#content").css("min-height", $("#wrapper").height()-$("#header").height()-$("#footer").height()-49);
	timer = setInterval(endresize, 500);
	
	$(window).resize(function(){
		flag = true;
	});
	
	$("input[maxlength=8]:not(#kodeTambahAdmin):not(#editkodeTambahAdmin)").css('text-transform','uppercase');
	$("input[maxlength=8]:not(#kodeTambahAdmin):not(#editkodeTambahAdmin)").keyup(function(event){
		var k = (event.keyCode ? event.keyCode : event.which);
		if(!(k > 36 && k < 41)){
			$(this).val($(this).val().toUpperCase());
		}
	});

	$("input[maxlength=10]:not(#kodeTambahAdmin):not(#editkodeTambahAdmin)").css('text-transform','uppercase');
	$("input[maxlength=10]:not(#kodeTambahAdmin):not(#editkodeTambahAdmin)").keyup(function(event){
		var k = (event.keyCode ? event.keyCode : event.which);
		if(!(k > 36 && k < 41)){
			$(this).val($(this).val().toUpperCase());
		}
	});

	// untuk subitem menu
	$(".menu div").hide();
	$("#right>div").hide();
	$("#right>div.homepage").show();
	$("#right>div.homepage div").show();
	reloadHomePage();
	$(".menu li").click(function(){
		if(String($(this).attr("class")) == 'logout'){
				location.replace("logout.php");
		} else {
			settricklength();

			var c = $(this).attr("class").split(' ')[0];
			var p = $(this).parent().attr("class").split(' ')[0];
			var v1 = $("#left div."+c).is(":visible");
			var v2 = $("#right>div."+c).is(":visible");
			if($("#right>div."+c).length == 0){
				$("."+p+" div").slideUp("700");
				if(!v1){
					$("div."+c).slideDown("700");
				}
			} else {
				if(!v2){
					$("#right>div").hide();
					if(!v1){
						$("."+p+" div").slideUp("700");
					}
					$("div."+c).slideDown("700");
					$("#right div."+c+" div").slideDown("700");
				} else {
					$("#left div."+c).slideToggle("700");
				}
			}
			// ganti warna menu tidak aktif
			$(".menu li").css("background-color", "rgb(225,225,225)");
			$(".menu li").css("color", "black");
			// ganti warna menu aktif
			$(this).css("background-color", "rgb(18,121,249)");
			$(this).css("color", "white");
		}
	});

	$(".menu li").each(function(){
		var c = $(this).attr("class");
		if($(".menu div."+c).length != 0){
			$(this).addClass("haveSub");
		}
	});

	// placeholder select
	$("select").change(function(){
		if($(this).val() != "" && $(this).val() != null) $(this).css("color","black");
		else $(this).css("color","rgb(161,161,161)");
	});
	placeholderselect();
	// akhir code untuk placeholder select
	

	// override reset supaya placeholder select berwarna abu
	$("input[type=reset]").click(function(event){
		event.preventDefault();
    	$(this).closest('form').get(0).reset();
    	placeholderselect();
	});

	// untuk editable select
	$("select").keydown(function(event){
		var c = $(this).attr("class");
		var k = (event.keyCode ? event.keyCode : event.which);
		if((k > 47 && k < 58) || (k > 64 && k < 91) || (k > 96 && k < 123)){
			$(".trick#"+c).focus();
		}
	});

	$(".trick").focusin(function(event){
		var i = $(this).attr("id");
		$(this).val($("select."+i).val().replace("_dot_","."));
		if($(this).val() != "") $(this).css("background-color", "white");

		$(this).css('width', $("select."+i).width()-25);
		$(this).css('height', $("select."+i).height()-6);
	});

	$(".trick").focusout(function(){
		var i = $(this).attr("id");
		var kode = $(this).val().replace(".", "_dot_");

		if($(this).val() == "") {
			$("select."+i).val("");
			$("select."+i).css("color","rgb(161,161,161)");
		}
		else if($("select."+i+" option[value='"+kode+"']").length > 0) {
			$("select."+i).val(kode);
			$("select."+i).trigger("change");
		}
		else {
			$("select."+i).val("");
			$("select."+i).css("color","rgb(161,161,161)");
		}
		$(this).val("");
		$(this).css("background-color", "transparent");
	});

	$(".trick").keydown(function(event){
		var i = $(this).attr("id");
		// $(this).val($(this).val().toUpperCase());
		var k = (event.keyCode ? event.keyCode : event.which);
		if(k==38 || k==40 || k==9){
			$("select."+i).focus();
		}
	});
	$(".trick").keyup(function(event){
		var i = $(this).attr("id");
		var k = (event.keyCode ? event.keyCode : event.which);
		if(k==8){
			if ($(this).val() == ""){
				$("select."+i).focus();
				$(this).focus();
			} 
		}
	});
	$(".trick").keypress(function(event){
		$(this).css("background-color", "white");
	});
	// akhir code untuk editable select

	$(".rptail").focusout(function(){
		var jumlah = $(this).val();
		if(jumlah < 0) {
			$(this).val("");
		}
	});

	$("textarea").focusin(function(){
		$(this).select();
	});

	$(".rptail").focusin(function(){
		var txt = $(this).val().replace(/\./g, "");
		$(this).val(parseInt(txt));
		$(this).attr("type", "number");
		$(this).css("padding-right", 0);
		$(this).width($(this).width()+15);
	});

	$(".rptail").focusout(function(){
		if($(this).val() != ""){
			var num = parseInt($(this).val());
			$(this).attr("type", "text");
			$(this).val(num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
		}
		$(this).attr("type", "text");
		$(this).width($(this).width()-15);
		$(this).css("padding-right", 15);
	});
});

function endresize(){
	if(flag == true){
		flag = false;
		$("#wrapper").height($(window).height() - $("body").offset().top*2);
		$("#content").css("min-height", $("#wrapper").height()-$("#header").height()-$("#footer").height()-49);
		reAdjustTrickBuktiPenerimaan();
	}
}

// fungsi untuk menyamakan panjang input dan select dalam suatu tabel
function uniform(scope, w){
	$("#"+scope+" input:not([type=checkbox]):not([type=date]):not([type=submit]):not([type=reset]):not(.ex):not(.trick)").css({width:w});
	$("#"+scope+" select:not(.ex)").css({width:w+7});
	$("#"+scope+" textarea:not(.ex)").css({width:w});
	$("#"+scope+" input.rptail").css({width:w-38});
	settricklength();
}

function placeholderselect(){
	$("select").each(function(){
		if($(this).val() != "" && $(this).val() != null) $(this).css("color","black");
		else $(this).css("color","rgb(161,161,161)");
	});
}

function settricklength(){
	$(".trick").each(function(){
		var i = $(this).attr("id");
		$(this).css('width', $("select."+i).width()-25);
		// $(this).css('height', $("select."+i).height()-6);
	});
}