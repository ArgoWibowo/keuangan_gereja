$(document).ready(function() {
    // Setup - add a text input to each footer cell
   

    uniform("formDetailKomisi",180);

    $(document).on('click','#mask, .btn_close, .close',function(){
        closeDetailKomisi();
    });

});

function closeDetailKomisi(){
    reloadKomisi();
    $('#mask').remove();  
    $('#detailKomisi').fadeOut(200); 
    $('#detailKomisi').css({ 
        'visibility' :'hidden',
        'display' :'none'
    }); 
}

 reloadKomisi();
 function reloadKomisi(){
   // alert("masuk");

	$.ajax({
        type	: 'POST',
        url		: 'ajax_datakomisi.php',
        data 	: 'actionfunction=showData&page=1',
        cache 	: false,
        success: function(result) {
     //       alert(result);
        	    
            var a = result.indexOf("<");
        	var rows = result.substring(0,a);
        	$('#lblJumlahDataKomisi').text(rows);
        	result = result.substring(a);
			$('#isiTabelKomisi').html(result);
			$('#jumlahData').text($('#jumlahDataKomisi').val());
			$.ajax({
			    url:"ajax_datakomisi.php",
		        type:"POST",	
		        data:"actionfunction=pagination&page=1",
		        cache: false,
		        success: function(response){
					$('#paginationKomisi').html(response);


				}
	   		});
        },
    });
//alert("masuk3");

    $('#paginationKomisi').on('click','.page-numbers',function(){
       $page = $(this).attr('href');
	   $pageind = $page.indexOf('page=');
	   $page = $page.substring(($pageind+5));
       
	   $.ajax({
		    url:"ajax_datakomisi.php",
	        type:"POST",
	        data:"actionfunction=showData&page="+$page,
	        cache: false,
	        success: function(response1){
			   $.ajax({
			    url:"ajax_datakomisi.php",
		        type:"POST",
		        data:"actionfunction=pagination&page="+$page,
		        cache: false,
		        success: function(response){
		        	$('#isiTabelKomisi').html(response1);
					$('#paginationKomisi').html(response);
				}
	   		});
			 
			}
	   });
	return false;
	});
  //  alert("masuk2");

}  


function dipilih(e){
   $(e).addClass('selected').siblings().removeClass('selected');  
}

function pilihanDataKomisi(index, komisi_kode){
	 
    if(index == "0") {
            
        $.ajax({
        type    : 'POST',
        url     : 'ajax_editkomisi.php',
        data    : {'kode':komisi_kode},
        dataType: 'json',
        success: function(result) {
                document.getElementById("lblTitleKomisi").innerHTML = "Detail Komisi";               
                document.getElementById("btnBatalEditKomisi").style.visibility = "hidden";
                document.getElementById("btnEditKomisi").style.visibility = "hidden";
                document.getElementById("btnKembaliKomisi").style.visibility = "visible";
                
                $('#editKodeKomisi').val(result[0]);
                $('#editKodeKomisi2').val(result[0]);                
                $('#editNamaKomisi').val(result[1]);
                $('.editBidangKomisi').val(result[2]);                
                $('.editJenisAkunKomisi').val(result[3]);
                $('.editPengurus1Komisi').val(result[4]);
                $('.editPengurus2Komisi').val(result[5]);
                $('.editPengurus3Komisi').val(result[6]);
                $('#editTanggalKomisi').val(result[7]);
                $('#editDeskripsiKomisi').val(result[8]);                
                $('#editStatusKomisi').val(result[9]);      

                $( "#editKodeKomisi" ).prop( "disabled", true );
                $( "#editNamaKomisi" ).prop( "disabled", true );
                $( ".editBidangKomisi" ).prop( "disabled", true );                
                $( "#editTanggalKomisi" ).prop( "disabled", true );
                $( "#editDeskripsiKomisi" ).prop( "disabled", true );
                $( "#editStatusKomisi" ).prop( "disabled", true );                
                $( ".editPengurus1Komisi" ).prop( "disabled", true );
                $( ".editPengurus2Komisi" ).prop( "disabled", true );
                $( ".editPengurus3Komisi" ).prop( "disabled", true );
                $( ".editJenisAkunKomisi" ).prop( "disabled", true );          
       
                $("div.datakomisi").slideUp("700");
                $("div.editkomisi").slideDown("700");                
            },


        });
    }
    else if (index == "1"){
		
       
        $.ajax({
        type	: 'POST',
        url		: 'ajax_editkomisi.php',
        data 	: {'kode':komisi_kode.replace(/_spasi_/g, " ")},
        dataType: 'json',
        success: function(result) {
       document.getElementById("lblTitleKomisi").innerHTML = "Edit Komisi";               
                document.getElementById("btnBatalEditKomisi").style.visibility = "visible";
                document.getElementById("btnEditKomisi").style.visibility = "visible";
                document.getElementById("btnKembaliKomisi").style.visibility = "hidden";
                
                $('#editKodeKomisi').val(result[0]);
                $('#editKodeKomisi2').val(result[0]);                
                $('#editNamaKomisi').val(result[1]);
                $('.editBidangKomisi').val(result[2]);                
                $('.editJenisAkunKomisi').val(result[3]);
                $('.editPengurus1Komisi').val(result[4]);
                $('.editPengurus2Komisi').val(result[5]);
                $('.editPengurus3Komisi').val(result[6]);
                
 /*               $('#editTanggalKomisi').val(result[7]);*/
                var tanggal = result[7].substr(8,2);
                var bulan = result[7].substr(5,2);
                var tahun = result[7].substr(0,4);
                
                $('#komisiEdit_tanggal').val(parseInt(tanggal));
                $('#komisiEdit_bulan').val(parseInt(bulan));
                $('#komisiEdit_tahun').val(parseInt(tahun));

                $('#editDeskripsiKomisi').val(result[8]);                
                $('#editStatusKomisi').val(result[9]);      

                $( "#editKodeKomisi" ).prop( "disabled", false );
                $( "#editNamaKomisi" ).prop( "disabled", false );
                $( ".editBidangKomisi" ).prop( "disabled", false );                
                $( "#editTanggalKomisi" ).prop( "disabled", false );
                $( "#editDeskripsiKomisi" ).prop( "disabled", false );
                $( "#editStatusKomisi" ).prop( "disabled", false );                
                $( ".editPengurus1Komisi" ).prop( "disabled", false );
                $( ".editPengurus2Komisi" ).prop( "disabled", false );
                $( ".editPengurus3Komisi" ).prop( "disabled", false );
                $( ".editJenisAkunKomisi" ).prop( "disabled", false ); 
                placeholderselect();         
       
	$("div.datakomisi").slideUp("700");
	$("div.editkomisi").slideDown("700");                
	        },

	    });

	}else{
        var cek = confirm("Apakah yakin akan menghapus komisi ini ?");
		if (cek){
            $.ajax({
            type    : 'POST',
            url     : 'ajax_hapuskomisi.php',
            data    : {'kode':komisi_kode.replace(/_spasi_/g, " ")},
            success: function(result) {
                    if (result == true){
                        $('pilihanDataKomisi').val("");
                        alert("Data komisi terpilih berhasil dihapus");
                        reloadKomisi();
                    }
                    
                },
            });
        }else{
            $('pilihanDataKomisi').val(""); 
            reloadKomisi();
        }
        
	}
    
}
$("#btnKembaliKomisi").click(function(){
    reloadKomisi();        
    $("div.editkomisi").slideUp("700");
    $("#right>div.datakomisi").slideDown("700");

});
$("#btnBatalEditKomisi").click(function(){
    reloadKomisi();        
    $("div.editkomisi").slideUp("700");
    $("#right>div.datakomisi").slideDown("700");

});


$("form#formTambahKomisi").submit(function(){
   reloadKomisi();
  var formData2 = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_tambahkomisi.php",
        type: 'POST',
        data: formData2,
        success: function (data) {
            //alert(data) ;     
            if ( data == true){
            	$('#kodeKomisi').val("");
				$('#namaKomisi').val("");
                $('.bidangKomisi').val("");                
				$('.jenisakunKomisi').val("");
				$('.pengurus2Komisi').val("");
				$('.pengurus3Komisi').val("");
                $('.pengurus1Komisi').val("");
                $('#komisi_tanggal').val("");
                $('#komisi_bulan').val("");
                $('#komisi_tahun').val("");
             
                $('#deskripsiKomisi').val("");                
/*
                $("div.tambahkomisi").slideUp("700");
				$("#right>div.datakomisi").slideDown("700");
				$("#right>div.datakomisi div").slideDown("700");
*/				alert("Komisi berhasil ditambahkan");
                                
          }
          else{
            alert("gagal");
          }
          reloadKomisi();
        },
        cache: false,
        contentType: false,
        processData: false
    });
    
    return false;
});

$("form#formEditKomisi").submit(function(){

  var formData3 = new FormData($(this)[0]);

   $.ajax({
        url: "ajax_editkomisi.php",
        type: 'POST',
        data: formData3,
        success: function (data3) { 
            //alert(data3);   
             if (data3 == true){
            	$('#editKodeKomisi').val("");
            	$('#editNamaKomisi').val("");
				$('.editBidangKomisi').val("");                
                $('.editJenisAkunKomisi').val("");
				$('.editPengurus1Komisi').val("");
                $('.editPengurus2Komisi').val("");
                $('.editPengurus3Komisi').val("");
                $('#komisiEdit_tanggal').val("");
                $('#komisiEdit_bulan').val("");
                $('#komisiEdit_tahun').val("");
             

                $('#editStatusKomisi').val("");
                $('#editDeskripsiKomisi').val("");
                
				alert("Data Komisi berhasil diubah");
               
              	$("div.editkomisi").slideUp("700");
				$("#right>div.datakomisi").slideDown("700");
				//$("#right>div.databidang div").slideDown("700");
				
            }            
            reloadKomisi(); 
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

function searchKomisi(){
	var keyword = $('#searchKomisi').val();
	var sortby  = $('#sortDataKomisi option:selected').val();
  	$.ajax({
        type	: 'POST',
        url		: 'ajax_datakomisi.php',
        data 	: 'actionfunction=showData&page=1&keyword='+keyword+'&sortby='+sortby,
        cache 	: false,
        success: function(result) {
        	var a = result.indexOf("<");
        	var rows = result.substring(0,a);
        	$('#lblJumlahDataKomisi').text(rows);
        	result = result.substring(a);
			$('#isiTabelKomisi').html(result);
			$.ajax({
			    url:"ajax_datakomisi.php",
		        type:"POST",
		        data:"actionfunction=pagination&page=1&keyword="+keyword+'&sortby='+sortby,
		        cache: false,
		        success: function(response){
					$('#paginationKomisi').html(response);
				}
	   		});
	   		$("#right>div.datakomisi").slideDown("700");
				$("#right>div.datakomisi div").slideDown("700");
        },
    });
}

function checkKodeKomisi(){
      var kodeInput = document.getElementById("kodeKomisi").value;
      $.ajax({
            url: "ajax_tambahkomisi.php",
            type: "POST",
            data: {
                "kode_komisi_check": kodeInput
            },
            success: function(data){
            	if(kodeInput != "" && data == true){
					$("#warningUsernameKomisi").attr("src","image/check.png");
                    document.getElementById("warningUsernameKomisi").style.visibility = "visible";
                }
                else {
                    document.getElementById("warningUsernameKomisi").style.visibility = "visible";
                    $("#warningUsernameKomisi").attr("src","image/warning.png");
                }   
            }
        });
        return false;
}

function detailKomisiData(div){

    var loginBox = div;

    window.location.href= div;

    var kodeKomisiDetail = String($('#datakomisi table tr.selected td:eq(1)').html()).trim();

    $.ajax({
                type: "POST",
                url: "loadDetailKomisi.php",
                dataType: "json",
                data: {"kodeKomisiDetail": kodeKomisiDetail},
                success: function(data){
                    if(data){

                      
                            document.getElementById("kodeKomisiDetail").value = data[0];
                            document.getElementById("namaKomisiDetail").value = data[1];
                            document.getElementById("bidangKomisiDetail").value = data[2];
                            document.getElementById("jenisAkunKomisiDetail").value = data[3];
                            document.getElementById("pengurus1KomisiDetail").value = data[4];
                            document.getElementById("pengurus2KomisiDetail").value = data[5];
                            document.getElementById("pengurus3KomisiDetail").value = data[6];
                            document.getElementById("tglDibentukKomisiDetail").value = data[7];
                            document.getElementById("desKomisiDetail").value = data[8];
                            document.getElementById("statusKomisiDetail").value = data[9];

                            var popMargTop = ($(loginBox).height() + 24) / 2; 
                            var popMargLeft = ($(loginBox).width() + 24) / 2; 
                            //  Add the mask to body
                            $('body').append('<div id="mask"></div>');
                            $('#mask').fadeIn(300);
                            $("#detailKomisi").fadeIn(500);
                            $("#detailKomisi").css({ 
                                'visibility' :'visible',
                                'display' :'block',
                                'margin-top' : -popMargTop,
                                'margin-left' : -popMargLeft
                            }); 
                            
                    }
                    
                }
    });
}


var tableToExcel = (function() {
    var tamp;
    $.ajax({
            type: "POST",
            url: "loadKopTanpaLogo2.php",
            success: function(data){
                if(data){
                    tamp = data;
                }
            }
    });
      var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
      return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: tamp + "<table border=1>" + table.innerHTML+ "</table>"}
        window.location.href = uri + base64(format(template, ctx))

      }
})()



function exportKomisi(){
    var value = $('#selectExportDataKomisi option:selected').val();
    /*if (value == "CSV"){
        window.location='exportCSV.php?exp=datatoko';
        $('#selectExportDataToko').val(''); 
    }*/if (value =="XLS"){
        
         var originalContents = $("#datakomisi").html();
         $("#datakomisi").find("tr th:nth-child(" + 8 + ")").remove();
         $("#datakomisi").find("tr td:nth-child(" + 8 + ")").remove();
         $("#datakomisi table tbody tr td select").remove();
         
         tableToExcel("datakomisi", 'komisi');
         $('#selectExportDataKomisi').val(''); 

         $("#datakomisi").html(originalContents);
         reloadKomisi();
    }else if (value == "PDF"){
        window.location='exportPDFBidang.php/?pdffor=komisi';   
        $('#selectExportDataKomisi').val(''); 
        // convertToPdfDataTokoPenanggungjawab("#tabletoko");
    }
}