var timer;
var flag;

var gantiPassNya = false;

$(document).ready(function() {

	uniform("formPemPass1",190);
    uniform("formCheckJawaban",190);
    uniform("formGantiPassAkhir",190);

	$('#passBaruPemulihan').on('input', function(){
		if($(this).val().length	>=8){
				if($(this).val() == $('#passBaruPemulihan2').val() ){
						$("#warningPassPem1").attr("src","image/check.png");
						$("#warningPassPem2").attr("src","image/check.png");
						gantiPassNya = true;
						document.getElementById("warningPassPem1").style.visibility = "visible";
						document.getElementById("warningPassPem2").style.visibility = "visible";
				}else{
						$("#warningPassPem1").attr("src","image/check.png");
						$("#warningPassPem2").attr("src","image/warning.png");
						gantiPassNya = false;
						document.getElementById("warningPassPem1").style.visibility = "visible";
						document.getElementById("warningPassPem2").style.visibility = "visible";
				}
		}else{
				$("#warningPassPem1").attr("src","image/warning.png");
				$("#warningPassPem2").attr("src","image/warning.png");
				gantiPassNya = false;
				document.getElementById("warningPassPem1").style.visibility = "visible";
				document.getElementById("warningPassPem2").style.visibility = "visible";
		}
	});

	$('#passBaruPemulihan2').on('input', function() {
		if($('#passBaruPemulihan').val().length	>=8){
				if($(this).val() == $('#passBaruPemulihan').val() ){
						$("#warningPassPem1").attr("src","image/check.png");
						$("#warningPassPem2").attr("src","image/check.png");
						gantiPassNya = true;
						document.getElementById("warningPassPem1").style.visibility = "visible";
						document.getElementById("warningPassPem2").style.visibility = "visible";
				}else{
						$("#warningPassPem1").attr("src","image/check.png");
						$("#warningPassPem2").attr("src","image/warning.png");
						gantiPassNya = false;
						document.getElementById("warningPassPem1").style.visibility = "visible";
						document.getElementById("warningPassPem2").style.visibility = "visible";
				}
		}else{
				$("#warningPassPem1").attr("src","image/warning.png");
				$("#warningPassPem2").attr("src","image/warning.png");
				gantiPassNya = false;
				document.getElementById("warningPassPem1").style.visibility = "visible";
				document.getElementById("warningPassPem2").style.visibility = "visible";
		}
	});


	$("#wrapper").height($(window).height() - $("body").offset().top*2);
	timer = setInterval(endresize, 500);

	$(window).resize(function(){
		flag = true;
	});

	$('#login-button').click(function()
	{
	var username=$("#username").val().trim();
	var password=$("#password").val();
	var dataString = 'username='+username+'&password='+password;
		if($.trim(username).length>0 && $.trim(password).length>0)
		{
			$.ajax({
				type: "POST",
				url: "login.php",
				data: dataString,
				cache: false,
				beforeSend: function(){ $("#err").html("<h3 style='color:white'>Tunggu Sebentar...</h3>");},
				success: function(data){
					// alert(data);
					if(data){
						if(data == "no"){
							$("#err").html("<h3 style='color:#f9f906'>Error: akun telah dinonaktifkan!</h3>");
						}else{
							// alert(data);
							//$("body").load("index.php").hide().fadeIn(1500).delay(6000);
							window.location.href = "index.php";
						}
						
					}
					else{
						$("#err").html("<h3 style='color:#f9f906'>Error: username atau password tidak valid!</h3>");
					}
				}
			});

			}else{
				$("#err").html("<h3 style='color:#f9f906'>Error: isi terlebih dahulu username dan password!</h3>");
			}
		
		return false;
	});

	$(document).on('click','#mask, .btn_close',function(){
		closePemulihanPassword1();
		closePemulihanPassword2();
    });

    $("form#formPemPass1").submit(function(){

    	var namaPemulihan=$("#namaAkunPemulihan").val().trim();
		var emailPemulihan=$("#emailAkunPemulihan").val().trim();

		$.ajax({
			type: "POST",
			url: "namaAkunEmail.php",
			data: {"namaPemulihan": namaPemulihan, "emailPemulihan": emailPemulihan},
			async: false,
			success: function(data){
				if(data){
					if(data == "no"){
						$("#errPemulihan br").remove();
						$("#errPemulihan").html("<label style='font-size:13px;color:orange'>Error: Tidak Ada akun!</label>");
					}else if(data == "nonaktif"){
						$("#errPemulihan br").remove();
						$("#errPemulihan").html("<label style='font-size:13px;color:orange'>Error: Akun Nonaktif!</label>");
					}else{
						document.getElementById("passBaruPemulihan").value = "";
						document.getElementById("passBaruPemulihan2").value = "";
						document.getElementById("jawabanPemulihan").value = "";
						document.getElementById("buttEditPassAkun").disabled = false;
						$("#errJawabanPemulihan label").remove();


						document.getElementById("passBaruPemulihan").disabled = true;
						document.getElementById("passBaruPemulihan2").disabled = true;
						document.getElementById("buttEditPassAkun2").disabled = true;
						document.getElementById("jawabanPemulihan").disabled = false;
						pemulihanPasswordLanjut2('#pemulihanPass2',data);
					}
				}
				
			}
		});

 		return false;
 		});


		$("form#formCheckJawaban").submit(function(){

				checkJawabanPemulihan();

 		return false;
 		});
 		
 		$("form#formGantiPassAkhir").submit(function(){

				pemulihanPasswordAkun();

 		return false;
 		});
});

function closePemulihanPassword1(){
	$('#mask').remove();  
    $('#pemulihanPassword1').fadeOut(200); 
    $('#pemulihanPassword1').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });
}

function closePemulihanPassword2(){
	$('#mask').remove();  
    $('#pemulihanPassword2').fadeOut(200); 
    $('#pemulihanPassword2').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });
}


function pemulihanPasswordAwal(div){

	var loginBox = div;

	var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2;

    $("#errPemulihan").html("</br>");
    $("#errPemulihan label").remove();

    document.getElementById("formPemPass1").reset();

    //  Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);

    $("#pemulihanPassword1").fadeIn(500);
    $("#pemulihanPassword1").css({ 
        'visibility' :'visible',
        'display' :'block',
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
}

function pemulihanPasswordLanjut(){

	var namaPemulihan=$("#namaAkunPemulihan").val();
	var emailPemulihan=$("#emailAkunPemulihan").val();

	$.ajax({
		type: "POST",
		url: "namaAkunEmail.php",
		data: {"namaPemulihan": namaPemulihan, "emailPemulihan": emailPemulihan},
		success: function(data){
			if(data){
				if(data == "no"){
					$("#errPemulihan br").remove();
					$("#errPemulihan").html("<label style='font-size:13px;color:orange'>Error: Tidak Ada akun!</label>");
				}else{
					document.getElementById("passBaruPemulihan").value = "";
					document.getElementById("passBaruPemulihan2").value = "";
					document.getElementById("formCheckJawaban").reset();
					document.getElementById("buttEditPassAkun").disabled = false;
					$("#errJawabanPemulihan label").remove();


					document.getElementById("passBaruPemulihan").disabled = true;
					document.getElementById("passBaruPemulihan2").disabled = true;
					document.getElementById("buttEditPassAkun2").disabled = true;
					document.getElementById("jawabanPemulihan").disabled = false;
					pemulihanPasswordLanjut2('#pemulihanPass2',data);
				}
			}
			
		}
	});
}

var sessionCheck;

function pemulihanPasswordLanjut2(div,ses){
	var sessionUserPemulihan = ses;
	sessionCheck = ses;

	$.ajax({
		type: "POST",
		url: "loadPertanyaan.php",
		data: {"sessionUserPemulihan": sessionUserPemulihan},
		success: function(data){
			if(data){
				document.getElementById("warningPassPem1").style.visibility = "hidden";
				document.getElementById("warningPassPem2").style.visibility = "hidden";
				$('#pertanyaanPemulihan label').text(data);
				closePemulihanPassword1();
				var loginBox = div;

				var popMargTop = ($(loginBox).height() + 24) / 2; 
			    var popMargLeft = ($(loginBox).width() + 24) / 2;

			    $("#errJawabanPemulihan").html("<label>&nbsp;</label>");
			    //  Add the mask to body
			    $('body').append('<div id="mask"></div>');
			    $('#mask').fadeIn(300);

			    $("#pemulihanPassword2").fadeIn(500);
			    $("#pemulihanPassword2").css({ 
			        'visibility' :'visible',
			        'display' :'block',
			        'margin-top' : -popMargTop,
			        'margin-left' : -popMargLeft
			    });
			}
			
		}
	});
}

function checkJawabanPemulihan(){
	var sessionCheckJawaban = sessionCheck;
	var jawabanUser = $('#jawabanPemulihan').val();

	$.ajax({
		type: "POST",
		url: "checkJawaban.php",
		data: {"sessionCheckJawaban": sessionCheckJawaban, "jawabanUser": jawabanUser},
		success: function(data){
			if(data){
				if(data=="no"){
					$("#errJawabanPemulihan").html("<label style='color:orange; font-size: 13px;'>Error: Jawaban Salah!</label>");
				}else if(data=="ya"){
					$("#errJawabanPemulihan").html("<label>&nbsp;</label>");
					document.getElementById("buttEditPassAkun").disabled = true;
					document.getElementById("passBaruPemulihan").disabled = false;
					document.getElementById("passBaruPemulihan2").disabled = false;
					document.getElementById("buttEditPassAkun2").disabled = false;
					document.getElementById("jawabanPemulihan").disabled = true;
					cekPassword();
				}
				
			}
			
		}
	});

}

function pemulihanPasswordAkun(){

	if(gantiPassNya == true){
		var userPemulihanPass = sessionCheck;
		var passwordBaruPemulihan=$("#passBaruPemulihan").val();
		$.ajax({
			type: "POST",
			url: "gantiPasswordPemulihan.php",
			data: {"userPemulihanPass": userPemulihanPass, "passwordBaruPemulihan": passwordBaruPemulihan},
			success: function(data){
				if(data){
					if(data == 1){
						alert("Password Telah Diganti");
						
					}else{
						alert("Password Tidak Diganti");
						
					}
				}
				
			}
		});

		closePemulihanPassword2();
	}else if(gantiPassNya == false){
		document.getElementById("passBaruPemulihan").focus();
	}
	

}


function endresize(){
	if(flag == true){
		flag = false;
		$("#wrapper").height($(window).height() - $("body").offset().top*2);
	}

}

// function checkPasswordPemulihan(){
// 	var passBaru = document.getElementById('passBaruPemulihan');
//     var passBaruConf = document.getElementById('passBaruPemulihan2');

//     var goodColor = "#66cc66";
//     var badColor = "#ff6666";

//     if(passBaru.value == ""){
//     	 passBaru.style.backgroundColor = badColor;
//     }
// }

function cekPassword(){
	var pass1 = $("#passBaruPemulihan").val();
	var pass2 = $("#passBaruPemulihan2").val();

	if($('#passBaruPemulihan').val().length	>=8){
			if($('#passBaruPemulihan').val() == $('#passBaruPemulihan2').val() ){
					$("#warningPassPem1").attr("src","image/check.png");
					$("#warningPassPem2").attr("src","image/check.png");
					gantiPassNya = true;	
					document.getElementById("warningPassPem1").style.visibility = "visible";
					document.getElementById("warningPassPem2").style.visibility = "visible";
			}else{
					$("#warningPassPem1").attr("src","image/check.png");
					$("#warningPassPem2").attr("src","image/warning.png");
					gantiPassNya = false;
					document.getElementById("warningPassPem1").style.visibility = "visible";
					document.getElementById("warningPassPem2").style.visibility = "visible";
			}
	}else{
			$("#warningPassPem1").attr("src","image/warning.png");
			$("#warningPassPem2").attr("src","image/warning.png");
			gantiPassNya = false;
			document.getElementById("warningPassPem1").style.visibility = "visible";
			document.getElementById("warningPassPem2").style.visibility = "visible";
	}


}

function uniform(scope, w){
	$("#"+scope+" input:not([type=checkbox]):not([type=date]):not([type=submit]):not([type=reset]):not(.ex)").css({width:w});
	// $("#"+scope+" input[type=date]").css({width:w-1});
	$("#"+scope+" select:not(.ex)").css({width:w+7});
	$("#"+scope+" textarea:not(.ex)").css({width:w});
	$("#"+scope+" input.rptail").css({width:w-23});
	$("#"+scope+" input.trick").css({width:w-20});
}