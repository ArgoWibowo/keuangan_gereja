var i = 1;
var gantiPassNyaTambahAdmin;
var gantiPassNyaEditAdmin;
var tambahAdminKode;

$(document).ready(function() {
    // Setup - add a text input to each footer cell
   

    uniform("formEditAdmin",300);
    uniform("formTambahAdmin",300);
    uniform("formDetailAdmin",180);

	
	$(document).on('click','#mask, .btn_close',function(){
        closeDetailAdmin();
    });

    $('#kodeTambahAdmin').on('input', function(){

      var kodeAdminInput = $('#kodeTambahAdmin').val();

      $.ajax({
            url: "checkKodeAdmin.php",
            type: "POST",
            data: {"kodeAdminInput": kodeAdminInput},
            success: function(data){
	            if(checkUserNameWhiteSpace($('#kodeTambahAdmin').val()) == false){ 
			        if($('#kodeTambahAdmin').val().length <=8 && $('#kodeTambahAdmin').val().length > 0 ){
							if(data == true){
									$("#warningTambahAdmin").attr("src","image/check.png");
									$("#lblwarningTambahAdmin").html("Username Valid!");
									$("#lblwarningTambahAdmin").css({ 
								        'color' :'green'
								    });
									tambahAdminKode = true;
									document.getElementById("warningTambahAdmin").style.visibility = "visible";
									document.getElementById("lblwarningTambahAdmin").style.visibility = "visible";
							}else{
									$("#warningTambahAdmin").attr("src","image/warning.png");
									$("#lblwarningTambahAdmin").html("Username Tidak Valid!");
									$("#lblwarningTambahAdmin").css({ 
								        'color' :'red'
								    });
									tambahAdminKode = false;
									document.getElementById("warningTambahAdmin").style.visibility = "visible";
									document.getElementById("lblwarningTambahAdmin").style.visibility = "visible";
							}
					}else{
							$("#warningTambahAdmin").attr("src","image/warning.png");
							$("#lblwarningTambahAdmin").html("Username Tidak Valid!");
							$("#lblwarningTambahAdmin").css({ 
						        'color' :'red'
						    });
							tambahAdminKode = false;
							document.getElementById("warningTambahAdmin").style.visibility = "visible";
							document.getElementById("lblwarningTambahAdmin").style.visibility = "visible";
					}
	            }else{
	            	$("#warningTambahAdmin").attr("src","image/warning.png");
					$("#lblwarningTambahAdmin").html("Username Tidak Valid!");
					$("#lblwarningTambahAdmin").css({ 
				        'color' :'red'
				    });
					tambahAdminKode = false;
					document.getElementById("warningTambahAdmin").style.visibility = "visible";
					document.getElementById("lblwarningTambahAdmin").style.visibility = "visible";
	            }
        	}
        });

      	return false;
	});


    $('#passwordBaru').on('input', function(){
		if($(this).val().length	>=8){
				if($(this).val() == $('#passwordBaru2').val() ){
						$("#warningPassTambahAdmin1").attr("src","image/check.png");
						
						$("#lblwarningPassTambahAdmin1").html("Password Valid!");
						$("#lblwarningPassTambahAdmin1").css({ 
					        'color' :'green'
					    });

						$("#warningPassTambahAdmin2").attr("src","image/check.png");
						$("#lblwarningPassTambahAdmin2").html("Konfirmasi Valid!");
						$("#lblwarningPassTambahAdmin2").css({ 
					        'color' :'green'
					    });
						gantiPassNyaTambahAdmin = true;
						document.getElementById("warningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("warningPassTambahAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin2").style.visibility = "visible";
				}else{
						$("#warningPassTambahAdmin1").attr("src","image/check.png");
						$("#lblwarningPassTambahAdmin1").html("Password Valid!");
						$("#lblwarningPassTambahAdmin1").css({ 
					        'color' :'green'
					    });

						$("#warningPassTambahAdmin2").attr("src","image/warning.png");
						$("#lblwarningPassTambahAdmin2").html("Konfirmasi Tidak Valid!");
						$("#lblwarningPassTambahAdmin2").css({ 
					        'color' :'red'
					    });
						gantiPassNyaTambahAdmin = false;
						document.getElementById("warningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("warningPassTambahAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin2").style.visibility = "visible";
				}
		}else{
				$("#warningPassTambahAdmin1").attr("src","image/warning.png");
				$("#lblwarningPassTambahAdmin1").html("Password Tidak Valid!");
				$("#lblwarningPassTambahAdmin1").css({ 
			        'color' :'red'
			    });
				$("#warningPassTambahAdmin2").attr("src","image/warning.png");
				$("#lblwarningPassTambahAdmin2").html("Konfirmasi Tidak Valid!");
				$("#lblwarningPassTambahAdmin2").css({ 
			        'color' :'red'
			    });
				gantiPassNyaTambahAdmin = false;
				document.getElementById("warningPassTambahAdmin1").style.visibility = "visible";
				document.getElementById("warningPassTambahAdmin2").style.visibility = "visible";
				document.getElementById("lblwarningPassTambahAdmin1").style.visibility = "visible";
				document.getElementById("lblwarningPassTambahAdmin2").style.visibility = "visible";
		}
	});

	$('#passwordBaru2').on('input', function() {
		if($('#passwordBaru').val().length	>=8){
				if($(this).val() == $('#passwordBaru').val() ){
						$("#warningPassTambahAdmin1").attr("src","image/check.png");
						$("#lblwarningPassTambahAdmin1").html("Password Valid!");
						$("#lblwarningPassTambahAdmin1").css({ 
					        'color' :'green'
					    });
						$("#warningPassTambahAdmin2").attr("src","image/check.png");
						$("#lblwarningPassTambahAdmin2").html("Konfirmasi Valid!");
						$("#lblwarningPassTambahAdmin2").css({ 
					        'color' :'green'
					    });
						gantiPassNyaTambahAdmin = true;
						document.getElementById("warningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("warningPassTambahAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin2").style.visibility = "visible";
				}else{
						$("#warningPassTambahAdmin1").attr("src","image/check.png");
						$("#lblwarningPassTambahAdmin1").html("Password Valid!");
						$("#lblwarningPassTambahAdmin1").css({ 
					        'color' :'green'
					    });

						$("#warningPassTambahAdmin2").attr("src","image/warning.png");
						$("#lblwarningPassTambahAdmin2").html("Konfirmasi Tidak Valid!");
						$("#lblwarningPassTambahAdmin2").css({ 
					        'color' :'red'
					    });

						gantiPassNyaTambahAdmin = false;
						document.getElementById("warningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("warningPassTambahAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin2").style.visibility = "visible";
				}
		}else{
				$("#warningPassTambahAdmin1").attr("src","image/warning.png");
				$("#lblwarningPassTambahAdmin1").html("Password Tidak Valid!");
				$("#lblwarningPassTambahAdmin1").css({ 
			        'color' :'red'
			    });
				$("#warningPassTambahAdmin2").attr("src","image/warning.png");
				$("#lblwarningPassTambahAdmin2").html("Konfirmasi Tidak Valid!");
				$("#lblwarningPassTambahAdmin2").css({ 
			        'color' :'red'
			    });
				gantiPassNyaTambahAdmin = false;
				document.getElementById("warningPassTambahAdmin1").style.visibility = "visible";
				document.getElementById("warningPassTambahAdmin2").style.visibility = "visible";
				document.getElementById("lblwarningPassTambahAdmin1").style.visibility = "visible";
				document.getElementById("lblwarningPassTambahAdmin2").style.visibility = "visible";
		}
	});

	$('#editpasswordBaru').on('input', function(){
		if($(this).val().length	>=8){
				if($(this).val() == $('#editpasswordBaru2').val() ){
						$("#warningPassEditAdmin1").attr("src","image/check.png");
						$("#lblwarningPassEditAdmin1").html("Password Valid!");
						$("#lblwarningPassEditAdmin1").css({ 
					        'color' :'green'
					    });
						$("#warningPassEditAdmin2").attr("src","image/check.png");
						$("#lblwarningPassEditAdmin2").html("Konfirmasi Valid!");
						$("#lblwarningPassEditAdmin2").css({ 
					        'color' :'green'
					    });
						gantiPassNyaEditAdmin = true;
						document.getElementById("warningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("warningPassEditAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin2").style.visibility = "visible";
				}else{
						$("#warningPassEditAdmin1").attr("src","image/check.png");
						$("#lblwarningPassEditAdmin1").html("Password Valid!");
						$("#lblwarningPassEditAdmin1").css({ 
					        'color' :'green'
					    });
						$("#warningPassEditAdmin2").attr("src","image/warning.png");
						$("#lblwarningPassEditAdmin2").html("Konfirmasi Tidak Valid!");
						$("#lblwarningPassEditAdmin2").css({ 
					        'color' :'red'
					    });
						gantiPassNyaEditAdmin = false;
						document.getElementById("warningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("warningPassEditAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin2").style.visibility = "visible";
				}
		}else{
				$("#warningPassEditAdmin1").attr("src","image/warning.png");
				$("#lblwarningPassEditAdmin1").html("Password Tidak Valid!");
				$("#lblwarningPassEditAdmin1").css({ 
			        'color' :'red'
			    });
				$("#warningPassEditAdmin2").attr("src","image/warning.png");
				$("#lblwarningPassEditAdmin2").html("Konfirmasi Tidak Valid!");
				$("#lblwarningPassEditAdmin2").css({ 
			        'color' :'red'
			    });
				gantiPassNyaEditAdmin = false;
				document.getElementById("warningPassEditAdmin1").style.visibility = "visible";
				document.getElementById("warningPassEditAdmin2").style.visibility = "visible";
				document.getElementById("lblwarningPassEditAdmin1").style.visibility = "visible";
				document.getElementById("lblwarningPassEditAdmin2").style.visibility = "visible";
		}
	});

	$('#editpasswordBaru2').on('input', function() {
		if($('#editpasswordBaru').val().length	>=8){
				if($(this).val() == $('#editpasswordBaru').val() ){
						$("#warningPassEditAdmin1").attr("src","image/check.png");
						$("#lblwarningPassEditAdmin1").html("Password Valid!");
						$("#lblwarningPassEditAdmin1").css({ 
					        'color' :'green'
					    });
						$("#warningPassEditAdmin2").attr("src","image/check.png");
						$("#lblwarningPassEditAdmin2").html("Konfirmasi Valid!");
						$("#lblwarningPassEditAdmin2").css({ 
					        'color' :'green'
					    });
						gantiPassNyaEditAdmin = true;
						document.getElementById("warningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("warningPassEditAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin2").style.visibility = "visible";
				}else{
						$("#warningPassEditAdmin1").attr("src","image/check.png");
						$("#lblwarningPassEditAdmin1").html("Password Valid!");
						$("#lblwarningPassEditAdmin1").css({ 
					        'color' :'green'
					    });
						$("#warningPassEditAdmin2").attr("src","image/warning.png");
						$("#lblwarningPassEditAdmin2").html("Konfirmasi Tidak Valid!");
						$("#lblwarningPassEditAdmin2").css({ 
					        'color' :'red'
					    });
						gantiPassNyaEditAdmin = false;
						document.getElementById("warningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("warningPassEditAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin2").style.visibility = "visible";
				}
		}else{
				$("#warningPassEditAdmin1").attr("src","image/warning.png");
				$("#lblwarningPassEditAdmin1").html("Password Tidak Valid!");
				$("#lblwarningPassEditAdmin1").css({ 
			        'color' :'red'
			    });
				$("#warningPassEditAdmin2").attr("src","image/warning.png");
				$("#lblwarningPassEditAdmin2").html("Konfirmasi Tidak Valid!");
				$("#lblwarningPassEditAdmin2").css({ 
			        'color' :'red'
			    });
				gantiPassNyaEditAdmin = false;
				document.getElementById("warningPassEditAdmin1").style.visibility = "visible";
				document.getElementById("warningPassEditAdmin2").style.visibility = "visible";
				document.getElementById("lblwarningPassEditAdmin1").style.visibility = "visible";
				document.getElementById("lblwarningPassEditAdmin2").style.visibility = "visible";
		}
	});

});

$("form#formEditAdmin").submit(function(){

		var editKodeAdmin = $("#editkodeTambahAdmin").val();

	    var editadminOptions = document.getElementById("editselect_otoritas_admin");
		var editotoritasAdmin = editadminOptions.options[editadminOptions.selectedIndex].value;



		var editpemasukanOptions = document.getElementById("editselect_otoritas_pemasukan");
		var editotoritasPemasukan = editpemasukanOptions.options[editpemasukanOptions.selectedIndex].value;



		var editpengeluaranOptions = document.getElementById("editselect_otoritas_pengeluaran");
		var editotoritasPengeluaran = editpengeluaranOptions.options[editpengeluaranOptions.selectedIndex].value;
	    


	    var editkodeDigunakanOptions = document.getElementById("editselect_kode");
		var editkodeDigunakan = editkodeDigunakanOptions.options[editkodeDigunakanOptions.selectedIndex].value;

		var editkode;

		if(editkodeDigunakan != "TIDAKADA"){
			var editkodeOptions = document.getElementById("editkomisiBidangAdmin");
			editkode = String(editkodeOptions.options[editkodeOptions.selectedIndex].value);
		}
			


		var editpertanyaanOptions = document.getElementById("editselect_pertanyaan_pemulihan");
		var editpertanyaan = editpertanyaanOptions.options[editpertanyaanOptions.selectedIndex].value;
	    
		var editnama = $("#editnamaAdmin").val();
		var editpass= $("#editpasswordBaru").val();
		var editjawaban = $("#editjawabanPertanyaan").val();
		var editalamat= $("#editalamatAdmin").val();
		var editnoTelp= $("#editteleponAdmin").val();
		var editemail= $("#editemailBaru").val();
		
		var editcekOptions = document.getElementById("editselect_status");
		var editcek=editcekOptions.options[editcekOptions.selectedIndex].value;
		var editstatus;
		var checkPass;
		if(editcek == 0) editstatus = 1;
		else if(editcek == 1) editstatus = 0;


		
		if (document.getElementById('checkGantiPass').checked) {
			checkPass = 1;
		}else {
	  		checkPass = 0;
	  	}

	  	if(checkPass == 1 && gantiPassNyaEditAdmin == true){
	  			$.ajax({
					type: "POST",
					url: "editAdmin2.php",
					data: {"editKodeAdmin": editKodeAdmin, 
					"editnama": editnama, 
					"editkodeDigunakan": editkodeDigunakan,
					"editkode": editkode, 
					"editpass": editpass,
					"editpertanyaan": editpertanyaan,
					"editjawaban": editjawaban, 
					"editalamat": editalamat, 
					"editnoTelp": editnoTelp, 
					"editemail": editemail, 
					"editotoritasAdmin": editotoritasAdmin, 
					"editotoritasPemasukan": editotoritasPemasukan, 
					"editotoritasPengeluaran": editotoritasPengeluaran, 
					"editstatus": editstatus,
					"checkPass": checkPass },
					async: false,
					dataType: "json",
					success: function(data){

						if(data){
								alert("Admin Telah diubah!");
								$('#tabeladmin tr.selected td:eq(1)').html(data[0][0]);
								$('#tabeladmin tr.selected td:eq(2)').html(data[0][1]);
								$('#tabeladmin tr.selected td:eq(3)').html(data[0][2]);
								$('#tabeladmin tr.selected td:eq(4)').html(data[0][3]);
								$('#tabeladmin tr.selected td:eq(5)').html(data[0][4]);
								if(data[0][5] == 0){
									$('#tabeladmin tr.selected td:eq(6)').html('Tidak Aktif');
									var b = document.getElementById('buttonStatus');
									b.innerHTML = 'Aktifkan';
								}else if(data[0][5] == 1){
									$('#tabeladmin tr.selected td:eq(6)').html('Aktif');
									var b = document.getElementById('buttonStatus');
									b.innerHTML = 'Nonaktifkan';
								}
								
						}
						
					}
				}); 

				//$(".editAdmin").hide();
				document.getElementById("formEditAdmin").reset();
				$("div.editAdmin").slideUp("700");
				$("div.menuadmin").slideDown("700"); 
	  	}else if(checkPass == 0){
	  			$.ajax({
					type: "POST",
					url: "editAdmin2.php",
					data: {"editKodeAdmin": editKodeAdmin, 
					"editnama": editnama, 
					"editkodeDigunakan": editkodeDigunakan,
					"editkode": editkode, 
					"editpass": editpass,
					"editpertanyaan": editpertanyaan,
					"editjawaban": editjawaban, 
					"editalamat": editalamat, 
					"editnoTelp": editnoTelp, 
					"editemail": editemail, 
					"editotoritasAdmin": editotoritasAdmin, 
					"editotoritasPemasukan": editotoritasPemasukan, 
					"editotoritasPengeluaran": editotoritasPengeluaran, 
					"editstatus": editstatus,
					"checkPass": checkPass },
					async: false,
					dataType: "json",
					success: function(data){
							// alert(data);
						if(data){
							// alert(data);
								alert("Admin Telah diubah!");
								$('#tabeladmin tr.selected td:eq(1)').html(data[0][0]);
								$('#tabeladmin tr.selected td:eq(2)').html(data[0][1]);
								$('#tabeladmin tr.selected td:eq(3)').html(data[0][2]);
								$('#tabeladmin tr.selected td:eq(4)').html(data[0][3]);
								$('#tabeladmin tr.selected td:eq(5)').html(data[0][4]);
								if(data[0][5] == 0){
									$('#tabeladmin tr.selected td:eq(6)').html('Tidak Aktif');
									var b = document.getElementById('buttonStatus');
									b.innerHTML = 'Aktifkan';
								}else if(data[0][5] == 1){
									$('#tabeladmin tr.selected td:eq(6)').html('Aktif');
									var b = document.getElementById('buttonStatus');
									b.innerHTML = 'Nonaktifkan';
								}
								
						}
						
					}
				}); 

				//$(".editAdmin").hide();
				document.getElementById("formEditAdmin").reset();
				$("div.editAdmin").slideUp("700");
				$("div.menuadmin").slideDown("700"); 
	  	}else if(checkPass == 1 && gantiPassNyaEditAdmin == false){
	  			document.getElementById("editpasswordBaru").focus();
	  	}

		return false;
	});


$("form#formTambahAdmin").submit(function(){		
		if(tambahAdminKode == true){
			if (gantiPassNyaTambahAdmin == true){

				var adminOptions = document.getElementById("select_otoritas_admin");
				var otoritasAdmin = adminOptions.options[adminOptions.selectedIndex].value;


				var pemasukanOptions = document.getElementById("select_otoritas_pemasukan");
				var otoritasPemasukan = pemasukanOptions.options[pemasukanOptions.selectedIndex].value;

				var pengeluaranOptions = document.getElementById("select_otoritas_pengeluaran");
				var otoritasPengeluaran = pengeluaranOptions.options[pengeluaranOptions.selectedIndex].value;
			    
			    var kodeDigunakanOptions = document.getElementById("select_kode");
				var kodeDigunakan = kodeDigunakanOptions.options[kodeDigunakanOptions.selectedIndex].value;

				var kode = '';

				if(kodeDigunakan != "TIDAKADA"){
					var kodeOptions = document.getElementById("komisiBidangAdmin");
					kode = kodeOptions.options[kodeOptions.selectedIndex].value;
				}else{
					kode = '';
				}
				

				var pertanyaanOptions = document.getElementById("select_pertanyaan_pemulihan");
				var pertanyaan = pertanyaanOptions.options[pertanyaanOptions.selectedIndex].value;

				var kodeAdminTambah = $("#kodeTambahAdmin").val();
				var nama = $("#namaAdmin").val();
				var pass= $("#passwordBaru").val();
				var jawaban= $("#jawabanPertanyaan").val();
				var alamat= $("#alamatAdmin").val();
				var noTelp= $("#teleponAdmin").val();
				var email= $("#emailBaru").val();

				var cekOptions = document.getElementById("select_status");
				var cek=cekOptions.options[cekOptions.selectedIndex].value;
				var status;
				if(cek == 0) status = 1;
				else if(cek == 1) status = 0;


				$.ajax({
						type: "POST",
						url: "ajax_adminTambah.php",
						data: {"kodeAdminTambah": kodeAdminTambah,
						"nama": nama, 
						"kodeDigunakan": kodeDigunakan,
						"kode": kode, 
						"pass": pass,
						"pertanyaan": pertanyaan,
						"jawaban": jawaban, 
						"alamat": alamat, 
						"noTelp": noTelp, 
						"email": email, 
						"otoritasAdmin": otoritasAdmin, 
						"otoritasPemasukan": otoritasPemasukan, 
						"otoritasPengeluaran": otoritasPengeluaran, 
						"statusTambahAdmin": status },
						async: false,
						success: function(data){
							// alert(data);
							if(data == 1){
								alert('data admin berhasil ditambahkan!');
							}else{
								alert('data admin tidak ditambahkan!');
							}								
							loadTableAdmin();
						}
					});
				document.getElementById("formTambahAdmin").reset();
				$("div.tambahAdmin").slideUp("700");
				$("#right>div.menuadmin").slideDown("700");
				$("#right>div.menuadmin div").slideDown("700");
			}else{
				document.getElementById("passwordBaru").focus();
			}
		}else{
			document.getElementById("kodeTambahAdmin").focus();
		}
		
	return false;
	});

function checkUserNameWhiteSpace(kode){

  var regexKodeAdmin = /^$|\s+/;
  return regexKodeAdmin.test(kode);
}

function closeDetailAdmin(){
	$('#mask').remove();  
    $('.detailAdmin').fadeOut(200); 
    $('.detailAdmin').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });
}

function orderBy(){
	loadTableAdmin();
}


function adminTambah(){
		//$(".menuadmin div").hide();
					// $("#kodeTambahAdmin").val(data);
					checkConfirmasiPassTambah();
					gantiKode();
					document.getElementById("warningTambahAdmin").style.visibility = "hidden";
					document.getElementById("lblwarningTambahAdmin").style.visibility = "hidden";
					$("div.menuadmin").slideUp("700");
					$("div.tambahAdmin").slideDown("700");
					document.getElementById("formTambahAdmin").reset();
					
		
}

function loadTableAdmin(){
		
		var kataKunci = $('input#searchAdmin').val().trim();

    	var searchOrderIndex = document.getElementById("selectOrderBy").selectedIndex;
	    var searchOrderOptions = document.getElementById("selectOrderBy").options;
	    var searchOrderBySelect = (searchOrderOptions[searchOrderIndex].value).toLowerCase();

		$.ajax({
				type: "POST",
				url: "loadadmin.php",
				data: {"kataKunci": kataKunci,
					   "searchOrderBySelect": searchOrderBySelect
				},
				// dataType: "json",
				success: function(data){
					if(data){
						$("#tabel_isi").empty();
						$("#tabel_isi").html(data);
						
					}
					
				}
			});
		
}

function batalTambahAdmin(){
		//$(".tambahAdmin").hide();

		document.getElementById("formTambahAdmin").reset();

		$("div.tambahAdmin").slideUp("700");
		$("#right>div.menuadmin").slideDown("700");
		$("#right>div.menuadmin div").slideDown("700");

}

function adminStatus(){

	// var x = $("#tabeladmin tr.selected td:first").html();
	// alert(x);
		
		// var x = document.getElementById("tabeladmin").row[0].cells;
  //   	x[0].innerHTML = "NEW CONTENT";
  			var kodeAdmin = String($('#tabeladmin tr.selected td:eq(1)').html());
  			var statusText = String($('#tabeladmin tr.selected td:eq(6)').html());
  			var statusAdmin;
  			if (statusText == "Aktif") statusAdmin = 1;
  			else statusAdmin = 0;
  		
  	if($('#tabeladmin tr.selected td:eq(1)').html()){
	 		$.ajax({
				type: "POST",
				url: "ubahStatusAdmin.php",
				data: {"kodeAdmin": kodeAdmin, "statusAdmin":statusAdmin},
				success: function(data){
					if(data){
						if(data == 0){
							$('#tabeladmin tr.selected td:eq(6)').html('Tidak Aktif');
							var b = document.getElementById('buttonStatus');
							b.innerHTML = 'Aktifkan';
						}else if(data == 1){
							$('#tabeladmin tr.selected td:eq(6)').html('Aktif');
							var b = document.getElementById('buttonStatus');
							b.innerHTML = 'Nonaktifkan';
						}else if(data == 3){
							alert('Admin tidak dapat menonaktifkan diri sendiri!');
						}
					}
					
				}
			});
	}else{
		alert('pilih dahulu data admin!');
	}
  	// var x=document.getElementById('tabeladmin').rows
    //var y=x[0].cells
    
}
		

function adminEdit(){
	var kodeAdminEdit = String($('#tabeladmin tr.selected td:eq(1)').html());
	editGantiKode();
	if($('#tabeladmin tr.selected td:eq(1)').html()){
			$.ajax({
				type: "POST",
				url: "loadEditAdmin.php",
				dataType: "json",
				data: {"kodeAdminEdit": kodeAdminEdit},
				success: function(data){

					if(data){
							// alert(data);
							document.getElementById("editkodeTambahAdmin").value = data[0][3];

							document.getElementById("editnamaAdmin").value = data[0][0];
							document.getElementById("editselect_kode").value = data[0][1];
							editGantiKode(data[0][2]);
							
							document.getElementById("editselect_pertanyaan_pemulihan").value = data[0][4];
							document.getElementById("editjawabanPertanyaan").value = data[0][5];
							document.getElementById("editalamatAdmin").value = data[0][6];
							document.getElementById("editteleponAdmin").value = data[0][7];
							document.getElementById("editemailBaru").value = data[0][8];
							document.getElementById("checkGantiPass").checked = false;
							checkBoxPass();
							document.getElementById("editselect_otoritas_admin").value = data[0][9];
							document.getElementById("editselect_otoritas_pemasukan").value = data[0][10];
							document.getElementById("editselect_otoritas_pengeluaran").value = data[0][11];
							document.getElementById("editselect_status").value = Math.abs(data[0][12]-1);

							if(data[0][13] == "1"){
								document.getElementById("editselect_otoritas_admin").disabled = true;
								document.getElementById("editselect_otoritas_pemasukan").disabled = true;
								document.getElementById("editselect_otoritas_pengeluaran").disabled = true;
								document.getElementById("editselect_status").disabled = true;
							}else{
								document.getElementById("editselect_otoritas_admin").disabled = false;
								document.getElementById("editselect_otoritas_pemasukan").disabled = false;
								document.getElementById("editselect_otoritas_pengeluaran").disabled = false;
								document.getElementById("editselect_status").disabled = false;
							}

							$("div.menuadmin").slideUp("700");
							$("div.editAdmin").slideDown("700");
							checkConfirmasiPassEdit();
					}
					
				}
			});
	}else{
		alert('pilih dahulu admin yang akan diubah!');
	}

	//$(".menuadmin div").hide();
			
}

function batalEditAdmin(){
		//$(".editAdmin").hide();
		document.getElementById("formEditAdmin").reset();
		$("div.editAdmin").slideUp("700");
		$("div.menuadmin").slideDown("700");
}

function adminDipilih(){
	$("#tabeladmin tr").click(function(){
			var b = document.getElementById('buttonStatus');
			if(this.rowIndex != 0 && this.rowIndex != 1){
			   $(this).addClass('selected').siblings().removeClass('selected');
			   if(String($('#tabeladmin tr.selected td:eq(6)').html()) == "Aktif"){
					b.innerHTML = 'Nonaktifkan';
				}else if(String($('#tabeladmin tr.selected td:eq(6)').html()) == "Tidak Aktif"){
					b.innerHTML = 'Aktifkan';
				}    
			}
			       
	});
}

var tampKode = 0;
function gantiKode(){
	var kodeOptions = document.getElementById("select_kode");
	var kodeDigunakan = kodeOptions.options[kodeOptions.selectedIndex].value;

	$.ajax({
			type: "POST",
			url: "loadKode.php",
			data: {"kodeDigunakan": kodeDigunakan},
			// dataType: "json",
			success: function(data){
				if(data){
					// alert(data);
					// document.getElementById('komisiBidangAdmin').options.length = 0;
					// var select = document.getElementById('komisiBidangAdmin');
					
					$('#komisiBidangAdmin').html(data);
					placeholderselect();
					// for( i = 0; i<data.length;i++){
					// 		    var opt = document.createElement('option');
					// 		    opt.value = data[i][0];
					// 		    opt.innerHTML = data[i][1];
					// 		    select.appendChild(opt);	
					// }				
				}
			}
	});

	if(kodeDigunakan == "KOMISI"){
		if(tampKode == 1){
			$("#kode_bidang_komisi td").slideDown("700");
			tampKode = 0;

		}
		$('#kodeTerpakai').html("Kode Komisi");
	}else if(kodeDigunakan == "BIDANG"){
		if(tampKode == 1){
			$("#kode_bidang_komisi td").slideDown("700");
			tampKode = 0;
		}
		$('#kodeTerpakai').html("Kode Bidang");
	}else{
		if(tampKode != 1){
			$("#kode_bidang_komisi td").slideUp("700");
			tampKode = 1;
		}
	}
	
							
}

var edittampKode = 0;
function editGantiKode(dataKode){

	var kodeDigunakanOptions = document.getElementById("editselect_kode");
	var kodeDigunakan = kodeDigunakanOptions.options[kodeDigunakanOptions.selectedIndex].value;

	$.ajax({
			type: "POST",
			url: "loadKode.php",
			data: {"kodeDigunakan": kodeDigunakan},
			// dataType: "json",
			success: function(data){
				if(data){
					// alert(data);
					// document.getElementById('editkomisiBidangAdmin').options.length = 0;
					// var select = document.getElementById('editkomisiBidangAdmin');

					// for( i = 0; i<data.length;i++){
					// 		    var opt = document.createElement('option');
					// 		    opt.value = data[i][0];
					// 		    opt.innerHTML = data[i][1];
					// 		    select.appendChild(opt);	
					// }		

					$('#editkomisiBidangAdmin').html(data);
					if(dataKode != ''){
						document.getElementById("editkomisiBidangAdmin").value = dataKode;
						placeholderselect();
					}
							
				}
			}
	});

	if(kodeDigunakan == "KOMISI"){
		if(edittampKode == 1){
			$("#editkode_bidang_komisi td").slideDown("700");
			edittampKode = 0;

		}
		$('#editkodeTerpakai').html("Kode Komisi");
	}else if(kodeDigunakan == "BIDANG"){
		if(edittampKode == 1){
			$("#editkode_bidang_komisi td").slideDown("700");
			edittampKode = 0;
		}
		$('#editkodeTerpakai').html("Kode Bidang");
	}else{
		if(edittampKode != 1){
			$("#editkode_bidang_komisi td").slideUp("700");
			edittampKode = 1;
		}
	}
							
}

function checkBoxPass(){
	if (document.getElementById('checkGantiPass').checked) {
    	$("#editpass1").slideDown("700");
    	$("#editpass2").slideDown("700");  
  	}else {
  		$("#editpass1").slideUp("700");
    	$("#editpass2").slideUp("700"); 
  	}
}

function adminDetailPopUp(div){
	var loginBox = div;
	window.location.href= div;
	
    var kodeAdminDetail = String($('#tabeladmin tr.selected td:eq(1)').html());
    if($('#tabeladmin tr.selected td:eq(1)').html()){
    $.ajax({
				type: "POST",
				url: "loadDetailAdmin.php",
				dataType: "json",
				data: {"kodeAdminDetail": kodeAdminDetail},
				success: function(data){
					
					if(data){
							
							document.getElementById("namaDetailAdmin").value = data[0][0];
							document.getElementById("kodeDetailAdmin").value = data[0][1];
							document.getElementById("kodeTerpilihDetailAdmin").value = data[0][2];
							document.getElementById("alamatDetailAdmin").value = data[0][3];
							document.getElementById("teleponDetailAdmin").value = data[0][4];
							document.getElementById("emailDetailAdmin").value = data[0][5];
							document.getElementById("statusDetailAdmin").value = data[0][6];

							var popMargTop = ($(loginBox).height() + 24) / 2; 
						    var popMargLeft = ($(loginBox).width() + 24) / 2; 
						    //  Add the mask to body
						    $('body').append('<div id="mask"></div>');
						    $('#mask').fadeIn(300);
						    $(".detailAdmin").fadeIn(500);
						    $(".detailAdmin").css({ 
						        'visibility' :'visible',
						        'display' :'block',
						        'margin-top' : -popMargTop,
						        'margin-left' : -popMargLeft
						    });	
							
					}
					
				}
			});
	}else{
		alert('pilih dahulu data admin!');
	}
}

function checkConfirmasiPassTambah(){
	if($('#passwordBaru').val().length	>=8){
				if($('#passwordBaru').val() == $('#passwordBaru2').val() ){
						$("#warningPassTambahAdmin1").attr("src","image/check.png");
						
						$("#lblwarningPassTambahAdmin1").html("Password Valid!");
						$("#lblwarningPassTambahAdmin1").css({ 
					        'color' :'green'
					    });

						$("#warningPassTambahAdmin2").attr("src","image/check.png");
						$("#lblwarningPassTambahAdmin2").html("Konfirmasi Valid!");
						$("#lblwarningPassTambahAdmin2").css({ 
					        'color' :'green'
					    });
						gantiPassNyaTambahAdmin = true;
						document.getElementById("warningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("warningPassTambahAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin2").style.visibility = "visible";
				}else{
						$("#warningPassTambahAdmin1").attr("src","image/check.png");
						$("#lblwarningPassTambahAdmin1").html("Password Valid!");
						$("#lblwarningPassTambahAdmin1").css({ 
					        'color' :'green'
					    });

						$("#warningPassTambahAdmin2").attr("src","image/warning.png");
						$("#lblwarningPassTambahAdmin2").html("Konfirmasi Tidak Valid!");
						$("#lblwarningPassTambahAdmin2").css({ 
					        'color' :'red'
					    });
						gantiPassNyaTambahAdmin = false;
						document.getElementById("warningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("warningPassTambahAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassTambahAdmin2").style.visibility = "visible";
				}
		}else{
				$("#warningPassTambahAdmin1").attr("src","image/warning.png");
				$("#lblwarningPassTambahAdmin1").html("Password Tidak Valid!");
				$("#lblwarningPassTambahAdmin1").css({ 
			        'color' :'red'
			    });
				$("#warningPassTambahAdmin2").attr("src","image/warning.png");
				$("#lblwarningPassTambahAdmin2").html("Konfirmasi Tidak Valid!");
				$("#lblwarningPassTambahAdmin2").css({ 
			        'color' :'red'
			    });
				gantiPassNyaTambahAdmin = false;
				document.getElementById("warningPassTambahAdmin1").style.visibility = "visible";
				document.getElementById("warningPassTambahAdmin2").style.visibility = "visible";
				document.getElementById("lblwarningPassTambahAdmin1").style.visibility = "visible";
				document.getElementById("lblwarningPassTambahAdmin2").style.visibility = "visible";
		}
}






function checkConfirmasiPassEdit(){
	if($('#editpasswordBaru').val().length	>=8){
				if($('#editpasswordBaru2').val() == $('#editpasswordBaru').val() ){
						$("#warningPassEditAdmin1").attr("src","image/check.png");
						$("#lblwarningPassEditAdmin1").html("Password Valid!");
						$("#lblwarningPassEditAdmin1").css({ 
					        'color' :'green'
					    });
						$("#warningPassEditAdmin2").attr("src","image/check.png");
						$("#lblwarningPassEditAdmin2").html("Konfirmasi Valid!");
						$("#lblwarningPassEditAdmin2").css({ 
					        'color' :'green'
					    });
						gantiPassNyaEditAdmin = true;
						document.getElementById("warningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("warningPassEditAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin2").style.visibility = "visible";
				}else{
						$("#warningPassEditAdmin1").attr("src","image/check.png");
						$("#lblwarningPassEditAdmin1").html("Password Valid!");
						$("#lblwarningPassEditAdmin1").css({ 
					        'color' :'green'
					    });
						$("#warningPassEditAdmin2").attr("src","image/warning.png");
						$("#lblwarningPassEditAdmin2").html("Konfirmasi Tidak Valid!");
						$("#lblwarningPassEditAdmin2").css({ 
					        'color' :'red'
					    });
						gantiPassNyaEditAdmin = false;
						document.getElementById("warningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("warningPassEditAdmin2").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin1").style.visibility = "visible";
						document.getElementById("lblwarningPassEditAdmin2").style.visibility = "visible";
				}
		}else{
				$("#warningPassEditAdmin1").attr("src","image/warning.png");
				$("#lblwarningPassEditAdmin1").html("Password Tidak Valid!");
				$("#lblwarningPassEditAdmin1").css({ 
			        'color' :'red'
			    });
				$("#warningPassEditAdmin2").attr("src","image/warning.png");
				$("#lblwarningPassEditAdmin2").html("Konfirmasi Tidak Valid!");
				$("#lblwarningPassEditAdmin2").css({ 
			        'color' :'red'
			    });
				gantiPassNyaEditAdmin = false;
				document.getElementById("warningPassEditAdmin1").style.visibility = "visible";
				document.getElementById("warningPassEditAdmin2").style.visibility = "visible";
				document.getElementById("lblwarningPassEditAdmin1").style.visibility = "visible";
				document.getElementById("lblwarningPassEditAdmin2").style.visibility = "visible";
		}
}