$("#formNota").ready(function(){
	uniform("formNota", 300);

	$("#nota_tanggal").focusout(function(){
		var objectd = $("#Nota_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#nota_bulan").focusout(function(){
		var m = $("#nota_bulan").val();
		if(m > 12 || m < 1) {
			$("#nota_bulan").val("");
			m = null;
		}
		if(m == null) $("#nota_tanggal").attr("max", 31);
		else if(m == 2) $("#nota_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#nota_tanggal").attr("max", 31);
			else $("#nota_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#nota_tanggal").attr("max", 31);
			else $("#nota_tanggal").attr("max", 30);
		}
		$("#nota_tahun").triggerHandler("focusout");
		$("#nota_tanggal").triggerHandler("focusout");
	});

	$("#nota_tahun").focusout(function(){
		var m = $("#nota_bulan").val(); 
		var y = $("#nota_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#nota_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#nota_tanggal").attr("max", 29);
				} else {
					$("#nota_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#nota_tanggal").attr("max", 29);
				} else {
					$("#nota_tanggal").attr("max", 28);
				}
			}
			$("#nota_tanggal").triggerHandler("focusout");
		}
	});

	$("#formNota").submit(function(){
		var kodenota = $("#kodeNota").val();
		var kodetoko = $("select.selectTokoNota").val(); 
		var debet = $("select.selectDebetNota").val().replace("_dot_", ".");
		var tanggalNota = $("#nota_tahun").val()+"-"+$("#nota_bulan").val()+"-"+$("#nota_tanggal").val();
		var jumlah = $("#nota_jumlah").val().replace(/\./g, "");
		var pelaksana = $("#pelaksanaNota").val();
		
		//alert(nokwitansi+"\n"+tanggalNota+"\n"+kredit+"\n"+pemberi+"\n"+jumlah+"\n"+penerima+"\n"+untukbulan+"\n"+untuktahun);

		$.ajax({
			url: "inputnota_proses.php",
			type: "POST",
			data: {
				"kodenota" : kodenota,
				"kodetoko" : kodetoko,
				"debet" : debet,
				"tanggalNota" : tanggalNota,
				"jumlah" : jumlah,
				"pelaksana" : pelaksana
			},
			success: function(result){
				// alert(result);
				if(result == true){
					alert("berhasil menyimpan!");
					$("#right>div.inputnota input[type=reset]").click();
					placeholderselect();
				} else {
					alert("gagal menyimpan!");
				}
			}
		});
		return false;
	});

	$("#right>div.inputnota select").focusin(function(){
		refreshSelectNota($(this));
	});

	$("#right>div.inputnota .trick").focusin(function(){
		var i = $(this).attr("id");
		refreshSelectNota($("select."+i));
	});

});

function refreshSelectNota(obj){
	var value = obj.val();
	var type;
	var c = obj.attr('class');
	if(c == 'selectDebetNota') c = "rekening";
	else if(c == 'selectTokoNota') c = "toko";
	$.ajax({
        type	: 'POST',
        url		: 'refreshSelectFunction.php',
        data 	: {
        	'type' : c
        },
        success: function(result) {
        	obj.html(result);
        	if(obj.children("[value="+value+"]").length > 0){
        		obj.val(value);
        	} else placeholderselect();
        }
    });	
}