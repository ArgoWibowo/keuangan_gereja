var timer;
var flag;

$(document).ready(function() {
	$("#wrapper").height($(window).height() - $("body").offset().top*2);
	timer = setInterval(endresize, 500);

	$(window).resize(function(){
		flag = true;
	});
	// untuk subitem menu
	$(".menu div").hide();
	$("#right>div").hide();
	$(".menu li").click(function(){

		if(String($(this).attr("class")) == 'logout'){
				location.replace("logout.php");
		}else{
			var c = $(this).attr("class");
			var p = $(this).parent().attr("class");
			var v1 = $("#left div."+c).is(":visible");
			var v2 = $("#right>div."+c).is(":visible");
			if($("#right>div."+c).length == 0){
				$("."+p+" div").slideUp("700");
				if(!v1){
					$("div."+c).slideDown("700");
				}
			} else {
				if(!v2){
					$("#right>div").hide();
					if(!v1){
						$("."+p+" div").slideUp("700");
						$("div."+c).slideDown("700");
						$("#right div."+c+" div").slideDown("700");
					} else {
						$("div."+c).slideDown("700");
						$("#right div."+c+" div").slideDown("700");
					}
				} else {
					$("#left div."+c).slideToggle("700");
				}
			}
		}
	});

	// untuk editable select
	$("select").mouseover(function(){
		var c = $(this).attr("class");
		var pos = $(this).offset();
		var w = $(this).width();
		var h = $(this).height();
		$(".trick#"+c).css({left:pos.left+1, top:pos.top+1, width:w-21, height:h-2});
	});

	$("select").focusin(function(){
		var c = $(this).attr("class");
		var pos = $(this).offset();
		var w = $(this).width();
		var h = $(this).height();
		$(".trick#"+c).css({left:pos.left+1, top:pos.top+1, width:w-21, height:h-2});
	});

	$("select").keyup(function(event){
		var c = $(this).attr("class");
		var pos = $(this).offset();
		if(($(".trick#"+c).offset().left != pos.left+1) || ($(".trick#"+c).offset().top != pos.top+1)){
			$(this).trigger("focusin");
		}

		var k = (event.keyCode ? event.keyCode : event.which);
		if((k > 47 && k < 58) || (k > 64 && k < 91) || (k > 96 && k < 123)){
			$(".trick#"+c).focus();
			$(".trick#"+c).val((String.fromCharCode(k)).toUpperCase());
			$(".trick#"+c).css("background-color", "white");
		}
	});

	$(".trick").focusin(function(event){
		$(this).css("background-color", "white");
	});

	$(".trick").focusout(function(){
		var i = $(this).attr("id");
		// if($("select."+i+" option[value="+$(this).val()+"]").length > 0){
			$("select."+i).val($(this).val());
		// }
		$(this).css("background-color", "transparent");
		$(this).val("");
	});

	$(".trick").keyup(function(event){
		var i = $(this).attr("id");
		var k = (event.keyCode ? event.keyCode : event.which);
		if($(this).val() == ""){
			if (k == 8){
				$(this).css("background-color", "transparent");
				$("select."+i).focus();
			} 
		}
	});

	// akhir code untuk editable select
});

// fungsi untuk menyamakan panjang input dan select dalam suatu tabel
function uniform(scope, w){
	$("#"+scope+" input:not([type=checkbox]):not([type=date]):not([type=submit]):not([type=reset]):not(.ex)").css({width:w});
	// $("#"+scope+" input[type=date]").css({width:w-1});
	$("#"+scope+" select:not(.ex)").css({width:w+7});
	$("#"+scope+" textarea:not(.ex)").css({width:w});
	$("#"+scope+" input.rptail").css({width:w-23});
	$("#"+scope+" input.trick").css({width:w-20});
}

function endresize(){
	if(flag == true){
		flag = false;
		$("#wrapper").height($(window).height() - $("body").offset().top*2);
		
	}
}