$("#formbuktiPengeluaran").ready(function(){
	uniform("buktiPengeluaranAtas tr:not(:nth-last-child(1)) td", $("#buktiPengeluaranAtas td[colspan=3]").width()-6);	
	
	$("#buktiPengeluaran_tanggal").focusout(function(){
		var objectd = $("#buktiPengeluaran_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
		else $("li.buktipengeluaran").click();
	});

	$("#buktiPengeluaran_bulan").focusout(function(){
		var m = $("#buktiPengeluaran_bulan").val();
		if(m > 12 || m < 1) {
			$("#buktiPengeluaran_bulan").val("");
			m = null;
		}
		if(m == null) $("#buktiPengeluaran_tanggal").attr("max", 31);
		else if(m == 2) $("#buktiPengeluaran_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#buktiPengeluaran_tanggal").attr("max", 31);
			else $("#buktiPengeluaran_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#buktiPengeluaran_tanggal").attr("max", 31);
			else $("#buktiPengeluaran_tanggal").attr("max", 30);
		}
		$("#buktiPengeluaran_tahun").triggerHandler("focusout");
		$("#buktiPengeluaran_tanggal").triggerHandler("focusout");
	});

	$("#buktiPengeluaran_tahun").focusout(function(){
		var m = $("#buktiPengeluaran_bulan").val(); 
		var y = $("#buktiPengeluaran_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#buktiPengeluaran_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#buktiPengeluaran_tanggal").attr("max", 29);
				} else {
					$("#buktiPengeluaran_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#buktiPengeluaran_tanggal").attr("max", 29);
				} else {
					$("#buktiPengeluaran_tanggal").attr("max", 28);
				}
			}
			$("#buktiPengeluaran_tanggal").triggerHandler("focusout");
		}
	});

	$("li.buktipengeluaran").click(function(){
		uniform("buktiPengeluaranAtas tr:not(:nth-last-child(1)) td", $("#buktiPengeluaranAtas td[colspan=3]").width()-7);	
		$.ajax({
	        type	: 'POST',
	        url		: 'refreshTableFunction.php',
	        data 	: {
	        	'type' : 'isiDetailDebet',
	        	'batas': $("#buktiPengeluaran_tahun").val()+"-"+$("#buktiPengeluaran_bulan").val()+"-"+$("#buktiPengeluaran_tanggal").val()
	        },
	        success: function(result) {
	        	$("#detailDebetBody").html(result);
	        	$("#totalSementaraDetailPengeluaran").html($("#totalSementaraDetailPengeluaranHidden").html());
	        }
        });
	});

	$("#detailDebetBody").on("click", ".delRow", function(){
		var num = $(this).parent().parent().nextAll().length;
		var minus = $(this).parent().parent().children(".numerik").html();
		var rowEl = $(this).parent().parent().next();
		for (var i = 0; i < num; i++) {
			var el = rowEl.children(".calonKodeTransaksi");
			el.html(parseInt(el.html())-1);
			rowEl = rowEl.next();
		}; 
		$(this).parent().parent().remove();
		var count = $("#lastCounterDetailPengeluaran").html();
		$("#lastCounterDetailPengeluaran").html(parseInt(count)-1);
		var total = $("#totalSementaraDetailPengeluaran").html();
		total = total.replace(/\./g, "");
		minus = minus.replace(/\./g, "");

		var numberFormat = new Intl.NumberFormat('es-ES');

		var tmp = parseInt(total)-parseInt(minus);

		$("#totalSementaraDetailPengeluaran").html(numberFormat.format(tmp));
	});

	$("#formBuktiPengeluaran").submit(function(){
		var kodebukti = $("#kodeBuktiPengeluaran").val();
		var kredit = $("select.selectKreditBuktiPengeluaran").val().replace("_dot_", ".");
		var tanggalbuktipengeluaran = $("#buktiPengeluaran_tahun").val()+"-"+$("#buktiPengeluaran_bulan").val()+"-"+$("#buktiPengeluaran_tanggal").val();
		var bendahara = $("select.selectBendaharaBuktiPengeluaran").val().replace("_dot_", ".");
		var penerima = $("select.selectPenerimaBuktiPengeluaran").val().replace("_dot_", ".");
		var penyetor = $("select.selectPenyetorBuktiPengeluaran").val().replace("_dot_", ".");
		var jumlah = $("#totalSementaraDetailPengeluaran").html();
		var keterangan = $("#keteranganBuktiPengeluaran").val();
		var detail = [];	
		var i = 0;
		$("#detailDebetBody tr:not([hidden])").each(function(){
			if($(this).children().html() == "<i>tidak ada nota baru</i>"){
				return false;
			}
			detail[i] = [];
			detail[i][0] = $(this).children(":nth-child(1)").html();
			detail[i][1] = $(this).children(":nth-child(2)").html();
			detail[i][2] = $(this).children(":nth-child(3)").children().val();
			detail[i][3] = $(this).children(":nth-child(5)").html();
			detail[i][4] = $(this).children(":nth-child(7)").html();
			i++;
		});
		if(i == 0){
			alert("tidak ada pengeluaran!");
			return false;
		}
		var detailJSON = JSON.stringify(detail);
		// alert(kodebukti+"\n"+tanggalbuktipengeluaran+"\n"+kredit+"\n"+bendahara+"\n"+penerima+"\n"+penyetor+"\n"+jumlah+"\n"+keterangan);

		$.ajax({
			url: "buktipengeluaran_proses.php",
			type: "POST",
			data: {
				"kodebukti" : kodebukti,
				"kredit" : kredit,
				"tanggalbuktipengeluaran" : tanggalbuktipengeluaran,
				"bendahara" : bendahara,
				"penerima" : penerima,
				"penyetor" : penyetor,
				"keterangan" : keterangan,
				"jumlah" : jumlah,
				"detail" : detailJSON
			},
			success: function(result){
				// alert(result);
				if(result == true){
					alert("berhasil menyimpan!");
					$("#right>div.buktipengeluaran input[type=reset]").click();
					placeholderselect();
					$("li.buktipengeluaran").click();
					
				} else {
					alert("gagal menyimpan!");
				}
			}
		});
		return false;
	});

	$("#right>div.buktipengeluaran select").focusin(function(){
		refreshSelectBuktiPengeluaran($(this));
	});

	$("#right>div.buktipengeluaran .trick").focusin(function(){
		var i = $(this).attr("id");
		refreshSelectBuktiPengeluaran($("select."+i));
	});
});

function refreshSelectBuktiPengeluaran(obj){
	var value = obj.val();
	var type;
	var c = obj.attr('class');
	if(c == 'selectKreditBuktiPengeluaran'){
		c = "rekeningmut";
	} else if(c == 'selectBendaharaBuktiPengeluaran'){
		c = "pj_bendahara";
	} else if(c == 'selectPenerimaBuktiPengeluaran'){
		c = "pj_penerima";
	} else if(c == 'selectPenyetorBuktiPengeluaran'){
		c = "pj_penyetor";
	}
	$.ajax({
        type	: 'POST',
        url		: 'refreshSelectFunction.php',
        data 	: {
        	'type' : c
        },
        success: function(result) {
        	obj.html(result);
        	if(obj.children("[value="+value+"]").length > 0){
        		obj.val(value);
        	} else placeholderselect();
        }
    });	
}