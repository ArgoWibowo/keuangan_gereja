$("#dataBukti").ready(function(){
	$("#isiBuktiTransaksi").on("change", ".optionDataBukti", function(){
		if($(this).val() == "post"){
			$(this).val("");
			if(confirm("Apakah anda yakin ingin post bukti transaksi ini?")){
				var p = $(this).parent().parent();
				$.ajax({
					url: "postbukti_proses.php",
					type: "POST",
					data: {
						"kodebukti" : p.children(":nth-child(2)").html(),
					},
					success: function(result){
						// alert(result);
						if(result == true){
							alert("berhasil posting!");
							$("li.databuktitransaksi").click();
						} else {
							alert("gagal posting!");
						}
					}
				});
			}
		} else if($(this).val() == "hapus"){
			$(this).val("");
			if(confirm("Apakah anda yakin ingin menghapus bukti transaksi ini?")){
				var p = $(this).parent().parent();
				$.ajax({
					url: "hapusbukti_proses.php",
					type: "POST",
					data: {
						"kodebukti" : p.children(":nth-child(2)").html(),
					},
					success: function(result){
						// alert(result);
						if(result == true){
							alert("berhasil menghapus!");
							$("li.databuktitransaksi").click();
						} else {
							alert("gagal menghapus!");
						}
					}
				});
			}
		}
		$(this).val("");
	});

	$("li.databuktitransaksi").click(function(){
		$.ajax({
	        type	: 'POST',
	        url		: 'refreshTableFunction.php',
	        data 	: {
	        	'type' : 'isiBuktiTransaksi'
	        },
	        success: function(result) {
	        	// alert(result);
	        	$("#isiBuktiTransaksi").html(result);
	        }
        });
	});
});

function searchDataBuktiTransaksi(){
	var keyword = $('#searchDataBuktiTransaksi').val();
	var sortby  = 'tanggal_bukti_transaksi';
  	$.ajax({
        type	: 'POST',
        url		: 'ajax_databuktitransaksi.php',
        data 	: 'actionfunction=showData&page=1&keyword='+keyword+'&sortby='+sortby,
        cache 	: false,
        success: function(result) {
        	var a = result.indexOf("<");
        	result = result.substring(a);
			$('#isiBuktiTransaksi').html(result);
			$.ajax({
			    url:"ajax_databuktitransaksi.php",
		        type:"POST",
		        data:"actionfunction=pagination&page=1&keyword="+keyword+'&sortby='+sortby,
		        cache: false,
		        success: function(response){
				}
	   		});
        },
    });
}