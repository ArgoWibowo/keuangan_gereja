var gantiPassNyaEditAkun;

$(document).ready(function() {

	uniform("formEditGereja",170);
	uniform("formEditAkunProfil",170);

	
	$('#passBaru').on('input', function(){
		if($(this).val().length	>=8){
				if($(this).val() == $('#passBaru2').val() ){
						$("#warningPassEditAkun1").attr("src","image/check.png");
						$("#warningPassEditAkun2").attr("src","image/check.png");
						gantiPassNyaEditAkun = true;
						document.getElementById("warningPassEditAkun1").style.visibility = "visible";
						document.getElementById("warningPassEditAkun2").style.visibility = "visible";
				}else{
						$("#warningPassEditAkun1").attr("src","image/check.png");
						$("#warningPassEditAkun2").attr("src","image/warning.png");
						gantiPassNyaEditAkun = false;
						document.getElementById("warningPassEditAkun1").style.visibility = "visible";
						document.getElementById("warningPassEditAkun2").style.visibility = "visible";
				}
		}else{
				$("#warningPassEditAkun1").attr("src","image/warning.png");
				$("#warningPassEditAkun2").attr("src","image/warning.png");
				gantiPassNyaEditAkun = false;
				document.getElementById("warningPassEditAkun1").style.visibility = "visible";
				document.getElementById("warningPassEditAkun2").style.visibility = "visible";
		}
	});

	$('#passBaru2').on('input', function() {
		if($('#passBaru').val().length	>=8){
				if($(this).val() == $('#passBaru').val() ){
						$("#warningPassEditAkun1").attr("src","image/check.png");
						$("#warningPassEditAkun2").attr("src","image/check.png");
						gantiPassNyaEditAkun = true;
						document.getElementById("warningPassEditAkun1").style.visibility = "visible";
						document.getElementById("warningPassEditAkun2").style.visibility = "visible";
				}else{
						$("#warningPassEditAkun1").attr("src","image/check.png");
						$("#warningPassEditAkun2").attr("src","image/warning.png");
						gantiPassNyaEditAkun = false;
						document.getElementById("warningPassEditAkun1").style.visibility = "visible";
						document.getElementById("warningPassEditAkun2").style.visibility = "visible";
				}
		}else{
				$("#warningPassEditAkun1").attr("src","image/warning.png");
				$("#warningPassEditAkun2").attr("src","image/warning.png");
				gantiPassNyaEditAkun = false;
				document.getElementById("warningPassEditAkun1").style.visibility = "visible";
				document.getElementById("warningPassEditAkun2").style.visibility = "visible";
		}
	});
	
	$(document).on('click','#mask, .btn_close',function(){
        closeEditProfileGereja();
        closeEditAkun();
        closeEditAkunAdmin();
        closeEditPassAkunAdmin();
    });

	
});

 $("form#formEditAkunProfil").submit(function(){
	    var sessionAkunEdit;
	    $.ajax({
				type: "POST",
				url: "loadSessionUser.php",
				success: function(data){
					if(data){
						sessionAkunEdit = data;
						var akunpertanyaanOptions = document.getElementById("akunselect_pertanyaan_pemulihan");
						var akunpertanyaan = akunpertanyaanOptions.options[akunpertanyaanOptions.selectedIndex].value;
						
						var akunnama = $("#akunnamaAdmin").val();
						var akunjawaban= $("#akunjawabanPertanyaan").val();
						var akunalamat= $("#akunalamatAdmin").val();
						var akunnoTelp= $("#akunteleponAdmin").val();
						var akunemail= $("#akunemailBaru").val();

						$.ajax({
									type: "POST",
									url: "EditAkunAdminNya.php",
									data: {"akunnama": akunnama,
											"akunpertanyaan": akunpertanyaan,
											"akunjawaban": akunjawaban,
											"akunalamat": akunalamat,
											"akunnoTelp": akunnoTelp,
											"akunemail": akunemail,
											"sessionAkunEdit": sessionAkunEdit},
									async: false,
									success: function(data){
										if(data){
											$('#sessionNamaAkunAdmin a').text(data);
											closeEditAkunAdmin();
											loadTableAdmin();
									}
								}
						});
					}
					
				}
		});
	   	return false;
	});

    $("form#formEditGereja").submit(function(){
		var nama_gereja = $("#namaGereja").val();
		var alamat_gereja= $("#alamatGereja").val();
		var telepon_gereja= $("#teleponGereja").val();
		var email_gereja= $("#emailGereja").val();
		var logo_gereja= document.getElementById("inputLogoGereja").files[0];
		var tahun_pembukuan_gereja= $("#tahunPembukuanProfilGereja").val();
	    
	    var formData = new FormData();
		formData.append('foto', logo_gereja);
		formData.append('nama_gereja', nama_gereja);
		formData.append('alamat_gereja', alamat_gereja);
		formData.append('telepon_gereja', telepon_gereja);
		formData.append('email_gereja', email_gereja);
		formData.append('tahun_pembukuan_gereja', tahun_pembukuan_gereja);
		
		// alert('saya');
	     	$.ajax({
					type: "POST",
					url: "editProfilGereja2.php",
					data: formData,
					dataType: "json",
					contentType: false,
	    			processData: false,
					success: function(data){
						if(data){
							alert('Profil Gereja Telah diganti');
							editTanggalLaporanRencana();
							$('#gereja_nama label').html(data[0][0]);
							$('#gereja_alamat label').html(data[0][1]);
							$('#gereja_alamatNoTelpEmail label').html(data[0][2]+" | "+ data[0][3]);
							var d = new Date();
							if(data[0][4]) $('#imgGambarHeaderGereja').attr("src", "image/"+data[0][4]+"?"+d.getTime());
						}
						
					}
			});
			closeEditProfileGereja();
    		return false;
	});

$("form#formGantiPassAkunAdmin").submit(function(){
	    $.ajax({
				type: "POST",
				url: "loadSessionUser.php",
				success: function(data){
					if(data){
						editPasswordAkunNya(data);
					}
					
				}
		});
		return false;
    });

	$("form#formGantiPasswordAkunCek").submit(function(){
    	var sessionAkunEditPass;
	    $.ajax({
				type: "POST",
				url: "loadSessionUser.php",
				success: function(data){
					if(data){
						CheckeditPasswordAkunNya(data);
					}
					
				}
		});
		return false;
    });


function closeEditProfileGereja(){
	$("#inputLogoGereja").val("");  
	$('#mask').remove();  
    $('.editProfilGereja').fadeOut(200); 
    $('.editProfilGereja').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });
}

function closeEditAkun(){
	$('#mask').remove();  
    $('.editPengaturanAkun').fadeOut(200); 
    $('.editPengaturanAkun').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });
}

function closeEditAkunAdmin(){
	$('#mask').remove();  
    $('.pengaturanAkunEdit').fadeOut(200); 
    $('.pengaturanAkunEdit').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });
}

function closeEditPassAkunAdmin(){
	$('#mask').remove();  
    $('.editPasswordAkun').fadeOut(200); 
    $('.editPasswordAkun').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });
}



function CheckeditPasswordAkunNya(ses){
	var sessionPasswordEdit = ses;
	var passwordLama=$("#passLama").val();
	$.ajax({
		type: "POST",
		url: "checkPassword.php",
		data: {"sessionPasswordEdit": sessionPasswordEdit, "passwordLama": passwordLama},
		success: function(data){
			if(data){
				
				if(data == "salah"){
					$("#errPassAkun").html("<label style='color:orange;text_decoration: nine;font-size: 13px;'>Error: password salah!</label>");
				}else if(data == "benar"){
					$("#errPassAkun label").remove();
					$("#errPassAkun").html("<label>&nbsp;</label>");
					document.getElementById("passBaru").disabled = false;
					document.getElementById("passBaru2").disabled = false;
					document.getElementById("buttEditPassAkun").disabled = false;
					document.getElementById("passLama").disabled = true;
					document.getElementById("buttNextPass").disabled = true;
					document.getElementById("passBaru").focus();
					checkConfirmasiPassEditAkun();
				}
				
			}
			
		}
	});
}


function kembaliEditProfil(div){
	window.location.href= div;
	var loginBox = div;
	var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    
	closeEditProfileGereja();
    //  Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
    $(".editPengaturanAkun").fadeIn(500);
    $(".editPengaturanAkun").css({ 
        'visibility' :'visible',
        'display' :'block',
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
	
}


function kembaliEditAkunAdmin(div){
	window.location.href= div;
	var loginBox = div;
	var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    
    closeEditAkunAdmin();
    //  Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
    $(".editPengaturanAkun").fadeIn(500);
    $(".editPengaturanAkun").css({ 
        'visibility' :'visible',
        'display' :'block',
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });	
		
}

function kembaliEditPassAkunAdmin(div){
	window.location.href= div;
	var loginBox = div;
	var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    
    closeEditPassAkunAdmin();
    //  Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
    $(".editPengaturanAkun").fadeIn(500);
    $(".editPengaturanAkun").css({ 
        'visibility' :'visible',
        'display' :'block',
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
	
}

function editInformasiGereja(div){
	window.location.href= div;
	$('.editPengaturanAkun').fadeOut(200); 
    $('.editPengaturanAkun').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });
	var loginBox = div;

			$.ajax({
				type: "POST",
				url: "loadEditProfilGereja.php",
				dataType: "json",
				success: function(data){
					if(data){
							document.getElementById("namaGereja").value = data[0][0];
							document.getElementById("alamatGereja").value = data[0][1];
							document.getElementById("teleponGereja").value = data[0][2];
							document.getElementById("emailGereja").value = data[0][3];
							var d = new Date();
							$('#logoGereja').attr("src", "image/"+data[0][4]+"?"+d.getTime());
							document.getElementById("tahunPembukuanProfilGereja").value = data[0][5];
							// alert(data);
					}

					var popMargTop = ($(loginBox).height() + 24) / 2; 
				    var popMargLeft = ($(loginBox).width() + 24) / 2; 
				    //  Add the mask to body
				    $('body').append('<div id="mask"></div>');
				    $('#mask').fadeIn(300);
				    $(".editProfilGereja").fadeIn(500);
				    $(".editProfilGereja").css({ 
				        'visibility' :'visible',
				        'display' :'block',
				        'margin-top' : -popMargTop,
				        'margin-left' : -popMargLeft
				    });
					
				}
			});	
	
}

function pengaturanInformasiAkun(div){
	window.location.href= div;
	var loginBox = div;
	var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    //  Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
    $(".editPengaturanAkun").fadeIn(500);
    $(".editPengaturanAkun").css({ 
        'visibility' :'visible',
        'display' :'block',
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });	
}

function loadImageFileAsURL()
{
    var filesSelected = document.getElementById("inputFileToLoad").files;
    if (filesSelected.length > 0)
    {
        var fileToLoad = filesSelected[0];

        if (fileToLoad.type.match("image.*"))
        {
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent) 
            {
                var imageLoaded = document.createElement("img");
                imageLoaded.src = fileLoadedEvent.target.result;
                document.body.appendChild(imageLoaded);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }
}

function editProfilnya(){
	var nama_gereja = $("#namaGereja").val();
	var alamat_gereja= $("#alamatGereja").val();
	var telepon_gereja= $("#teleponGereja").val();
	var email_gereja= $("#emailGereja").val();
	var logo_gereja= document.getElementById("inputLogoGereja").files[0];
    
    var formData = new FormData();
	formData.append('foto', logo_gereja);
	formData.append('nama_gereja', nama_gereja);
	formData.append('alamat_gereja', alamat_gereja);
	formData.append('telepon_gereja', telepon_gereja);
	formData.append('email_gereja', email_gereja);
	// alert('saya');
     	$.ajax({
				type: "POST",
				url: "editProfilGereja2.php",
				data: formData,
				dataType: "json",
				contentType: false,
    			processData: false,
				success: function(data){
					if(data){
						// alert(data);
						$('#gereja_nama label').html(data[0][0]);
						$('#gereja_alamat label').html(data[0][1]);
						$('#gereja_alamatNoTelpEmail label').html(data[0][2]+" | "+ data[0][3]);
						var d = new Date();
						if(data[0][4]) $('#imgGambarHeaderGereja').attr("src", "image/"+data[0][4]+"?"+d.getTime());
					}
					
				}
		});
		closeEditProfileGereja();   

}

function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#logoGereja')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
 }

function clearFileInput(id) 
{ 
    var oldInput = document.getElementById(id); 

    var newInput = document.createElement("input"); 

    newInput.type = "file"; 
    newInput.id = oldInput.id; 
    newInput.name = oldInput.name; 
    newInput.className = oldInput.className; 
    newInput.style.cssText = oldInput.style.cssText; 

    oldInput.parentNode.replaceChild(newInput, oldInput); 
}

function editInformasiAkunAdmin(div,ses){

	window.location.href= div;

	// alert("bisa");
	
	$('.editPengaturanAkun').fadeOut(200); 
    $('.editPengaturanAkun').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });

	var loginBox = div;
	var sessionAkun = ses;
		$.ajax({
				type: "POST",
				url: "loadEditAkunAdmin.php",
				data: {"sessionAkun": sessionAkun},
				dataType: "json",
				success: function(data){
					if(data){

						document.getElementById("akunnamaAdmin").value = data[0][0];
						document.getElementById("akunselect_pertanyaan_pemulihan").value = data[0][1];
						document.getElementById("akunjawabanPertanyaan").value = data[0][2];
						document.getElementById("akunalamatAdmin").value = data[0][3];
						document.getElementById("akunteleponAdmin").value = data[0][4];
						document.getElementById("akunemailBaru").value = data[0][5];
						var popMargTop = ($(loginBox).height() + 24) / 2; 
					    var popMargLeft = ($(loginBox).width() + 24) / 2; 
					    //  Add the mask to body
					    $('body').append('<div id="mask"></div>');
					    $('#mask').fadeIn(300);
					    $(".pengaturanAkunEdit").fadeIn(500);
					    $(".pengaturanAkunEdit").css({ 
					        'visibility' :'visible',
					        'display' :'block',
					        'margin-top' : -popMargTop,
					        'margin-left' : -popMargLeft
					    });
				}
					
				}
		});

		

}



function editAkunAdminnya(ses){
	
	var akunpertanyaanOptions = document.getElementById("akunselect_pertanyaan_pemulihan");
	var akunpertanyaan = akunpertanyaanOptions.options[akunpertanyaanOptions.selectedIndex].value;

	var sessionAkunEdit = ses;
	
	var akunnama = $("#akunnamaAdmin").val();
	var akunjawaban= $("#akunjawabanPertanyaan").val();
	var akunalamat= $("#akunalamatAdmin").val();
	var akunnoTelp= $("#akunteleponAdmin").val();
	var akunemail= $("#akunemailBaru").val();

	$.ajax({
				type: "POST",
				url: "EditAkunAdminNya.php",
				data: {"akunnama": akunnama,
						"akunpertanyaan": akunpertanyaan,
						"akunjawaban": akunjawaban,
						"akunalamat": akunalamat,
						"akunnoTelp": akunnoTelp,
						"akunemail": akunemail,
						"sessionAkunEdit": sessionAkunEdit},
				success: function(data){
					if(data){
						$('#sessionNamaAkunAdmin a').text(data);
						closeEditAkunAdmin();
						loadTableAdmin();
				}
			}
		});
}

function editPassAkun(div,ses){
	window.location.href= div;
	$('.editPengaturanAkun').fadeOut(200); 
    $('.editPengaturanAkun').css({ 
        'visibility' :'hidden',
        'display' :'none'
    });

    var loginBox = div;
	var sessionAkun = ses;
	document.getElementById("passBaru").value = "";
	document.getElementById("passBaru2").value = "";
	document.getElementById("passLama").value = "";
	$("#errPassAkun").html("<label>&nbsp;</label>");

	document.getElementById("passBaru").disabled = true;
	document.getElementById("passBaru2").disabled = true;
	document.getElementById("buttEditPassAkun").disabled = true;
	document.getElementById("passLama").disabled = false;
	document.getElementById("buttNextPass").disabled = false;
	document.getElementById("warningPassEditAkun1").style.visibility = "hidden";
	document.getElementById("warningPassEditAkun2").style.visibility = "hidden";
	document.getElementById("passLama").focus();
	
	var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    //  Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
    $(".editPasswordAkun").fadeIn(500);
    $(".editPasswordAkun").css({ 
        'visibility' :'visible',
        'display' :'block',
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });


}

function editPasswordAkunNya(ses){
	if (gantiPassNyaEditAkun == true){	
		var userYangGantiPass = ses;
		var passwordBaru=$("#passBaru").val();
		$.ajax({
			type: "POST",
			url: "gantiPasswordAkun.php",
			data: {"userYangGantiPass": userYangGantiPass, "passwordBaru": passwordBaru},
			success: function(data){
				if(data){
					if(data == 1){
						alert("Password Telah Diganti");
						closeEditPassAkunAdmin();
					}else{
						alert("Password Tidak Diganti");
						closeEditPassAkunAdmin();
					}
				}
				
			}
		});
	}else{
		document.getElementById("passwordBaru").focus();
	}

}


function checkConfirmasiPassEditAkun(){
	if($('#passBaru').val().length	>=8){
				if($('#passBaru').val() == $('#epassBaru2').val() ){
						$("#warningPassEditAkun1").attr("src","image/check.png");
						$("#warningPassEditAkun2").attr("src","image/check.png");
						gantiPassNyaEditAkun = true;
						document.getElementById("warningPassEditAkun1").style.visibility = "visible";
						document.getElementById("warningPassEditAkun2").style.visibility = "visible";
				}else{
						$("#warningPassEditAkun1").attr("src","image/check.png");
						$("#warningPassEditAkun2").attr("src","image/warning.png");
						gantiPassNyaEditAkun = false;
						document.getElementById("warningPassEditAkun1").style.visibility = "visible";
						document.getElementById("warningPassEditAkun2").style.visibility = "visible";
				}
		}else{
				$("#warningPassEditAkun1").attr("src","image/warning.png");
				$("#warningPassEditAkun2").attr("src","image/warning.png");
				gantiPassNyaEditAkun = false;
				document.getElementById("warningPassEditAkun1").style.visibility = "visible";
				document.getElementById("warningPassEditAkun2").style.visibility = "visible";
		}
}