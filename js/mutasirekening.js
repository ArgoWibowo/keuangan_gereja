$("#formMutasiRekening").ready(function(){
	uniform("mutasiRekeningAtas tr:not(:nth-last-child(1)) td", $("#mutasiRekeningAtas td[colspan=3]").width()-6);		
	uniform("boxUraianMutasiTambahan", $("#boxUraianMutasiTambahan input").width());
	uniform("boxJumlahMutasiTambahan", $("#boxJumlahMutasiTambahan input").width());

	$("#mutasiRekening_tanggal").focusout(function(){
		var objectd = $("#mutasiRekening_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#mutasiRekening_bulan").focusout(function(){
		var m = $("#mutasiRekening_bulan").val();
		if(m > 12 || m < 1) {
			$("#mutasiRekening_bulan").val("");
			m = null;
		}
		if(m == null) $("#mutasiRekening_tanggal").attr("max", 31);
		else if(m == 2) $("#mutasiRekening_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#mutasiRekening_tanggal").attr("max", 31);
			else $("#mutasiRekening_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#mutasiRekening_tanggal").attr("max", 31);
			else $("#mutasiRekening_tanggal").attr("max", 30);
		}
		$("#mutasiRekening_tahun").triggerHandler("focusout");
		$("#mutasiRekening_tanggal").triggerHandler("focusout");
	});

	$("#mutasiRekening_tahun").focusout(function(){
		var m = $("#mutasiRekening_bulan").val(); 
		var y = $("#mutasiRekening_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#mutasiRekening_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#mutasiRekening_tanggal").attr("max", 29);
				} else {
					$("#mutasiRekening_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#mutasiRekening_tanggal").attr("max", 29);
				} else {
					$("#mutasiRekening_tanggal").attr("max", 28);
				}
			}
			$("#mutasiRekening_tanggal").triggerHandler("focusout");
		}
	});

	$("li.mutasirekening").click(function(){
		uniform("mutasiRekeningAtas tr:not(:nth-last-child(1)) td", $("#mutasiRekeningAtas td[colspan=3]").width()-7);	
		uniform("boxUraianMutasiTambahan", $("#boxUraianMutasiTambahan").width()-7);
		uniform("boxJumlahMutasiTambahan", $("#boxJumlahMutasiTambahan").width()-7);
		$.ajax({
	        type	: 'POST',
	        url		: 'refreshTableFunction.php',
	        data 	: {
	        	'type' : 'isiDetailMutasi'
	        },
	        success: function(result) {
	        	// alert(result);
	        	var n = $("#detailMutasiBody tr").length;
	        	var oldhtml = $("#detailMutasiBody").html();

	        	$("#detailMutasiBody").html(result);
	        	var no = parseInt($("#lastCounterDetailMutasi").html());
	        	
	        	var sstart = oldhtml.indexOf("<tr hidden");
	        	var search = oldhtml.substring(sstart);
	        	var sstop  = search.indexOf("</tr>");
	        	search  = search.substring(0, sstop+5);
	        	oldhtml = oldhtml.replace(search, "");
	        	for (var i = 1; i < n; i++) {
	        		sstart = oldhtml.indexOf("<td id=");
	        		search = oldhtml.substring(sstart+8);
	        		sstop  = search.indexOf("\">");
	        		search = search.substring(0, sstop);
	        		
	        		sstart = oldhtml.indexOf("</td>");
	        		sstop  = oldhtml.indexOf("</tr>");
        			result += '<tr><td class="calonKodeTransaksi">' + no + '</td>';
        			result += oldhtml.substring(sstart+5, sstop+5);
        			no 	    = no + 1; 
					oldhtml = oldhtml.substring(sstop+5);
	    		}
	        	$("#detailMutasiBody").html(result);
	        	$("#lastCounterDetailMutasi").html(no);
	        }
        });
	});

	$("#tambahMutasi").submit(function(){
		var debet = $("select.selectDebetMutasiTambahan").val();
		var plus = parseInt($("#mutasiTambahan_jumlah").val().replace(/\./g, ""));
		
		var full = $("#detailMutasiBody").html();
		var no = $("#lastCounterDetailMutasi").html();
		$("#detailMutasiBody").html(
			full +
			'<tr>' +
	    		'<td class="calonKodeTransaksi">' + no + '</td>' +
	    		'<td id="' + debet + '">' + debet.replace("_dot_", ".") + '</td>' +
	    		'<td><input class="uraianMutasi" value="' + $("#uraianMutasiTambahan").val() + '"></td>' +
		    	'<td class="rpCol">Rp.</td>' +
	    		'<td class="numerik">' + plus + '</td>' +
	    		'<td><label class="delRow">x</label></td>' +
	    	'</tr>'
	    );
		$("#lastCounterDetailMutasi").html(parseInt(no)+1);
		
		var total = $("#totalSementaraDetailMutasi").html();
		$("#totalSementaraDetailMutasi").html(parseInt(total)+plus);
		$(this).closest('form').get(0).reset();
		placeholderselect();
    	return false;
	});

	$("#detailMutasiBody").on("click", ".delRow", function(){
		var num = $(this).parent().parent().nextAll().length;
		var minus = $(this).parent().parent().children(".numerik").html();
		var rowEl = $(this).parent().parent().next();
		for (var i = 0; i < num; i++) {
			var el = rowEl.children(".calonKodeTransaksi");
			el.html(parseInt(el.html())-1);
			rowEl = rowEl.next();
		}; 
		$(this).parent().parent().remove();
		var count = $("#lastCounterDetailMutasi").html();
		$("#lastCounterDetailMutasi").html(parseInt(count)-1);
		var total = $("#totalSementaraDetailMutasi").html();
		$("#totalSementaraDetailMutasi").html(parseInt(total)-minus);
	});

	$("#formMutasiRekening").submit(function(){
		var kodebukti = $("#kodeMutasiRekening").val();
		var kredit = $("select.selectKreditMutasiRekening").val().replace("_dot_", ".");
		var tanggalMutasiRekening = $("#mutasiRekening_tahun").val()+"-"+$("#mutasiRekening_bulan").val()+"-"+$("#mutasiRekening_tanggal").val();
		var bendahara = $("select.selectBendaharaMutasiRekening").val().replace("_dot_", ".");
		var penerima = $("select.selectPenerimaMutasiRekening").val().replace("_dot_", ".");
		var penyetor = $("select.selectPenyetorMutasiRekening").val().replace("_dot_", ".");
		var jumlah = $("#totalSementaraDetailMutasi").html();
		var keterangan = $("#keteranganMutasiRekening").val();
		var detail = [];	
		var i = 0;
		$("#detailMutasiBody tr:not([hidden])").each(function(){
			detail[i] = [];
			detail[i][0] = $(this).children(":nth-child(1)").html();
			detail[i][1] = $(this).children(":nth-child(2)").html();
			detail[i][2] = $(this).children(":nth-child(3)").children().val();
			detail[i][3] = $(this).children(":nth-child(5)").html();
			i++;
		});
		// alert(kodebukti+"\n"+tanggalMutasiRekening+"\n"+kredit+"\n"+bendahara+"\n"+penerima+"\n"+penyetor+"\n"+jumlah+"\n"+keterangan);
		if(i == 0){
			alert("tidak ada mutasi!");
			return false;
		}
		var detailJSON = JSON.stringify(detail);
		
		$.ajax({
			url: "mutasirekening_proses.php",
			type: "POST",
			data: {
				"kodebukti" : kodebukti,
				"kredit" : kredit,
				"tanggalMutasiRekening" : tanggalMutasiRekening,
				"bendahara" : bendahara,
				"penerima" : penerima,
				"penyetor" : penyetor,
				"keterangan" : keterangan,
				"jumlah" : jumlah,
				"detail" : detailJSON
			},
			success: function(result){
				//alert(result);
				if(result == true){
					alert("berhasil menyimpan!");
					$("#right>div.mutasirekening input[type=reset]").click();
					placeholderselect();
					$("#detailMutasiBody").html("");
					$("li.mutasirekening").click();
					$("#totalSementaraDetailMutasi").html(parseInt(0));
				} else {
					alert("gagal menyimpan!");
				}
			}
		});
		return false;
	});

	$("#right>div.mutasirekening select").focusin(function(){
		refreshSelectMutasiRekening($(this));
	});

	$("#right>div.mutasirekening .trick").focusin(function(){
		var i = $(this).attr("id");
		refreshSelectMutasiRekening($("select."+i));
	});
});


function refreshSelectMutasiRekening(obj){
	var value = obj.val();
	var type;
	var c = obj.attr('class');
	if(c == 'selectKreditMutasiRekening') c = "rekeningmut"; //tambahan
	else if(c == 'selectDebetMutasiTambahan') c = "rekeningmut";
	else if(c == 'selectBendaharaMutasiRekening') c = "pj_bendahara";
	else if(c == 'selectPenerimaMutasiRekening') c = "pj_penerima";
	else if(c == 'selectPenyetorMutasiRekening') c = "pj_penyetor";
	$.ajax({
        type	: 'POST',
        url		: 'refreshSelectFunction.php',
        data 	: {
        	'type' : c
        },
        success: function(result) {
        	obj.html(result);
        	if(obj.children("[value="+value+"]").length > 0){
        		obj.val(value);
        	} else placeholderselect();
        }
    });	
}