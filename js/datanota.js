$(document).ready(function() {
    // Setup - add a text input to each footer cell
   

    uniform("formDetailNota",180);

    $(document).on('click','#mask, .btn_close, .close',function(){
        closeDetailNota();
    });

});

function closeDetailNota(){
    reloadNota();
    $('#mask').remove();  
    $('#detailNota').fadeOut(200); 
    $('#detailNota').css({ 
        'visibility' :'hidden',
        'display' :'none'
    }); 
}

 reloadNota();
 function reloadNota(){

   $.ajax({
        type	: 'POST',
        url		: 'ajax_datanota.php',
        data 	: 'actionfunction=showData&page=1',
        cache 	: false,
        success: function(result) {
        	var a = result.indexOf("<");
        	var rows = result.substring(0,a);
        	$('#lblJumlahDataNota').text(rows);
        	result = result.substring(a);
			$('#isiTabelNota').html(result);
			$('#jumlahData').text($('#jumlahDataBidang').val());
			$.ajax({
			    url:"ajax_datanota.php",
		        type:"POST",	
		        data:"actionfunction=pagination&page=1",
		        cache: false,
		        success: function(response){
					$('#paginationNota').html(response);


				}
	   		});
        },
    });
   // alert("bisa");
    $('#paginationNota').on('click','.page-numbers',function(){
       $page = $(this).attr('href');
	   $pageind = $page.indexOf('page=');
	   $page = $page.substring(($pageind+5));
       
	   $.ajax({
		    url:"ajax_datanota.php",
	        type:"POST",
	        data:"actionfunction=showData&page="+$page,
	        cache: false,
	        success: function(response1){
			   $.ajax({
			    url:"ajax_datanota.php",
		        type:"POST",
		        data:"actionfunction=pagination&page="+$page,
		        cache: false,
		        success: function(response){
		        	$('#isiTabelNota').html(response1);
					$('#paginationNota').html(response);
				}
	   		});
			 
			}
	   });
	return false;
	});
}  


function dipilih(e){
   $(e).addClass('selected').siblings().removeClass('selected');  
}

function pilihanDataNota(index, toko_kode,nota_kode){
	if (index == "1"){
		//$('#formEditBidang').html(result);
//        alert(toko_kode + " " + nota_kode);
        $.ajax({
        type	: 'POST',
        url		: 'ajax_editnota.php',
        data 	: {'kode':nota_kode,
                    'kode2':toko_kode
                    },
        dataType: 'json',
        success: function(result) {
                
                $('#editKodeNota').val(result[0]);
                $('#editKodeNota2').val(result[0]);                
                $('.editKodeTokoNota').val(result[1]);
                $('#editKodeTokoNota2').val(result[1]);
                $('.editDebetNota').val(result[3]);
                $('#editTanggalNota').val(result[2]);
                var tanggal = result[2].substr(8,2);
                var bulan = result[2].substr(5,2);
                var tahun = result[2].substr(0,4);
                
                $('#notaEdit_tanggal').val(parseInt(tanggal));
                $('#notaEdit_bulan').val(parseInt(bulan));
                $('#notaEdit_tahun').val(parseInt(tahun));

                $('#editJumlahNota').val(result[4]);                
                $('#editPelaksanaNota').val(result[5]);                                
				$("div.datanota").slideUp("700");
				$("div.editnota").slideDown("700");                
	        },
	    });

	}else{
        var cek = confirm("Apakah yakin akan menghapus nota ini ?");
		if (cek){
            $.ajax({
            type    : 'POST',
            url     : 'ajax_hapusnota.php',
            data    : {'kode':nota_kode},
            success: function(result) {
                    if (result == true){
                        $('pilihanDataNota').val("");
                        alert("Data nota terpilih berhasil dihapus");
                        reloadNota();
                    }
                    
                },
            });
        }else{
            $('pilihanDataNota').val(""); 
            reloadNota();
        }
        
	}
}

$("#btnBatalEditNota").click(function(){
    reloadNota();
    $("div.editnota").slideUp("700");
    $("#right>div.datanota").slideDown("700");

});

$("form#formTambahNota").submit(function(){
  
  var formData2 = new FormData($(this)[0]);
  //alert(formData2);
   $.ajax({
        url: "ajax_tambahnota.php",
        type: 'POST',
        data: formData2,
        success: function (data) {
           /* alert(data);*/
            if ( data == true){
            	alert("Berhasil menambahkan data");
                $('#kodeNota').val("");
				$('.kodeTokoNota').val("");
				$('.debetNota').val("");
				$('#pelaksanaNota').val("");
	            $('#nota_tanggal').val("");
                $('#nota_bulan').val("");
                $('#nota_tahun').val("");            
                $('#jumlahNota').val("");
                
              	$("div.tambahnota").slideUp("700");
				$("#right>div.datanota").slideDown("700");
				$("#right>div.datanota div").slideDown("700");
				reloadNota();
          }
          else{
            alert("gagal");
          }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

$("form#formEditNota").submit(function(){

  var formData4 = new FormData($(this)[0]);

   $.ajax({
        url: "ajax_editnota.php",
        type: 'POST',
        data: formData4,
        success: function (data4) {
       //         alert(data4);
             if (data4 == 1){
                $('#editKodeNota').val("");
                $('.editKodeTokoNota').val("");
                $('.editDebetNota').val("");
                $('#editPelaksanaNota').val("");
                $('#notaEdit_tanggal').val("");
                $('#notaEdit_bulan').val("");
                $('#notaEdit_tahun').val("");
                $('#editJumlahNota').val("");
                
				alert("Data Nota berhasil diubah");
              	$("div.editnota").slideUp("700");
				$("#right>div.datanota").slideDown("700");
				//$("#right>div.databidang div").slideDown("700");
				
            }            
            reloadNota();   
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

function searchNota(){
	var keyword = $('#searchNota').val();
	var sortby  = $('#sortDataNota option:selected').val();
    // 
  	$.ajax({
        type	: 'POST',
        url		: 'ajax_datanota.php',
        data 	: 'actionfunction=showData&page=1&keyword='+keyword+'&sortby='+sortby,
        cache 	: false,
        success: function(result) {
        	var a = result.indexOf("<");
        	var rows = result.substring(0,a);
        	$('#lblJumlahDataNota').text(rows);
        	result = result.substring(a);
			$('#isiTabelNota').html(result);
			$.ajax({
			    url:"ajax_datanota.php",
		        type:"POST",
		        data:"actionfunction=pagination&page=1&keyword="+keyword+'&sortby='+sortby,
		        cache: false,
		        success: function(response){
					$('#paginationNota').html(response);
				}
	   		});
	   		$("#right>div.datanota").slideDown("700");
				$("#right>div.datanota div").slideDown("700");
        },
    });
}

function checkKodeNota(){
      
      var kodeInput = document.getElementById("kodeNota").value;
      $.ajax({
            url: "ajax_tambahnota.php",
            type: "POST",
            data: {
                "kode_nota_check": kodeInput
            },
            success: function(data){
                if(kodeInput != "" && data == true){
					$("#warningToko").attr("src","image/check.png");
                    document.getElementById("warningToko").style.visibility = "visible";
                    //alert("true");
                }
                else {
                    document.getElementById("warningToko").style.visibility = "visible";
                    $("#warningToko").attr("src","image/warning.png");
                    //alert("false");
                }   
            }
        });
        return false;
}


function detailNotaData(div){
    var loginBox = div;

    window.location.href= div;

    var kodeNotaDetail = String($('#datanota table tr.selected td:eq(1)').html()).trim();

    //alert(kodeNotaDetail);
    $.ajax({
                type: "POST",
                url: "loadDetailNota.php",
                dataType: "json",
                data: {"kodeNotaDetail": kodeNotaDetail},
                success: function(data){
                    //alert(data);
                    if(data){
                            
                            //$("#detailNota").visibility(true);
                            document.getElementById("kodeNotaDetail").value = data[0];
                            document.getElementById("kodeTokoNotaDetail").value = data[1];
                            document.getElementById("jenisAkunNotaDetail").value = data[3];
                            document.getElementById("tanggalPengeluaranNotaDetail").value = data[2];
                            document.getElementById("jumlahNotaDetail").value = data[4];
                            document.getElementById("pelaksanaNotaDetail").value = data[5];
                            document.getElementById("waktuInputNotaDetail").value = data[6];
                            document.getElementById("kodeAdminNotaDetail").value = data[7];
                            //document.getElementById("statusNotaDetail").value = data[8];

                            var popMargTop = ($(loginBox).height() + 24) / 2; 
                            var popMargLeft = ($(loginBox).width() + 24) / 2; 
                            //  Add the mask to body
                            $('body').append('<div id="mask"></div>');
                            $('#mask').fadeIn(300);
                            $("#detailNota").fadeIn(500);
                            $("#detailNota").css({ 
                                'visibility' :'visible',
                                'display' :'block',
                                'margin-top' : -popMargTop,
                                'margin-left' : -popMargLeft
                            }); 
                            
                    }
                    
                }
    });
}


function printDataKomisiNota(div)
{
    $.ajax({
            type: "POST",
            url: "loadKop.php",
            success: function(data){
                if(data){
                   var divToPrint=document.getElementById(div);
                   newWin= window.open("");
                   newWin.document.write(data+"<br>"+"<br>"+divToPrint.outerHTML);
                   newWin.print();
                   newWin.close();
                }
            }
    });
   
}

var tableToExcel = (function() {
    var tamp;
    $.ajax({
            type: "POST",
            url: "loadKopTanpaLogo2.php",
            success: function(data){
                if(data){
                    tamp = data;
                }
            }
    });
      var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
      return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: tamp + "<table border=1>" + table.innerHTML+ "</table>"}
        window.location.href = uri + base64(format(template, ctx))

      }
})()



function exportNota(){
    var value = $('#selectExportDataNota option:selected').val();
    /*if (value == "CSV"){
        window.location='exportCSV.php?exp=datatoko';
        $('#selectExportDataToko').val(''); 
    }*/if (value =="XLS"){
        
         var originalContents = $("#datanota").html();
         $("#datanota").find("tr th:nth-child(" + 8 + ")").remove();
         $("#datanota").find("tr td:nth-child(" + 8 + ")").remove();
         $("#datanota table tbody tr td select").remove();
         
         tableToExcel("datanota", 'nota');
         $('#selectExportDataNota').val(''); 

         $("#datanota").html(originalContents);
         reloadBidang();
    }else if (value == "PDF"){
        window.location='exportPDFBidang.php/?pdffor=nota';   
        $('#selectExportDataNota').val(''); 
        // convertToPdfDataTokoPenanggungjawab("#tabletoko");
    }
}

