$("#formbuktipenerimaan").ready(function(){
	uniform("buktiPenerimaanAtas tr:not(:nth-last-child(1)) td", $("#buktiPenerimaanAtas td[colspan=3]").width()-6);		
	uniform("boxUraianPenerimaanTambahan", $("#boxUraianPenerimaanTambahan input").width());
	uniform("boxJumlahPenerimaanTambahan", $("#boxJumlahPenerimaanTambahan input").width());
	
	$("#buktipenerimaan_tanggal").focusout(function(){
		var objectd = $("#buktipenerimaan_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
		else $("li.buktipenerimaan").click();
	});

	$("#buktipenerimaan_bulan").focusout(function(){
		var m = $("#buktipenerimaan_bulan").val();
		if(m > 12 || m < 1) {
			$("#buktipenerimaan_bulan").val("");
			m = null;
		}
		if(m == null) $("#buktipenerimaan_tanggal").attr("max", 31);
		else if(m == 2) $("#buktipenerimaan_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#buktipenerimaan_tanggal").attr("max", 31);
			else $("#buktipenerimaan_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#buktipenerimaan_tanggal").attr("max", 31);
			else $("#buktipenerimaan_tanggal").attr("max", 30);
		}
		$("#buktipenerimaan_tahun").triggerHandler("focusout");
		$("#buktipenerimaan_tanggal").triggerHandler("focusout");
	});

	$("#buktipenerimaan_tahun").focusout(function(){
		var m = $("#buktipenerimaan_bulan").val(); 
		var y = $("#buktipenerimaan_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#buktipenerimaan_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#buktipenerimaan_tanggal").attr("max", 29);
				} else {
					$("#buktipenerimaan_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#buktipenerimaan_tanggal").attr("max", 29);
				} else {
					$("#buktipenerimaan_tanggal").attr("max", 28);
				}
			}
			$("#buktipenerimaan_tanggal").triggerHandler("focusout");
		}
	});

	$("li.buktipenerimaan").click(function(){
		uniform("buktiPenerimaanAtas tr:not(:nth-last-child(1)) td", $("#buktiPenerimaanAtas td[colspan=3]").width()-7);	
		uniform("boxUraianPenerimaanTambahan", $("#boxUraianPenerimaanTambahan").width()-7);
		uniform("boxJumlahPenerimaanTambahan", $("#boxJumlahPenerimaanTambahan").width()-7);
		$.ajax({
	        type	: 'POST',
	        url		: 'refreshTableFunction.php',
	        data 	: {
	        	'type' : 'isiDetailKredit',
	        	'batas': $("#buktipenerimaan_tahun").val()+"-"+$("#buktipenerimaan_bulan").val()+"-"+$("#buktipenerimaan_tanggal").val()
	        },
	        success: function(result) {
	        	// alert(result);
	        	var n = $("#detailKreditBody tr").length;
	        	var oldhtml = $("#detailKreditBody").html();

	        	$("#detailKreditBody").html(result);
	        	var total  = parseInt($("#totalSementaraDetailPenerimaanHidden").html());
	        	var no 	   = parseInt($("#lastCounterDetailPenerimaan").html());
	        	
	        	var sstart = oldhtml.indexOf("<tr hidden");
	        	var search = oldhtml.substring(sstart);
	        	var sstop  = search.indexOf("</tr>");
	        	search  = search.substring(0, sstop+5);
	        	oldhtml = oldhtml.replace(search, "");
	        	for (var i = 1; i < n; i++) {
	        		sstart = oldhtml.indexOf("<td id=");
	        		search = oldhtml.substring(sstart+8);
	        		sstop  = search.indexOf("\">");
	        		search = search.substring(0, sstop);
	        		
	        		sstart = oldhtml.indexOf("</td>");
	        		sstop  = oldhtml.indexOf("</tr>");
	        		if($("select.selectKreditPenerimaanTambahan option[value="+search+"]").length > 0){
	        			result += '<tr><td class="calonKodeTransaksi">' + no + '</td>';
	        			result += oldhtml.substring(sstart+5, sstop+5);
	        			
	        			search = oldhtml.substring(oldhtml.indexOf('<td class="numerik">')+20);
	        			search = search.substring(0, search.indexOf('</td>'));
	        			total  = total + parseInt(search);
	        			no 	   = no + 1; 
	        		}
	        		oldhtml = oldhtml.substring(sstop+5);
	    		}
	        	$("#detailKreditBody").html(result);
	        	$("#lastCounterDetailPenerimaan").html(no);
	        	$("#totalSementaraDetailPenerimaan").html(total);
	        }
        });
	});

	$("#tambahPenerimaan").submit(function(){
		var kredit = $("select.selectKreditPenerimaanTambahan").val();
		var plus = parseInt($("#penerimaanTambahan_jumlah").val().replace(/\./g, ""));
		/*
		if($("#detailKreditBody #" + kredit).length > 0){
			var box = $("#detailKreditBody #" + kredit).parent().children(".numerik");
			var jumlah = parseInt(box.html()) + plus;
			box.html(jumlah);
		} else {
		//*/
			var full = $("#detailKreditBody").html();
			var no = $("#lastCounterDetailPenerimaan").html();
			$("#detailKreditBody").html(
				full +
				'<tr>' +
		    		'<td class="calonKodeTransaksi">' + no + '</td>' +
		    		'<td id="' + kredit + '">' + kredit.replace("_dot_", ".") + '</td>' +
		    		'<td><input class="uraianPenerimaan" value="' + $("#uraianPenerimaanTambahan").val() + '"></td>' +
			    	'<td class="rpCol">Rp.</td>' +
		    		'<td class="numerik">' + plus + '</td>' +
		    		'<td><label class="delRow">x</label></td>' +
		    	'</tr>'
		    	);
			$("#lastCounterDetailPenerimaan").html(parseInt(no)+1);
		//}
		var total = $("#totalSementaraDetailPenerimaan").html();
		$("#totalSementaraDetailPenerimaan").html(parseInt(total)+plus);
		$(this).closest('form').get(0).reset();
		placeholderselect();
    	return false;
	});

	$("#detailKreditBody").on("click", ".delRow", function(){
		var num = $(this).parent().parent().nextAll().length;
		var minus = $(this).parent().parent().children(".numerik").html();
		var rowEl = $(this).parent().parent().next();
		for (var i = 0; i < num; i++) {
			var el = rowEl.children(".calonKodeTransaksi");
			el.html(parseInt(el.html())-1);
			rowEl = rowEl.next();
		}; 
		$(this).parent().parent().remove();
		var count = $("#lastCounterDetailPenerimaan").html();
		$("#lastCounterDetailPenerimaan").html(parseInt(count)-1);
		var total = $("#totalSementaraDetailPenerimaan").html();

		total = total.replace(/\./g, "");
		minus = minus.replace(/\./g, "");

		var numberFormat = new Intl.NumberFormat('es-ES');

		var tmp = parseInt(total)-parseInt(minus);

		$("#totalSementaraDetailPenerimaan").html(tmp);
	});

	$("#formbuktipenerimaan").submit(function(){
		var kodebukti = $("#kodeBuktiPenerimaan").val();
		var debet = $("select.selectdebetbuktipenerimaan").val().replace("_dot_", ".");
		var tanggalbuktipenerimaan = $("#buktipenerimaan_tahun").val()+"-"+$("#buktipenerimaan_bulan").val()+"-"+$("#buktipenerimaan_tanggal").val();
		var bendahara = $("select.selectBendaharaBuktiPenerimaan").val().replace("_dot_", ".");
		var penerima = $("select.selectPenerimaBuktiPenerimaan").val().replace("_dot_", ".");
		var penyetor = $("select.selectPenyetorBuktiPenerimaan").val().replace("_dot_", ".");
		var jumlah = $("#totalSementaraDetailPenerimaan").html();
		var keterangan = $("#keteranganBuktiPenerimaan").val();
		var detail = [];	
		var i = 0;
		$("#detailKreditBody tr:not([hidden])").each(function(){
			detail[i] = [];
			detail[i][0] = $(this).children(":nth-child(1)").html();
			detail[i][1] = $(this).children(":nth-child(2)").html();
			detail[i][2] = $(this).children(":nth-child(3)").children().val();
			detail[i][3] = $(this).children(":nth-child(5)").html();
			i++;
		});
		if(i == 0){
			alert("tidak ada penerimaan!");
			return false;
		}
		var detailJSON = JSON.stringify(detail);
		// alert(kodebukti+"\n"+tanggalbuktipenerimaan+"\n"+debet+"\n"+bendahara+"\n"+penerima+"\n"+penyetor+"\n"+jumlah+"\n"+keterangan);

		$.ajax({
			url: "buktipenerimaan_proses.php",
			type: "POST",
			data: {
				"kodebukti" : kodebukti,
				"debet" : debet,
				"tanggalbuktipenerimaan" : tanggalbuktipenerimaan,
				"bendahara" : bendahara,
				"penerima" : penerima,
				"penyetor" : penyetor,
				"keterangan" : keterangan,
				"jumlah" : jumlah,
				"detail" : detailJSON
			},
			success: function(result){
				// alert(result);
				if(result == true){
					alert("berhasil menyimpan!");
					$("#right>div.buktipenerimaan input[type=reset]").click();
					placeholderselect();
					$("#detailKreditBody").html("");
					$("li.buktipenerimaan").click();
					
				} else {
					alert("gagal menyimpan!");
				}
			}
		});
		return false;
	});

	$("#right>div.buktipenerimaan select").focusin(function(){
		refreshSelectBuktiPenerimaan($(this));
	});

	$("#right>div.buktipenerimaan .trick").focusin(function(){
		var i = $(this).attr("id");
		refreshSelectBuktiPenerimaan($("select."+i));
	});
});

function refreshSelectBuktiPenerimaan(obj){
	var value = obj.val();
	var type;
	var c = obj.attr('class');
	if(c == 'selectdebetbuktipenerimaan') c = "rekening";
	else if(c == 'selectKreditPenerimaanTambahan') c = "rekeningnonindividu";
	else if(c == 'selectBendaharaBuktiPenerimaan') c = "pj_bendahara";
	else if(c == 'selectPenerimaBuktiPenerimaan') c = "pj_penerima";
	else if(c == 'selectPenyetorBuktiPenerimaan') c = "pj_penyetor";
	$.ajax({
        type	: 'POST',
        url		: 'refreshSelectFunction.php',
        data 	: {
        	'type' : c
        },
        success: function(result) {
        	obj.html(result);
        	if(obj.children("[value="+value+"]").length > 0){
        		obj.val(value);
        	} else placeholderselect();
        }
    });	
}

function reAdjustTrickBuktiPenerimaan(){
	uniform("buktiPenerimaanAtas tr:not(:nth-last-child(1)) td", $("#buktiPenerimaanAtas td[colspan=3]").width()-7);	
	uniform("boxUraianPenerimaanTambahan", $("#boxUraianPenerimaanTambahan").width()-7);
	uniform("boxJumlahPenerimaanTambahan", $("#boxJumlahPenerimaanTambahan").width()-7);
}