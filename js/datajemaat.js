 reloadJemaat();
 function reloadJemaat(){
   // alert("masuk");
	$.ajax({
        type	: 'POST',
        url		: 'ajax_datajemaat.php',
        data 	: 'actionfunction=showData&page=1',
        cache 	: false,
        success: function(result) {
     //       alert(result);
        	    
            var a = result.indexOf("<");
        	var rows = result.substring(0,a);
        	$('#lblJumlahDataJemaat').text(rows);
        	result = result.substring(a);
			$('#isiTabelJemaat').html(result);
			$('#jumlahData').text($('#jumlahDataJemaat').val());
			$.ajax({
			    url:"ajax_datajemaat.php",
		        type:"POST",	
		        data:"actionfunction=pagination&page=1",
		        cache: false,
		        success: function(response){
					$('#paginationJemaat').html(response);


				}
	   		});
        },
    });
//alert("masuk3");

    $('#paginationJemaat').on('click','.page-numbers',function(){
       $page = $(this).attr('href');
	   $pageind = $page.indexOf('page=');
	   $page = $page.substring(($pageind+5));
       
	   $.ajax({
		    url:"ajax_datajemaat.php",
	        type:"POST",
	        data:"actionfunction=showData&page="+$page,
	        cache: false,
	        success: function(response1){
			   $.ajax({
			    url:"ajax_datajemaat.php",
		        type:"POST",
		        data:"actionfunction=pagination&page="+$page,
		        cache: false,
		        success: function(response){
		        	$('#isiTabelJemaat').html(response1);
					$('#paginationJemaat').html(response);
				}
	   		});
			 
			}
	   });
	return false;
	});
  //  alert("masuk2");

}  


function dipilih(e){
   $(e).addClass('selected').siblings().removeClass('selected');  
}

function pilihanDataJemaat(index, jemaat_kode){
	if (index == "1"){
		
        //$('#formEditBidang').html(result);
        
        $.ajax({
        type	: 'POST',
        url		: 'ajax_editjemaat.php',
        data 	: {'kode':jemaat_kode.replace(/_spasi_/g, " ")},
        dataType: 'json',
        success: function(result) {
                $('#editKodeJemaat').val(result[2]);
                $('#editKodeJemaat2').val(result[2]);                
                $('#editNamaJemaat').val(result[0]);
                $('#editWilayahJemaat').val(result[1]);
                                
            	$("div.datajemaat").slideUp("700");
            	$("div.editjemaat").slideDown("700");                
	        },

	    });

	}else{
        var cek = confirm("Apakah yakin akan menghapus jemaat ini ?");
		if (cek){
            $.ajax({
            type    : 'POST',
            url     : 'ajax_hapusjemaat.php',
            data    : {'kode':jemaat_kode.replace(/_spasi_/g, " ")},
            success: function(result) {
                    if (result == true){
                        $('pilihanDataJemaat').val("");
                        alert("Data jemaat terpilih berhasil dihapus");
                        reloadJemaat();
                    }
                    
                },
            });
        }else{
            $('pilihanDataJemaat').val(""); 
            reloadJemaat();
        }
        
	}
}

$("form#formTambahJemaat").submit(function(){
  
  var formData2 = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_tambahjemaat.php",
        type: 'POST',
        data: formData2,
        success: function (data) {
            //alert(data) ;     
            if ( data == true){
            	
                $('#kodeJemaat').val("");
				$('#namaJemaat').val("");
				$('#wilayahJemaat').val("");
                
                /*$("div.tambahJemaat").slideUp("700");
				$("#right>div.datajemaat").slideDown("700");
				$("#right>div.datajemaat div").slideDown("700");*/
				alert("Jemaat berhasil ditambahkan");                
          }
          else{
            alert("Gagal menambahkan jemaat");
          }
          reloadJemaat();

        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

$("#btnBatalEditJemaat").click(function(){
    reloadJemaat();
    $("div.editjemaat").slideUp("700");
    $("#right>div.datajemaat").slideDown("700");

});

$("form#formEditJemaat").submit(function(){

  var formData3 = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_editjemaat.php",
        type: 'POST',
        data: formData3,
        success: function (data3) {            
  
             if (data3 == true){
            	$('#editKodeJemaat').val("");
            	$('#editNamaJemaat').val("");
				$('#editWilayahJemaat').val("");                
				alert("Data jemaat berhasil di edit");
              	$("div.editjemaat").slideUp("700");
				$("#right>div.datajemaat").slideDown("700");
				//$("#right>div.databidang div").slideDown("700");
				
            }            
            reloadJemaat();   
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

function searchJemaat(){
	var keyword = $('#searchJemaat').val();
	var sortby  = $('#sortDataJemaat option:selected').val();
  	$.ajax({
        type	: 'POST',
        url		: 'ajax_datajemaat.php',
        data 	: 'actionfunction=showData&page=1&keyword='+keyword+'&sortby='+sortby,
        cache 	: false,
        success: function(result) {
        	var a = result.indexOf("<");
        	var rows = result.substring(0,a);
        	$('#lblJumlahDataJemaat').text(rows);
        	result = result.substring(a);
			$('#isiTabelJemaat').html(result);
			$.ajax({
			    url:"ajax_datajemaat.php",
		        type:"POST",
		        data:"actionfunction=pagination&page=1&keyword="+keyword+'&sortby='+sortby,
		        cache: false,
		        success: function(response){
					$('#paginationJemaat').html(response);
				}
	   		});
	   		$("#right>div.datajemaat").slideDown("700");
				$("#right>div.datajemaat div").slideDown("700");
        },
    });
}

function checkKodeJemaat(){
      var kodeInput = document.getElementById("kodeJemaat").value;
      $.ajax({
            url: "ajax_tambahjemaat.php",
            type: "POST",
            data: {
                "kode_jemaat_check": kodeInput
            },
            success: function(data){
            	if(kodeInput != "" && data == true){
					$("#warningUsernameJemaat").attr("src","image/check.png");
                    document.getElementById("warningUsernameJemaat").style.visibility = "visible";
                }
                else {
                    document.getElementById("warningUsernameJemaat").style.visibility = "visible";
                    $("#warningUsernameJemaat").attr("src","image/warning.png");
                }   
            }
        });
        return false;
}
