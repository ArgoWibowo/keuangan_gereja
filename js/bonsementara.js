$("#formBonSementara").ready(function(){
	uniform("bonSementaraAtas tr:not(:nth-last-child(1)) td", $("#bonSementaraAtas td[colspan=3]").width()-6);		
	uniform("boxUraianBonTambahan", $("#boxUraianBonTambahan input").width());
	uniform("boxJumlahBonTambahan", $("#boxJumlahBonTambahan input").width());
	
	$("#bonSementara_tanggal").focusout(function(){
		var objectd = $("#bonSementara_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#bonSementara_bulan").focusout(function(){
		var m = $("#bonSementara_bulan").val();
		if(m > 12 || m < 1) {
			$("#bonSementara_bulan").val("");
			m = null;
		}
		if(m == null) $("#bonSementara_tanggal").attr("max", 31);
		else if(m == 2) $("#bonSementara_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#bonSementara_tanggal").attr("max", 31);
			else $("#bonSementara_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#bonSementara_tanggal").attr("max", 31);
			else $("#bonSementara_tanggal").attr("max", 30);
		}
		$("#bonSementara_tahun").triggerHandler("focusout");
		$("#bonSementara_tanggal").triggerHandler("focusout");
	});

	$("#bonSementara_tahun").focusout(function(){
		var m = $("#bonSementara_bulan").val(); 
		var y = $("#bonSementara_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#bonSementara_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#bonSementara_tanggal").attr("max", 29);
				} else {
					$("#bonSementara_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#bonSementara_tanggal").attr("max", 29);
				} else {
					$("#bonSementara_tanggal").attr("max", 28);
				}
			}
			$("#bonSementara_tanggal").triggerHandler("focusout");
		}
	});

	$("li.bonsementara").click(function(){
		uniform("bonSementaraAtas tr:not(:nth-last-child(1)) td", $("#bonSementaraAtas td[colspan=3]").width()-7);	
		uniform("boxUraianBonTambahan", $("#boxUraianBonTambahan").width()-7);
		uniform("boxJumlahBonTambahan", $("#boxJumlahBonTambahan").width()-7);
		$.ajax({
	        type	: 'POST',
	        url		: 'refreshTableFunction.php',
	        data 	: {
	        	'type' : 'isiDetailBon'
	        },
	        success: function(result) {
	        	// alert(result);
	        	var n = $("#detailBonBody tr").length;
	        	var oldhtml = $("#detailBonBody").html();

	        	$("#detailBonBody").html(result);
	        	var no = parseInt($("#lastCounterDetailBon").html());
	        	
	        	var sstart = oldhtml.indexOf("<tr hidden");
	        	var search = oldhtml.substring(sstart);
	        	var sstop  = search.indexOf("</tr>");
	        	search  = search.substring(0, sstop+5);
	        	oldhtml = oldhtml.replace(search, "");
	        	for (var i = 1; i < n; i++) {
	        		sstart = oldhtml.indexOf("<td id=");
	        		search = oldhtml.substring(sstart+8);
	        		sstop  = search.indexOf("\">");
	        		search = search.substring(0, sstop);
	        		
	        		sstart = oldhtml.indexOf("</td>");
	        		sstop  = oldhtml.indexOf("</tr>");
        			result += '<tr><td class="calonKodeTransaksi">' + no + '</td>';
        			result += oldhtml.substring(sstart+5, sstop+5);
        			no 	    = no + 1; 
					oldhtml = oldhtml.substring(sstop+5);
	    		}
	        	$("#detailBonBody").html(result);
	        	$("#lastCounterDetailBon").html(no);
	        }
        });
	});

	$("#tambahBon").submit(function(){
		var debet = $("select.selectDebetBonTambahan").val();
		var plus = parseInt($("#bonTambahan_jumlah").val().replace(/\./g, ""));
		
		var full = $("#detailBonBody").html();
		var no = $("#lastCounterDetailBon").html();
		$("#detailBonBody").html(
			full +
			'<tr>' +
	    		'<td class="calonKodeTransaksi">' + no + '</td>' +
	    		'<td id="' + debet + '">' + debet.replace("_dot_", ".") + '</td>' +
	    		'<td><input class="uraianBon" value="' + $("#uraianBonTambahan").val() + '"></td>' +
		    	'<td class="rpCol">Rp.</td>' +
	    		'<td class="numerik">' + plus + '</td>' +
	    		'<td><label class="delRow">x</label></td>' +
	    	'</tr>'
	    );
		$("#lastCounterDetailBon").html(parseInt(no)+1);
		
		var total = $("#totalSementaraDetailBon").html();
		$("#totalSementaraDetailBon").html(parseInt(total)+plus);
		$(this).closest('form').get(0).reset();
		placeholderselect();

    	return false;
	});

	$("#detailBonBody").on("click", ".delRow", function(){
		var num = $(this).parent().parent().nextAll().length;
		var minus = $(this).parent().parent().children(".numerik").html();
		var rowEl = $(this).parent().parent().next();
		for (var i = 0; i < num; i++) {
			var el = rowEl.children(".calonKodeTransaksi");
			el.html(parseInt(el.html())-1);
			rowEl = rowEl.next();
		}; 
		$(this).parent().parent().remove();
		var count = $("#lastCounterDetailBon").html();
		$("#lastCounterDetailBon").html(parseInt(count)-1);
		var total = $("#totalSementaraDetailBon").html();
		$("#totalSementaraDetailBon").html(parseInt(total)-minus);
	});

	$("#formBonSementara").submit(function(){
		var kodebukti = $("#kodeBonSementara").val();
		var kredit = $("select.selectKreditBonSementara").val().replace("_dot_", ".");
		var tanggalBonSementara = $("#bonSementara_tahun").val()+"-"+$("#bonSementara_bulan").val()+"-"+$("#bonSementara_tanggal").val();
		var bendahara = $("select.selectBendaharaBonSementara").val().replace("_dot_", ".");
		var penerima = $("select.selectPenerimaBonSementara").val().replace("_dot_", ".");
		var penyetor = $("select.selectPenyetorBonSementara").val().replace("_dot_", ".");
		var jumlah = $("#totalSementaraDetailBon").html();
		var keterangan = $("#keteranganBonSementara").val();
		var detail = [];	
		var i = 0;
		$("#detailBonBody tr:not([hidden])").each(function(){
			detail[i] = [];
			detail[i][0] = $(this).children(":nth-child(1)").html();
			detail[i][1] = $(this).children(":nth-child(2)").html();
			detail[i][2] = $(this).children(":nth-child(3)").children().val();
			detail[i][3] = $(this).children(":nth-child(5)").html();
			i++;
		});
		if(i == 0){
			alert("tidak ada bon!");
			return false;
		}
		var detailJSON = JSON.stringify(detail);
		// alert(kodebukti+"\n"+tanggalBonSementara+"\n"+kredit+"\n"+bendahara+"\n"+penerima+"\n"+penyetor+"\n"+jumlah+"\n"+keterangan);

		$.ajax({
			url: "bonsementara_proses.php",
			type: "POST",
			data: {
				"kodebukti" : kodebukti,
				"kredit" : kredit,
				"tanggalBonSementara" : tanggalBonSementara,
				"bendahara" : bendahara,
				"penerima" : penerima,
				"penyetor" : penyetor,
				"keterangan" : keterangan,
				"jumlah" : jumlah,
				"detail" : detailJSON
			},
			success: function(result){
				//alert(result);
				if(result == true){
					alert("berhasil menyimpan!");
					$("#right>div.bonsementara input[type=reset]").click();
					placeholderselect();
					$("#detailBonBody").html("");
					$("li.bonsementara").click();
					$("#totalSementaraDetailBon").html(parseInt(0));
				} else {
					alert("gagal menyimpan!");
				}
			}
		});
		return false;
	});

	$("#right>div.bonsementara select").focusin(function(){
		refreshSelectBonSementara($(this));
	});

	$("#right>div.bonsementara .trick").focusin(function(){
		var i = $(this).attr("id");
		refreshSelectBonSementara($("select."+i));
	});
});

function refreshSelectBonSementara(obj){
	var value = obj.val();
	var type;
	var c = obj.attr('class');
	if(c == 'selectKreditBonSementara') c = "rekening";//tambahan
	else if(c == 'selectDebetBonTambahan') c = "rekening";
	else if(c == 'selectBendaharaBonSementara') c = "pj_bendahara";
	else if(c == 'selectPenerimaBonSementara') c = "pj_penerima";
	else if(c == 'selectPenyetorBonSementara') c = "pj_penyetor";
	$.ajax({
        type	: 'POST',
        url		: 'refreshSelectFunction.php',
        data 	: {
        	'type' : c
        },
        success: function(result) {
        	obj.html(result);
        	if(obj.children("[value="+value+"]").length > 0){
        		obj.val(value);
        	} else placeholderselect();
        }
    });	
}