$(document).ready(function() {
    $("#divBoxPencarian").hide();
    uniform("formEditPenanggungJawab", 300);
    uniform("formTambahPenanggungJawab", 300);
    reloadPenanggungJawab();
});


function checkKodePenanggungJawab(){
      var kodeInput = document.getElementById("kodePenanggungJawab").value;
      $.ajax({
            url: "ajax_tambahpenanggungjawab.php",
            type: "POST",
            data: {
                "kode_penanggungjawab_check": kodeInput
            },
            success: function(data){
            	if(kodeInput != "" && data == true){
					$("#warningKodePenanggungJawab").attr("src","image/check.png");
                    document.getElementById("warningKodePenanggungJawab").style.visibility = "visible";
                }
                else {
                    document.getElementById("warningKodePenanggungJawab").style.visibility = "visible";
                    $("#warningKodePenanggungJawab").attr("src","image/warning.png");
                }   
            }
        });
        return false;
}

$("form#formTambahPenanggungJawab").submit(function(){
  var formData = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_tambahpenanggungjawab.php",
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
            if ( data == true){
                $('#kodePenanggungJawab').val("");
                $('#namaPenanggungJawab').val("");
                $('#jabatanPenanggungJawab').val("");
                $('#alamatPenanggungJawab').val("");
                $('#noTeleponPenanggungJawab').val("");

                $("div.tambahpenanggungjawab").slideUp("700");
                $("#right>div.datapenanggungjawab").slideDown("700");
                $("#right>div.datapenanggungjawab div").slideDown("700");
                reloadPenanggungJawab();
          }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

$("form#formTambahPenanggungJawabImport").submit(function(){
    var formData        = new FormData($(this)[0]);
    if ($('#filePenanggungJawabImport').get(0).files.length === 0) {
        alert("Pilih file CSV terlebih dahulu");
    }else{
        $.ajax({
            type    : 'POST',
            url     : 'importCSV.php',
            data    : formData,
            async: false,
            success: function(result) {
                if (result == "false"){
                    alert("Import gagal. Keseluruhan data sebelumnya tidak bisa dihapus karena masih berelasi dengan data lain dan tidak ada data baru yang berbeda");
                }else {
                    var hasil = result.split('-');
                    if (hasil[0] == true){
                        alert(hasil[2]+" gagal import karena data sebelumnya sudah ada. Import berhasil untuk "+hasil[1]+" data");                    
                    }else{
                        alert("Terdapat data yang tidak bisa dihapus karena masih berelasi dengan data lain. "+hasil[2]+" gagal import karena data sebelumnya sudah ada. Import berhasil untuk "+hasil[1]+" data");                    
                    }
                    $("div.tambahpenanggungjawab").slideUp("700");
                    $("#right>div.datapenanggungjawab").slideDown("700");
                    $("#right>div.datapenanggungjawab div").slideDown("700");
                    reloadPenanggungJawab();
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
    return false;
});

function reloadPenanggungJawab(){
    $.ajax({
        type    : 'POST',
        url     : 'ajax_datapenanggungjawab.php',
        data    : 'actionfunction=showData&page=1',
        cache   : false,
        success: function(result) {
            var a = result.indexOf("<");
            var rows = result.substring(0,a);
            $('#lblJumlahDataPenanggungJawab').text(rows);
            result = result.substring(a);
            $('#isiTabelPenanggungJawab').html(result);
            //$('#lblJumlahDataPenanggungJawab').text($('#jumlahDataToko').val());
            $.ajax({
                url:"ajax_datapenanggungjawab.php",
                type:"POST",    
                data:"actionfunction=pagination&page=1",
                cache: false,
                success: function(response){
                    $('#paginationPenanggungJawab').html(response);
                }
            });
        },
    });

    $('#paginationPenanggungJawab').on('click','.page-numbers',function(){
       $page = $(this).attr('href');
       $pageind = $page.indexOf('page=');
       $page = $page.substring(($pageind+5));
       
       $.ajax({
            url:"ajax_datapenanggungjawab.php",
            type:"POST",
            data:"actionfunction=showData&page="+$page,
            cache: false,
            success: function(response1){
               $.ajax({
                url:"ajax_datapenanggungjawab.php",
                type:"POST",
                data:"actionfunction=pagination&page="+$page,
                cache: false,
                success: function(response){
                    $('#isiTabelPenanggungJawab').html(response1);
                    $('#selectExportDataPenanggungJawab').val(''); 
                    $('#paginationPenanggungJawab').html(response);
                }
            });
             
            }
       });
    return false;
    });
}  

function searchPenanggungJawab(){
    var keyword = $('#searchPenanggungJawab').val();
    var sortby  = $('#sortDataPenanggungJawab option:selected').val();
    $.ajax({
        type    : 'POST',
        url     : 'ajax_datapenanggungjawab.php',
        data    : 'actionfunction=showData&page=1&keyword='+keyword+'&sortby='+sortby,
        cache   : false,
        success: function(result) { 
            var a = result.indexOf("<");
            var rows = result.substring(0,a);
            $('#lblJumlahDataPenanggungJawab').text(rows);
            result = result.substring(a);
            $('#isiTabelPenanggungJawab').html(result);
            $.ajax({
                url:"ajax_datapenanggungjawab.php",
                type:"POST",
                data:"actionfunction=pagination&page=1&keyword="+keyword+'&sortby='+sortby,
                cache: false,
                success: function(response){
                    $('#paginationPenanggungJawab').html(response);
                }
            });
            $("#right>div.datapenanggungjawab").slideDown("700");
            $("#right>div.datapenanggungjawab div").slideDown("700");
        },
    });
}

$("#btnDropPencarianPenanggungJawab").click(function(){
    $("#formCariPenanggungJawab").toggle(1000);
});

function tambahPenanggungJawab(){
    $("div.datapenanggungjawab").slideUp("700");
    $("div.tambahpenanggungjawab").slideDown("700");
}

function dipilihPenanggungJawab(e){
   $(e).addClass('selected').siblings().removeClass('selected');  
}

$("form#formCariPenanggungJawab").submit(function(){
    var formData        = new FormData($(this)[0]);
    var sortby          = $('#sortDataPenanggungJawab option:selected').val();
    var search_kode     = $('#kataKunciKodePenanggungJawab').val();
    var search_nama     = $('#kataKunciNamaPenanggungJawab').val();
    var search_jabatan  = $('#kataKunciJabatanPenanggungJawab').val();
    var search_alamat   = $('#kataKunciAlamatPenanggungJawab').val();
    var search_telepon  = $('#kataKunciNoTeleponPenanggungJawab').val();
    $.ajax({
        type    : 'POST',
        url     : 'ajax_datapenanggungjawab.php',
        data    : 'actionfunction=showData&page=1&search_kode='+search_kode+'&search_nama='+search_nama+'&search_jabatan='+search_jabatan+'&search_alamat='+search_alamat+'&search_telepon='+search_telepon+'&sortby='+sortby,
        cache: false,
        success: function(result) {
            var a = result.indexOf("<");
            var rows = result.substring(0,a);
            $('#lblJumlahDataPenanggungJawab').text(rows);
            result = result.substring(a);
            $('#isiTabelPenanggungJawab').html(result);
            $.ajax({
                url:"ajax_datapenanggungjawab.php",
                type:"POST",
                data:'actionfunction=pagination&page=1&search_kode='+search_kode+'&search_nama='+search_nama+'&search_jabatan='+search_jabatan+'&search_alamat='+search_alamat+'&search_telepon='+search_telepon+'&sortby='+sortby,
                cache: false,
                success: function(response){
                    $('#paginationPenanggungJawab').html(response);
                }
            });
            $("#right>div.datapenanggungjawab").slideDown("700");
            $("#right>div.datapenanggungjawab div").slideDown("700");
        },
    });
 return false;
});

function pilihanDataPenanggungJawab(index, penanggungjawab_kode){
    //alert(penanggungjawab_kode.replace(/_spasi_/g, " "));
    if (index == "1"){
        // alert(''+penanggungjawab_kode);
        $.ajax({
        type    : 'POST',
        url     : 'ajax_editpenanggungjawab.php',
        data    : {'kode':penanggungjawab_kode.replace(/_spasi_/g, " ")},
        dataType: 'json',
        success: function(result) {

                $('pilihanDataPenanggungJawab').val("");
                $('#editKodePenanggungJawab').val(result[0]);
                $('#editKodePenanggungJawab2').val(result[0]);
                $('#editNamaPenanggungJawab').val(result[1]);
                $('#editJabatanPenanggungJawab').val(result[2]);
                $('#editAlamatPenanggungJawab').val(result[3]);
                $('#editNoTeleponPenanggungJawab').val(result[4]);
                

                $("div.datapenanggungjawab").slideUp("700");
                $("div.editpenanggungjawab").slideDown("700");
                
            },
        });
    }else{
        var cek = confirm("Apakah yakin akan menghapus penanggungjawab ini ?");
        if (cek){
            $.ajax({
            type    : 'POST',
            url     : 'ajax_hapuspenanggungjawab.php',
            data    : {'kode':penanggungjawab_kode.replace(/_spasi_/g, " ")},
            success: function(result) {
                    if (result == true){
                        $('pilihanDataPenanggungJawab').val("");
                        alert("Data penanggungjawab terpilih berhasil dihapus");
                        reloadPenanggungJawab();
                    }else{
                        alert(result);
                    }
                    
                },
            });
        }else{
            $('pilihanDataPenanggungJawab').val(""); 
            reloadPenanggungJawab();
        }
        
    }
}

$("form#formEditPenanggungJawab").submit(function(){

  var formData = new FormData($(this)[0]);
   $.ajax({
        url: "ajax_editpenanggungjawab.php",
        type: 'POST',
        data: formData,
        async: false,
        success: function (data2eee) {

             if (data2eee == true){
                $('#editKodePenanggungJawab').val("");
                $('#editKodePenanggungJawab2').val("");
                $('#editNamaPenanggungJawab').val("");
                $('#editJabatanPenanggungJawab').val("");
                $('#editAlamatPenanggungJawab').val("");
                $('#editNoTeleponPenanggungJawab').val("");

                $("div.editpenanggungjawab").slideUp("700");
                $("#right>div.datapenanggungjawab").slideDown("700");
                $("#right>div.datapenanggungjawab div").slideDown("700");
                reloadPenanggungJawab();
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

var tableToExcel2Penanggungjawab = (function() {
    var tamp;
    $.ajax({
            type: "POST",
            url: "loadKopTanpaLogo2.php",
            success: function(data){
                if(data){
                    tamp = data;
                }
            }   
    });
      var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
      return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        $.ajax({
         type    : 'POST',
         url     : 'ajax_print.php',
         data    : {'tentang':"penanggungjawab"},
         success: function(result) {
                var originalContents = "";
                originalContents = result;
                // alert(originalContents);
                var ctx = {worksheet: name || 'Worksheet', table: tamp + originalContents}
                window.location.href = uri + base64(format(template, ctx))
            },
         });  
      }
})()

function exportPenanggungJawab(){
    var value = $('#selectExportDataPenanggungJawab option:selected').val();
    if (value == "CSV"){
        window.location='exportCSV.php?exp=penanggungjawab';
        $('#selectExportDataPenanggungJawab').val(""); 
        $('#selectExportDataPenanggungJawab option:selected').val(""); 
    }else if (value =="XLS"){        
         tableToExcel2Penanggungjawab("datapenanggungjawab", 'PenanggungJawab');
         $('#selectExportDataPenanggungJawab').val(""); 
         reloadPenanggungJawab();
         
    }else if (value == "PDF"){
        window.location='exportpdf.php/?pdffor=penanggungjawab';
        $('#selectExportDataPenanggungJawab').val(""); 
    }

}