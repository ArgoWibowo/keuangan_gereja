$(document).ready(function() {


	// $('.thnMulaiLaporan').on('input', function(){
	//     $('.thnSampaiLaporan').val($(this).val());
	// });

	// $('.thnSampaiLaporan').on('input', function() {
	//     $('.thnMulaiLaporan').val($(this).val());
	// });

});

$("form#formTampilBukuBesar").submit(function(){

			loadBukuBesarAktiva();
			loadBukuBesarPasiva();
	    return false;
	});

	$("form#formTampilSaldoRekening").submit(function(){

			loadDataSaldoRekening();
	    return false;
	});

	$("form#formTampilPosisiSaldoRekening").submit(function(){

			loadDataPosisiSaldoRekening();
	    return false;
	});


	$("form#formTampilRekapSaldoRekening").submit(function(){

			loadDataRekapSaldoRekening();
	    return false;
	});

	$("form#formTampilMutasiPerRek").submit(function(){
			loadDataMutasiPerRekening();	
	    return false;
	});

	$("form#formMutasiKas").submit(function(){
			loadDataMutasiKas();
	    return false;
	});

	$("form#formJurnalMutasi").submit(function(){
			loadDataJurnalMutasi();
	    return false;
	});

	$("form#formMutasiPerAkun").submit(function(){
			loadDataMutasiPerAkun();
	    return false;
	});

	$("form#formTampilLapPerRek").submit(function(){
			loadLaporanPerKelAkun();
			
	    return false;
	});

	$("form#formLaporanPemasukan").submit(function(){
			loadDataLaporanPemasukan();
	    return false;
	});

	$("form#formLaporanPengeluaran").submit(function(){
			loadDataLaporanPengeluaran();
	    return false;
	});

	$("form#formLaporanPemasukanTahunan").submit(function(){
			loadDataLaporanPemasukanTahunan();
	    return false;
	});

	$("form#formTampilPenInd").submit(function(){
			loadPenerimaanInduvidu();
			
	    return false;
	});

	$("form#formTampilPerJem").submit(function(){
			loadLaporanPersembahanJemaat();
			
	    return false;
	});

	$("form#formPengeluaranPerTahun").submit(function(){
			loadLapPengeluaranPerThn();
			
	    return false;
	});

function loadDataSaldoRekening(){
	var awalperTanggalSalRek = $("#thnSalRek").val()+"/01/01";
	var perTanggalSalRek = $("#thnSalRek").val()+"/"+$("#blnSalRek").val()+"/"+$("#tglSalRek").val();
	var thnPemSalRek = $('#thnPemRekSaldo').val();

	$.ajax({
		type: "POST",
		url: "loadDataRekeningSaldo.php",
		data: {"awalperTanggalSalRek": awalperTanggalSalRek,
			   "perTanggalSalRek": perTanggalSalRek,
			   "thnPemSalRek": thnPemSalRek
			},
		success: function(data){
			if(data){
				$("#rekeningSaldo tbody").html(data);
				$('#judulRekSaldo').html("SALDO REKENING TAHUN "+thnPemSalRek);
				$("#perTanggalTampilRekSaldo").html($("#tglSalRek").val()+"/"+$("#blnSalRek").val()+"/"+$("#thnSalRek").val());
			}
			
		}
	});
}

function loadDataPosisiSaldoRekening(){
	var awalperTanggalSalRek = $("#thnPosSalRek").val()+"/01/01";
	var perTanggalSalRek = $("#thnPosSalRek").val()+"/"+$("#blnPosSalRek").val()+"/"+$("#tglPosSalRek").val();
	var thnPemSalRek = $('#thnPemPosRekSaldo').val();

	$.ajax({
		type: "POST",
		url: "loadDataPosisiSaldoRekening.php",
		data: {"awalperTanggalSalRek": awalperTanggalSalRek,
			   "perTanggalSalRek": perTanggalSalRek,
			   "thnPemSalRek": thnPemSalRek
			},
		success: function(data){
			if(data){
				$("#posisiRekeningSaldo tbody").html(data);
				$('#judulPosRekSaldo').html("POSISI SALDO REKENING TAHUN "+thnPemSalRek);
				$("#perTanggalTampilPosRekSaldo").html($("#tglPosSalRek").val()+"/"+$("#blnPosSalRek").val()+"/"+$("#thnPosSalRek").val());
			}
			
		}
	});
}

function loadDataRekapSaldoRekening(){
	var awalperTanggalSalRek = $("#thnSalRek").val()+"/01/01";
	var perTanggalSalRek = $("#thnRekapSalRek").val()+"/"+$("#blnRekapSalRek").val()+"/"+$("#tglRekapSalRek").val();
	var thnPemSalRek = $('#thnPemRekapRekSaldo').val();

	$.ajax({
		type: "POST",
		url: "loadDataRekapRekeningSaldo.php",
		data: {"awalperTanggalSalRek": awalperTanggalSalRek,
			   "perTanggalSalRek": perTanggalSalRek,
			   "thnPemSalRek": thnPemSalRek
			},
		success: function(data){
			if(data){
				$("#rekapRekeningSaldo tbody").html(data);
				$('#judulRekSaldo').html("REKAP SALDO REKENING TAHUN "+thnPemSalRek);
				$("#perTanggalTampilRekSaldo").html($("#tglSalRek").val()+"/"+$("#blnSalRek").val()+"/"+$("#thnSalRek").val());
			}
			
		}
	});
}


function loadPenerimaanInduvidu(){

	var awalperTanggalPenInd = $("#thnMulaiPenInd").val()+"/"+$("#blnMulaiPenInd").val()+"/"+$("#tglMulaiPenInd").val();
	var perTanggalPenInd = $("#thnPenInd").val()+"/"+$("#blnPenInd").val()+"/"+$("#tglPenInd").val();

	// var thnPemMutPenInd = $('#thnPembukuanPenIndLaporan').val();

	$.ajax({
				type: "POST",
				url: "loadPenerimaanInduvidu.php",
				data: {
					"awalperTanggalPenInd": awalperTanggalPenInd,
					"perTanggalPenInd": perTanggalPenInd
				},
				success: function(data){
					if(data){
						$("#laporanPenInduvidu").html(data);
						$("#perTanggalTampilPenInd").html($("#tglMulaiPenInd").val()+"/"+$("#blnMulaiPenInd").val()+"/"+$("#thnMulaiPenInd").val()+" - "+$("#tglPenInd").val()+"/"+$("#blnPenInd").val()+"/"+$("#thnPenInd").val());
					}
					
				}
			});
}

function loadBukuBesarAktiva(){
	
	var awalperTanggalBukBes = $("#thnBukBes").val()+"/01/01";
	var perTanggalBukBes = $("#thnBukBes").val()+"/"+$("#blnBukBes").val()+"/"+$("#tglBukBes").val();
	var thnPemBukBesAktiva = $('#thnPembukuanBukBes').val();
	$.ajax({
				type: "POST",
				url: "loadBukuBesarAktiva.php",
				data: {"awalperTanggalBukBes": awalperTanggalBukBes,
					   "perTanggalBukBes": perTanggalBukBes,
					   "thnPemBukBesAktiva": thnPemBukBesAktiva},
				success: function(data){
					if(data){
						$("#buku_besar_aktiva tbody").html(data);
						$('#judulBukuBesar').html("BUKU BESAR TAHUN "+thnPemBukBesAktiva);
						$("#perTanggalTampilBukuBesar").html($("#tglBukBes").val()+"/"+$("#blnBukBes").val()+"/"+$("#thnBukBes").val());
					
					}
					
				}
			});
}

function loadBukuBesarPasiva(){
	var awalperTanggalBukBes = $("#thnBukBes").val()+"/01/01";
	var perTanggalBukBes = $("#thnBukBes").val()+"/"+$("#blnBukBes").val()+"/"+$("#tglBukBes").val();
	var thnPemBukBesPas = $('#thnPembukuanBukBes').val();
	$.ajax({
				type: "POST",
				url: "loadBukuBesarPasiva.php",
				data: {"awalperTanggalBukBes": awalperTanggalBukBes,
					   "perTanggalBukBes": perTanggalBukBes,
					   "thnPemBukBesPas": thnPemBukBesPas},
				success: function(data){
					if(data){
						$("#buku_besar_pasiva tbody").html(data);
			
					}
					
				}
			});
}

function loadLaporanPerKelAkun(){
	var laporanPerKelAkunOptions = document.getElementById("selectTampilLapKelAkun");
	var kelLaporanPerKelAkun = laporanPerKelAkunOptions.options[laporanPerKelAkunOptions.selectedIndex].value;

	var awalperTanggalKelAK = $("#thnKelAK").val()+"/01/01";
	var perTanggalKelAK = $("#thnKelAK").val()+"/"+$("#blnKelAK").val()+"/"+$("#tglKelAK").val();
	var renAngKelAK = $("#thnPembukuanLapPerRek").val();
	var thnPemLapPerRek = $('#thnPembukuanLapPerRek').val();

	$.ajax({
				type: "POST",
				url: "loadLaporanPerKelAkun.php",
				data:{"kelLaporanPerKelAkun": kelLaporanPerKelAkun,
					  "awalperTanggalKelAK": awalperTanggalKelAK,
					  "perTanggalKelAK": perTanggalKelAK,
					  "renAngKelAK": renAngKelAK,
					  "thnPemLapPerRek": thnPemLapPerRek},
				success: function(data){
					if(data){
						$("#LaporanPerRekening tbody").html(data);
						$('#judulLaporanPerRekening').html("LAPORAN PER REKENING TAHUN "+thnPemLapPerRek+", " + $( "#selectTampilLapKelAkun option:selected").text());
						$("#perTanggalTampilLaporanPerRekening").html($("#tglKelAK").val()+"/"+$("#blnKelAK").val()+"/"+$("#thnKelAK").val());
	
					}
					
				}
			});
}



function loadselectTampilLapKelAkun(){
	$.ajax({
			type: "POST",
			url: "loadselectTampilLapKelAkun.php",
			success: function(data){
				if(data){
					$('#selectTampilLapKelAkun').html(data);		
				}
			}
	});
}

function loadDataMutasiPerRekening(){

	var mutasiAwal = $("#thnMulaiMutasi").val()+"/01/01";

	var mutasiMulai = $("#thnMulaiMutasi").val()+"/"+$("#blnMulaiMutasi").val()+"/"+$("#tglMulaiMutasi").val();

	var mutasiSampai = $("#thnSampaiMutasi").val()+"/"+$("#blnSampaiMutasi").val()+"/"+$("#tglSampaiMutasi").val();


	var mutasiKelompokAkunOptions = document.getElementById("selectMutasiKelompokAkun");
	var mutasiKelompokAkun = mutasiKelompokAkunOptions.options[mutasiKelompokAkunOptions.selectedIndex].value;

	var mutasiKodeGolonganOptions = document.getElementById("selectMutasiKodeGolongan");
	var mutasiKodeGolongan = mutasiKodeGolonganOptions.options[mutasiKodeGolonganOptions.selectedIndex].value;

	var mutasiKodeSubGolonganOptions = document.getElementById("selectMutasiKodeSubGolongan");
	var mutasiKodeSubGolongan = mutasiKodeSubGolonganOptions.options[mutasiKodeSubGolonganOptions.selectedIndex].value;

	var mutasiJenisAkunOptions = document.getElementById("selectMutasiJenisAkun");
	var mutasiJenisAkun = mutasiJenisAkunOptions.options[mutasiJenisAkunOptions.selectedIndex].value;

	var thnPemMutPerRek = $('#thnPembukuanMutasiPerRek').val();



	$.ajax({
				type: "POST",
				url: "loadDataPerRekening.php",
				data:{
					"mutasiAwal": mutasiAwal,
					"mutasiMulai": mutasiMulai,
					"mutasiSampai": mutasiSampai,
					"mutasiKelompokAkun": mutasiKelompokAkun,
					"mutasiKodeGolongan": mutasiKodeGolongan,
					"mutasiKodeSubGolongan": mutasiKodeSubGolongan,
					"mutasiJenisAkun": mutasiJenisAkun,
					"thnPemMutPerRek": thnPemMutPerRek
				},
				success: function(data){
					if(data){
						$("#mutasi_per_rek").html(data);
							
						if(mutasiJenisAkun == "semua"){
							if(mutasiKodeSubGolongan == "semua"){
								if(mutasiKodeGolongan == "semua"){
									if(mutasiKelompokAkun == "semua"){
										$('#judulMutasiPerRek').html("MUTASI REKENING TAHUN "+thnPemMutPerRek+", Jurnal Gabungan");
									}else{
										$('#judulMutasiPerRek').html("MUTASI REKENING TAHUN "+thnPemMutPerRek+", Kemlompok Akun " + $( "#selectMutasiKelompokAkun option:selected" ).text());
									}
								}else{
									$('#judulMutasiPerRek').html("MUTASI REKENING TAHUN "+thnPemMutPerRek+", " + $( "#selectMutasiKodeGolongan option:selected" ).text());
								}	
							}else{
								$('#judulMutasiPerRek').html("MUTASI REKENING TAHUN "+thnPemMutPerRek+", " + $( "#selectMutasiKodeSubGolongan option:selected" ).text());
							}
						}else{
							$('#judulMutasiPerRek').html("MUTASI REKENING TAHUN "+thnPemMutPerRek+", " + $( "#selectMutasiJenisAkun option:selected" ).text());
						}

						$("#perTanggalTampilMutasiPerRek").html($("#tglMulaiMutasi").val()+"/"+$("#blnMulaiMutasi").val()+"/"+$("#thnMulaiMutasi").val()+" - "+$("#tglSampaiMutasi").val()+"/"+$("#blnSampaiMutasi").val()+"/"+$("#thnSampaiMutasi").val());
					}
					
				}
			});
}

function loadDataMutasiKas(){
	var mutasiMulaiKas = $("#thnMulaiKas").val()+"/"+$("#blnMulaiKas").val()+"/"+$("#tglMulaiKas").val();
	var mutasiSampaiKas = $("#thnSampaiKas").val()+"/"+$("#blnSampaiKas").val()+"/"+$("#tglSampaiKas").val();
	var thnPembukuanMutasiKas = $('#thnPembukuanMutasiKas').val();
	$.ajax({
		type: "POST",
		url : "loadDataKas.php",
		data:{
			"mutasiMulaiKas": mutasiMulaiKas,
			"mutasiSampaiKas": mutasiSampaiKas,
			"thnPembukuanMutasiKas": thnPembukuanMutasiKas
		},
		success:function(data){
			if(data){
				$("#mutasi_kas").html(data);
				$("#perTanggalMutasiKas").html($("#tglMulaiKas").val()+"/"+$("#blnMulaiKas").val()+"/"+$("#thnMulaiKas").val()+" - "+$("#tglSampaiKas").val()+"/"+$("#blnSampaiKas").val()+"/"+$("#thnSampaiKas").val());
			}
		}
	});
}

function loadDataJurnalMutasi(){
	var mulaiJurnalMutasi = $("#thnMulaiJurnalMutasi").val()+"/"+$("#blnMulaiJurnalMutasi").val()+"/"+$("#tglMulaiJurnalMutasi").val();
	var sampaiJurnalMutasi = $("#thnSampaiJurnalMutasi").val()+"/"+$("#blnSampaiJurnalMutasi").val()+"/"+$("#tglSampaiJurnalMutasi").val();
	$.ajax({
				type: "POST",
				url: "loadJurnalMutasi.php",
				data:{
					"mulaiJurnalMutasi": mulaiJurnalMutasi,
					"sampaiJurnalMutasi": sampaiJurnalMutasi

				},
				success: function(data){
					if(data){
						$("#jurnal_mutasi").html(data);
						
						if($("#blnSampaiJurnalMutasi").val() == 1){
							$("#perBulanJurnalMutasi").html("BULAN : Januari")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 2){
							$("#perBulanJurnalMutasi").html("BULAN : Februari")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 3){
							$("#perBulanJurnalMutasi").html("BULAN : Maret")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 4){
							$("#perBulanJurnalMutasi").html("BULAN : April")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 5){
							$("#perBulanJurnalMutasi").html("BULAN : Mei")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 6){
							$("#perBulanJurnalMutasi").html("BULAN : Juni")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 7){
							$("#perBulanJurnalMutasi").html("BULAN : Juli")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 8){
							$("#perBulanJurnalMutasi").html("BULAN : Agustus")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 9){
							$("#perBulanJurnalMutasi").html("BULAN : September")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 10){
							$("#perBulanJurnalMutasi").html("BULAN : Oktober")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 11){
							$("#perBulanJurnalMutasi").html("BULAN : November")
						}
						else if($("#blnSampaiJurnalMutasi").val() == 12){
							$("#perBulanJurnalMutasi").html("BULAN : Desember")
						}
						$("#perTahunJurnalMutasi").html("TAHUN : "+$("#thnSampaiJurnalMutasi").val())
					}
				}
			});
}

function loadDataMutasiPerAkun(){
	var mutasiMulaiPerAkun = $("#thnMulaiPerAkun").val()+"/"+$("#blnMulaiPerAkun").val()+"/"+$("#tglMulaiPerAkun").val();

	var mutasiSampaiPerAkun = $("#thnSampaiPerAkun").val()+"/"+$("#blnSampaiPerAkun").val()+"/"+$("#tglSampaiPerAkun").val();

	var mutasiKodeAkunOptions = document.getElementById("selectMutasiPerAkun");
	var mutasiKodeAkun = mutasiKodeAkunOptions.options[mutasiKodeAkunOptions.selectedIndex].value;

	//var thnPembukuanMutasiPerAkun = $('#thnPembukuanMutasiPerAkun').val();
	$.ajax({
		type: "POST",
		url : "loadMutasiPerAkun.php",
		data:{
			"mutasiMulaiPerAkun": mutasiMulaiPerAkun,
			"mutasiSampaiPerAkun": mutasiSampaiPerAkun,
			" mutasiKodeAkun" :  mutasiKodeAkun,
			//"thnPembukuanMutasiPerAkun": thnPembukuanMutasiPerAkun
		},
		success:function(data){
			if(data){
				$("#mutasi_per_akun tbody").html(data);
				$("#perNamaMutasiPerAkun").html(mutasiKodeAkun);
				$("#perTahunMutasiPerAkun").html($("#thnSampaiPerAkun").val());
			}
		}
	});
}

function loadDataLaporanPemasukan(){
	var pemasukanMulai=$("#thnMulaiPemasukan").val()+"/"+$("#blnMulaiPemasukan").val()+"/"+$("#tglMulaiPemasukan").val();
	var pemasukanSampai=$("#thnSampaiPemasukan").val()+"/"+$("#blnSampaiPemasukan").val()+"/"+$("#tglSampaiPemasukan").val();
	$.ajax({
				type: "POST",
				url: "loadLaporanPemasukan.php",
				data:{
					"pemasukanMulai": pemasukanMulai,
					"pemasukanSampai": pemasukanSampai
				},
				success: function(data){
					if(data){
						$("#laporan_pemasukan").html(data);
					}
				}
	});
}

function loadDataLaporanPengeluaran(){
	var pengeluaranMulai=$("#thnMulaiPengeluaran").val()+"/"+$("#blnMulaiPengeluaran").val()+"/"+$("#tglMulaiPengeluaran").val();
	var pengeluaranSampai=$("#thnSampaiPengeluaran").val()+"/"+$("#blnSampaiPengeluaran").val()+"/"+$("#tglSampaiPengeluaran").val();
	$.ajax({
				type: "POST",
				url: "loadLaporanPengeluaran.php",
				data:{
					"pengeluaranMulai": pengeluaranMulai,
					"pengeluaranSampai": pengeluaranSampai
				},
				success: function(data){
					if(data){
						$("#laporan_pengeluaran").html(data);
					}
				}
	});
}

function loadDataLaporanPemasukanTahunan(){
	var thnLaporanPemasukanTahunan= $("#thnLapPemasukanTahunan").val();
	$.ajax({
		type: "POST",
		url: "loadLaporanPemasukanTahunan.php",
		data:{
			"thnLaporanPemasukanTahunan": thnLaporanPemasukanTahunan
		},
		success: function(data){
			if(data){
				$("#laporan_pemasukan_tahunan").html(data);
			}
		}
	});
}

function loadLapPengeluaranPerThn(){
	//var pengeluaranMulaiperThn = $("#thnMulaiPengeluaranPerThn").val()+"/"+$("#blnMulaiPengeluaranPerThn").val()+"/"+$("#tglMulaiPengeluaranPerThn").val();

	//var pengeluaranSampaiperThn = $("#thnSampaiPengeluaranPerThn").val()+"/"+$("#blnSampaiPengeluaranPerThn").val()+"/"+$("#tglSampaiPengeluaranPerThn").val();
	var thnPengeluaranPerThn = $('#thnPembukuanPengeluaranPerThn').val();
	$('#loading').attr("hidden",false);
	$.ajax({
		type: "POST",
		 url: "loadPengeluaranPerTahun.php",
		//url: "test.php",
		data: {
			   "thnPengeluaranPerThn": thnPengeluaranPerThn
			},
		success: function(data){
			if(data){
				$('#pengeluaran_per_tahun').html(data);
				$('#loading').attr("hidden",true);
				//$("#blnPengeluaranPerThn").html($("#blnMulaiPengeluaranPerThn").val()+"S/D"+$("#blnSampaiPengeluaranPerThn").val());
			}
			
		}
	});
}

function loadselectTampilJenisAkun(){
	$.ajax({
			type: "POST",
			url: "loadselectTampilPerAkun.php",
			success: function(data){
				if(data){
					$('#selectMutasiPerAkun').html(data);		
				}
			}
	});
}

function loadselectMutasiKelompokAkun(){

	var mutasiKelompokAkunOptions = document.getElementById("selectMutasiKelompokAkun");
	var mutasiKelompokAkun = mutasiKelompokAkunOptions.options[mutasiKelompokAkunOptions.selectedIndex].value;



	$.ajax({
			type: "POST",
			url: "loadselectMutasiKelompokAkun.php",
			data:{"mutasiKelompokAkun": mutasiKelompokAkun},
			success: function(data){
				if(data){
					$('#selectMutasiKodeGolongan').html(data);
					loadselectMutasiKodeGolongan();
					loadselectMutasiKodeSubGolongan();			
				}
			}
	});
}

function loadselectMutasiKodeGolongan(){
	var mutasiKodeGolonganOptions = document.getElementById("selectMutasiKodeGolongan");
	var mutasiKodeGolongan = mutasiKodeGolonganOptions.options[mutasiKodeGolonganOptions.selectedIndex].value;

	$.ajax({
			type: "POST",
			url: "loadselectMutasiKodeGolongan.php",
			data:{"mutasiKodeGolongan": mutasiKodeGolongan},
			success: function(data){
				if(data){
					$('#selectMutasiKodeSubGolongan').html(data);
					loadselectMutasiKodeSubGolongan();				
				}
			}
	});
}

function loadselectMutasiKodeSubGolongan(){
	var mutasiKodeSubGolonganOptions = document.getElementById("selectMutasiKodeSubGolongan");
	var mutasiKodeSubGolongan = mutasiKodeSubGolonganOptions.options[mutasiKodeSubGolonganOptions.selectedIndex].value;

	$.ajax({
			type: "POST",
			url: "loadselectMutasiKodeSubGolongan.php",
			data:{"mutasiKodeSubGolongan": mutasiKodeSubGolongan},
			success: function(data){
				if(data){
					$('#selectMutasiJenisAkun').html(data);			
				}
			}
	});
}

function loadselectMutasiJenisAkun(){
	$.ajax({
			type: "POST",
			url: "loadselectMutasiJenisAkun.php",
			success: function(data){
				if(data){
					$('#selectMutasiJenisAkun').html(data);			
				}
			}
	});
}

function loadselectJenisAkunPersembahanJemaat(){
	// var thnPemMutPerJem = $('#thnPembukuanPerJem').val();
	$.ajax({
			type: "POST",
			url: "loadselectJenisAkunPersembahanJemaat.php",
			success: function(data){
				if(data){
					// alert("ya");
					// alert(data);
					$('#selectLaporanPersembahanJenisAkun').html(data);
								
				}
			}
	});
}

function loadselectPersembahanJemaat(){
	$.ajax({
			type: "POST",
			url: "loadselectPersembahanJemaat.php",
			success: function(data){
				if(data){
					// alert("ya");
					// alert(data);
					$('#selectLaporanPersembahanTiapJemaat').html(data);			
				}
			}
	});
}

function loadLaporanPersembahanJemaat(){
	var perJemMulai = $("#thnMulaiPerJem").val()+"/"+$("#blnMulaiPerJem").val()+"/"+$("#tglMulaiPerJem").val();
	var perJemSampai = $("#thnPerJem").val()+"/"+$("#blnPerJem").val()+"/"+$("#tglPerJem").val();
	
	var persembahanJemaatTerpilihOptions = document.getElementById("selectLaporanPersembahanTiapJemaat");
	var persembahanJemaatTerpilih = persembahanJemaatTerpilihOptions.options[persembahanJemaatTerpilihOptions.selectedIndex].value;
	
	var persembahanJenisAkunTerpilihOptions = document.getElementById("selectLaporanPersembahanJenisAkun");
	var persembahanJenisAkunTerpilih = persembahanJenisAkunTerpilihOptions.options[persembahanJenisAkunTerpilihOptions.selectedIndex].value;
	$.ajax({
				type: "POST",
				url: "loadPersembahanJemaat.php",
				data: {
					"perJemMulai": perJemMulai,
					"perJemSampai": perJemSampai,
					"persembahanJemaatTerpilih": persembahanJemaatTerpilih,
					"persembahanJenisAkunTerpilih": persembahanJenisAkunTerpilih
				},
				success: function(data){
					if(data){
						// alert(data);
						$("#laporanPerJemaat").html(data);
						if(persembahanJemaatTerpilih == "semua"){
							if(persembahanJenisAkunTerpilih == "semua"){
								$('#judulPerJemaat').html("LAPORAN PERSEMBAHAN JEMAAT");
							}else{
								$('#judulPerJemaat').html("LAPORAN PERSEMBAHAN JEMAAT, " + $("#selectLaporanPersembahanJenisAkun option:selected").text());
							}
						}else{
							if(persembahanJenisAkunTerpilih == "semua"){
								$('#judulPerJemaat').html("LAPORAN PERSEMBAHAN JEMAAT, " + $( "#selectLaporanPersembahanTiapJemaat option:selected").text());
							}else{
								$('#judulPerJemaat').html("LAPORAN PERSEMBAHAN JEMAAT, " + $( "#selectLaporanPersembahanTiapJemaat option:selected").text() + ", " + $("#selectLaporanPersembahanJenisAkun option:selected").text());
							}
						}
						
						$("#perTanggalTampilPerJemaat").html($("#tglMulaiPerJem").val()+"/"+$("#blnMulaiPerJem").val()+"/"+$("#thnMulaiPerJem").val()+" - "+$("#tglPerJem").val()+"/"+$("#blnPerJem").val()+"/"+$("#thnPerJem").val());
					}
					
				}
			});
}



function printData(div,ket1,ket2,ket3)
{
	$.ajax({
			type: "POST",
			url: "loadKop.php",
			success: function(data){
				if(data){
				   var divToPrint=document.getElementById(div);
				   newWin= window.open("");
				   newWin.document.write(data+"<br> <br> <br> <br> <br> <br><label>"
				   							+$('#'+ket1).text()+"</label><br><label>"+$('#'+ket2).text()
				   						+' '+$('#'+ket3).text()+"</label> <br>"+"<br>"+"<br>"+divToPrint.outerHTML);
				   newWin.print();
				   newWin.close();			
				}
			}
	});
   
}

function printDataRen(div,ket1,ket2)
{

	$.ajax({
			type: "POST",
			url: "loadKop.php",
			success: function(data){
				if(data){
				   var divToPrint=document.getElementById(div);
				   newWin= window.open("");
				   newWin.document.write(data
				   						+"<br> <br> <br> <br> <br> <br><label>"
				   							+$('#'+ket1).text()+" "+$('#'+ket2).text()
				   						+"</label> <br>"+"<br>"+"<br>"+divToPrint.outerHTML);
				   newWin.print();
				   newWin.close();			
				}
			}
	});
   
}

function printDiv(divName) {
     var divToPrint=document.getElementById("printTable");
	 newWin= window.open("");
	 newWin.document.write(divToPrint.outerHTML);
	 newWin.print();
	 newWin.close();
}

function exportLaporanBukBek(div1,div2,select,ket1,ket2,ket3){
	var valueExRenAng = $('#'+select+' option:selected').val();
	 if(valueExRenAng == "XLS"){
	 	tableToExcelBukuBesar(div1,div2,'Table',ket1,ket2,ket3);
	 }else if(valueExRenAng == "PDF"){
	 	convertToPdfDataBukBes('#'+div1,'#'+div2,ket1,ket2,ket3);
	 }else if(valueExRenAng == "CSV"){
	 	$('#'+div).tableExport({type:'csv',escape:'false'});
	 }
	 $('#'+select).val('');
}

function exportLaporan(div,select,ket1,ket2,ket3){
	var valueExRenAng = $('#'+select+' option:selected').val();
	 if(valueExRenAng == "XLS"){
	 	
	 	tableToExcelLap(div,'Table',ket1,ket2,ket3);

	 }else if(valueExRenAng == "PDF"){
	 	convertToPdfData('#'+div,ket1,ket2,ket3);
	 }else if(valueExRenAng == "CSV"){
	 	$('#'+div).tableExport({type:'csv',escape:'false'});
	 }

	 $('#'+select).val('');
}

function convertToPdfDataBukBes(div1,div2,ket1,ket2,ket3){
	tamp1 = $('#'+ket1).text();
	tamp2 = $('#'+ket2).text();
	tamp3 = $('#'+ket3).text();	

	var columns_aktiva = [];
	var rows_aktiva = [];
	var columns_passiva = [];
	var rows_passiva = [];
	
	var img = new Image();
	img.src = 'image/logoukdw.png';
	var ext;


		

	var panjangTabelAktiva = document.getElementById("buku_besar_aktiva").rows.length;
	var	panjangTabelPassiva = document.getElementById("buku_besar_pasiva").rows.length;

	$.each( $(div1+' tr'), function (i, row){
		var tamp = [];
        $.each( $(row).find("td, th"), function(j, cell){
            var txt = $(cell).text();
           	tamp[j] = txt
        });
        if(i == 0){
        	columns_aktiva = tamp;
        }else if(i == panjangTabelAktiva-1){
        	var sum = [" "];
        	rows_aktiva[i] =  sum.concat(tamp);
        }else{
        	rows_aktiva[i] = tamp;	
        }
         
    });

	$.each( $(div2+' tr'), function (i, row){
		var tamp = [];
        $.each( $(row).find("td, th"), function(j, cell){
            var txt = $(cell).text();
           	tamp[j] = txt
        });
        if(i == 0){
        	columns_passiva = tamp;
        }else if(i == panjangTabelPassiva-1){
        	var sum = [" "];
        	rows_passiva[i] =  sum.concat(tamp);
        }else{
        	rows_passiva[i] = tamp;	
        }
         
    });



	var doc = new jsPDF('p', 'pt');

	var status;
	var namaGereja;
	var alamatGereja;
	var teleponGereja;
	var emailGereja;
	var logoGereja;
	$.ajax({
		type: "POST",
		url: "loadUntukKopPdf.php",
		dataType: "json",
		success: function(data){

			if(data){
					namaGereja = String(data[0][0]);
					alamatGereja = data[0][1];
					teleponGereja = data[0][2];
					emailGereja = data[0][3];
					var d = new Date();
					img.src = document.getElementById('imgGambarHeaderGereja').src;
					ext = data[0][4].substring(12);
					

			        doc.setFontSize(10);
			        doc.setTextColor(40);
			        doc.setFontStyle('bold');
			        if(data[0][5] == 1){
			        	doc.addImage(img, ext.toUpperCase(), 40, 40, 70, 70);
			        }else if(data[0][5] == 0){
			        	img.src = 'image/logoukdw.png';
			        	doc.addImage(img, 'PNG', 40, 40, 70, 70);
			        }
			        doc.text(namaGereja, 20 + 100, 50);
			        
			        doc.setFontSize(9);
			        doc.text(alamatGereja, 20 + 100, 70);
			        doc.text(emailGereja, 20 + 100, 90);
			        doc.text(teleponGereja, 20 + 100, 110);

			        doc.setFontSize(10);
			        doc.setTextColor(40);
			        doc.setFontStyle('bold');

			        doc.text(tamp1, 40, 160);
			    	doc.text(tamp2 +" "+ tamp3, 40, 180);

			    	doc.setFontStyle('normal');
			    	doc.autoTable(columns_aktiva, rows_aktiva, {
			            startY: 200,
			            theme: 'grid',
			            pageBreak: 'avoid',
			        });

			        doc.autoTable(columns_passiva, rows_passiva, {
			            startY: doc.autoTableEndPosY() + 30,
			            theme: 'grid',
			            pageBreak: 'avoid',
			        });
					
					doc.save('bukuBesar.pdf');
			}		
		}
	});
}

function convertToPdfData(div,ket1,ket2,ket3){

	tamp1 = $('#'+ket1).text();
	tamp2 = $('#'+ket2).text();
	tamp3 = "";
	if(ket3 != ''){
		tamp3 = $('#'+ket3).text();	
	}

	var doc = new jsPDF('p', 'pt');

	var columns = [];
	var rows = [];
	var img = new Image();
	img.src = 'image/logoukdw.png';
	

	if(div == '#mutasi_per_rek' || div == '#LaporanPerRekening'){
			if(div == '#mutasi_per_rek'){
				var mutasiKelompokAkunOptions = document.getElementById("selectMutasiKelompokAkun");
				var mutasiKelompokAkun = mutasiKelompokAkunOptions.options[mutasiKelompokAkunOptions.selectedIndex].value;
				if(mutasiKelompokAkun == "semua"){
					columns = ["No","No Bukti","Tgl","No Rekg","Uraian","Debet","Kredit"];	
				}else{
					columns = ["No","No Bukti","Tgl","No Rekg","Uraian","Debet","Kredit","Saldo"];
				}
				
			}else{
				columns = ["No","Kode","Nama Rek","Anggaran Tahunan","Realisasi","% Real","Sisa","% Sisa"];
			}

			$.each( $(div+' tr'), function (i, row){
				var tamp = [];
		        $.each( $(row).find("td, th"), function(j, cell){
		            var txt = $(cell).text();
		           	tamp[j] = txt;
		        });
		        if(i >= 2){
		        	rows[i] = tamp;	
		        }
		         
		    });
	}else if(div == '#laporanPenInduvidu' || div == '#laporanPerJemaat'){
		
		var panjangTabel;

		doc = new jsPDF('l', 'pt');

		if(div == '#laporanPenInduvidu'){
			panjangTabel = document.getElementById("laporanPenInduvidu").rows.length;
		}else if(div == '#laporanPerJemaat'){
			panjangTabel = document.getElementById("laporanPerJemaat").rows.length;
		}

		$.each( $(div+' tr'), function (i, row){
			var tamp = [];
	        $.each( $(row).find("td, th"), function(j, cell){
	            var txt = $(cell).text();
	           	tamp[j] = txt;
	        });
	        if(i == 0){
	        	columns = tamp;
	        }else if(i == panjangTabel-1){
	        	var sum = [" "," "," "," "," ", " "];
	        	rows[i] =  sum.concat(tamp);
	        }else{
	        	rows[i] = tamp;	
	        }
	         
	    });

	}else if(div == '#rencanaAnggaran' || div == '#rekeningSaldo'){
		
		$.each( $(div+' tr'), function (i, row){
			var tamp = [];
	        $.each( $(row).find("td, th"), function(j, cell){
	            var txt;
	            if(i == 0) txt = $(cell).text().trim();
	            else txt = $(cell).text();
	           	tamp[j] = txt;
	        });
	        if(i == 0){
	        	columns = tamp;
	        }else{
	        	rows[i] = tamp;	
	        }
	         
	    });

	}else{
		$.each( $(div+' tr'), function (i, row){
			var tamp = [];
	        $.each( $(row).find("td, th"), function(j, cell){
	            var txt = $(cell).text().trim();
	           	tamp[j] = txt;
	        });
	        if(i == 0){
	        	columns = tamp;
	        }else{
	        	rows[i] = tamp;	
	        }
	         
	    });
	}
		


	

	var status;
	var namaGereja;
	var alamatGereja;
	var teleponGereja;
	var emailGereja;
	var logoGereja;
	var ext;
	var d = new Date();
	$.ajax({
		type: "POST",
		url: "loadUntukKopPdf.php",
		dataType: "json",
		success: function(data){

			if(data){
					namaGereja = String(data[0][0]);
					alamatGereja = data[0][1];
					teleponGereja = data[0][2];
					emailGereja = data[0][3];
					img.src = document.getElementById('imgGambarHeaderGereja').src;
					ext = data[0][4].substring(12);

			        doc.setFontSize(10);
			        doc.setTextColor(40);
			        doc.setFontStyle('bold');
			        if(data[0][5] == 1){
			        	doc.addImage(img, ext.toUpperCase(), 40, 40, 70, 70);
			        }else if(data[0][5] == 0){
			        	img.src = 'image/logoukdw.png';
			        	doc.addImage(img, 'PNG', 40, 40, 70, 70);
			        }
			        doc.text(namaGereja, 20 + 100, 50);
			        
			        doc.setFontSize(9);
			        doc.text(alamatGereja, 20 + 100, 70);
			        doc.text(emailGereja, 20 + 100, 90);
			        doc.text(teleponGereja, 20 + 100, 110);

			        doc.setFontSize(10);
			        doc.setTextColor(40);
			        doc.setFontStyle('bold');
			        if(div == '#rencanaAnggaran'){
			        	doc.text(tamp1+" "+tamp2, 40, 160);
			    		
			        }else{
			        	doc.text(tamp1, 40, 160);
			    		doc.text(tamp2 +" "+ tamp3, 40, 180);
			        }

			        doc.setFontStyle('normal');
					doc.autoTable(columns, rows, {startY: 200,theme: 'grid'});


					doc.save(div.substr(1)+'.pdf');
			}		
		}
	});

	
}
