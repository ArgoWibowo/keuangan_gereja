$("#formpenerimaan").ready(function(){
	uniform("formpenerimaan", 300);

	$("#nokwitansi").keyup(function(){
		if($("#nokwitansi").val() == "") $("#nokwitansi_cek").children().hide();
		else{
			$.ajax({
				url: "cekPrimaryKey.php",
				type: "POST",
				data: {
					"nokwitansi" : $("#nokwitansi").val()
				},
				success: function(result){
					//alert(result);
					if(result == 1){
						$("#nokwitansi_cek").children().first().show();
						$("#nokwitansi_cek").children().last().hide();
					} else {
						$("#nokwitansi_cek").children().first().hide();
						$("#nokwitansi_cek").children().last().show();
					}
				}
			});
		}
	});

	$("#penerimaan_tanggal").focusout(function(){
		var objectd = $("#penerimaan_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#penerimaan_bulan").focusout(function(){
		var m = $("#penerimaan_bulan").val();
		if(m > 12 || m < 1) {
			$("#penerimaan_bulan").val("");
			m = null;
		}
		if(m == null) $("#penerimaan_tanggal").attr("max", 31);
		else if(m == 2) $("#penerimaan_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#penerimaan_tanggal").attr("max", 31);
			else $("#penerimaan_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#penerimaan_tanggal").attr("max", 31);
			else $("#penerimaan_tanggal").attr("max", 30);
		}
		$("#penerimaan_tahun").triggerHandler("focusout");
		$("#penerimaan_tanggal").triggerHandler("focusout");
	});

	$("#penerimaan_tahun").focusout(function(){
		var m = $("#penerimaan_bulan").val(); 
		var y = $("#penerimaan_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#penerimaan_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#penerimaan_tanggal").attr("max", 29);
				} else {
					$("#penerimaan_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#penerimaan_tanggal").attr("max", 29);
				} else {
					$("#penerimaan_tanggal").attr("max", 28);
				}
			}
			$("#penerimaan_tanggal").triggerHandler("focusout");
		}
	});

	$("#untuktahun").focusout(function(){
		var y = $("#untuktahun").val();
		if(y < 2000 || y > 9999) {
			$("#untuktahun").val("");
		}
	});

	$("#formpenerimaan").submit(function(){
		var nokwitansi = $("#nokwitansi").val();
		var tanggalpenerimaan = $("#penerimaan_tahun").val()+"-"+$("#penerimaan_bulan").val()+"-"+$("#penerimaan_tanggal").val();
		var kredit = $("select.selectkreditpenerimaan").val().replace("_dot_", ".");
		var pemberi = $("select.selectpemberi").val().replace("_dot_", ".");
		var jumlah = $("#penerimaan_jumlah").val().replace(/\./g, "");
		var penerima = $("select.selectpenerimaindividu").val();
		var untukbulan = $("#untukbulan").val();
		var untuktahun = $("#untuktahun").val();
		
		//alert(nokwitansi+"\n"+tanggalpenerimaan+"\n"+kredit+"\n"+pemberi+"\n"+jumlah+"\n"+penerima+"\n"+untukbulan+"\n"+untuktahun);

		$.ajax({
			url: "penerimaan_proses.php",
			type: "POST",
			data: {
				"nokwitansi" : nokwitansi,
				"tanggalpenerimaan" : tanggalpenerimaan,
				"kredit" : kredit,
				"pemberi" : pemberi,
				"jumlah" : jumlah,
				"penerima" : penerima,
				"untukbulan" : untukbulan,
				"untuktahun" : untuktahun
			},
			success: function(result){
				//alert(result);
				if(result == true){
					alert("berhasil menyimpan!");
					//$("#right>div.penerimaanindividu input[type=reset]").click();
					$("#nokwitansi").val("");
					$("select.selectkreditpenerimaan").val("");
					$("#penerimaan_jumlah").val("");
					placeholderselect();
				} else {
					alert("gagal menyimpan!");
				}
			}
		});
		return false;
	});

	$("#right>div.penerimaanindividu .trick").focusin(function(){
		var i = $(this).attr("id");
		refreshSelectPenerimaan($("select."+i));
	});

});

function refreshSelectPenerimaan(obj){
	var value = obj.val();
	var type;
	var c = obj.attr('class');
	if(c == "selectkreditpenerimaan") c = "rekeningindividu";
	else if(c == "selectpemberi") c = "jemaat";
	else if(c == "selectpenerimaindividu") c = "pj_penerima"; 
	$.ajax({
        type	: 'POST',
        url		: 'refreshSelectFunction.php',
        data 	: {
        	'type' : c
        },
        success: function(result) {
        	obj.html(result);
        	if(obj.children("[value="+value+"]").length > 0){
        		obj.val(value);
        	} else placeholderselect();
        }
    });	
}