$("#dataPenerimaan").ready(function(){
	$("#formEditPenerimaan").hide();
	$("#isiPenerimaanIndividu").on("change", ".optionDataPenerimaan", function(){
		if($(this).val() == "ubah"){
			$("#dataPenerimaan").slideUp("700");
			$("#formEditPenerimaan").slideDown("700");

			var p = $(this).parent().parent();
			$("#nokwitansiedit").val(p.children(":nth-child(1)").html());
			var tanggal = p.children(":nth-child(2)").html().split("-");
			$("#penerimaanedit_tahun").val(parseInt(tanggal[2]));
			$("#penerimaanedit_bulan").val(parseInt(tanggal[1]));
			$("#penerimaanedit_tanggal").val(parseInt(tanggal[0]));
			$("select.selectkreditpenerimaanedit").val(p.children(":nth-child(3)").html().split(" - ")[0].replace(".", "_dot_"));
			$("select.selectkreditpenerimaanedit").change();
			$("select.selectpemberiedit").val(p.children(":nth-child(4)").html().split(" - ")[0].replace(".", "_dot_"));
			$("select.selectpemberiedit").change();
			$("#penerimaanedit_jumlah").val(p.children(":nth-child(6)").html());
			$("select.selectpenerimaindividuedit").val(p.children(":nth-child(7)").html().replace(".", "_dot_"));
			$("select.selectpenerimaindividuedit").change();
			var keterangan = p.children(":nth-child(8)").html().split(" tahun ");
			$("#untukbulanedit").val(keterangan[0]);
			$("#untuktahunedit").val(parseInt(keterangan[1]));
		} else if($(this).val() == "hapus"){
			$(this).val("");
			if(confirm("Apakah anda yakin ingin menghapus penerimaan ini?")){
				var p = $(this).parent().parent();
				$.ajax({
					url: "hapuspenerimaan_proses.php",
					type: "POST",
					data: {
						"nokwitansi" : p.children(":nth-child(1)").html(),
						"jumlah" : p.children(":nth-child(6)").html()
					},
					success: function(result){
						// alert(result);
						if(result == true){
							alert("berhasil menghapus!");
							$("li.datapenerimaanindividu").click();
						} else {
							alert("gagal menghapus!");
						}
					}
				});
			}
		}
		$(this).val("");
	});

	$("li.datapenerimaanindividu").click(function(){
		$.ajax({
	        type	: 'POST',
	        url		: 'refreshTableFunction.php',
	        data 	: {
	        	'type' : 'isiPenerimaanIndividu'
	        },
	        success: function(result) {
	        	// alert(result);
	        	$("#formEditPenerimaan").hide();
	        	$("#dataPenerimaan").slideDown("700");
				$("#isiPenerimaanIndividu").html(result);
	        }
        });
	});

	uniform("formEditPenerimaan", 300);

	$("#penerimaanedit_tanggal").focusout(function(){
		var objectd = $("#penerimaanedit_tanggal");
		if(objectd.val() > parseInt(objectd.attr("max")) || objectd.val() < 1) objectd.val("");
	});

	$("#penerimaanedit_bulan").focusout(function(){
		var m = $("#penerimaanedit_bulan").val();
		if(m > 12 || m < 1) {
			$("#penerimaanedit_bulan").val("");
			m = null;
		}
		if(m == null) $("#penerimaanedit_tanggal").attr("max", 31);
		else if(m == 2) $("#penerimaanedit_tanggal").attr("max", 28);
		else if(m < 8) {
			if(m%2 == 1) $("#penerimaanedit_tanggal").attr("max", 31);
			else $("#penerimaanedit_tanggal").attr("max", 30);
		} else {
			if(m%2 == 0) $("#penerimaanedit_tanggal").attr("max", 31);
			else $("#penerimaanedit_tanggal").attr("max", 30);
		}
		$("#penerimaanedit_tahun").triggerHandler("focusout");
		$("#penerimaanedit_tanggal").triggerHandler("focusout");
	});

	$("#penerimaanedit_tahun").focusout(function(){
		var m = $("#penerimaanedit_bulan").val(); 
		var y = $("#penerimaanedit_tahun").val();
		if(y < 2000 || y > 9999) {
			$("#penerimaanedit_tahun").val("");
			y = null;
		}
		if(m == 2){
			if(y % 100 == 0){
				if(y % 400 == 0){
					$("#penerimaanedit_tanggal").attr("max", 29);
				} else {
					$("#penerimaanedit_tanggal").attr("max", 28);
				}
			} else {
				if(y % 4 == 0){
					$("#penerimaanedit_tanggal").attr("max", 29);
				} else {
					$("#penerimaanedit_tanggal").attr("max", 28);
				}
			}
			$("#penerimaanedit_tanggal").triggerHandler("focusout");
		}
	});

	$("#untuktahunedit").focusout(function(){
		var y = $("#untuktahunedit").val();
		if(y < 2000 || y > 9999) {
			$("#untuktahunedit").val("");
		}
	});

	$("#formEditPenerimaan button").click(function(){
		$("li.datapenerimaanindividu").click();
	});

	$("#formEditPenerimaan").submit(function(){
		var nokwitansi = $("#nokwitansiedit").val();
		var tanggalpenerimaan = $("#penerimaanedit_tahun").val()+"-"+$("#penerimaanedit_bulan").val()+"-"+$("#penerimaanedit_tanggal").val();
		var kredit = $("select.selectkreditpenerimaanedit").val().replace("_dot_", ".");
		var pemberi = $("select.selectpemberiedit").val().replace("_dot_", ".");
		var jumlah = $("#penerimaanedit_jumlah").val().replace(/\./g, "");
		var penerima = $("select.selectpenerimaindividuedit").val();
		var untukbulan = $("#untukbulanedit").val();
		var untuktahun = $("#untuktahunedit").val();
		if(untukbulan == "") untukbulan = null;
		if(untuktahun == "") untuktahun = null;
		
		// alert(nokwitansi+"\n"+tanggalpenerimaan+"\n"+kredit+"\n"+pemberi+"\n"+jumlah+"\n"+penerima+"\n"+untukbulan+"\n"+untuktahun);

		$.ajax({
			url: "editpenerimaan_proses.php",
			type: "POST",
			data: {
				"nokwitansi" : nokwitansi,
				"tanggalpenerimaan" : tanggalpenerimaan,
				"kredit" : kredit,
				"pemberi" : pemberi,
				"jumlah" : jumlah,
				"penerima" : penerima,
				"untukbulan" : untukbulan,
				"untuktahun" : untuktahun
			},
			success: function(result){
				// alert(result);
				if(result == true){
					alert("berhasil mengubah!");
					$("li.datapenerimaanindividu").click();
				} else {
					alert("gagal mengubah!");
				}
			}
		});
		return false;
	});

	$("#formEditPenerimaan select").focusin(function(){
		refreshSelectEditPenerimaan($(this));
	});

	$("#formEditPenerimaan .trick").focusin(function(){
		var i = $(this).attr("id");
		refreshSelectEditPenerimaan($("select."+i));
	});
});

function refreshSelectEditPenerimaan(obj){
	var value = obj.val();
	var type;
	var c = obj.attr('class');
	if(c == "selectkreditpenerimaanedit") c = "rekeningindividu";
	else if(c == "selectpemberiedit") c = "jemaat";
	else if(c == "selectpenerimaindividuedit") c = "pj_penerima"; 
	$.ajax({
        type	: 'POST',
        url		: 'refreshSelectFunction.php',
        data 	: {
        	'type' : c
        },
        success: function(result) {
        	obj.html(result);
        	if(obj.children("[value="+value+"]").length > 0){
        		obj.val(value);
        	} else placeholderselect();
        }
    });	
}

function searchPenerimaan(){
	var keyword = $('#searchPenerimaan').val();
	var sortby  = 'tanggal_penerimaan';
  	$.ajax({
        type	: 'POST',
        url		: 'ajax_datapenerimaan.php',
        data 	: 'actionfunction=showData&page=1&keyword='+keyword+'&sortby='+sortby,
        cache 	: false,
        success: function(result) {
        	var a = result.indexOf("<");
        	result = result.substring(a);
			$('#isiPenerimaanIndividu').html(result);
			$.ajax({
			    url:"ajax_datapenerimaan.php",
		        type:"POST",
		        data:"actionfunction=pagination&page=1&keyword="+keyword+'&sortby='+sortby,
		        cache: false,
		        success: function(response){
				}
	   		});
	   		/*$("#right>div.datatoko").slideDown("700");
				$("#right>div.datatoko div").slideDown("700");*/
        },
    });
}