<?php  
	include 'koneksi2.php';
	include 'koneksi.php';
?>	
	<div id="divTitle">
		<label class="lblTitle">DATA TOKO</label>
	</div>
	<div id="navJumlahDataToko" class="navJumlahData">
		<label>Jumlah Data : </label> <label id="lblJumlahDataToko" class="lblJumlahDataToko" ></label>
	</div>
	<div id="navSearchToko" class="navSearch">
		<ul class="nav">
			<li class='search'>
				<div class='divSearch'>
					<input name ="kataKunciToko" onchange="searchToko()" onkeyup="searchToko()"  id='searchToko' class='searchToko' type='text' placeholder="Cari Nama Toko">
				</div>
			</li>
			<li class="search">
				<div class='divSearch'>
					<button id="btnDropPencarian">Pencarian Lanjutan</button>
				</div>
			</li>
		</ul>
		
	</div>
	<div id="navSortToko" class="navSort" >
		<label>Urutkan berdasarkan : </label>
		<select id='sortDataToko' class="sortData" onchange=searchToko()>
		 	<option value='kode_toko'>Kode</option>
		 	<option value='nama_toko'>Nama</option>
		 	<option value='alamat_toko'>Alamat</option>
		 	<option value='no_telepon_toko'>No Telepon</option>
		 	<option value='cp_toko'>CP Toko</option>		 	
		</select>
	</div>
	<div id="divBoxPencarian" class="divBoxPencarian" >
		<form id="formCariToko" name="formCariToko" method="post" enctype="multipart/form-data">
			<table class="tabelData" id="tablePencarian">
				<tr><td colspan="2"><label>Pencarian Berdasarkan</label></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td class="tdLabelTablePencarian"><label class="inputPencarian">Kode Toko</label></td>
					<td class="tdInputTablePencarian"><input name ="kataKunciKodeToko"  id='kataKunciKodeToko' class='searchToko' type='text' placeholder="Cari Kode Toko"></td>
					<td class="tdLabelTablePencarian"><label class="inputPencarian">Nama Toko</label></td>
					<td class="tdInputTablePencarian"><input name ="kataKunciNamaToko"  id='kataKunciNamaToko' class='searchToko' type='text' placeholder="Cari Nama Toko"></td>
					<td class="tdLabelTablePencarian"><label class="inputPencarian">Alamat Toko</label></td>
					<td class="tdInputTablePencarian"><input name ="kataKunciAlamatToko"  id='kataKunciAlamatToko' class='searchToko' type='text' placeholder="Cari Alamat Toko"></td>
					
				</tr>
				<tr>
					<td class="tdLabelTablePencarian"><label class="inputPencarian">No Telepon Toko</label></td>
					<td class="tdInputTablePencarian"><input name ="kataKunciNoTeleponToko"  id='kataKunciNoTeleponToko' class='searchToko' type='text' placeholder="Cari No Telepon Toko"></td>
					<td class="tdLabelTablePencarian"><label class="inputPencarian">CP Toko</label></td>
					<td class="tdInputTablePencarian"><input name ="kataKunciCPToko"  id='kataKunciCPToko' class='searchToko' type='text' placeholder="Cari CP Toko"></td>
					<td></td>
					<td><input type="submit" value="Cari"  id="btnPencarian" name="cariToko" ></td>
				</tr>
			</table>
		</form>
	</div>
	
	<div id="datatoko" class="dataTable">
		<table id="tabletoko" class="tabelData striped" style="padding :20px 20px; align:"center";">
			<thead>
				<tr >
					<th id="nomortabeltoko" style="text-align:left">No</th>
					<th id="kodetabeltoko" class="kodetabeltoko"  style="text-align:left">Kode Toko</th>
					<th id="namatabeltoko" class="namatabeltoko"  style="text-align:left">Nama Toko</th>
					<th id="alamattabeltoko" class="alamattabeltoko" style="text-align:left">Alamat Toko</th>
					<th id="telpontabeltoko" class="telepontabeltoko" style="text-align:left">No Telepon</th>
					<th id="cptabeltoko" class="cptabeltoko" style="text-align:left">CP Toko</th>
					<?php
						$admin = $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'TULIS';
		 
						if ($admin){
					?>
						<th id="actiontabeltoko" class="actiontabeltoko" style="text-align:left">Pilihan</th>
					<?php 
						} 
					?>

				</tr>	
			</thead>
			<tbody id ="isiTabelToko" class="isiTabel">
			<tbody>
		</table>
	</div>
	<div id="paginationToko" class="pagination" cellspacing="0">
	</div>

	<div id="divOptionDataToko" class="divOptionDataToko">
		<?php
			$admin = $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'BACATULIS' || $_SESSION['0t0rit4s_Adm1n1s7r451_K3uan94n_G3r3j4'] == 'TULIS';
		  
			if ($admin){
		?>
			<button id="btnTambahToko2" onclick="tambahToko();">Tambah Toko</button>
		<?php 
			} 
		?>
		
		<input type="button"  class="submit_button" onclick="printDiv2('toko')" value="Cetak" style="margin-left:0px; width:100px; height:25px;" />
		<label id="lblExportToko">Export : </label>
		<select id='selectExportDataToko' class="selectExportDataToko" onchange=exportToko()>
			<option value=''>Pilih</option>
		 	<option value='XLS'>XLS</option>
		 	<option value='CSV'>CSV</option>
		 	<option value='PDF'>PDF</option>
		</select>
		
	</div>


<!-- 	<form name="export" method="post">
    	<input type="submit" value="Export CSV" name="submit">
	</form>
	 
	<?php
	
	// if(isset($_POST["submit"]))
	// {
	// 	include('exportCSV.php');
	// 	ExportExcel("csv");
	// }
	 
	?>
 -->