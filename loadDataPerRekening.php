<?php
session_start();
include("koneksi.php");

		$sql;
		$jmlKredit = 0;
		$jmlDebet = 0;

		if($_POST['mutasiKelompokAkun'] == 'semua'){
			$sql = "select * from ((select  (select MAX(kode_transaksi) FROM tbl_transaksi t3 WHERE t3.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi) hidden,999999999 kode_transaksi, tbl_bukti_transaksi.kode_bukti_transaksi, tbl_bukti_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_bukti_transaksi.keterangan, tbl_bukti_transaksi.jumlah, tbl_bukti_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
								   from tbl_bukti_transaksi
								   inner join tbl_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi)
								   UNION
								   (select  tbl_transaksi.kode_transaksi hidden, tbl_transaksi.kode_transaksi, tbl_transaksi.kode_bukti_transaksi, tbl_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_transaksi.uraian, tbl_transaksi.jumlah, tbl_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
							 	   from tbl_transaksi
								   inner join tbl_bukti_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi))
					AS transaksi_data
					NATURAL JOIN tbl_jenis_akun
					NATURAL JOIN tbl_gol_akun
					NATURAL JOIN tbl_sub_gol_akun
					WHERE tanggal_bukti_transaksi <= '".$_POST['mutasiSampai']."'
					AND tahun_pembukuan = '".$_POST['thnPemMutPerRek']."'
					AND transaksi_data.waktu_posting != 'NULL'
					order by hidden ASC, kode_transaksi ASC, kode_bukti_transaksi ASC";

		}else{
			if($_POST['mutasiKodeGolongan'] == 'semua'){
				$sql = "select * from ((select  (select MAX(kode_transaksi) FROM tbl_transaksi t3 WHERE t3.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi) hidden,999999999 kode_transaksi, tbl_bukti_transaksi.kode_bukti_transaksi, tbl_bukti_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_bukti_transaksi.keterangan, tbl_bukti_transaksi.jumlah, tbl_bukti_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
								   from tbl_bukti_transaksi
								   inner join tbl_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi)
								   UNION
								   (select  tbl_transaksi.kode_transaksi hidden, tbl_transaksi.kode_transaksi, tbl_transaksi.kode_bukti_transaksi, tbl_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_transaksi.uraian, tbl_transaksi.jumlah, tbl_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
							 	   from tbl_transaksi
								   inner join tbl_bukti_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi))
					AS transaksi_data
					NATURAL JOIN tbl_jenis_akun
					NATURAL JOIN tbl_gol_akun
					NATURAL JOIN tbl_sub_gol_akun
						WHERE kelompok_akun='".$_POST['mutasiKelompokAkun']."'";
			}else{
				if($_POST['mutasiKodeSubGolongan'] == 'semua'){
					$sql = "select * from ((select  (select MAX(kode_transaksi) FROM tbl_transaksi t3 WHERE t3.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi) hidden,999999999 kode_transaksi, tbl_bukti_transaksi.kode_bukti_transaksi, tbl_bukti_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_bukti_transaksi.keterangan, tbl_bukti_transaksi.jumlah, tbl_bukti_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
								   from tbl_bukti_transaksi
								   inner join tbl_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi)
								   UNION
								   (select  tbl_transaksi.kode_transaksi hidden, tbl_transaksi.kode_transaksi, tbl_transaksi.kode_bukti_transaksi, tbl_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_transaksi.uraian, tbl_transaksi.jumlah, tbl_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
							 	   from tbl_transaksi
								   inner join tbl_bukti_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi))
					AS transaksi_data
					NATURAL JOIN tbl_jenis_akun
					NATURAL JOIN tbl_gol_akun
					NATURAL JOIN tbl_sub_gol_akun
							WHERE kode_golongan='".$_POST['mutasiKodeGolongan']."'";
				}else{
					if($_POST['mutasiJenisAkun'] == 'semua'){
						$sql = "select * from ((select  (select MAX(kode_transaksi) FROM tbl_transaksi t3 WHERE t3.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi) hidden,999999999 kode_transaksi, tbl_bukti_transaksi.kode_bukti_transaksi, tbl_bukti_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_bukti_transaksi.keterangan, tbl_bukti_transaksi.jumlah, tbl_bukti_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
								   from tbl_bukti_transaksi
								   inner join tbl_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi)
								   UNION
								   (select  tbl_transaksi.kode_transaksi hidden, tbl_transaksi.kode_transaksi, tbl_transaksi.kode_bukti_transaksi, tbl_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_transaksi.uraian, tbl_transaksi.jumlah, tbl_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
							 	   from tbl_transaksi
								   inner join tbl_bukti_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi))
					AS transaksi_data
					NATURAL JOIN tbl_jenis_akun
					NATURAL JOIN tbl_gol_akun
					NATURAL JOIN tbl_sub_gol_akun
								WHERE kode_sub_gol_akun='".$_POST['mutasiKodeSubGolongan']."'";
					}else{
						$sql = "select * from ((select  (select MAX(kode_transaksi) FROM tbl_transaksi t3 WHERE t3.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi) hidden,999999999 kode_transaksi, tbl_bukti_transaksi.kode_bukti_transaksi, tbl_bukti_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_bukti_transaksi.keterangan, tbl_bukti_transaksi.jumlah, tbl_bukti_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
								   from tbl_bukti_transaksi
								   inner join tbl_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi)
								   UNION
								   (select  tbl_transaksi.kode_transaksi hidden, tbl_transaksi.kode_transaksi, tbl_transaksi.kode_bukti_transaksi, tbl_transaksi.kode_jenis_akun, tbl_bukti_transaksi.tanggal_bukti_transaksi, tbl_transaksi.uraian, tbl_transaksi.jumlah, tbl_transaksi.tipe_transaksi, tbl_transaksi.tahun_pembukuan, tbl_bukti_transaksi.waktu_posting
							 	   from tbl_transaksi
								   inner join tbl_bukti_transaksi
								   on tbl_transaksi.kode_bukti_transaksi = tbl_bukti_transaksi.kode_bukti_transaksi))
					AS transaksi_data
					NATURAL JOIN tbl_jenis_akun
					NATURAL JOIN tbl_gol_akun
					NATURAL JOIN tbl_sub_gol_akun
								WHERE kode_jenis_akun ='".$_POST['mutasiJenisAkun']."'";
					}
				}
			}
		}



		if($_POST['mutasiKelompokAkun'] != 'semua'){
			$sql .= " and tanggal_bukti_transaksi <= '".$_POST['mutasiSampai']."'
				AND tahun_pembukuan = '".$_POST['thnPemMutPerRek']."'
				AND transaksi_data.waktu_posting != 'NULL'
				order by hidden ASC, kode_transaksi ASC, kode_bukti_transaksi ASC";
		}
		

		if($_POST['mutasiKelompokAkun'] == 'semua'){
			echo "<thead>";
			echo "<tr>";
				echo "<td id='noMutasi' rowspan='2' >";
					echo "No Tran";
				echo "</td>";
				echo "<td id='noBuktiMutasi' rowspan='2' >";
					echo "Nomer Bukti";
				echo "</td>";
				echo "<td id='tglMutasi' rowspan='2' >";
					echo "Tanggal";
				echo "</td>";
				echo "<td id='noRekMutasi' rowspan='2' >";
					echo "No Rekg";
				echo "</td>";
				echo "<td id='uraianMutasi' rowspan='2' >";
					echo "Uraian";
				echo "</td>";
				echo "<td id='tipeMutasi' colspan='2' rowspan='1' >";
					echo "Mutasi";
				echo "</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<td id='debetMutasi'>";
					echo "Debet";
				echo "</td>";
				echo "<td id='kreditMutasi'>";
					echo "Kredit";
				echo "</td>";
			echo "</tr>";
			echo "</thead>";
		}else{
			echo "<thead>";
			echo "<tr>";
				echo "<td id='noMutasi' rowspan='2' >";
					echo "No Tran";
				echo "</td>";
				echo "<td id='noBuktiMutasi' rowspan='2' >";
					echo "Nomer Bukti";
				echo "</td>";
				echo "<td id='tglMutasi' rowspan='2' >";
					echo "Tanggal";
				echo "</td>";
				echo "<td id='noRekMutasi' rowspan='2' >";
					echo "No Rekg";
				echo "</td>";
				echo "<td id='uraianMutasi' rowspan='2' >";
					echo "Uraian";
				echo "</td>";
				echo "<td id='tipeMutasi' colspan='2' rowspan='1' >";
					echo "Mutasi";
				echo "</td>";
				echo "<td id='saldoMutasi' rowspan='2' >";
					echo "Saldo Rp";
				echo "</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<td id='debetMutasi'>";
					echo "Debet";
				echo "</td>";
				echo "<td id='kreditMutasi'>";
					echo "Kredit";
				echo "</td>";
			echo "</tr>";
			echo "</thead>";
		}

		$hasil = mysql_query($sql);
		$saldo = 0 ;

		if($hasil == FALSE) { 
		    die(mysql_error());
		}
		
		echo "<tbody>";
		$noUrut = 1;

		while ($row3 = mysql_fetch_array($hasil)){

												
							if($_POST['mutasiKelompokAkun'] == 'semua'){
								if($row3['tipe_transaksi'] == 'DEBET'){
									$jmlDebet += $row3['jumlah'];
									
									if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
										echo "<tr>";
											echo "<td id='noMutasi' >";
												if($row3['kode_transaksi'] == 999999999)
												echo 'T';
												else
												echo $row3['kode_transaksi'];
											echo "</td>";
											echo "<td id='noBuktiMutasi' >";
												echo $row3['kode_bukti_transaksi'];
											echo "</td>";
											echo "<td id='tglMutasi'>";
												$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
												echo $dateTransaksi;
											echo "</td>";
											echo "<td id='noRekMutasi' >";
												echo $row3['kode_jenis_akun'];
											echo "</td>";
											echo "<td id='uraianMutasi'>";
												echo $row3['keterangan'];
											echo "</td>";
											echo "<td id='debetMutasi'>";
												echo "Rp. ".number_format($row3['jumlah'], 0, ".", ".");
											echo "</td>";
											echo "<td id='kreditMutasi'>";
												echo "&nbsp;";
											echo "</td>";
										echo "</tr>";
										$noUrut += 1;
									}	
										
								}else if($row3['tipe_transaksi'] == 'KREDIT'){
									$jmlKredit+= $row3['jumlah'];
									
									if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
										echo "<tr>";
											echo "<td id='noMutasi' >";
												if($row3['kode_transaksi'] == 999999999)
												echo 'T';
												else
												echo $row3['kode_transaksi'];
											echo "</td>";
											echo "<td id='noBuktiMutasi'>";
												echo $row3['kode_bukti_transaksi'];
											echo "</td>";
											echo "<td id='tglMutasi'>";
												$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
												echo $dateTransaksi;
											echo "</td>";
											echo "<td id='noRekMutasi' >";
												echo $row3['kode_jenis_akun'];
											echo "</td>";
											echo "<td id='uraianMutasi'>";
												echo $row3['keterangan'];
											echo "</td>";
											echo "<td id='debetMutasi'>";
												echo "&nbsp;";
											echo "</td>";
											echo "<td id='kreditMutasi'>";
												echo "Rp. ".number_format($row3['jumlah'], 0, ".", ".");
											echo "</td>";
										echo "</tr>";
										$noUrut += 1;
									}
								}


							}else if($_POST['mutasiKelompokAkun'] == 'K'){
								if($row3['tipe_transaksi'] == 'DEBET'){
									$saldo -= $row3['jumlah'];
									
									if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
										echo "<tr>";
											echo "<td id='noMutasi'  >";
												if($row3['kode_transaksi'] == 999999)
												echo 'T';
												else
												echo $row3['kode_transaksi'];
											echo "</td>";
											echo "<td id='noBuktiMutasi' >";
												echo $row3['kode_bukti_transaksi'];
											echo "</td>";
											echo "<td id='tglMutasi'>";
												$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
												echo $dateTransaksi;
											echo "</td>";
											echo "<td id='noRekMutasi' >";
												echo $row3['kode_jenis_akun'];
											echo "</td>";
											echo "<td id='uraianMutasi'>";
												echo $row3['keterangan'];
											echo "</td>";
											echo "<td id='debetMutasi'>";
												echo "Rp. ".number_format($row3['jumlah'], 0, ".", ".");
											echo "</td>";
											echo "<td id='kreditMutasi'>";
												echo "&nbsp;";
											echo "</td>";
											echo "<td id='saldoMutasi'>";
												echo "Rp. ".number_format($saldo, 0, ".", ".");
											echo "</td>";
										echo "</tr>";
										$noUrut += 1;
									}
										
								}else if($row3['tipe_transaksi'] == 'KREDIT'){
									$saldo += $row3['jumlah'];
									
									if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
										echo "<tr>";
											echo "<td id='noMutasi'  >";
												if($row3['kode_transaksi'] == 999999999)
												echo 'T';
												else
												echo $row3['kode_transaksi'];
											echo "</td>";
											echo "<td id='noBuktiMutasi'>";
												echo $row3['kode_bukti_transaksi'];
											echo "</td>";
											echo "<td id='tglMutasi'>";
												$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
												echo $dateTransaksi;
											echo "</td>";
											echo "<td id='noRekMutasi' >";
												echo $row3['kode_jenis_akun'];
											echo "</td>";
											echo "<td id='uraianMutasi'>";
												echo $row3['keterangan'];
											echo "</td>";
											echo "<td id='debetMutasi'>";
												echo "&nbsp;";
											echo "</td>";
											echo "<td id='kreditMutasi'>";
												echo "Rp. ".number_format($row3['jumlah'], 0, ".", ".");
											echo "</td>";
											echo "<td id='saldoMutasi'>";
												echo "Rp. ".number_format($saldo, 0, ".", ".");
											echo "</td>";
										echo "</tr>";
										$noUrut += 1;
									}
								}
							}else if($_POST['mutasiKelompokAkun'] == 'D'){
								if($row3['tipe_transaksi'] == 'DEBET'){
									$saldo += $row3['jumlah'];
									
									if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
										echo "<tr>";
											echo "<td id='noMutasi' >";
												if($row3['kode_transaksi'] == 999999999)
												echo 'T';
												else
												echo $row3['kode_transaksi'];
											echo "</td>";
											echo "<td id='noBuktiMutasi' >";
												echo $row3['kode_bukti_transaksi'];
											echo "</td>";
											echo "<td id='tglMutasi'>";
												$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
												echo $dateTransaksi;
											echo "</td>";
											echo "<td id='noRekMutasi' rowspan='2' >";
												echo $row3['kode_jenis_akun'];
											echo "</td>";
											echo "<td id='uraianMutasi'>";
												echo $row3['keterangan'];
											echo "</td>";
											echo "<td id='debetMutasi'>";
												echo "Rp. ".number_format($row3['jumlah'], 0, ".", ".");
											echo "</td>";
											echo "<td id='kreditMutasi'>";
												echo "&nbsp;";
											echo "</td>";
											echo "<td id='saldoMutasi'>";
												echo "Rp. ".number_format($saldo, 0, ".", ".");
											echo "</td>";
										echo "</tr>";
										$noUrut += 1;
									}
										
								}else if($row3['tipe_transaksi'] == 'KREDIT'){
									$saldo -= $row3['jumlah'];
									
									if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
										echo "<tr>";
											echo "<td id='noMutasi' >";
												if($row3['kode_transaksi'] == 999999999)
												echo 'T';
												else
												echo $row3['kode_transaksi'];
											echo "</td>";
											echo "<td id='noBuktiMutasi'>";
												echo $row3['kode_bukti_transaksi'];
											echo "</td>";
											echo "<td id='tglMutasi'>";
												$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
												echo $dateTransaksi;
											echo "</td>";
											echo "<td id='noRekMutasi'  >";
												echo $row3['kode_jenis_akun'];
											echo "</td>";
											echo "<td id='uraianMutasi'>";
												echo $row3['keterangan'];
											echo "</td>";
											echo "<td id='debetMutasi'>";
												echo "&nbsp;";
											echo "</td>";
											echo "<td id='kreditMutasi'>";
												echo "Rp. ".number_format($row3['jumlah'], 0, ".", ".");
											echo "</td>";
											echo "<td id='saldoMutasi'>";
												echo "Rp. ".number_format($saldo, 0, ".", ".");
											echo "</td>";
										echo "</tr>";
										$noUrut += 1;
									}
								}
							}

												
						 	
		}

		// if ($_POST['mutasiKelompokAkun'] == 'semua'){
		// 	echo "<tr>";
		// 		echo "<td style='width:55%;' colspan='4'>";
		// 			echo "Jumlah";
		// 		echo "</td>";
		// 		echo "<td id='debetMutasi'>";
		// 			echo $jmlDebet;
		// 		echo "</td>";
		// 		echo "<td id='kreditMutasi'>";
		// 			echo $jmlKredit;
		// 		echo "</td>";
		// 	echo "</tr>";
		// }else{
		// 	echo "";
		// }

		echo "</tbody>";





			 	
		// while ($row = mysql_fetch_array($hasil)){
		// 	$sql1;

		// 	if($_POST['mutasiKodeGolongan'] == 'semua'){
		// 		$sql1 = "select * from tbl_sub_gol_akun where kode_golongan = '".$row['kode_golongan']."'";
		// 	}else{
		// 		$sql1 = "select * from tbl_sub_gol_akun where kode_golongan = '".$_POST['mutasiKodeGolongan']."'";
		// 	}
			
		// 	$hasil1 = mysql_query($sql1);

		// 	if($hasil1 == FALSE) { 
		// 	    die(mysql_error());
		// 	}
			 	 	
		// 	while ($row1 = mysql_fetch_array($hasil1)){

		// 			$sql2;

		// 			if($_POST['mutasiKodeSubGolongan'] == 'semua'){
		// 				$sql2 = "select * from tbl_jenis_akun where kode_sub_gol_akun ='".$row1['kode_sub_gol_akun']."'";
		// 			}else{
		// 				$sql2 = "select * from tbl_jenis_akun where kode_sub_gol_akun ='".$_POST['mutasiKodeSubGolongan']."'";
		// 			}
					
		// 			$hasil2 = mysql_query($sql2);

		// 			if($hasil2 == FALSE) { 
		// 			    die(mysql_error());
		// 			}

		// 			while ($row2 = mysql_fetch_array($hasil2)){
		// 				$sql3;

		// 				if($_POST['mutasiJenisAkun'] == 'semua'){
		// 					$sql3 = "select * from tbl_bukti_transaksi where kode_jenis_akun ='".$row2['kode_jenis_akun']."'
		// 							AND tanggal_bukti_transaksi >= '".$_POST['mutasiAwal']."' and tanggal_bukti_transaksi <= '".$_POST['mutasiSampai']."'
		// 							order by tanggal_bukti_transaksi ASC, kode_bukti_transaksi ASC";
		// 				}else{
		// 					$sql3 = "select * from tbl_bukti_transaksi where kode_jenis_akun ='".$_POST['mutasiJenisAkun']."'
		// 							AND tanggal_bukti_transaksi >= '".$_POST['mutasiAwal']."' and tanggal_bukti_transaksi <= '".$_POST['mutasiSampai']."'
		// 							order by tanggal_bukti_transaksi ASC, kode_bukti_transaksi ASC";
		// 				}

						
		// 				$hasil3 = mysql_query($sql3);

		// 				if($hasil3 == FALSE) { 
		// 				    die(mysql_error());
		// 				}

		// 				if(mysql_num_rows($hasil3) < 1){
		// 					echo " ";
		// 				}

		// 				while ($row3 = mysql_fetch_array($hasil3)){

												
		// 					if($_POST['mutasiKelompokAkun'] == 'semua'){
		// 						if($row3['tipe_transaksi'] == 'DEBET'){
		// 							$jmlDebet += $row3['jumlah'];
									
		// 							if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
		// 								echo "<tr>";
		// 									echo "<td id='noBuktiMutasi' >";
		// 										echo $row3['kode_bukti_transaksi'];
		// 									echo "</td>";
		// 									echo "<td id='tglMutasi'>";
		// 										$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
		// 										echo $dateTransaksi;
		// 									echo "</td>";
		// 									echo "<td id='noRekMutasi' >";
		// 										echo $row3['kode_jenis_akun'];
		// 									echo "</td>";
		// 									echo "<td id='uraianMutasi'>";
		// 										echo $row3['uraian'];
		// 									echo "</td>";
		// 									echo "<td id='debetMutasi'>";
		// 										echo $row3['jumlah'];
		// 									echo "</td>";
		// 									echo "<td id='kreditMutasi'>";
		// 										echo "&nbsp;";
		// 									echo "</td>";
		// 								echo "</tr>";
		// 							}	
										
		// 						}else if($row3['tipe_transaksi'] == 'KREDIT'){
		// 							$jmlKredit+= $row3['jumlah'];
									
		// 							if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
		// 								echo "<tr>";
		// 									echo "<td id='noBuktiMutasi'>";
		// 										echo $row3['kode_bukti_transaksi'];
		// 									echo "</td>";
		// 									echo "<td id='tglMutasi'>";
		// 										$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
		// 										echo $dateTransaksi;
		// 									echo "</td>";
		// 									echo "<td id='noRekMutasi' >";
		// 										echo $row3['kode_jenis_akun'];
		// 									echo "</td>";
		// 									echo "<td id='uraianMutasi'>";
		// 										echo $row3['uraian'];
		// 									echo "</td>";
		// 									echo "<td id='debetMutasi'>";
		// 										echo "&nbsp;";
		// 									echo "</td>";
		// 									echo "<td id='kreditMutasi'>";
		// 										echo $row3['jumlah'];
		// 									echo "</td>";
		// 								echo "</tr>";
		// 							}
		// 						}
		// 					}else if($_POST['mutasiKelompokAkun'] == 'K'){
		// 						if($row3['tipe_transaksi'] == 'DEBET'){
		// 							$saldo -= $row3['jumlah'];
									
		// 							if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
		// 								echo "<tr>";
		// 									echo "<td id='noBuktiMutasi' >";
		// 										echo $row3['kode_bukti_transaksi'];
		// 									echo "</td>";
		// 									echo "<td id='tglMutasi'>";
		// 										$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
		// 										echo $dateTransaksi;
		// 									echo "</td>";
		// 									echo "<td id='noRekMutasi' >";
		// 										echo $row3['kode_jenis_akun'];
		// 									echo "</td>";
		// 									echo "<td id='uraianMutasi'>";
		// 										echo $row3['uraian'];
		// 									echo "</td>";
		// 									echo "<td id='debetMutasi'>";
		// 										echo $row3['jumlah'];
		// 									echo "</td>";
		// 									echo "<td id='kreditMutasi'>";
		// 										echo "&nbsp;";
		// 									echo "</td>";
		// 									echo "<td id='saldoMutasi'>";
		// 										echo $saldo;
		// 									echo "</td>";
		// 								echo "</tr>";
		// 							}
										
		// 						}else if($row3['tipe_transaksi'] == 'KREDIT'){
		// 							$saldo += $row3['jumlah'];
									
		// 							if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
		// 								echo "<tr>";
		// 									echo "<td id='noBuktiMutasi'>";
		// 										echo $row3['kode_bukti_transaksi'];
		// 									echo "</td>";
		// 									echo "<td id='tglMutasi'>";
		// 										$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
		// 										echo $dateTransaksi;
		// 									echo "</td>";
		// 									echo "<td id='noRekMutasi' >";
		// 										echo $row3['kode_jenis_akun'];
		// 									echo "</td>";
		// 									echo "<td id='uraianMutasi'>";
		// 										echo $row3['uraian'];
		// 									echo "</td>";
		// 									echo "<td id='debetMutasi'>";
		// 										echo "&nbsp;";
		// 									echo "</td>";
		// 									echo "<td id='kreditMutasi'>";
		// 										echo $row3['jumlah'];
		// 									echo "</td>";
		// 									echo "<td id='saldoMutasi'>";
		// 										echo $saldo;
		// 									echo "</td>";
		// 								echo "</tr>";
		// 							}
		// 						}
		// 					}else if($_POST['mutasiKelompokAkun'] == 'D'){
		// 						if($row3['tipe_transaksi'] == 'DEBET'){
		// 							$saldo += $row3['jumlah'];
									
		// 							if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
		// 								echo "<tr>";
		// 									echo "<td id='noBuktiMutasi' >";
		// 										echo $row3['kode_bukti_transaksi'];
		// 									echo "</td>";
		// 									echo "<td id='tglMutasi'>";
		// 										$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
		// 										echo $dateTransaksi;
		// 									echo "</td>";
		// 									echo "<td id='noRekMutasi' rowspan='2' >";
		// 										echo $row3['kode_jenis_akun'];
		// 									echo "</td>";
		// 									echo "<td id='uraianMutasi'>";
		// 										echo $row3['uraian'];
		// 									echo "</td>";
		// 									echo "<td id='debetMutasi'>";
		// 										echo $row3['jumlah'];
		// 									echo "</td>";
		// 									echo "<td id='kreditMutasi'>";
		// 										echo "&nbsp;";
		// 									echo "</td>";
		// 									echo "<td id='saldoMutasi'>";
		// 										echo $saldo;
		// 									echo "</td>";
		// 								echo "</tr>";
		// 							}
										
		// 						}else if($row3['tipe_transaksi'] == 'KREDIT'){
		// 							$saldo -= $row3['jumlah'];
									
		// 							if($row3['tanggal_bukti_transaksi'] >= date('Y-m-d',strtotime($_POST['mutasiMulai']))){
		// 								echo "<tr>";
		// 									echo "<td id='noBuktiMutasi'>";
		// 										echo $row3['kode_bukti_transaksi'];
		// 									echo "</td>";
		// 									echo "<td id='tglMutasi'>";
		// 										$dateTransaksi = date("d-m-Y", strtotime($row3['tanggal_bukti_transaksi']));
		// 										echo $dateTransaksi;
		// 									echo "</td>";
		// 									echo "<td id='noRekMutasi'  >";
		// 										echo $row3['kode_jenis_akun'];
		// 									echo "</td>";
		// 									echo "<td id='uraianMutasi'>";
		// 										echo $row3['uraian'];
		// 									echo "</td>";
		// 									echo "<td id='debetMutasi'>";
		// 										echo "&nbsp;";
		// 									echo "</td>";
		// 									echo "<td id='kreditMutasi'>";
		// 										echo $row3['jumlah'];
		// 									echo "</td>";
		// 									echo "<td id='saldoMutasi'>";
		// 										echo $saldo;
		// 									echo "</td>";
		// 								echo "</tr>";
		// 							}
		// 						}
		// 					}					
						 	
		// 				}
		// 				if($_POST['mutasiJenisAkun'] != 'semua') break;
						
		// 			}
		// 			if($_POST['mutasiKodeSubGolongan'] != 'semua') break;
					
		// 		}
		// 		if($_POST['mutasiKodeGolongan'] != 'semua') break;
		// 	}
			
		// 	if ($_POST['mutasiKelompokAkun'] == 'semua'){
		// 		echo "<tr>";
		// 			echo "<td style='width:55%;' colspan='4'>";
		// 				echo "Jumlah";
		// 			echo "</td>";
		// 			echo "<td id='debetMutasi'>";
		// 				echo $jmlDebet;
		// 			echo "</td>";
		// 			echo "<td id='kreditMutasi'>";
		// 				echo $jmlKredit;
		// 			echo "</td>";
		// 		echo "</tr>";
		// 	}else{
		// 		echo "";
		// 	}

?>