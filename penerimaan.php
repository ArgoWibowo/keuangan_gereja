<?php 
	if(isset($_POST['type'])){
		
	} else {
		getViewPenerimaan();
	}

	function getViewPenerimaan(){
		?>
		<link rel="stylesheet" type="text/css" href="css/penerimaan.css">
		<script type="text/javascript" src="js/penerimaan.js"></script>
		<form id="formpenerimaan">
			<div class="fTitle">FORM INPUT PERSEMBAHAN INDIVIDUAL</div>
			<table>
				<tr>
					<td colspan=5 style="text-align:center;"><i>Wajib Diisi</i><hr></td>
				</tr>
				<tr>
					<td>Nomor kwitansi</td><td>:</td>
					<td colspan=3><input id="nokwitansi" maxlength=12 placeholder="max 12 karakter" autocomplete="off" required></td>
					<td id="nokwitansi_cek">
						<img width=17 height=17 src="image/warning.png" style="padding-top:4px; display:none">
						<img width=17 height=17 src="image/check.png" style="padding-top:4px; display:none">
					</td>
				</tr>
				<tr>
					<td>Kredit</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectkreditpenerimaan" required>
							<?php isiSelectRekeningIndividu(); ?>
							</select><input class="trick" autocomplete="off" id="selectkreditpenerimaan" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Tanggal penerimaan</td><td>:</td>
					<td class="customdate"><input type="number" class="ex" id="penerimaan_tanggal" min=1 max=31 placeholder="tanggal" value=<?php echo date("d") ?> required></td>
					<td class="customdate"><input type="number" class="ex" id="penerimaan_bulan" min=1 max=12 placeholder="bulan" value=<?php echo date("m") ?> required></td>
					<td class="customdate"><input type="number" class="ex" id="penerimaan_tahun" min=2000 max=9999 placeholder="tahun" value=<?php echo date("y")+2000 ?> required></td>
				</tr>
				<tr>
					<td>Pemberi</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectpemberi" required>
							<?php isiSelectPemberiPenerimaan(); ?>
							</select><input class="trick" autocomplete="off" id="selectpemberi" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Jumlah</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<input value="Rp." class="rp ex" tabindex="-1" disabled>
							<input class="rptail" autocomplete="off" min=0 step=100 id="penerimaan_jumlah" required>
						</div>
					</td>
				</tr>
				<tr>
					<td>Penerima</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectpenerimaindividu" required>
							<?php isiSelectPenanggungJawab("penerima"); ?>
							</select><input class="trick" autocomplete="off" id="selectpenerimaindividu" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td colspan=5 style="text-align:center;"><br><i>Opsional</i><hr></td>
				</tr>
				<tr>
					<td>Untuk bulan</td><td>:</td>
					<td colspan=3>
						<select id="untukbulan">
							<option value=""></option>
							<option value="Januari">Januari</option>
							<option value="Februari">Februari</option>
							<option value="Maret">Maret</option>
							<option value="April">April</option>
							<option value="Mei">Mei</option>
							<option value="Juni">Juni</option>
							<option value="Juli">Juli</option>
							<option value="Agustus">Agustus</option>
							<option value="September">September</option>
							<option value="Oktober">Oktober</option>
							<option value="November">November</option>
							<option value="Desember">Desember</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Untuk tahun</td><td>:</td>
					<td colspan=3><input type="number" min=2000 max=9999 id="untuktahun"></td>
				</tr>
				<tr>
					<td colspan=5 style="text-align:center;"><br><hr></td>
				</tr>
				<tr>
					<td colspan=5 style="text-align:center;">
						<input type="reset" value="bersihkan" tabindex=-1 style="width: 120px; height: 35px;">
						<input type="submit" value="simpan" style="width: 120px; height: 35px;">
					</td>
				</tr>
			</table>
		</form>

		<?php 
	}
?>
