<div id="divTitle">
	<label class="lblTitle">EDIT JENIS AKUN</label>
</div>
<form id="formEditJenisAkunRekening" name="formEditJenisAkunRekening" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabelRekening">Kode Sub Golongan</td>
			<td> : <select id='pilihanEditKodeSubRekening' name="pilihanEditKodeSubRekening">
			 	
				</select></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Kode Jenis Akun</td>
			<td> : <input type="text" id="editKodeJenisAkunRekening" name="editKodeJenisAkunRekening" onKeyUp="checkKodeJenis()" spellcheck="false" required/></td>
			<td class="warning"><img id="warningKodeJenisAkunRekening" src="image/warning.png" alt="warning"></td>
		</tr>
		<tr>
			<td class="kolomLabelRekening">Nama Jenis Akun</td>
			<td> : <input type="text" id="editNamaJenisAkunRekening" name="editNamaJenisAkunRekening" spellcheck="false"  required/></td>
		</tr>
	</table>
	<div class="divKeteranganEditRekening" id="divKeteranganEditJenisRekening">
		<label id="lblKeteranganEditRekening" class="lblKeteranganEditRekening">Sukses</label>
	</div>
	<input type="submit" value="Tambah"  id="btnEditJenisAkunRekening" name="editJenisAkunRekening" class="button" >
</form>
