<div id="divTitle">
	<label class="lblTitle">EDIT PENANGGUNG JAWAB</label>
</div>
<form id="formEditPenanggungJawab" name="formEditPenanggungJawab" method="post" enctype="multipart/form-data">
	<table style="padding :10px;">
		<tr>
			<td class="kolomLabel">Kode</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="editKodePenanggungJawab" name="editKodePenanggungJawab" spellcheck="false" disabled placeholder="Masukan Kode Penanggung Jawab" style="width: 246px;" onKeyUp="checkKodePenanggungJawab()" onFocusOut="checkKodePenanggungJawab()" required/></td>
		</tr>
		<tr>
			<td class="kolomLabel">Nama</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="editNamaPenanggungJawab" name="editNamaPenanggungJawab" spellcheck="false" placeholder="Masukan Nama Penanggung Jawab" style="width: 246px;" required/></td>
		</tr>
		<tr>
			<td class="kolomLabel">Jabatan</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="text" id="editJabatanPenanggungJawab" name="editJabatanPenanggungJawab" spellcheck="false" placeholder="Masukan Jabatan Penanggung Jawab" style="width: 246px;" required/></td>
		</tr>
		<tr style="vertical-align: top;">
			<td class="kolomLabel">Alamat</td>
			<td class="kolomTitikDua"> : </td>
			<td style="padding-bottom: 0px;"><textarea style="width: 244px; height: 70px;" name ="editAlamatPenanggungJawab" id ="editAlamatPenanggungJawab" >Masukan Alamat Penanggung Jawab</textarea></td>
		</tr>
		<tr>
			<td class="kolomLabel">No Telepon</td>
			<td class="kolomTitikDua"> : </td>
			<td><input type="number" id="editNoTeleponPenanggungJawab" name="editNoTeleponPenanggungJawab" spellcheck="false" placeholder="Masukan No Telepon Penanggung Jawab" style="width: 246px;" required/></td>
		</tr>
	</table>
	<input type="submit" value="Ubah"  id="btnEditPenanggungJawab" name="editPenanggungJawab" class="button" >
	<button id="btnBatalEditPenanggungJawab" name="btnBatalEditPenanggungJawab" >Batal</button>
	<input type="text" id="editKodePenanggungJawab2" name="editKodePenanggungJawab2" style="visibility:hidden";>
</form>