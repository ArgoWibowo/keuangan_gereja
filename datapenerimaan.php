<?php 
	if(isset($_POST['type'])){
		
	} else {
		getViewDataPenerimaan();
	}

	function getViewDataPenerimaan(){
		?>
		<link rel="stylesheet" type="text/css" href="css/datapenerimaan.css">
		<script type="text/javascript" src="js/datapenerimaan.js"></script>
		
		<div id="navSearchToko" class="navSearch">
			<ul class="nav">
				<li class='search'>
					<div class='divSearch'>
						<input name ="searchPenerimaan" onchange="searchPenerimaan()" onkeyup="searchPenerimaan()"  id='searchPenerimaan' class='searchPenerimaan' type='text' placeholder="Cari Kwitansi, Keterangan">
					</div>
				</li>
			</ul>
			
		</div>

		<div id="dataPenerimaan">
			<div class="fTitle">DATA PERSEMBAHAN INDIVIDUAL</div>
			<table>
				<thead>
					<tr>
						<th width="5%">No.</th>
						<th width="10%">Kwitansi</th>
						<th width="8%">Tanggal</th>
						<th width="22%">Kredit</th>
						<th width="22%">Pemberi</th>
						<th colspan=2>Jumlah</th>
						<th width="15%">Keterangan</th>
						<th width="6%"></th>
					</tr>
				</thead>
				<tbody id="isiPenerimaanIndividu" class="isiPenerimaanIndividu">
				</tbody>
			</table>
		</div>
		<form id="formEditPenerimaan">
			<div class="fTitle">FORM EDIT PERSEMBAHAN INDIVIDUAL</div>
			<table>
				<tr>
					<td>Nomor kwitansi</td><td>:</td>
					<td colspan=3><input id="nokwitansiedit" maxlength=12 placeholder="max 12 karakter"  readonly required></td>
				</tr>
				<tr>
					<td>Kredit</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectkreditpenerimaanedit" disabled required>
							<?php isiSelectRekeningIndividu(); ?>
							</select><!-- <input class="trick" autocomplete="off" id="selectkreditpenerimaanedit" tabindex="-1"> -->
						</div>
					</td>
				</tr>
				<tr>
					<td colspan=5 style="text-align:center;"><br><i>Wajib Diisi</i><hr></td>
				</tr>
				<tr>
					<td>Tanggal penerimaan</td><td>:</td>
					<td class="customdate"><input type="number" class="ex" id="penerimaanedit_tanggal" min=1 max=31 placeholder="tanggal" value=<?php echo date("d") ?> required></td>
					<td class="customdate"><input type="number" class="ex" id="penerimaanedit_bulan" min=1 max=12 placeholder="bulan" value=<?php echo date("m") ?> required></td>
					<td class="customdate"><input type="number" class="ex" id="penerimaanedit_tahun" min=2000 max=9999 placeholder="tahun" value=<?php echo date("y")+2000 ?> required></td>
				</tr>
				<tr>
					<td>Pemberi</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectpemberiedit" required>
							<?php isiSelectPemberiPenerimaan(); ?>
							</select><input class="trick" autocomplete="off" id="selectpemberiedit" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td>Jumlah</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<input value="Rp." class="rp ex" tabindex="-1" disabled>
							<input class="rptail" type="number" min=0 step=100 id="penerimaanedit_jumlah" required>
						</div>
					</td>
				</tr>
				<tr>
					<td>Penerima</td><td>:</td>
					<td colspan=3>
						<div class="relativeBox">
							<select class="selectpenerimaindividuedit" required>
							<?php isiSelectPenanggungJawab("penerima"); ?>
							</select><input class="trick" autocomplete="off" id="selectpenerimaindividuedit" tabindex="-1">
						</div>
					</td>
				</tr>
				<tr>
					<td colspan=5 style="text-align:center;"><br><i>Opsional</i><hr></td>
				</tr>
				<tr>
					<td>Untuk bulan</td><td>:</td>
					<td colspan=3><input id="untukbulanedit" placeholder="khusus persembahan bulanan"></td>
				</tr>
				<tr>
					<td>Untuk tahun</td><td>:</td>
					<td colspan=3><input type="number" min=2000 max=9999 id="untuktahunedit" placeholder="khusus persembahan bulanan"></td>
				</tr>
				<tr>
					<td colspan=5 style="text-align:center;"><br><hr></td>
				</tr>
				<tr>
					<td colspan=5 style="text-align:center;">
						<button type="button" tabindex=-1 style="width: 120px; height: 35px;">batal</button>
						<input type="reset" value="bersihkan" tabindex=-1 style="width: 120px; height: 35px;" hidden>
						<input type="submit" value="simpan" style="width: 120px; height: 35px;">
					</td>
				</tr>
			</table>
		</form>
		<?php 
	}
?>